/**
 * addGroupPosts
 */

export interface PostObject {
  id: string;
  message: string;
  updated_time?: string;
  created_time: string;
  from?: FromUserObject;
  comments: PostComment;
  reactions: PostReaction;
}

export interface CommentObject {
  id: string;
  message: string;
  updated_time?: string;
  created_time: string;
  from?: FromUserObject;
  comment_count: number;
  like_count: number;
}

export interface ReactionObject {
  id: string;
  name?: string;
  type: string;
}

export interface FromReference {
  id: string;
  type: 'post' | 'comment';
  updatedAt: string;
  msg: string;
}

interface FromUserObject {
  id: string;
  name: string;
}

interface PostComment {
  data: CommentObject[];
}

interface PostReaction {
  data: ReactionObject[];
}

/**
 * addNewGroups
 */

export interface Group {
  id: string;
  created_time: string;
  email: string;
  icon: string;
  member_count: number;
  member_request_count: number;
  name: string;
  privacy: 'OPEN' | 'CLOSED';
  updated_time: string;
}

/**
 * getActiveUsers
 */

export interface ActiveUserData {
  uid: string;
  lastSeen: string;
  diff?: number;
}

/**
 * getTopicsStatistics
 */

export interface TopicMap {
  [index: string]: number;
}

export interface TopicArrayItem {
  topic: string;
  occurrence: number;
}

/**
 * postFacebookComment
 */

export interface CommentData {
  id: string;
  content: string;
  adminEmail: string;
}

/**
 * recordMembership
 */

export interface MembershipData {
  email: string;
  code: string;
  name: string;
  adminEmail: string;
  group: string;
}

/**
 * resolveSms
 */

export interface ResolveSMSData {
  uid: string;
  email: string;
}

/**
 * saveManualLog
 */

export interface ManualLogsData {
  uid: string;
  content: string;
  email: string;
}

/**
 * optOutUser
 */

export interface OptOutData {
  uid: string;
  content: string;
  email: string;
}

/**
 * sendNotification
 */

export interface FollowupData {
  uid: string;
  to: string;
  content: string;
  firstName: string;
  phone: string;
  email: string;
}

/**
 * updateCallLogs
 */

export interface CallLogsData {
  uid: string;
  status: 'done' | 'running' | 'ended' | 'sent';
  email: string;
}

export interface FollowupMessages {
  type: 'Call' | 'Email' | 'SMS';
  startTime: any;
  endTime?: any;
  admin: string;
  notes: string;
  status: 'done' | 'running' | 'ended' | 'sent';
}

/**
 * saveMessages
 */

export interface SMSMessages {
  createdAt: any;
  from: 'parent' | 'admin';
  message: string;
  replied: boolean | null;
}

/**
 * sendEmail
 */

export interface EmailParams {
  [index: string]: string | boolean;
}

export interface EmailAttachment {
  content: string;
  filename: string;
  type: string;
  disposition: 'attachment';
}

export interface EmailMessge {
  to: string;
  from: string;
  replyTo: string;
  bcc?: string;
  templateId: string;
  dynamic_template_data?: EmailParams;
  attachments?: EmailAttachment[];
}

/**
 * completed
 */

export interface CompletionData {
  scope: 'survey' | 'topic';
  k6?: 'moderate' | 'high';
  rcads?: RCADSData;
}

interface RCADSData {
  subjectPronoun: 'he' | 'she' | 'they';
  objectPronoun: 'him' | 'her' | 'them';
  symptom: 'depression' | 'anxiety' | 'depression and anxiety';
  isAre: 'is' | 'are';
}

/**
 * moduleOnSelected
 */

export interface TopicSeq {
  id: string;
  seq: number;
}

/**
 * moduleUnlock
 */

export interface EmailRefs {
  uid: string;
  email: string;
  phone: string;
  firstName: string;
}

/**
 * getUserProgramData
 */

export interface UserRefs {
  uid: string;
  email?: string;
  status: 'active' | 'inactive' | '> 14 Days' | '> 7 Days';
  userCategroy: string;
  isFacebookUser: boolean;
  surveyCompleted?: boolean;
}

export interface ModuleCompletionObj {
  label: string;
  value: number;
}

/**
 * updateUserDetails
 */

export interface UpdateUserDetailData {
  parentId: string;
  adminEmail: string;
  type: 'email' | 'phone';
  value: string;
}
