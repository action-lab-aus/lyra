export const formatPhone = (phone: string) => {
  if (phone.startsWith('+61')) return phone;
  else if (phone.startsWith('61')) return '+' + phone;
  else return '+61' + phone.substring(1);
};
