import * as functions from 'firebase-functions';
const environment = functions.config();
import * as sgMail from '@sendgrid/mail';
sgMail.setApiKey(environment.sendgrid.api_key);
const SENDGRID_SENDER = environment.sendgrid.sender;
const TRIAL_EMAIL = environment.sendgrid.trial_email;
import { EmailParams, EmailMessge, EmailAttachment } from './types';

/**
 * Send SendGrid notification emails with optional attachments using Dynamic Templates
 *
 * @param email Target email address
 * @param dynamicId SendGrid dynamic template id
 * @param params SendGrid Handle Bar parameters (e.g. firstName, trialEmail)
 * @param attachments An array of email attachmenmts
 *
 * @returns SendGrid emailing client response
 */
export const sendSendGridEmail = async (
  email: string,
  dynamicId: string,
  params: EmailParams,
  attachments?: EmailAttachment[],
) => {
  functions.logger.info('params: ', params);

  let msg: EmailMessge = {
    to: email,
    from: SENDGRID_SENDER,
    replyTo: TRIAL_EMAIL,
    templateId: dynamicId,
    dynamic_template_data: params,
  };

  // Add bcc to the trial email if it is a parent notification
  if (email !== TRIAL_EMAIL) msg.bcc = TRIAL_EMAIL;

  if (attachments) msg.attachments = attachments;

  const response = await sgMail.send(msg);
  functions.logger.info(response[0].statusCode);
  return response;
};
