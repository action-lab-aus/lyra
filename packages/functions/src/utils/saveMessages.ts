import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.firestore();
const fieldValue = admin.firestore.FieldValue;
import { map } from 'lodash';
import { SMSMessages } from './types';

/**
 * Resolve all SMS message for the user and set the replied flag to be true
 *
 * @param uid Firebase use ID
 */
export const resolveSmsStatus = async (uid: string) => {
  try {
    // Get all messages for the current user
    const currentMsgs = (
      await db
        .collection('sms')
        .doc(uid)
        .get()
    ).data()?.messages;

    if (currentMsgs) {
      const updated = [
        ...map(currentMsgs, (msg: SMSMessages) => {
          if (msg.from === 'parent' && !msg.replied) msg.replied = true;
          return msg;
        }),
      ];
      await db.doc(`sms/${uid}`).update({ messages: updated });
    }
  } catch (error) {
    functions.logger.error('Resolve SMS Status Error: ', error);
  }
};

/**
 * Save the incoming SMS messages to Firestore SMS collection
 *
 * @param uid Firebase user ID
 * @param from From user type - parent or admin user
 * @param content Incoming SMS message content
 */
export const saveSmsMessages = async (uid: string, from: 'parent' | 'admin', content: string) => {
  try {
    // Get all messages for the current user
    const currentMsgs = (
      await db
        .collection('sms')
        .doc(uid)
        .get()
    ).data()?.messages;

    const timestamp = admin.firestore.Timestamp.now();
    if (currentMsgs) {
      // If message is from a parent, append the message to the list
      if (from === 'parent')
        await db
          .collection('sms')
          .doc(uid)
          .update({
            messages: fieldValue.arrayUnion({
              from: from,
              createdAt: timestamp,
              message: content,
            }),
          });
      // Otherwise, update all previous parent messages to be replied and append the message
      else {
        const updated = [
          ...map(currentMsgs, (msg: SMSMessages) => {
            if (msg.from === 'parent' && !msg.replied) msg.replied = true;
            return msg;
          }),
          {
            from: from,
            createdAt: timestamp,
            message: content,
          },
        ];
        await db.doc(`sms/${uid}`).update({ messages: updated });
      }
    } else
      await db
        .collection('sms')
        .doc(uid)
        .set({
          messages: [
            {
              from: from,
              createdAt: timestamp,
              message: content,
            },
          ],
        });
  } catch (error) {
    functions.logger.error('Save SMS Message Error: ', error);
  }
};

/**
 * Save the incoming followup logs to the Firestore Followup collection
 *
 * @param uid Firebase user ID
 * @param email Admin account email
 * @param content Incoming SMS message content
 * @param type Type of the follow up action
 */
export const saveFollowupLogs = async (uid: string, email: string, content: string, type: 'Email' | 'SMS') => {
  // Check whether the log exist or not
  const logExist = (await db.doc(`followup/${uid}`).get()).exists;
  const timestamp = admin.firestore.Timestamp.now();

  // If exists, add a new followup log entry into the logs array
  if (logExist)
    await db
      .collection('followup')
      .doc(uid)
      .update({
        logs: fieldValue.arrayUnion({
          type: type,
          startTime: timestamp,
          status: 'sent',
          admin: email,
          notes: content,
        }),
      });
  // Otherwise, set the logs array with the current followup log
  else
    await db
      .collection('followup')
      .doc(uid)
      .set({
        logs: [
          {
            type: type,
            startTime: timestamp,
            status: 'sent',
            admin: email,
            notes: content,
          },
        ],
      });
};
