import * as functions from 'firebase-functions';
import * as firestore from '@google-cloud/firestore';

export const firestoreBackup = functions.pubsub.schedule('every 12 hours').onRun(async context => {
  const projectId = process.env.GCP_PROJECT || process.env.GCLOUD_PROJECT;
  const baseUrl = `gs://${projectId}.appspot.com`;
  functions.logger.info(`Start to export firestore to ${baseUrl} for project ${projectId}`);

  try {
    const client = new firestore.v1.FirestoreAdminClient();
    const databaseName = client.databasePath(projectId!, '(default)');
    await client.exportDocuments({
      name: databaseName,
      outputUriPrefix: baseUrl,
      collectionIds: [],
    });
  } catch (err) {
    functions.logger.error(err);
  }
});
