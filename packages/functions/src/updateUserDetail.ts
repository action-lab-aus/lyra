import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.firestore();
import { saveFollowupLogs } from './utils/saveMessages';
import { UpdateUserDetailData } from './utils/types';

/**
 * HTTP Callable function called to update user details (email/phone number) from the admin dashboard.
 * Push a new admin log for the current user to make the change.
 *
 * @param data Update user detail data including update type and value
 * @param context Firebase HTTPS callable function context
 */
export const updateUserDetail = functions.https.onCall(async (data: UpdateUserDetailData, context) => {
  const isAdmin = context.auth?.token.admin;

  const { parentId, adminEmail, type, value } = data;
  if (!parentId) return { status: 403, message: `Missing the 'parentId' field for the request...` };
  if (!adminEmail) return { status: 403, message: `Missing the 'adminEmail' field for the request...` };
  if (!type) return { status: 403, message: `Missing the 'type' field for the request...` };
  if (!value) return { status: 403, message: `Missing the 'value' field for the request...` };
  if (!isAdmin) return { status: 401, message: `Unauthorized` };

  try {
    // Update Firebase authenticated user email & Firestore user object
    if (type === 'email') {
      await admin.auth().updateUser(parentId, { email: value });
      await db.doc(`users/${parentId}`).update({ email: value, emailFlag: admin.firestore.FieldValue.delete() });
    }

    // Update Firestore user phone number
    if (type === 'phone')
      await db.doc(`users/${parentId}`).update({ userPhone: value, smsFlag: admin.firestore.FieldValue.delete() });

    // Add admin logs to Firestore under the current parent's object
    const logType = type === 'email' ? 'Email' : 'SMS';
    await saveFollowupLogs(parentId, adminEmail, `SYSTEM LOG: UPDATE PARENT ${type.toUpperCase()}`, logType);

    return { status: 200, message: `Successfully updated user details!` };
  } catch (error) {
    functions.logger.error(error);
    return { status: 400, message: `Error: ${error}` };
  }
});
