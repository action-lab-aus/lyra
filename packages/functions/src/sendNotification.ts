import * as functions from 'firebase-functions';
const environment = functions.config();
import { Twilio } from 'twilio';
import { sendSendGridEmail } from './utils/sendEmail';
import { saveSmsMessages, saveFollowupLogs } from './utils/saveMessages';
import { EmailParams, FollowupData } from './utils/types';
import { formatPhone } from './utils/utils';

// SendGrid Credentials
const FOLLOW_UP_ID = environment.sendgrid.follow_up;

// Twilio Credentials
const TWILIO_ACCOUNT_SID = environment.twilio.account_sid;
const TWILIO_AUTH_TOKEN = environment.twilio.auth_token;
const TWILIO_NUMBER = environment.twilio.twilio_number;
const TWILIO_STATUS_CALLBACK = environment.twilio.status_callback;
const client = new Twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

/**
 * Firebase HTTPS callable function to send email or SMS notifications to parents when follow-up
 * is needed for the PiP+ program (e.g. user inactive or has incoming SMS messages)
 *
 * @param data Follow up data including user id, follow up content, parent first name and phone number
 * @param context Firebase HTTPS callable function context
 *
 * @returns Response data with status - status code & message - response message
 */
export const sendNotification = functions.https.onCall(async (data: FollowupData, context) => {
  const isAdmin = context.auth?.token.admin;
  const { uid, to, content, firstName, phone, email } = data;

  if (!uid) return { status: 403, message: `Missing the 'uid' field for the request...` };
  if (!to) return { status: 403, message: `Missing the 'to' field for the request...` };
  if (!content) return { status: 403, message: `Missing the 'content' field for the request...` };
  if (!firstName) return { status: 403, message: `Missing the 'firstName' field for the request...` };
  if (!email) return { status: 403, message: `Missing the 'email' field for the request...` };
  if (!isAdmin) return { status: 401, message: `Unauthorized` };

  try {
    // Email Notification
    if (!phone) {
      // Trigger the follow_up email to parents
      // FOLLOW_UP_ID - firstName, content
      const response = await sendSendGridEmail(email, FOLLOW_UP_ID, {
        firstName: firstName,
        content: content.replace('\n\n', '<br><br>'),
      } as EmailParams);

      functions.logger.info('SendGrid Response: ', response);
      await saveFollowupLogs(uid, email, content, 'Email');

      const statusCode = response[0].statusCode;
      if (statusCode === 202) return { status: 200, message: `Email: Successfully sent the follow-up email!` };
      return { status: 400, message: `Email Error: ${response[0].body}` };
    }

    // SMS Notification
    else {
      const message = await client.messages.create({
        body: content,
        from: TWILIO_NUMBER,
        statusCallback: TWILIO_STATUS_CALLBACK,
        to: formatPhone(phone),
      });
      functions.logger.info('SMS Message: ', message);
      await saveSmsMessages(uid, 'admin', content);
      await saveFollowupLogs(uid, email, content, 'SMS');

      if (message.status !== 'failed' && message.status !== 'undelivered')
        return { status: 200, message: `SMS: Successfully sent the follow-up SMS!` };
      return { status: 400, message: `SMS Error: ${message.status} ${message.errorMessage}` };
    }
  } catch (error) {
    functions.logger.error(error);
    return { status: 400, message: `Error: ${error}` };
  }
});
