import * as functions from 'firebase-functions';
import { saveFollowupLogs, resolveSmsStatus } from './utils/saveMessages';
import { ResolveSMSData } from './utils/types';

/**
 * Firebase HTTPS callable function to set replied flag to be true for all incoming
 * SMS messages for a specific parent
 *
 * @param data Resolve SMS data including user id, and admin email
 * @param context Firebase HTTPS callable function context
 *
 * @returns Response data with status - status code & message - response message
 */
export const resolveSms = functions.https.onCall(async (data: ResolveSMSData, context) => {
  const isAdmin = context.auth?.token.admin;
  const { uid, email } = data;

  if (!uid) return { status: 403, message: `Missing the 'uid' field for the request...` };
  if (!email) return { status: 403, message: `Missing the 'email' field for the request...` };
  if (!isAdmin) return { status: 401, message: `Unauthorized` };

  try {
    await resolveSmsStatus(uid);
    await saveFollowupLogs(uid, email, 'SYSTEM LOG: RESOLVE SMS MESSAGES', 'SMS');

    return { status: 200, message: `SMS: Successfully updated the SMS followup status!` };
  } catch (error) {
    functions.logger.error(error);
    return { status: 400, message: `Error: ${error}` };
  }
});
