import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.firestore();
const FieldValue = admin.firestore.FieldValue;
import { ManualLogsData } from './utils/types';

/**
 * Save new manual logs for a given user initiated by the PiP+ admin
 *
 * @param data Manual logs data including user id, admin email, and notes
 * @param context Firebase HTTPS callable function context
 *
 * @returns Response data with status - status code & message - response message
 */
export const saveManualLog = functions.https.onCall(async (data: ManualLogsData, context) => {
  const isAdmin = context.auth?.token.admin;
  const { uid, content, email } = data;

  if (!uid) return { status: 403, message: `Missing the 'uid' field for the request...` };
  if (!email) return { status: 403, message: `Missing the 'email' field for the request...` };
  if (!content) return { status: 403, message: `Missing the 'content' field for the request...` };
  if (!isAdmin) return { status: 401, message: `Unauthorized` };

  try {
    const timestamp = admin.firestore.Timestamp.now();

    // Check whether the log exist or not
    const logExist = (await db.doc(`followup/${uid}`).get()).exists;

    // If exists, add a new manual log log entry into the calls array
    if (logExist)
      await db
        .collection('followup')
        .doc(uid)
        .update({
          logs: FieldValue.arrayUnion({
            type: 'Manual',
            startTime: timestamp,
            status: 'done',
            admin: email,
            notes: content,
          }),
        });
    // Otherwise, set the calls array with the current log
    else
      await db
        .collection('followup')
        .doc(uid)
        .set({
          logs: [
            {
              type: 'Manual',
              startTime: timestamp,
              status: 'done',
              admin: email,
              notes: content,
            },
          ],
        });

    return { status: 200, message: `Successfully added new manual log!` };
  } catch (error) {
    functions.logger.error(error);
    return { status: 400, message: `Error: ${error}` };
  }
});
