import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.firestore();
import { find } from 'lodash';
import { ActiveUserData } from './utils/types';

/**
 * Firebase scheduler function to update the last seen user statistics for the User
 * Management dashboard and landing statistics
 *
 * @param context Firebase event context
 */
export const getActiveUsers = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '512MB',
  })
  .pubsub.schedule('every 1 hours')
  .onRun(async context => {
    // Get the current date time for last seen comparsion
    const currentDate = new Date().getTime();

    // Fetch all users from Firebase Admin
    let current = await admin.auth().listUsers(1000);
    let users = current.users;
    while (current.pageToken) {
      current = await admin.auth().listUsers(1000, current.pageToken);
      users = users.concat(current.users);
    }

    // Fetch all user documents from Firestore
    const firestoreUsers = (await db.collection('users').get()).docs;

    // Define active user data and statistics map
    const activeData: ActiveUserData[] = [];
    const lastSeenStatistics = {
      active: 0,
      moreThan7: 0,
      moreThan14: 0,
      inactive: 0,
    };

    // Define the new registrant statistics map
    const newRegistrants = {
      last3Days: 0,
      last7Days: 0,
      last14Days: 0,
      last21Days: 0,
      other: 0,
    };

    firestoreUsers.forEach(firestoreUser => {
      const user: admin.auth.UserRecord | undefined = find(users, u => {
        return u.uid === firestoreUser.id;
      });

      // Only show active user and last seen date for eligible parents
      if (!user || firestoreUser.data().userCategory !== 'parent') return;

      // Get the last seen UTC date time string from `lastRefreshTime` or `creationTime`
      const lastSeen = user.metadata.lastRefreshTime ? user.metadata.lastRefreshTime : user.metadata.creationTime;
      const userCreation = user.metadata.creationTime;

      let data: ActiveUserData = { uid: user.uid, lastSeen: lastSeen };

      // Calculate difference in days (last seen)
      const diffDay = (currentDate - new Date(lastSeen).getTime()) / 1000 / 60 / 60 / 24;
      data.diff = diffDay;

      // Calculate difference in days (registration)
      const diffDayReg = (currentDate - new Date(userCreation).getTime()) / 1000 / 60 / 60 / 24;

      // Update last seen statistics
      if (diffDay < 7) lastSeenStatistics.active += 1;
      else if (diffDay >= 7 && diffDay < 14) lastSeenStatistics.moreThan7 += 1;
      else if (diffDay >= 14 && diffDay < 21) lastSeenStatistics.moreThan14 += 1;
      else lastSeenStatistics.inactive += 1;

      // Update new registrant statistics
      if (diffDayReg < 3) newRegistrants.last3Days += 1;
      else if (diffDayReg >= 3 && diffDayReg < 7) newRegistrants.last7Days += 1;
      else if (diffDayReg >= 7 && diffDayReg < 14) newRegistrants.last14Days += 1;
      else if (diffDayReg >= 14 && diffDayReg < 21) newRegistrants.last21Days += 1;
      else newRegistrants.other += 1;

      activeData.push(data);
    });

    // Update the `config/lastSeen` document
    await db
      .doc('config/lastSeen')
      .update({ activeUsers: activeData, statistics: lastSeenStatistics, registration: newRegistrants });
  });
