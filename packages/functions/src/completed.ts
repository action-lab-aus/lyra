import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { withUid, unlockOptional, UserProfile, notifications, SurveySymptons, addFollowupRecord } from './helpers';

export type CompletedInput = {
  scope: 'survey' | 'topic' | 'followup';
  surveySymptons?: SurveySymptons;
};

/**
 * HTTP Callable function called when the survey is fully completed. Notification emails to
 * both RA & Parents will be triggered. In cases where symptom evelation for K6 & RCADS is
 * identified, trigger the corresponding email notifications to parents as well
 *
 * @param data CompletedInput
 * @param context Firebase HTTPS callable function context
 */
export const completed = functions.https.onCall(async (data: CompletedInput, context) => {
  return withUid(context, async uid => {
    const { scope, surveySymptons } = data;
    if (['survey', 'topic', 'followup'].indexOf(scope) < 0) {
      throw new functions.https.HttpsError('invalid-argument', 'Invalid scope');
    }

    try {
      const completedField = `${scope}CompletedAt`;
      const userDocRef = admin
        .firestore()
        .collection('users')
        .doc(uid);

      await userDocRef.update({ [completedField]: admin.firestore.FieldValue.serverTimestamp() });

      const email = (await admin.auth().getUser(uid)).email;
      if (!email) return;

      const userDoc = await userDocRef.get();
      const userProfile = userDoc.data() as UserProfile;

      if (scope === 'survey') {
        await notifications.surveyCompleted(email, userProfile, uid);
        surveySymptons && (await notifications.surveyFollowup(email, userProfile, surveySymptons));
        await addFollowupRecord(uid);
      }

      if (scope === 'topic') {
        await notifications.topicCompleted(email, userProfile);
        await unlockOptional(userProfile, uid);
      }

      if (scope === 'followup') {
        await notifications.followupCompleted(email, userProfile, uid);
        surveySymptons && (await notifications.surveyFollowup(email, userProfile, surveySymptons));
      }
    } catch (err) {
      throw new functions.https.HttpsError('internal', err.message);
    }
  });
});
