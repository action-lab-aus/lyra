import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.firestore();
import { find } from 'lodash';
import { UserRefs, ModuleCompletionObj } from './utils/types';

/**
 * Firebase scheduler function to update the user program data every 3 hours with email, name,
 * phone number (append +61), survey completion, module completion, opt-out status, active status,
 * and user type
 *
 * @param context Firebase event context
 */
export const getUserProgramData = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '512MB',
  })
  .pubsub.schedule('every 1 hours')
  .onRun(async context => {
    // Get the current date time for last seen comparsion
    const currentDate = new Date().getTime();

    // Fetch all users from Firebase Admin
    let current = await admin.auth().listUsers(1000);
    let users = current.users;
    while (current.pageToken) {
      current = await admin.auth().listUsers(1000, current.pageToken);
      users = users.concat(current.users);
    }

    // Fetch all user documents from Firestore
    const firestoreUsers = (await db.collection('users').get()).docs;

    const userRefs: UserRefs[] = [];
    const surveyPromises: Promise<FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData>>[] = [];
    const topicPromises: Promise<FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData>>[] = [];

    // Loop through the Firestore user collection and fetch relevant information from
    // each user object. Push query for the `surveys` and `topics` collection for each
    // user object into the promise array
    let noOfResearchers = 0;
    let noOfParents = 0;
    let noOfUnder12 = 0;
    let noOfOver17 = 0;

    const firestoreUIDs: string[] = [];
    firestoreUsers.forEach(user => {
      firestoreUIDs.push(user.id);
      const { userCategory, userFirstname, userSurname, userPhone, surveyCompletedAt } = user.data();
      // Continue only if the relevant fields exist
      if (userCategory && userFirstname && userSurname && userPhone) {
        // Update no. of each user type
        if (userCategory === 'parent') noOfParents++;
        if (userCategory === 'ineligible-under-12') noOfUnder12++;
        if (userCategory === 'ineligible-over-17') noOfOver17++;
        if (userCategory === 'professional') noOfResearchers++;

        const adminUser = find(users, u => {
          return u.uid === user.id;
        });

        if (adminUser) {
          // Check whether the user is a Facebook User
          let isFacebookUser = false;
          adminUser.providerData.forEach(provider => {
            if (provider.providerId === 'facebook.com') isFacebookUser = true;
          });

          const diffDay = getDiffDay(adminUser, currentDate);
          let userObj: UserRefs = {
            uid: user.id,
            email: adminUser.email,
            userCategroy: userCategory,
            isFacebookUser: isFacebookUser,
            status: getUserStatus(diffDay),
          };

          if (surveyCompletedAt) userObj.surveyCompleted = true;

          userRefs.push(userObj);
          surveyPromises.push(db.collection(`users/${user.id}/surveys`).get());
          topicPromises.push(db.collection(`users/${user.id}/topics`).get());
        }
      }
    });

    // Find all users who signed up but haven't created their profiles yet
    const signedUpUsers: { uid: string; email: string }[] = [];
    users.forEach(user => {
      const { uid, email } = user;
      if (firestoreUIDs.indexOf(uid) < 0) {
        signedUpUsers.push({ uid, email: email || '' });
      }
    });
    await db.doc('config/lastSeen').update({ signedUpUsers });

    const surveyResults = await Promise.all(surveyPromises);
    const topicResults = await Promise.all(topicPromises);

    // Construct the `userProgress` map
    let userProgress: { [id: string]: number } = { survey: 0 };
    for (let i = 1; i <= 10; i++) userProgress[`${i}_module`] = 0;

    const programDataPromises: Promise<FirebaseFirestore.WriteResult>[] = [];

    // Loop through the survey and topic results, calculate the progress and update the
    // `user` document under the `users` collection
    for (let i = 0; i < surveyResults.length; i++) {
      const surveys = surveyResults[i].docs;
      const topics = topicResults[i].docs;
      const userRef = userRefs[i];

      const surveyCompletion = userRef.surveyCompleted ? 1 : getSurveyProgress(surveys);
      const followupSurveyCompletion = getFollowupSurveyProgress(surveys);
      if (surveyCompletion === 1 && userRef.userCategroy === 'parent') userProgress['survey'] += 1;

      const { moduleCompletion, completedCount } = getModuleProgress(topics);
      if (completedCount !== 0 && userRef.userCategroy === 'parent') userProgress[`${completedCount}_module`] += 1;

      programDataPromises.push(
        db.doc(`users/${userRef.uid}`).update({
          email: userRef.email ? userRef.email : '',
          status: userRef.status,
          isFacebookUser: userRef.isFacebookUser,
          surveyCompletion: surveyCompletion,
          followupSurveyCompletion: followupSurveyCompletion,
          moduleCompletion: moduleCompletion,
        }),
      );
    }

    await Promise.all(programDataPromises);
    await db
      .doc('config/userTypes')
      .update({ parents: noOfParents, professional: noOfResearchers, under12: noOfUnder12, over17: noOfOver17 });
    await db.doc('config/userProgress').update(userProgress);
  });

/**
 * Get the last seen timestamp difference comparing to the current date time
 *
 * @param adminUser Firebase admin user record
 * @param currentDate Current date number
 * @returns Difference in days
 */
const getDiffDay = (adminUser: admin.auth.UserRecord, currentDate: number) => {
  const lastSeen = adminUser.metadata.lastRefreshTime
    ? adminUser.metadata.lastRefreshTime
    : adminUser.metadata.creationTime;
  return (currentDate - new Date(lastSeen).getTime()) / 1000 / 60 / 60 / 24;
};

/**
 * Get the user status string by passing the difference days
 *
 * @param diffDay Last seen difference in days
 * @returns User status 'active', '> 7 Days', '> 14 Days', or 'inactive'
 */
const getUserStatus = (diffDay: number) => {
  if (diffDay < 7) return 'active';
  else if (diffDay >= 7 && diffDay < 14) return '> 7 Days';
  else if (diffDay >= 14 && diffDay < 21) return '> 14 Days';
  else return 'inactive';
};

/**
 * Get user survey completion percentage
 *
 * @param surveys Firestore user survey collection
 * @returns Survey completion percentage
 */
const getSurveyProgress = (surveys: FirebaseFirestore.QueryDocumentSnapshot<FirebaseFirestore.DocumentData>[]) => {
  if (surveys.length === 0) return 0;

  const total = 136;
  let completed = 0;

  surveys.forEach(survey => {
    if (survey.id.startsWith('s')) completed += survey.data()._completed;
  });

  return completed === 0 ? 0 : parseFloat((completed / total).toFixed(2));
};

/**
 * Get user follow-up survey completion percentage
 *
 * @param surveys Firestore user survey collection
 * @returns Follow-up survey completion percentage
 */
const getFollowupSurveyProgress = (
  surveys: FirebaseFirestore.QueryDocumentSnapshot<FirebaseFirestore.DocumentData>[],
) => {
  if (surveys.length === 0) return 0;

  const total = 146;
  let completed = 0;

  surveys.forEach(survey => {
    if (survey.id.startsWith('f')) completed += survey.data()._completed;
  });

  if (completed === 0) return 0;
  else {
    const completion = parseFloat((completed / total).toFixed(2));
    return completion >= 1 ? 1 : completion;
  }
};

/**
 * Get user module completion progress
 *
 * @param topics Firestore user topic collection
 * @returns Module completion progress array
 */
const getModuleProgress = (topics: FirebaseFirestore.QueryDocumentSnapshot<FirebaseFirestore.DocumentData>[]) => {
  let moduleCompletion: ModuleCompletionObj[] = [];
  let completedCount = 0;

  topics.forEach(topic => {
    const topicId = topic.id;
    const { progress, mandatory } = topic.data();

    // Only push the mandatory module completion progress
    if (mandatory) {
      if (progress) {
        const completion = parseFloat((progress.completed / progress.total).toFixed(2));
        if (completion === 1) completedCount++;
        moduleCompletion.push({
          label: topicId,
          value: completion,
        });
      } else
        moduleCompletion.push({
          label: topicId,
          value: 0,
        });
    }
  });

  return { moduleCompletion, completedCount };
};
