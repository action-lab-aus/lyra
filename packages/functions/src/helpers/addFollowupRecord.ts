import * as admin from 'firebase-admin';
const db = admin.firestore();

/**
 * Add followup survey notification record for triggering
 *
 * @param uid Firebase user id
 */
export const addFollowupRecord = async (uid: string) => {
  const meta = (await db.doc('config/meta').get()).data();
  const unlockInterval = meta!.unlockIntervalMins;
  const unlockFollowupInterval = meta!.unlockFollowupHours;

  // Staging: 1 hrs, Production: 24 * 90 = 2,160 hrs
  const baseOffset = 60000 * 60 * unlockFollowupInterval;
  const followupTypes = [
    { type: 'followup_survey', offset: 0 },
    { type: 'followup_survey_reminder_7', offset: 60000 * unlockInterval },
    { type: 'followup_survey_reminder_14', offset: 60000 * unlockInterval * 2 },
    { type: 'followup_survey_reminder_21', offset: 60000 * unlockInterval * 3 },
  ];

  const promises: Promise<FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>>[] = [];

  followupTypes.forEach(item => {
    let date = new Date();
    date = new Date(date.getTime() + baseOffset + item.offset);
    const timestamp = admin.firestore.Timestamp.fromDate(date);
    promises.push(db.collection('followupSurvey').add({ userId: uid, sendAt: timestamp, type: item.type }));
  });

  await Promise.all(promises);
};
