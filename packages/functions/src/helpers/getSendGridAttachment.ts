import * as fs from 'fs';
import * as path from 'path';
import { EmailAttachment } from '../utils/types';

export const getSendGridAttachment = (fileName: string) => {
  const content = fs.readFileSync(path.join(__dirname, `../../files/${fileName}`)).toString('base64');
  const attachment: EmailAttachment = {
    content: content,
    filename: fileName,
    type: 'application/doc',
    disposition: 'attachment',
  };
  return attachment;
};
