import * as functions from 'firebase-functions';
import * as MailService from '@sendgrid/mail';
import { Twilio } from 'twilio';
import { SurveySymptons, UserProfile } from './types';
import { userVocabulary } from './userVocabulary';
import { getSendGridAttachment } from './getSendGridAttachment';

const config = functions.config();
MailService.setApiKey(config.sendgrid.api_key);

const twiloClient = () => {
  const {
    twilio: { account_sid, auth_token },
  } = config;
  return new Twilio(account_sid, auth_token);
};

export const smsTemplates = {
  survey_completion_parents: (name: string) =>
    `Hi ${name}, thanks for completing the PiP+ survey. Please check your email or log in to your Dashboard to view your feedback and see the next steps in your program. partnersinparenting.com.au/dashboard. Feel free to reply with any questions. Thanks, PiP+ team.`,
  module_completion: (name: string) =>
    `Hi ${name}, Congratulations on completing all the modules in the PiP+ program! You can revisit any of the modules, including those you didn’t select, at any time via your dashboard: partnersinparenting.com.au/dashboard. Feel free to contact us with any questions. Thanks, PiP+ team.`,
};

/**
 * Notify admin when user opt out of the program
 * @param userProfile : UserProfile
 * @param uid: user id
 */
export async function optOut(email: string | undefined, userProfile: UserProfile, uid: string) {
  try {
    await MailService.send({
      to: config.sendgrid.ra_email,
      from: config.sendgrid.sender,
      templateId: config.sendgrid.opt_out,
      dynamicTemplateData: {
        userId: uid,
        firstName: userProfile.userFirstname,
        surname: userProfile.userSurname,
      },
    });

    if (email) {
      await MailService.send({
        to: email,
        from: config.sendgrid.sender,
        replyTo: config.sendgrid.trial_email,
        bcc: config.sendgrid.trial_email,
        templateId: config.sendgrid.opt_out_parents,
        dynamicTemplateData: {
          firstName: userProfile.userFirstname,
        },
      });
    }
  } catch (err) {
    functions.logger.error(err);
  }
}

export async function surveyCompleted(email: string, userProfile: UserProfile, uid: string) {
  const {
    sendgrid: {
      sender,
      ra_email,
      trial_email,
      survey_completion_ra,
      survey_completion_parents,
      dashboard_link,
      feedback_link,
    },
    twilio: { twilio_number, status_callback },
  } = config;

  try {
    await MailService.send({
      to: ra_email,
      from: sender,
      templateId: survey_completion_ra,
      dynamicTemplateData: {
        userId: uid,
      },
    });

    const attachment = getSendGridAttachment('Teen_Guidelines_COVID_update.pdf');
    await MailService.send({
      to: email,
      from: sender,
      replyTo: trial_email,
      bcc: trial_email,
      templateId: survey_completion_parents,
      dynamicTemplateData: {
        firstName: userProfile.userFirstname,
        feedbackLink: feedback_link,
        dashboardLink: dashboard_link,
      },
      attachments: [attachment],
    });

    userProfile.userPhone &&
      userProfile.smsNotification &&
      (await twiloClient().messages.create({
        body: smsTemplates.survey_completion_parents(userProfile.userFirstname),
        from: twilio_number,
        statusCallback: status_callback,
        to: userProfile.userPhone,
      }));
  } catch (err) {
    functions.logger.error(err);
  }
}

export async function surveyFollowup(email: string, userProfile: UserProfile, symptons: SurveySymptons) {
  const vocabulary = userVocabulary(userProfile);
  const {
    sendgrid: { trial_email, symptom_followup_k6, symptom_followup_rcads, symptom_followup_all, trail_phone, sender },
  } = config;

  const attachment = getSendGridAttachment('Useful_Resources.pdf');
  try {
    if (symptons.k6 && !symptons.rcads) {
      MailService.send({
        to: email,
        from: sender,
        replyTo: trial_email,
        bcc: trial_email,
        templateId: symptom_followup_k6,
        dynamicTemplateData: {
          firstName: userProfile.userFirstname,
          trailPhone: trail_phone,
          moderate: symptons.k6 === 'moderate',
          high: symptons.k6 === 'high',
          childName: vocabulary.childName,
        },
        attachments: [attachment],
      });
      return;
    }

    if (symptons.rcads && !symptons.k6) {
      MailService.send({
        to: email,
        from: sender,
        replyTo: trial_email,
        bcc: trial_email,
        templateId: symptom_followup_rcads,
        dynamicTemplateData: {
          firstName: userProfile.userFirstname,
          trailPhone: trail_phone,
          symptom: symptons.rcads,
          childName: vocabulary.childName,
          subjectPronoun: vocabulary.they,
          objectPronoun: vocabulary.them,
          isAre: vocabulary.are,
        },
        attachments: [attachment],
      });
      return;
    }

    if (symptons.rcads && symptons.k6) {
      MailService.send({
        to: email,
        from: sender,
        replyTo: trial_email,
        bcc: trial_email,
        templateId: symptom_followup_all,
        dynamicTemplateData: {
          firstName: userProfile.userFirstname,
          trailPhone: trail_phone,
          moderate: symptons.k6 === 'moderate',
          high: symptons.k6 === 'high',
          symptom: symptons.rcads,
          childName: vocabulary.childName,
          subjectPronoun: vocabulary.they,
          objectPronoun: vocabulary.them,
          isAre: vocabulary.are,
        },
        attachments: [attachment],
      });
      return;
    }
  } catch (err) {}
}

export async function topicCompleted(email: string, userRecord: UserProfile) {
  try {
    MailService.send({
      to: email,
      from: config.sendgrid.sender,
      replyTo: config.sendgrid.trial_email,
      bcc: config.sendgrid.trial_email,
      templateId: config.sendgrid.module_completion,
      dynamicTemplateData: {
        firstName: userRecord.userFirstname,
        dashboardLink: config.sendgrid.dashboard_link,
      },
    });

    userRecord.userPhone &&
      userRecord.smsNotification &&
      (await twiloClient().messages.create({
        body: smsTemplates.module_completion(userRecord.userFirstname),
        from: config.twilio.twilio_number,
        statusCallback: config.twilio.status_callback,
        to: userRecord.userPhone,
      }));
  } catch (err) {
    functions.logger.error(err);
  }
}

/**
 * Notify admin when user completes the follow-up survey
 * @param email Firebase auth email
 * @param userProfile PiP user profile
 * @param uid Firebase user id
 */
export async function followupCompleted(email: string, userProfile: UserProfile, uid: string) {
  const {
    sendgrid: {
      sender,
      ra_email,
      trial_email,
      followup_survey_completion,
      followup_survey_completion_parents,
      dashboard_link,
      feedback_followup_link,
    },
  } = config;

  try {
    await MailService.send({
      to: ra_email,
      from: sender,
      templateId: followup_survey_completion,
      dynamicTemplateData: {
        userId: uid,
      },
    });

    await MailService.send({
      to: email,
      from: sender,
      replyTo: trial_email,
      bcc: trial_email,
      templateId: followup_survey_completion_parents,
      dynamicTemplateData: {
        firstName: userProfile.userFirstname,
        feedbackLink: feedback_followup_link,
        dashboardLink: dashboard_link,
      },
    });
  } catch (err) {
    functions.logger.error(err);
  }
}
