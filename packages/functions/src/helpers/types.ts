import * as admin from 'firebase-admin';

export type Gender = 'male' | 'female' | 'non-binary';

export type UserProfileStage = 'survey' | 'topic' | 'followup';

export type UserProfile = {
  userType: 'parent' | 'professional' | 'ineligible-under-12' | 'ineligible-over-17';
  userFirstname: string;
  userSurname: string;
  userAge: string;
  userGender: Gender;
  userPostcode: string;
  userPhone: string;
  userPhoneAlt?: string;
  userEducation?: string;
  userContactEmail?: string;
  familyEthnicity?: string;
  familyRelationship?: string;
  familyIncome?: string;
  familyCovidImpact?: string;
  childName: string;
  childDob: string;
  childGrade: number;
  childGender: Gender;
  childRelationship: string;
  childOtherParentsJoined: string;
  discoverChannels?: string;
  discoverReason?: string;
  discoverFutureResearch?: string;
  currentStage?: UserProfileStage;
  topicSuggestion?: string[];
  optOutAt?: admin.firestore.Timestamp;
  surveyCompletedAt?: admin.firestore.Timestamp;
  smsNotification?: boolean;
  followupCompletedAt?: admin.firestore.Timestamp;
};

export type UserRecord = {
  id: string;
} & UserProfile;

export type SurveySymptons = {
  k6?: 'moderate' | 'high';
  rcads?: 'depression' | 'anxiety' | 'depression and anxiety';
};
