import * as admin from 'firebase-admin';
const db = admin.firestore();

/**
 * Add profile creation reminder notification record for triggering
 *
 * @param uid Firebase user id
 */
export const addProfileReminder = async (uid: string) => {
  const meta = (await db.doc('config/meta').get()).data();
  const profileReminder = meta!.profileReminderMins;

  // Staging: 5 mins, Production: 24 * 60 = 1,440 mins
  const reminderTypes = [
    { type: 'profile_reminder_1', offset: profileReminder * 60000 },
    { type: 'profile_reminder_7', offset: profileReminder * 7 * 60000 },
  ];

  const promises: Promise<FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>>[] = [];

  reminderTypes.forEach(item => {
    let date = new Date();
    date = new Date(date.getTime() + item.offset);
    const timestamp = admin.firestore.Timestamp.fromDate(date);
    promises.push(db.collection('profileReminder').add({ userId: uid, sendAt: timestamp, type: item.type }));
  });

  await Promise.all(promises);
};
