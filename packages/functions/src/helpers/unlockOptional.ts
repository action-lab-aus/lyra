import * as admin from 'firebase-admin';
const db = admin.firestore();
import { UserProfile } from './types';

/**
 * Unlock all optional modules when the module is completed
 *
 * @param userRecord Firestore user record
 * @param uid Firebase user id
 */
export const unlockOptional = async (userRecord: UserProfile, uid: string) => {
  const topics = (await db.collection(`users/${uid}/topics`).get()).docs;
  const promises: Promise<FirebaseFirestore.WriteResult>[] = [];

  // Unlock all optional modules
  topics.forEach(topic => {
    const { mandatory, locked } = topic.data();
    if (!mandatory && locked) promises.push(db.doc(`users/${uid}/topics/${topic.id}`).update({ locked: false }));
  });

  await Promise.all(promises);
};
