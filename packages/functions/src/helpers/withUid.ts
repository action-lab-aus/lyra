import * as functions from 'firebase-functions';

export async function withUid(context: functions.https.CallableContext, body: (uid: string) => Promise<any>) {
  const uid = context.auth?.uid;
  if (!uid) {
    throw new functions.https.HttpsError('unauthenticated', `Unauthenticated user...`);
  }
  return await body(uid);
}
