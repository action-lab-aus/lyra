import { UserProfile } from './types';

const pronouns: Record<string, [string, string, string, string]> = {
  male: ['he', 'his', 'him', 'himself'],
  female: ['she', 'her', 'her', 'herself'],
  'non-binary': ['they', 'their', 'them', 'themselves'],
};

const thirdPersonSingularVerbs: Record<string, string | [string, string]> = {
  trust: 'trusts',
  appear: 'appears',
  have: 'has',
  are: 'is',
  realise: 'realises',
  see: 'sees',
  seem: 'seems',
  need: 'needs',
  shut: 'shuts',
  take: 'takes',
  ask: 'asks',
  find: 'finds',
  think: 'thinks',
  dont: ["don't", "doesn't"],
};

export function userVocabulary(userProfile: UserProfile): Record<string, string> {
  const { childName = 'Your teenager', childGender = 'non-binary' } = userProfile;
  const [they, their, them, themselves] = pronouns[childGender];
  const verbIndex = childGender === 'non-binary' ? 0 : 1;
  const verbs = Object.entries(thirdPersonSingularVerbs).reduce<Record<string, string>>((verbs, [key, value]) => {
    const map: [string, string] = typeof value === 'string' ? [key, value] : value;
    return { ...verbs, [key]: map[verbIndex] };
  }, {});
  return { childName, they, their, them, themselves, ...verbs };
}
