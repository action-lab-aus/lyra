import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { OptOutData } from './utils/types';
import { notifications, UserProfile } from './helpers';

/**
 * Opt parents out on their behalf initiated by the PiP+ admin
 *
 * @param data Opt out data including user id, admin email, and notes
 * @param context Firebase HTTPS callable function context
 *
 * @returns Response data with status - status code & message - response message
 */
export const optOutUser = functions.https.onCall(async (data: OptOutData, context) => {
  const isAdmin = context.auth?.token.admin;
  const { uid, content, email } = data;

  if (!uid) return { status: 403, message: `Missing the 'uid' field for the request...` };
  if (!email) return { status: 403, message: `Missing the 'email' field for the request...` };
  if (!content) return { status: 403, message: `Missing the 'content' field for the request...` };
  if (!isAdmin) return { status: 401, message: `Unauthorized` };

  try {
    const userDocRef = admin
      .firestore()
      .collection('users')
      .doc(uid);
    const userRecord = await admin.auth().updateUser(uid, { disabled: true });

    await userDocRef.update({
      optOutAt: admin.firestore.FieldValue.serverTimestamp(),
    });

    const userDoc = await userDocRef.get();
    notifications.optOut(userRecord.email, userDoc.data() as UserProfile, uid);

    return { status: 200, message: `Successfully opted the user out!` };
  } catch (error) {
    functions.logger.error(error);
    return { status: 400, message: `Error: ${error}` };
  }
});
