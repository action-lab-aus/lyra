import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { withUid, notifications, UserProfile } from './helpers';

/**
 * HTTP Callable function called when the user chooses to opt out from the study, send
 * an notification email to the RA
 *
 * @param data Empty
 * @param context Firebase HTTPS callable function context
 */
export const revokeConsent = functions.https.onCall(async (data, context) => {
  return await withUid(context, async uid => {
    try {
      const userDocRef = admin
        .firestore()
        .collection('users')
        .doc(uid);

      const userRecord = await admin.auth().updateUser(uid, { disabled: true });

      await userDocRef.update({
        optOutAt: admin.firestore.FieldValue.serverTimestamp(),
      });

      const userDoc = await userDocRef.get();
      notifications.optOut(userRecord.email, userDoc.data() as UserProfile, uid);
    } catch (error) {
      throw new functions.https.HttpsError('internal', error.message);
    }
  });
});
