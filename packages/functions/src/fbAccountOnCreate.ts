import * as functions from 'firebase-functions';
import { addProfileReminder } from './helpers/addProfileReminder';

exports.fbAccountOnCreate = functions.auth.user().onCreate(async user => {
  return await addProfileReminder(user.uid);
});
