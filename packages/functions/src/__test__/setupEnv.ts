import * as admin from 'firebase-admin';
import { testEnv, userCredential } from './testEnv';

admin.initializeApp();

let uid: string;

beforeAll(async () => {
  const user = await admin.auth().createUser(userCredential);
  uid = user.uid;
});

afterAll(async () => {
  await admin.auth().deleteUser(uid);
  await testEnv.firestore.clearFirestoreData('pip-plus-staging');
  await testEnv.cleanup();
});
