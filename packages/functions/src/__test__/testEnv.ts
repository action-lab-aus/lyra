import * as createTest from 'firebase-functions-test';

export const userCredential = {
  email: 'user@example.com',
};

export const config = {
  sendgrid: {
    reg_successful_parents: 'reg_successful_parents_template',
    reg_successful_ra: 'reg_successful_ra_template',
    survey_reminder_7: 'survey_reminder_7_template',
    survey_reminder_14: 'survey_reminder_14_template',
    survey_reminder_21: 'survey_reminder_21_template',
    survey_completion_parents: 'survey_completion_parents_template',
    survey_completion_ra: 'survey_completion_ra_template',
    weekly_module_access: 'weekly_module_access_template',
    symptom_followup_k6: 'symptom_followup_k6_template',
    symptom_followup_rcads: 'symptom_followup_rcads_template',
    symptom_followup_all: 'symptom_followup_all_template',
    module_completion: 'd-76754bba328d42c5b29d0645110af1c9',
    followup_survey: 'followup_survey',
    followup_survey_reminder_7: 'followup_survey_reminder_7',
    followup_survey_reminder_14: 'followup_survey_reminder_14',
    followup_survey_reminder_21: 'followup_survey_reminder_21',
    followup_survey_completion: 'followup_survey_completion',
    followup_survey_completion_parents: 'followup_survey_completion_parents',
    opt_out: 'opt_out_template',
    follow_up: 'follow_up_template',
    api_key: 'sendgrid_api_key',
    sender: 'no-reply@example.com',
    ra_email: 'admin@example.com',
    testing_email: 'peter.chen@monash.edu',
    testing_phone: '+61400000000',
    dashboard_link: 'https://partnersinparenting.com.au/dashboard',
    feedback_link: 'https://partnersinparenting.com.au/feedback',
    feedback_followup_link: 'https://partnersinparenting.com.au/followupFeedback',
    trail_phone: '+61400000000',
    trail_email: 'med-pip-plus@monash.edu',
  },
  twilio: {
    account_sid: 'twilio_account_sid',
    auth_token: 'twilio_auth_token',
    twilio_number: '+61400000000',
    twiml_app_sid: 'twilio_app_sid',
    twilio_user_agent: 'TwilioProxy',
    domains: 'http://example.com',
    status_callback: 'https://example.com/webhooks/twilioAppFn/smsStatus',
  },
  facebook: {
    page_id: 'facebook_page_id',
    page_token: 'facebook_page_token',
  },
};

export const testEnv = createTest();
testEnv.mockConfig(config);

export const twilio = {
  messages: {
    create: jest.fn(),
  },
};

jest.mock('@sendgrid/mail');
jest.mock('twilio', () => {
  return {
    Twilio: jest.fn().mockImplementation(() => twilio),
  };
});
