import { twilio, testEnv, userCredential, config } from './testEnv';
import * as MailService from '@sendgrid/mail';
import * as admin from 'firebase-admin';
import { smsTemplates } from '../helpers/notifications';
import { UserProfile } from '../helpers';
import { completed, CompletedInput } from '../completed';
import { getSendGridAttachment } from '../helpers/getSendGridAttachment';
const userfulResources = getSendGridAttachment('Useful_Resources.pdf');
const teenGuideliines = getSendGridAttachment('Teen_Guidelines_COVID_update.pdf');

const callCompleted = testEnv.wrap(completed);
const send = MailService.send as jest.Mock;

const userProfile: Partial<UserProfile> = {
  userFirstname: 'Sam',
  userSurname: 'Jackson',
  userPhone: '+61400000000',
  childName: 'Alice',
  childGender: 'female',
  smsNotification: true, // Required for triggering SMS notifications
};

describe('completed', () => {
  let uid: string;

  beforeAll(async () => {
    const user = await admin.auth().getUserByEmail(userCredential.email);
    uid = user.uid;
    await admin
      .firestore()
      .collection(`/users`)
      .doc(uid)
      .set(userProfile);
    await admin
      .firestore()
      .doc('config/meta')
      .set({ unlockIntervalMins: 5, unlockFollowupHours: 1 });
  });

  beforeEach(async () => {
    send.mockClear();
    twilio.messages.create.mockClear();
  });

  test('should raise error given there are no auth in the call context', async () => {
    const data: CompletedInput = { scope: 'survey' };
    const context = {};
    await expect(callCompleted(data, context)).rejects.toThrow();
  });

  test('should raise error given scope is incorrect', async () => {
    const data = { scope: 'unknown' };
    const context = { auth: { uid } };
    await expect(callCompleted(data, context)).rejects.toThrow();
  });

  test('should set topicCompletedAt field and send notification given scope is topic', async () => {
    const data: CompletedInput = { scope: 'topic' };
    const context = { auth: { uid } };

    await callCompleted(data, context);

    const userDoc = await admin
      .firestore()
      .collection('users')
      .doc(uid)
      .get();

    expect(userDoc.get('topicCompletedAt')).toBeDefined();
    expect(send).toBeCalled();
    expect(twilio.messages.create).toBeCalled();
    expect(twilio.messages.create.mock.calls.length).toEqual(1);

    expect(send.mock.calls[0]).toEqual([
      {
        to: userCredential.email,
        from: config.sendgrid.sender,
        templateId: config.sendgrid.module_completion,
        dynamicTemplateData: {
          firstName: userProfile.userFirstname,
          dashboardLink: config.sendgrid.dashboard_link,
        },
      },
    ]);

    expect(twilio.messages.create.mock.calls[0]).toEqual([
      {
        to: userProfile.userPhone,
        from: config.twilio.twilio_number,
        body: smsTemplates.module_completion(userProfile.userFirstname!),
        statusCallback: config.twilio.status_callback,
      },
    ]);
  });

  test('should set surveyCompletedAt field and send notification given scope is survey', async () => {
    const data: CompletedInput = { scope: 'survey' };
    const context = { auth: { uid } };

    await callCompleted(data, context);

    const userDoc = await admin
      .firestore()
      .collection('users')
      .doc(uid)
      .get();

    expect(userDoc.get('surveyCompletedAt')).toBeDefined();
    expect(send).toBeCalled();
    expect(send.mock.calls.length).toBe(2);
    expect(twilio.messages.create).toBeCalled();
    expect(twilio.messages.create.mock.calls.length).toEqual(1);

    expect(send.mock.calls[0]).toEqual([
      {
        to: config.sendgrid.ra_email,
        from: config.sendgrid.sender,
        templateId: config.sendgrid.survey_completion_ra,
        dynamicTemplateData: {
          userId: uid,
        },
      },
    ]);

    expect(send.mock.calls[1]).toEqual([
      {
        to: userCredential.email,
        from: config.sendgrid.sender,
        templateId: config.sendgrid.survey_completion_parents,
        dynamicTemplateData: {
          firstName: userProfile.userFirstname,
          feedbackLink: config.sendgrid.feedback_link,
          dashboardLink: config.sendgrid.dashboard_link,
        },
        attachments: [teenGuideliines],
      },
    ]);

    expect(twilio.messages.create.mock.calls[0]).toEqual([
      {
        to: userProfile.userPhone,
        from: config.twilio.twilio_number,
        body: smsTemplates.survey_completion_parents(userProfile.userFirstname!),
        statusCallback: config.twilio.status_callback,
      },
    ]);
  });

  test('should send survey k6 followup notification', async () => {
    const data: CompletedInput = { scope: 'survey', surveySymptons: { k6: 'moderate' } };
    const context = { auth: { uid } };

    await callCompleted(data, context);

    expect(send).toBeCalled();
    expect(send.mock.calls.length).toEqual(3);

    const followupArgs = send.mock.calls[2];
    expect(followupArgs).toEqual([
      {
        to: userCredential.email,
        from: config.sendgrid.sender,
        templateId: config.sendgrid.symptom_followup_k6,
        dynamicTemplateData: {
          firstName: userProfile.userFirstname,
          trailPhone: config.sendgrid.trail_phone,
          moderate: true,
          high: false,
          childName: userProfile.childName,
        },
        attachments: [userfulResources],
      },
    ]);
  });

  test('should send survey rcads followup notification', async () => {
    const data: CompletedInput = { scope: 'survey', surveySymptons: { rcads: 'depression' } };
    const context = { auth: { uid } };

    await callCompleted(data, context);

    expect(send).toBeCalled();

    const followupArgs = send.mock.calls[2];

    expect(followupArgs).toEqual([
      {
        to: userCredential.email,
        from: config.sendgrid.sender,
        templateId: config.sendgrid.symptom_followup_rcads,
        dynamicTemplateData: {
          firstName: userProfile.userFirstname,
          trailPhone: config.sendgrid.trail_phone,
          symptom: 'depression',
          childName: userProfile.childName,
          subjectPronoun: 'she',
          objectPronoun: 'her',
          isAre: 'is',
        },
        attachments: [userfulResources],
      },
    ]);
  });

  test('should send survey all followup notification', async () => {
    const data: CompletedInput = { scope: 'survey', surveySymptons: { k6: 'high', rcads: 'depression and anxiety' } };
    const context = { auth: { uid } };

    await callCompleted(data, context);

    expect(send).toBeCalled();

    const followupArgs = send.mock.calls[2];
    expect(followupArgs).toEqual([
      {
        to: userCredential.email,
        from: config.sendgrid.sender,
        templateId: config.sendgrid.symptom_followup_all,
        dynamicTemplateData: {
          firstName: userProfile.userFirstname,
          trailPhone: config.sendgrid.trail_phone,
          moderate: false,
          high: true,
          symptom: 'depression and anxiety',
          childName: userProfile.childName,
          subjectPronoun: 'she',
          objectPronoun: 'her',
          isAre: 'is',
        },
        attachments: [userfulResources],
      },
    ]);
  });

  test('should set followupCompletedAt field and send notification given scope is followup', async () => {
    const data: CompletedInput = { scope: 'followup' };
    const context = { auth: { uid } };

    await callCompleted(data, context);

    const userDoc = await admin
      .firestore()
      .collection('users')
      .doc(uid)
      .get();

    expect(userDoc.get('followupCompletedAt')).toBeDefined();
    expect(send).toBeCalled();

    expect(send.mock.calls[0]).toEqual([
      {
        to: config.sendgrid.ra_email,
        from: config.sendgrid.sender,
        templateId: config.sendgrid.followup_survey_completion,
        dynamicTemplateData: {
          userId: uid,
        },
      },
    ]);

    expect(send.mock.calls[1]).toEqual([
      {
        to: userCredential.email,
        from: config.sendgrid.sender,
        templateId: config.sendgrid.followup_survey_completion_parents,
        dynamicTemplateData: {
          firstName: userProfile.userFirstname,
          feedbackLink: config.sendgrid.feedback_followup_link,
          dashboardLink: config.sendgrid.dashboard_link,
        },
      },
    ]);
  });
});
