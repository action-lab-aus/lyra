import { config, testEnv, userCredential } from './testEnv';
import * as admin from 'firebase-admin';
import * as MailService from '@sendgrid/mail';
import { revokeConsent } from '../revokeConsent';
import { UserProfile } from '../helpers';

const callRevokeConsent = testEnv.wrap(revokeConsent);
const send = MailService.send as jest.Mock;

describe('revokeConsent', () => {
  beforeEach(async () => {
    send.mockClear();
  });

  test('should raise error given there are no auth in the call context', async () => {
    await expect(callRevokeConsent(null)).rejects.toThrow();
  });

  test('should disable user, set revoke date and send notification', async () => {
    const { uid } = await admin.auth().getUserByEmail(userCredential.email);
    const userProfile: Partial<UserProfile> = {
      userFirstname: 'Sam',
      userSurname: 'Jackson',
    };
    const userDocRef = admin
      .firestore()
      .collection('users')
      .doc(uid);
    await userDocRef.set(userProfile);

    // invoke function
    await callRevokeConsent(null, { auth: { uid } });

    // expections
    const user = await admin.auth().getUser(uid);
    const userDoc = await userDocRef.get();

    expect(user.disabled).toBeTruthy();
    expect(userDoc.get('optOutAt')).toBeDefined();
    expect(send).toBeCalled();
    expect(send.mock.calls.length).toBe(2); // One to RA, one to the parent
    expect(send.mock.calls[0]).toEqual([
      {
        to: config.sendgrid.ra_email,
        from: config.sendgrid.sender,
        templateId: config.sendgrid.opt_out,
        dynamicTemplateData: {
          userId: uid,
          firstName: userProfile.userFirstname,
          surname: userProfile.userSurname,
        },
      },
    ]);
  });
});
