import * as functions from 'firebase-functions';
const environment = functions.config();
import * as admin from 'firebase-admin';
const db = admin.firestore();
import * as express from 'express';
import * as cors from 'cors';
import * as path from 'path';
import * as logger from 'morgan';
import { find } from 'lodash';

// Import routes
import { indexRouter } from './routes/index';
import { tokenRouter } from './routes/token';
import { voiceRouter } from './routes/voice';
import { conferenceRouter } from './routes/conference';
import { saveSmsMessages } from './utils/saveMessages';
const allowedOrigins = environment.twilio.domains.split(',');

const app = express();

// Middleware
app.use(cors({ origin: true }));
app.use(function(req, res, next) {
  const origin = req.headers.origin;
  if (allowedOrigins.includes(origin)) {
    res.header('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// Use routes
app.use('/', indexRouter);
app.use('/token', tokenRouter);
app.use('/voice', voiceRouter);
app.use('/conference', conferenceRouter);

app.post('/sms', async (req, res) => {
  const { From, Body } = req.body;

  // Find the user with the phone number
  const userSnapshot = await db
    .collection('users')
    .where('userPhone', 'in', [From, From.replace('+61', '0'), From.replace('+61', '+610')])
    .get();

  // If user is a registered PiP+ user, record the inbound SMS message
  if (userSnapshot.size !== 0) await saveSmsMessages(userSnapshot.docs[0].id, 'parent', Body);
  // Otherwise, save the SMS message for recording keeping purpose
  else await saveSmsMessages(From, 'parent', Body);

  res.writeHead(200);
  res.end();
});

// SendGrid notifications for events including bounces and dropped
// Check whether the user exists in PiP+, if yes, set a flag on the user object
// with the error details to be displayed on the admin dashboard
app.post('/event', async function(req, res) {
  const events = req.body;
  const promises: Promise<admin.auth.UserRecord>[] = [];

  try {
    // Loop thourgh all incoming events, and find the user document in Firebase
    events.forEach(function(e: any) {
      const { email, event, reason } = e;
      functions.logger.info(`email: ${email}, event: ${event}, reason: ${reason}`);
      promises.push(admin.auth().getUserByEmail(email));
    });

    // Filter only the successful requests for `getUserByEmail`
    const results = await Promise.all(promises.map(p => p.catch(e => e)));
    const validResults = results.filter(result => !(result instanceof Error));
    const flagPromises: Promise<FirebaseFirestore.WriteResult>[] = [];

    // Loop through the successful requests and update the `emailFlag` to indicate the sending error
    validResults.forEach((user: admin.auth.UserRecord) => {
      if (user) {
        const uid = user.uid;
        const userEmail = user.email;
        const userEvent = find(events, e => e.email === userEmail);
        if (userEvent)
          flagPromises.push(
            db.doc(`users/${uid}`).update({ emailFlag: userEvent.reason ? userEvent.reason : 'Failed to Send Email' }),
          );
      }
    });

    await Promise.all(flagPromises);

    res.writeHead(200);
    res.end();
  } catch (error) {
    functions.logger.error('POST `twilioApp/event` Error: ', error);
    res.writeHead(200);
    res.end();
  }
});

// Callback URL for Twilio Programmable Voice to get `failed` and `undelivered` SMS notifications
app.post('/smsStatus', async function(req, res) {
  const event = req.body;

  try {
    const { To, MessageStatus } = event;
    functions.logger.info(`To ${To}, MessageStatus: ${MessageStatus}`);

    // If message status is `failed` or `undelivered`, find the user with the `To` phone
    // number and update the `smsFlag` to indicate the sending error
    if (MessageStatus === 'failed' || MessageStatus === 'undelivered') {
      const usersSnap = await db
        .collection('users')
        .where('userPhone', '==', To)
        .get();

      if (usersSnap.size !== 0) {
        const user = usersSnap.docs[0];
        await db.doc(`users/${user.id}`).update({ smsFlag: MessageStatus });
      }
    }

    res.writeHead(200);
    res.end();
  } catch (error) {
    functions.logger.error('POST `twilioApp/smsStatus` Error: ', error);
    res.writeHead(200);
    res.end();
  }
});

export const twilioApp = functions.https.onRequest(app);
