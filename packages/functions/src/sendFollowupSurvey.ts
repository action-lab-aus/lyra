import * as functions from 'firebase-functions';
const environment = functions.config();
import * as admin from 'firebase-admin';
const db = admin.firestore();
import { Twilio } from 'twilio';
import { sendSendGridEmail } from './utils/sendEmail';
import { formatPhone } from './utils/utils';
import { EmailParams } from './utils/types';
import { UserProfile } from './helpers';

// SendGrid Credentials
const RA_EMAIL = environment.sendgrid.ra_email;
const DASHBOARD_LINK = environment.sendgrid.dashboard_link;

// Twilio Credentials
const TWILIO_ACCOUNT_SID = environment.twilio.account_sid;
const TWILIO_AUTH_TOKEN = environment.twilio.auth_token;
const TWILIO_NUMBER = environment.twilio.twilio_number;
const TWILIO_STATUS_CALLBACK = environment.twilio.status_callback;
const client = new Twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

/**
 * Firebase Pub/Sub function runs every minute to find all the followup survey notifications
 * that need to be sent to parents (90 days after they complete the baseline survey).
 * Check whether the user has opt-out for SMS or the entire program before sending notifications
 *
 * @param context Firebase Pub/Sub function context
 */
export const sendFollowupSurvey = functions.pubsub.schedule('every 1 minutes').onRun(async context => {
  try {
    const timestamp = admin.firestore.Timestamp.fromDate(new Date());

    // Fetch all module unlock documents where unlockAt is before the current time
    const sendDocs = (
      await db
        .collection('followupSurvey')
        .where('sendAt', '<=', timestamp)
        .get()
    ).docs;

    const userPromises: Promise<FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>>[] = [];
    const userEmailPromises: Promise<admin.auth.UserRecord>[] = [];
    const deletePromises: Promise<FirebaseFirestore.WriteResult>[] = [];

    // Loop through all unlock documents and push into promises to fetch module content
    // and for future deletion
    sendDocs.forEach(doc => {
      const uid = doc.data().userId;
      userPromises.push(db.doc(`users/${uid}`).get());
      userEmailPromises.push(admin.auth().getUser(uid));
      deletePromises.push(db.doc(`followupSurvey/${doc.id}`).delete());
    });

    const userResults = await Promise.all(userPromises);
    const userEmails = await Promise.all(userEmailPromises);
    const metaData = (await db.doc(`config/meta`).get()).data();

    const sendGridPromises = [];
    const twilioPromises = [];

    // Loop through the module results and check whether the module is still locked or not
    for (let i = 0; i < userResults.length; i++) {
      const currentUser = userResults[i];
      const {
        userFirstname,
        userPhone,
        optOutAt,
        smsNotification,
        followupCompletedAt,
      } = currentUser.data() as UserProfile;
      const email = userEmails[i].email;
      const type = sendDocs[i].data().type;

      // If the user has not opted out and the followup survey has not been completed yet,
      // trigger the follow survey notifications accordingly
      if (!optOutAt && email && !followupCompletedAt) {
        if (type !== 'followup_survey_reminder_21') {
          // Trigger the followup survey notification email
          // FOLLOWUP_SURVEY || FOLLOWUP_REMINDER_7 || FOLLOWUP_REMINDER_14 - firstName, dashboardLink
          sendGridPromises.push(
            sendSendGridEmail(email, getEmailTemplateId(type), {
              firstName: userFirstname,
              dashboardLink: DASHBOARD_LINK,
            } as EmailParams),
          );

          // Trigger the SMS notification to parents with a status callback
          if (metaData && smsNotification) {
            let smsTemplate = metaData.smsTemplates[type];
            smsTemplate = smsTemplate.replace('{{Name}}', userFirstname);
            twilioPromises.push(
              client.messages.create({
                body: smsTemplate,
                from: TWILIO_NUMBER,
                statusCallback: TWILIO_STATUS_CALLBACK,
                to: formatPhone(userPhone),
              }),
            );
          }
        } else {
          // Trigger the followup survey notification email (to RA)
          // FOLLOWUP_REMINDER_21 - userId
          sendGridPromises.push(
            sendSendGridEmail(RA_EMAIL, getEmailTemplateId(type), {
              userId: currentUser.id,
            } as EmailParams),
          );
        }
      }
    }

    await Promise.allSettled(sendGridPromises);
    await Promise.allSettled(twilioPromises);
    await Promise.allSettled(deletePromises);
  } catch (error) {
    functions.logger.error('sendFollowupSurvey Error: ', error);
  }
});

const getEmailTemplateId = (
  type:
    | 'followup_survey'
    | 'followup_survey_reminder_7'
    | 'followup_survey_reminder_14'
    | 'followup_survey_reminder_21',
) => {
  switch (type) {
    case 'followup_survey':
      return environment.sendgrid.followup_survey;
    case 'followup_survey_reminder_7':
      return environment.sendgrid.followup_survey_reminder_7;
    case 'followup_survey_reminder_14':
      return environment.sendgrid.followup_survey_reminder_14;
    default:
      return environment.sendgrid.followup_survey_reminder_21;
  }
};
