import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.firestore();

/**
 * Local emulator HTTP function to load initial configurationd data into Firestore for testing
 *
 * @param request HTTPS request data
 * @param response HTTPS response data
 */
export const localEmulatorData = functions.https.onRequest(async (request, response) => {
  try {
    await db.doc('config/meta').set({
      smsTemplates: {
        module_completion:
          "Hi {{Name}}, Congratulations on completing all the modules in the PiP program! You can revisit any of the modules, including those you didn't select, at any time. Check your email for details. Feel free to contact us with any questions. Thanks, PiP team.",
        reg_successful_parents:
          "Hi {{Name}}. Your PiP account has been verified. To start your program, you'll need to complete an initial survey. Check your email for details. Thanks, PiP team.",
        survey_completion_parents:
          'Hi {{Name}}, thanks for completing the PiP survey. Please check your email for feedback and next steps in your program. Thanks, PiP team.',
        survey_reminder_14:
          "Hi {{Name}}, thanks for signing up for PiP. We noticed you haven't started your program. See your email, or reply with any questions. Thanks, PiP team.",
        survey_reminder_7:
          "Hi {{Name}}, thanks for signing up for PiP. We noticed you haven't started your program. See your email, or reply with any questions. Thanks, PiP team.",
        weekly_module_access:
          'Hi {{Name}}, your next PiP module {{moduleTitle}} is now available. Log in to your dashboard to continue your program. Thanks, PiP team.',
      },
    });

    response.status(200).send('Successfully loaded local emulator data!');
  } catch (error) {
    functions.logger.error('localEmulatorData Error: ', error);
    response.status(400).send('Error occurred when loading emulator data...');
  }
});
