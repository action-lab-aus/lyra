import * as functions from 'firebase-functions';
const environment = functions.config();
import * as admin from 'firebase-admin';
const db = admin.firestore();
import axios from 'axios';
const PAGE_TOKEN = environment.facebook.page_token;
import { PostObject, CommentObject, ReactionObject, FromReference } from './utils/types';

/**
 * Firebase HTTPS function to add new Facebook Group posts/comments/reactions when identified
 * from Facebook Graph API (Convert to a Pub/Sub Function for Live Deployment)
 *
 * @param request Firebase function HTTPS request data
 * @param response Firebase function HTTPS response data
 */
export const addGroupPosts = functions.pubsub.schedule('every 3 hours').onRun(async context => {
  try {
    await addFacebookPostsAndComments();
  } catch (error) {
    functions.logger.error('addGroupPosts Error: ', error);
  }
});

/**
 * HTTP Callable function called by admin users to fetch new Facebook posts/comments before
 * deleting any comments on Facebook
 *
 * @param data {}
 * @param context Firebase HTTPS callable function context
 */
export const addGroupPostsAdmin = functions.https.onCall(async (data, context) => {
  const isAdmin = context.auth?.token.admin;
  if (!isAdmin) return { status: 401, message: `Unauthorized` };

  try {
    await addFacebookPostsAndComments();
    return { status: 200, message: `Successfully fetched new Facebook posts and comments!` };
  } catch (error) {
    functions.logger.error(error);
    return { status: 500, message: `Error: ${error}` };
  }
});

/**
 * Get all existing Facebook group IDs
 *
 * @returns string[] An arrary of Facebook Group IDs
 */
async function getGroupIds() {
  try {
    const groups = (await db.collection('groups').get()).docs;
    return groups.map(g => g.id);
  } catch (error) {
    functions.logger.error('getGroupIds Error: ', error);
    return [];
  }
}

async function addFacebookPostsAndComments() {
  const timestamp = Math.round((Date.now() - 3600 * 3 * 1000) / 1000);
  functions.logger.info('timestamp: ', timestamp);

  const groupIds = await getGroupIds();
  const getGroupPostPromises = groupIds.map(id =>
    axios.get(
      `https://graph.facebook.com/v11.0/${id}/feed?fields=id,from,updated_time,created_time,message,comments{comment_count,like_count,message,from,updated_time,created_time},reactions&since=${timestamp}&access_token=${PAGE_TOKEN}`,
    ),
  );
  const getGroupPostResponses = (await Promise.all(getGroupPostPromises)).map(res => res.data.data);

  const dbPostsPromises: Promise<FirebaseFirestore.WriteResult>[] = [];
  const dbCommentsPromises: Promise<FirebaseFirestore.WriteResult>[] = [];
  const dbReactionPromises: Promise<FirebaseFirestore.WriteResult>[] = [];
  const updateGroupPromises: Promise<FirebaseFirestore.WriteResult>[] = [];

  const fromRefs: FromReference[] = [];
  const fromPromises: Promise<FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>>[] = [];

  for (let i = 0; i < getGroupPostResponses.length; i++) {
    const posts = getGroupPostResponses[i];
    let sumPosts = 0;
    let sumComments = 0;

    if (posts.length > 0) {
      const groupId = posts[0].id.split('_')[0];

      posts.map((post: PostObject) => {
        if (post.message) {
          const updatedAt = post.updated_time ? post.updated_time : post.created_time;

          const postTimestamp = new Date(updatedAt).getTime() / 1000;
          if (postTimestamp - timestamp >= 0) sumPosts++;

          let postObj: any = {
            id: post.id,
            message: post.message,
            updatedAt: updatedAt,
          };

          if (post.from) {
            postObj['userName'] = post.from.name;
            postObj['userId'] = post.from.id;
          } else {
            fromRefs.push({ id: post.id, type: 'post', updatedAt: updatedAt, msg: post.message });
            fromPromises.push(db.doc(`fbMapping/${post.id}`).get());
          }

          dbPostsPromises.push(db.doc(`groups/${groupId}/posts/${post.id}`).set(postObj));

          if (post.comments) {
            post.comments.data.forEach((comment: CommentObject) => {
              const commentUpdatedAt = comment.updated_time ? comment.updated_time : comment.created_time;

              const commentTimestamp = new Date(commentUpdatedAt).getTime() / 1000;
              if (commentTimestamp - timestamp >= 0) sumComments++;

              let commentObj: any = {
                id: comment.id,
                message: comment.message,
                updatedAt: commentUpdatedAt,
                comment_count: comment.comment_count,
                like_count: comment.like_count,
              };

              if (comment.from) {
                commentObj['userName'] = comment.from.name;
                commentObj['userId'] = comment.from.id;
              } else {
                fromRefs.push({
                  id: `${post.id}-${comment.id}`,
                  type: 'comment',
                  updatedAt: commentUpdatedAt,
                  msg: comment.message,
                });
                fromPromises.push(db.doc(`fbMapping/${post.id}-${comment.id}`).get());
              }

              dbCommentsPromises.push(
                db.doc(`groups/${groupId}/posts/${post.id}/comments/${comment.id}`).set(commentObj),
              );
            });
          }

          if (post.reactions) {
            post.reactions.data.forEach((reaction: ReactionObject) => {
              let reactionObj: any = {
                id: reaction.id,
                type: reaction.type,
              };

              if (reaction.name) reactionObj['userName'] = reaction.name;

              dbReactionPromises.push(
                db.doc(`groups/${groupId}/posts/${post.id}/reactions/${reaction.id}`).set(reactionObj),
              );
            });
          }
        }
      });

      updateGroupPromises.push(db.doc(`groups/${groupId}`).update({ noOfPosts: sumPosts, noOfComments: sumComments }));
    } else
      updateGroupPromises.push(
        db.doc(`groups/${groupIds[i]}`).update({ noOfPosts: sumPosts, noOfComments: sumComments }),
      );
  }

  await Promise.all(dbPostsPromises);
  await Promise.all(dbCommentsPromises);
  await Promise.all(dbReactionPromises);
  await Promise.all(updateGroupPromises);
  const fromResults = await Promise.all(fromPromises);

  const tagPromises: Promise<FirebaseFirestore.WriteResult>[] = [];
  for (let i = 0; i < fromResults.length; i++) {
    const item = fromResults[i];
    const ref = fromRefs[i];
    if (!item.exists)
      tagPromises.push(
        db.doc(`fbMapping/${ref.id}`).set({
          type: ref.type,
          updatedAt: ref.updatedAt,
          msg: ref.msg,
          status: 'pending',
        }),
      );
  }
  await Promise.all(tagPromises);
}
