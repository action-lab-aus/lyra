import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.firestore();
const FieldValue = admin.firestore.FieldValue;
import { findIndex } from 'lodash';
import { CallLogsData, FollowupMessages } from './utils/types';

/**
 * Update follow-up call logs for the user management dashboard when the call starts running
 * or ended
 *
 * @param data Call logs data including user id, call status, and user email
 * @param context Firebase HTTPS callable function context
 *
 * @returns Response data with status - status code & message - response message
 */
export const updateCallLogs = functions.https.onCall(async (data: CallLogsData, context) => {
  const isAdmin = context.auth?.token.admin;
  const { uid, status, email } = data;

  if (!uid) return { status: 403, message: `Missing the 'uid' field for the request...` };
  if (!email) return { status: 403, message: `Missing the 'email' field for the request...` };
  if (!status) return { status: 403, message: `Missing the 'status' field for the request...` };
  if (!isAdmin) return { status: 401, message: `Unauthorized` };

  try {
    const timestamp = admin.firestore.Timestamp.now();

    // For new call logs, append the new object to the call array
    if (status === 'running') {
      // Update the `phoneInUse` flag to be true
      await db.doc('config/meta').update({ phoneInUse: true });

      // Check whether the log exist or not
      const logExist = (await db.doc(`followup/${uid}`).get()).exists;

      // If exists, add a new call log entry into the calls array
      if (logExist)
        await db
          .collection('followup')
          .doc(uid)
          .update({
            logs: FieldValue.arrayUnion({
              type: 'Call',
              startTime: timestamp,
              status: status,
              admin: email,
            }),
          });
      // Otherwise, set the calls array with the current log
      else
        await db
          .collection('followup')
          .doc(uid)
          .set({
            logs: [
              {
                type: 'Call',
                startTime: timestamp,
                status: status,
                admin: email,
              },
            ],
          });

      return { status: 200, message: `Successfully added new call logs` };
    }
    // For ended calls, find the log item and update the status & endTime
    else if (status === 'ended') {
      // Update the `phoneInUse` flag to be true
      await db.doc('config/meta').update({ phoneInUse: false });

      // Fetch all the call logs from Firestore
      const callsData = (await db.doc(`followup/${uid}`).get()).data();

      if (callsData && callsData.logs) {
        // Find the call to be updated
        const idx = findIndex(callsData.logs, (log: FollowupMessages) => {
          return log.status === 'running';
        });

        // If call exists, update the follow-up call notes
        if (idx >= 0) {
          const tmpMessages = [...callsData.logs];
          tmpMessages[idx].status = 'ended';
          tmpMessages[idx].endTime = timestamp;
          await db.doc(`followup/${uid}`).update({ logs: tmpMessages });
        }
      }

      return { status: 200, message: `Successfully updated call log!` };
    } else return { status: 403, message: `Invalid 'status' field for the request...` };
  } catch (error) {
    functions.logger.error(error);
    return { status: 400, message: `Error: ${error}` };
  }
});
