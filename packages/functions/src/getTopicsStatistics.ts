import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.firestore();
import { TopicMap, TopicArrayItem } from './utils/types';

/**
 * Firebase scheduler function to update the topic statistics (get number of occurrences for
 * each topic, sort topics, and daily sum of topics)
 *
 * @param context Firebase event context
 */
export const getTopicsStatistics = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '512MB',
  })
  .pubsub.schedule('every 1 hours')
  .onRun(async context => {
    const topics = await db.collection('topics').get();

    let topicSum = 0;
    const topicMap: TopicMap = {};

    // Construct the topic map with occurrences of each topic
    topics.docs.forEach(group => {
      const topicsData = group.data().topics;
      for (let i = 0; i < topicsData.length; i++) {
        topicSum += 1;
        const topic: string = topicsData[i];
        if (topicMap[topic]) topicMap[topic] += 1;
        else topicMap[topic] = 1;
      }
    });

    // Convert the topic map into an array for sorting
    const tmpArr: TopicArrayItem[] = [];
    Object.keys(topicMap).forEach(key => {
      tmpArr.push({ topic: key, occurrence: topicMap[key] });
    });

    // Sort the topic array
    const topicArr = tmpArr.sort(compare);

    // Update the `config/topicStats` document
    const previousSum = (await db.doc('config/topicStats').get()).data()?.topicSum;
    await db
      .doc('config/topicStats')
      .update({ topicsArray: topicArr, topicSumPrevious: previousSum ? previousSum : 0, topicSum: topicSum });
  });

/**
 * Function used to determine the order of the topic array (based on the number of occurrences)
 *
 * @param a Sort item A
 * @param b Sort item B
 *
 * @returns -1 if first argument is larger than second argument, zero if they're equal and +1 otherwise
 */
function compare(a: TopicArrayItem, b: TopicArrayItem) {
  if (a.occurrence > b.occurrence) return -1;
  if (a.occurrence < b.occurrence) return 1;
  return 0;
}
