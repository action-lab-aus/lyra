import * as functions from 'firebase-functions';
const environment = functions.config();
import * as admin from 'firebase-admin';
const db = admin.firestore();
import { Twilio } from 'twilio';
import { sendSendGridEmail } from './utils/sendEmail';
import { formatPhone } from './utils/utils';
import { EmailParams } from './utils/types';
import { getSendGridAttachment } from './helpers/getSendGridAttachment';

// SendGrid Credentials
const RA_EMAIL = environment.sendgrid.ra_email;
const DASHBOARD_LINK = environment.sendgrid.dashboard_link;
const REG_SUCCESSUL_PARENTS_ID = environment.sendgrid.reg_successful_parents;
const REG_SUCCESSUL_RA_ID = environment.sendgrid.reg_successful_ra;
const VERIFICATION_ID = environment.sendgrid.verification;

// Twilio Credentials
const TWILIO_ACCOUNT_SID = environment.twilio.account_sid;
const TWILIO_AUTH_TOKEN = environment.twilio.auth_token;
const TWILIO_NUMBER = environment.twilio.twilio_number;
const TWILIO_STATUS_CALLBACK = environment.twilio.status_callback;
const client = new Twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

/**
 * Triggered when new user profile is created in Firestore to trigger the registration successful
 * email notification to both parent and RA
 *
 * @param snapshot Firestore document snapshot
 * @param context Firebase function event context
 */
export const userOnCreate = functions.firestore.document('users/{uid}').onCreate(async (snapshot, context) => {
  const userData = (await db.doc(`users/${context.params.uid}`).get()).data();

  let { email, userContactEmail, sendVerification, childName, smsNotification } = userData!;
  let firstName = userData?.userFirstname;
  let surname = userData?.userSurname;
  let phone = userData?.userPhone;

  const currentUser = await admin.auth().getUser(context.params.uid);
  if (!email) email = currentUser.email;

  functions.logger.info(
    `email: ${email}, firstName: ${firstName}, surname: ${surname}, childName: ${childName}, phone: ${phone}`,
  );

  if ((!email && !userContactEmail) || !firstName || !surname || !childName || !phone) return;

  // Update user email into the Firebase auth if flagged during profile creation
  if (sendVerification && userContactEmail && !currentUser.emailVerified) {
    email = userContactEmail;
    await admin.auth().updateUser(context.params.uid, { email: userContactEmail });

    // Send verification email to the parent
    // VERIFICATION_ID - verificationLink
    const verificationLink = await admin
      .auth()
      .generateEmailVerificationLink(userContactEmail, { url: environment.auth.auth_url });

    await sendSendGridEmail(userContactEmail, VERIFICATION_ID, {
      verificationLink,
    } as EmailParams);
  }

  // Update user with the createdAt timestamp
  await db.doc(`users/${context.params.uid}`).update({ createdAt: admin.firestore.FieldValue.serverTimestamp() });

  // Trigger the notification email to RA
  // REG_SUCCESSUL_RA_ID - userId, parentEmail, firstName, surname
  await sendSendGridEmail(RA_EMAIL, REG_SUCCESSUL_RA_ID, {
    userId: context.params.uid,
    parentEmail: email,
    firstName: firstName,
    surname: surname,
  } as EmailParams);

  // Trigger the notification email to parents
  // REG_SUCCESSUL_PARENTS_ID - firstName, childName, dashboardLink
  const attachment = getSendGridAttachment('Useful_Resources.pdf');
  await sendSendGridEmail(
    email,
    REG_SUCCESSUL_PARENTS_ID,
    {
      firstName: firstName,
      childName: childName,
      dashboardLink: DASHBOARD_LINK,
    } as EmailParams,
    [attachment],
  );

  // Trigger the SMS notification to parents with a status callback
  if (smsNotification) {
    const metaData = (await db.doc(`config/meta`).get()).data();
    if (metaData) {
      let smsTemplate = metaData.smsTemplates['reg_successful_parents'];
      smsTemplate = smsTemplate.replace('{{Name}}', firstName);
      await client.messages.create({
        body: smsTemplate,
        from: TWILIO_NUMBER,
        statusCallback: TWILIO_STATUS_CALLBACK,
        to: formatPhone(phone),
      });
    }
  }
});
