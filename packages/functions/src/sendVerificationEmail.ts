import * as functions from 'firebase-functions';
const environment = functions.config();
import * as admin from 'firebase-admin';
import { withUid } from './helpers';
import { sendSendGridEmail } from './utils/sendEmail';
import { EmailParams } from './utils/types';
const VERIFICATION_ID = environment.sendgrid.verification;

export type VerificationInput = {
  email: string;
};

/**
 * HTTP Callable function called to send verification emails to Facebook users who didn't provide
 * their email address and haven't finished profile creation after 24 hours
 *
 * @param data VerificationInput
 * @param context Firebase HTTPS callable function context
 */
export const sendVerificationEmail = functions.https.onCall(async (data: VerificationInput, context) => {
  return withUid(context, async uid => {
    const { email } = data;
    if (!email) throw new functions.https.HttpsError('invalid-argument', 'Invalid input');

    functions.logger.info('email: ', email);

    try {
      await admin.auth().updateUser(uid, { email });

      // Send verification email to the parent
      const verificationLink = await admin
        .auth()
        .generateEmailVerificationLink(email, { url: environment.auth.auth_url });

      await sendSendGridEmail(email, VERIFICATION_ID, {
        verificationLink,
      } as EmailParams);
    } catch (err) {
      throw new functions.https.HttpsError('internal', err.message);
    }
  });
});
