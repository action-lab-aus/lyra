import * as functions from 'firebase-functions';
const environment = functions.config();
import axios from 'axios';
const PAGE_TOKEN = environment.facebook.page_token;
import { CommentData } from './utils/types';

/**
 * Post a comment to a Facebook Group post or comment via the Graph API /comments endpoint
 *
 * @param data Facebook comment data including feed id, content, and admin email
 * @param context Firebase HTTPS callable function context
 *
 * @returns Response data with status - status code & message - response message
 */
export const postFacebookComment = functions.https.onCall(async (data: CommentData, context) => {
  const isAdmin = context.auth?.token.admin;
  const { id, content, adminEmail } = data;

  if (!id) return { status: 403, message: `Missing the 'id' field for the request...` };
  if (!content) return { status: 403, message: `Missing the 'content' field for the request...` };
  if (!adminEmail) return { status: 403, message: `Missing the 'admin' field for the request...` };
  if (!isAdmin) return { status: 401, message: `Unauthorized` };

  try {
    // Post a comment on Facebook via the Graph API /comments endpoint
    await axios.post(`https://graph.facebook.com/v11.0/${id}/comments?message=${content}&access_token=${PAGE_TOKEN}`);

    return { status: 200, message: `Successfully posted to Facebook!` };
  } catch (error) {
    functions.logger.error(error);
    return { status: 400, message: `Error: ${error}` };
  }
});
