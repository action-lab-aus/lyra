import * as functions from 'firebase-functions';
const environment = functions.config();
import * as express from 'express';
const router = express.Router();
const cors = require('cors');
import { verifyToken, corsWhiteListOptions } from './utils';
import { Twilio } from 'twilio';
const client = new Twilio(environment.twilio.account_sid, environment.twilio.auth_token);
const phoneNumber = environment.twilio.twilio_number;

/* Pre-flight Option Request */
router.options('/', cors());

/* POST Conference Main */
router.post(
  '/',
  cors(corsWhiteListOptions(environment.twilio.domains.split(','))),
  verifyToken,
  async (req, res, next) => {
    let result;
    await client
      .conferences('PiP_Followup')
      .participants.create({
        from: phoneNumber,
        to: req.body.number,
      })
      .then(participant => {
        result = participant.callSid;
      })
      .catch(error => {
        console.log(error);
      });

    res.type('text/xml');
    res.status(200).send(result);
  },
);

export const conferenceRouter = router;
