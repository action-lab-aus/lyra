import * as functions from 'firebase-functions';
const environment = functions.config();
import * as admin from 'firebase-admin';

/**
 * Function to validate tokens
 */
export const verifyToken = async (req: any, res: any, next: any) => {
  if (!req.headers['authorization']) return res.status(403).send({ auth: false, message: 'Missing Token.' });

  const idToken = req.headers['authorization'].split('Bearer ')[1];
  if (!idToken) return res.status(403).send({ auth: false, message: 'Missing Token.' });

  const decodedIdToken = await admin.auth().verifyIdToken(idToken);
  functions.logger.info('Firestore id token correctly decoded: ', decodedIdToken);
  if (!decodedIdToken.admin) return `Unauthorized`;

  req.userId = decodedIdToken.uid;
  next();
};

/**
 * Function to verify header from Twilio
 */
export const verifyHeader = (req: any, res: any, next: any) => {
  if (!req.headers['user-agent']) return res.status(403).send({ auth: false, message: 'Missing `user-agent` Header.' });

  const user_agent = req.headers['user-agent'].split('/')[0];
  if (!user_agent) return res.status(403).send({ auth: false, message: 'Missing `user-agent` Header.' });
  else {
    if (user_agent !== environment.twilio.twilio_user_agent)
      return res.status(403).send({ auth: false, message: 'Invalid `user-agent` Header.' });
    next();
  }
};

/**
 * Function to verify CORS whitelist
 */
export const corsWhiteListOptions = (array: any) => {
  const whitelist = array;

  let corsOptions = {
    origin: function(origin: any, callback: any) {
      if (whitelist.indexOf(origin) !== -1) {
        callback(null, true);
      } else callback(new Error('Not Allowed by CORS.'));
    },
  };
  return corsOptions;
};
