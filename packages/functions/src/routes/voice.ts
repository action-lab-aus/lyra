import * as functions from 'firebase-functions';
const environment = functions.config();
import * as express from 'express';
const router = express.Router();
const cors = require('cors');
import { twiml } from 'twilio';
const VoiceResponse = twiml.VoiceResponse;
import { verifyHeader } from './utils';
const phoneNumber = environment.twilio.twilio_number;

/* Pre-flight Option Request */
router.options('/', cors());

/* POST Voice Main */
router.post('/', cors(), verifyHeader, (req, res, next) => {
  // Create TwiML response
  const twiml = new VoiceResponse();

  // Start with a <Dial> verb
  const dial = twiml.dial();

  switch (req.body.code) {
    case 'caller':
      dial.conference(
        {
          startConferenceOnEnter: true,
          endConferenceOnExit: true,
        },
        'PiP_Followup',
      );
      break;
    default:
      twiml.dial({ callerId: phoneNumber }, req.body.number);
      break;
  }

  res.type('text/xml');
  res.status(200).send(twiml.toString());
});

export const voiceRouter = router;
