import * as express from 'express';
const router = express.Router();
import { twiml } from 'twilio';
const VoiceResponse = twiml.VoiceResponse;

/* POST Main */
router.post('/', (req, res, next) => {
  // Create TwiML response
  const twiml = new VoiceResponse();
  twiml.say('Sorry, the person you are calling is currently unavailable. Bye! Bye!');

  res.writeHead(200, { 'Content-Type': 'text/xml' });
  res.end(twiml.toString());
});

export const indexRouter = router;
