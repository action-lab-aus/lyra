import * as functions from 'firebase-functions';
const environment = functions.config();
import * as express from 'express';
const router = express.Router();
const cors = require('cors');
import { jwt } from 'twilio';
const ClientCapability = jwt.ClientCapability;
import { verifyToken, corsWhiteListOptions } from './utils';

/* Pre-flight Option Request */
router.options('/', cors());

/* GET Users Listing */
router.get('/', cors(corsWhiteListOptions(environment.twilio.domains.split(','))), verifyToken, (req, res, next) => {
  const capability = new ClientCapability({
    accountSid: environment.twilio.account_sid,
    authToken: environment.twilio.auth_token,
    ttl: 21600,
  });

  capability.addScope(
    new ClientCapability.OutgoingClientScope({
      applicationSid: environment.twilio.twiml_app_sid,
    }),
  );

  // Include token in a JSON response
  const token = capability.toJwt();
  res.send({ token: token });
});

export const tokenRouter = router;
