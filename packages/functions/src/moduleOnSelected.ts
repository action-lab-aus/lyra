import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.firestore();
import { TopicSeq } from './utils/types';

/**
 * Triggered when modules are selected by parents and the sequence get finalized. Push the unlock
 * documents under the `moduleUnlock` collection with the `unlockAt` field for future notifications
 *
 * @param snapshot Firestore document snapshot
 * @param context Firebase function event context
 */
export const moduleOnSelected = functions.firestore
  .document('users/{uid}/topics/{topidId}')
  .onCreate(async (snapshot, context) => {
    if (context.params.topidId !== 'm1') return;

    // Get all topics for the current user
    const topics = (await db.collection(`users/${context.params.uid}/topics`).get()).docs;
    const unlockIntervalMins = (await db.doc('config/meta').get()).data()!.unlockIntervalMins;

    // Construct the selected array with all mandatory modules with the sequence sorted
    let selectedArr: TopicSeq[] = [];
    topics.forEach(topic => {
      if (topic.data().mandatory) selectedArr.push({ id: topic.id, seq: topic.data().seq });
    });
    selectedArr = selectedArr.sort((a, b) => (a.seq > b.seq ? 1 : b.seq > a.seq ? -1 : 0));
    functions.logger.info('selectedArr: ', selectedArr);

    // Fetch the current user email and profile data
    const currentUser = await admin.auth().getUser(context.params.uid);
    const firestoreUser = (await db.doc(`users/${context.params.uid}`).get()).data();
    if (!currentUser.email || !firestoreUser) return;

    // Loop through the selected array and push data to the `moduleUnlock` collection
    const promises = [];
    for (let i = 0; i < selectedArr.length; i++) {
      const obj = selectedArr[i];
      // Calculate and get the Firestore timestamp for unlocking the module
      // and push the document to the `moduleUnlock` collection
      let date = new Date();
      date = new Date(date.getTime() + unlockIntervalMins * i * 60000);
      const timestamp = admin.firestore.Timestamp.fromDate(date);

      const unlockObj = {
        unlockAt: timestamp,
        userId: context.params.uid,
        module: obj.id,
        email: currentUser.email,
        phone: firestoreUser.userPhone,
        firstName: firestoreUser.userFirstname,
      };
      promises.push(db.collection('moduleUnlock').add(unlockObj));
    }

    await Promise.all(promises);
  });
