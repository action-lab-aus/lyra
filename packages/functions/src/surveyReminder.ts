import * as functions from 'firebase-functions';
const environment = functions.config();
import * as admin from 'firebase-admin';
const db = admin.firestore();
import { Twilio } from 'twilio';
import { sendSendGridEmail } from './utils/sendEmail';
import { formatPhone } from './utils/utils';
import { EmailParams } from './utils/types';

// SendGrid Credentials
const RA_EMAIL = environment.sendgrid.ra_email;
const DASHBOARD_LINK = environment.sendgrid.dashboard_link;
const SURVEY_REMINDER_7 = environment.sendgrid.survey_reminder_7;
const SURVEY_REMINDER_14 = environment.sendgrid.survey_reminder_14;
const SURVEY_REMINDER_21 = environment.sendgrid.survey_reminder_21;

// Twilio Credentials
const TWILIO_ACCOUNT_SID = environment.twilio.account_sid;
const TWILIO_AUTH_TOKEN = environment.twilio.auth_token;
const TWILIO_NUMBER = environment.twilio.twilio_number;
const TWILIO_STATUS_CALLBACK = environment.twilio.status_callback;
const client = new Twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

/**
 * Firebase HTTPS function to add loop through all user objects and send survey completion
 * reminder email and SMS messages if needed
 *
 * @param request Firebase function HTTPS request data
 * @param response Firebase function HTTPS response data
 */
export const surveyReminder = functions.pubsub.schedule('every 1 minutes').onRun(async context => {
  try {
    const timestamp = Math.round(Date.now() / 1000);

    // Fetch all current users
    const users = (await db.collection('users').get()).docs;
    const interval = (await db.doc('config/meta').get()).data()!.unlockIntervalMins;

    // Find users that need to trigger survey reminder emails and push the requests into the
    // `userPromises` to get user PiP+ email accounts
    const userPromises: Promise<admin.auth.UserRecord>[] = [];
    const userPromisesRef: string[] = [];

    users.forEach(user => {
      const data = user.data();
      // If survey not completed yet, check whether reminders need to be sent
      if (!data.surveyCompletedAt && !data.optOutAt) {
        // Get user createdAt timestamps and flag of whether the survey reminder was sent
        let phone = data.userPhone;
        const { userFirstname, createdAt, surveyReminder7, surveyReminder14, surveyReminder21, smsNotification } = data;

        if (createdAt) {
          // Get time difference in minutes - 5 for staging, 60 * 24 * 7
          const diff = (timestamp - createdAt.seconds) / 60;

          // Check the type of survey reminder
          if (diff >= interval * 3 && !surveyReminder21) {
            userPromises.push(admin.auth().getUser(user.id));
            userPromisesRef.push(`${user.id}@21@${phone}@${userFirstname}@${smsNotification}`);
          } else if (diff >= interval * 2 && diff < interval * 3 && !surveyReminder14) {
            userPromises.push(admin.auth().getUser(user.id));
            userPromisesRef.push(`${user.id}@14@${phone}@${userFirstname}@${smsNotification}`);
          } else if (diff >= interval && diff < interval * 2 && !surveyReminder7) {
            userPromises.push(admin.auth().getUser(user.id));
            userPromisesRef.push(`${user.id}@7@${phone}@${userFirstname}@${smsNotification}`);
          }
        }
      }
    });

    const userResults = await Promise.all(userPromises);
    const sendGridPromises = [];
    const twilioPromises = [];
    const updatePromises = [];
    const notifications = [];

    // Get smsTemplates for the survey reminders
    const metaData = (await db.doc(`config/meta`).get()).data();

    // Loop through the `UserRecord` and push the notifications into promises
    for (let i = 0; i < userResults.length; i++) {
      const user = userResults[i];
      const userRef = userPromisesRef[i];

      let email = user.email;
      const userRefArr = userRef.split('@');
      const userId = userRefArr[0];
      const diff = userRefArr[1];
      const phone = userRefArr[2];
      const firstName = userRefArr[3];
      const smsNotification = userRefArr[4];
      functions.logger.info(
        `userId: ${userId}, diff: ${diff}, phone: ${phone}, firstName: ${firstName}, smsNotification: ${smsNotification}`,
      );

      if (email) {
        notifications.push(`${userId}@${diff}`);

        if (diff === '21') {
          // Trigger the 21 day survey reminder notification email to RA
          // SURVEY_REMINDER_21 - userId
          sendGridPromises.push(
            sendSendGridEmail(RA_EMAIL, SURVEY_REMINDER_21, {
              userId: userId,
            } as EmailParams),
          );

          // Set the 21 day survey reminder flag
          updatePromises.push(db.doc(`users/${userId}`).update({ surveyReminder21: true }));
        } else if (diff === '14') {
          // Trigger the 14 day survey reminder notification email to parents
          // SURVEY_REMINDER_14 - firstName, dashboardLink
          sendGridPromises.push(
            sendSendGridEmail(email, SURVEY_REMINDER_14, {
              firstName: firstName,
              dashboardLink: DASHBOARD_LINK,
            } as EmailParams),
          );

          // Trigger the SMS notification to parents with a status callback
          if (metaData && smsNotification && smsNotification === 'true') {
            let smsTemplate = metaData.smsTemplates['survey_reminder_14'];
            smsTemplate = smsTemplate.replace('{{Name}}', firstName);
            twilioPromises.push(
              client.messages.create({
                body: smsTemplate,
                from: TWILIO_NUMBER,
                statusCallback: TWILIO_STATUS_CALLBACK,
                to: formatPhone(phone),
              }),
            );
          }

          // Set the 14 day survey reminder flag
          updatePromises.push(db.doc(`users/${userId}`).update({ surveyReminder14: true }));
        } else if (diff === '7') {
          // Trigger the 7 day survey reminder notification email to parents
          // SURVEY_REMINDER_7 - firstName, dashboardLink
          sendGridPromises.push(
            sendSendGridEmail(email, SURVEY_REMINDER_7, {
              firstName: firstName,
              dashboardLink: DASHBOARD_LINK,
            } as EmailParams),
          );

          // Trigger the SMS notification to parents with a status callback
          if (metaData && smsNotification && smsNotification === 'true') {
            let smsTemplate = metaData.smsTemplates['survey_reminder_7'];
            smsTemplate = smsTemplate.replace('{{Name}}', firstName);
            twilioPromises.push(
              client.messages.create({
                body: smsTemplate,
                from: TWILIO_NUMBER,
                statusCallback: TWILIO_STATUS_CALLBACK,
                to: formatPhone(phone),
              }),
            );
          }

          // Set the 7 day survey reminder flag
          updatePromises.push(db.doc(`users/${userId}`).update({ surveyReminder7: true }));
        }
      }
    }

    await Promise.allSettled(sendGridPromises);
    await Promise.allSettled(updatePromises);
    await Promise.allSettled(twilioPromises);
  } catch (error) {
    functions.logger.error('surveyReminder Error: ', error);
  }
});
