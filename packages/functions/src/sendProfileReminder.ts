import * as functions from 'firebase-functions';
const environment = functions.config();
import * as admin from 'firebase-admin';
const db = admin.firestore();
import { sendSendGridEmail } from './utils/sendEmail';
import { EmailParams } from './utils/types';

// SendGrid Credentials
const DASHBOARD_LINK = environment.sendgrid.dashboard_link;
const PROFILE_CREATION_REMINDER = environment.sendgrid.profile_creation_reminder;

/**
 * Firebase Pub/Sub function runs every minute to find all the profile creation reminder
 * notifications that need to be sent to parents (1 & 7 days after they create their Firebase account).
 * Check whether the user has created profile already before sending notifications
 *
 * @param context Firebase Pub/Sub function context
 */
export const sendProfileReminder = functions.pubsub.schedule('every 1 minutes').onRun(async context => {
  try {
    const timestamp = admin.firestore.Timestamp.fromDate(new Date());

    // Fetch all module unlock documents where unlockAt is before the current time
    const sendDocs = (
      await db
        .collection('profileReminder')
        .where('sendAt', '<=', timestamp)
        .get()
    ).docs;

    const userPromises: Promise<FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>>[] = [];
    const userEmailPromises: Promise<admin.auth.UserRecord>[] = [];
    const deletePromises: Promise<FirebaseFirestore.WriteResult>[] = [];

    // Loop through all reminder documents and push into promises to fetch user profile data
    // and for future deletion
    sendDocs.forEach(doc => {
      const uid = doc.data().userId;
      userPromises.push(db.doc(`users/${uid}`).get());
      userEmailPromises.push(admin.auth().getUser(uid));
      deletePromises.push(db.doc(`profileReminder/${doc.id}`).delete());
    });

    const userResults = await Promise.all(userPromises);
    const userEmails = await Promise.all(userEmailPromises);

    const sendGridPromises = [];

    // Loop through the users results and check whether the user profile document exists or not
    for (let i = 0; i < userResults.length; i++) {
      if (!userResults[i].exists) {
        const email = userEmails[i].email;

        // If the user has not created a profile yet, trigger the reminder notification
        if (email) {
          // Trigger the profile creation reminder notification email
          // PROFILE_CREATION_REMINDER - dashboardLink
          sendGridPromises.push(
            sendSendGridEmail(email, PROFILE_CREATION_REMINDER, {
              dashboardLink: DASHBOARD_LINK,
            } as EmailParams),
          );
        }
      }
    }

    await Promise.allSettled(sendGridPromises);
    await Promise.allSettled(deletePromises);
  } catch (error) {
    functions.logger.error('sendProfileReminder Error: ', error);
  }
});
