import * as functions from 'firebase-functions';
const environment = functions.config();
import * as admin from 'firebase-admin';
const db = admin.firestore();
import { Twilio } from 'twilio';
import { sendSendGridEmail } from './utils/sendEmail';
import { formatPhone } from './utils/utils';
import { EmailParams, EmailRefs } from './utils/types';

// SendGrid Credentials
const MODULE_UNLOCK = environment.sendgrid.weekly_module_access;
const DASHBOARD_LINK = environment.sendgrid.dashboard_link;

// Twilio Credentials
const TWILIO_ACCOUNT_SID = environment.twilio.account_sid;
const TWILIO_AUTH_TOKEN = environment.twilio.auth_token;
const TWILIO_NUMBER = environment.twilio.twilio_number;
const TWILIO_STATUS_CALLBACK = environment.twilio.status_callback;
const client = new Twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

/**
 * Firebase Pub/Sub function runs every 10 minutes to find all the module unlock notifications
 * that need to be sent to parents (at a 7-day interval). Check whether the module has been
 * manually unlocked to decide whether system notification is required
 *
 * @param context Firebase Pub/Sub function context
 */
export const moduleUnlock = functions.pubsub.schedule('every 1 minutes').onRun(async context => {
  try {
    const timestamp = admin.firestore.Timestamp.fromDate(new Date());

    // Fetch all module unlock documents where unlockAt is before the current time
    const unlockDocs = (
      await db
        .collection('moduleUnlock')
        .where('unlockAt', '<=', timestamp)
        .get()
    ).docs;

    const userPromises: Promise<FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>>[] = [];
    const topicPromises: Promise<FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>>[] = [];
    const deletePromises: Promise<FirebaseFirestore.WriteResult>[] = [];
    const emailRefs: EmailRefs[] = [];

    // Loop through all unlock documents and push into promises to fetch module content
    // and for future deletion
    unlockDocs.forEach(doc => {
      const uid = doc.data().userId;
      const topic = doc.data().module;
      const { email, firstName, phone } = doc.data();

      userPromises.push(db.doc(`users/${uid}`).get());
      topicPromises.push(db.doc(`users/${uid}/topics/${topic}`).get());
      emailRefs.push({ uid, email, firstName, phone });
      deletePromises.push(db.doc(`moduleUnlock/${doc.id}`).delete());
    });

    const userResults = await Promise.all(userPromises);
    const topicResults = await Promise.all(topicPromises);

    // Get all module titles for mapping
    const metaData = (await db.doc(`config/meta`).get()).data();
    const moduleTopics = metaData?.moduleTopics;

    const sendGridPromises = [];
    const twilioPromises = [];
    const unlockPromises = [];

    // Loop through the module results and check whether the module is still locked or not
    for (let i = 0; i < topicResults.length; i++) {
      const topic = topicResults[i];
      const locked = topic.data()?.locked;

      // If the current module is still locked, set the unlock notification & unlock the module
      if (locked) {
        let email = emailRefs[i].email;
        let phone = emailRefs[i].phone;
        const smsNotification = userResults[i].data()?.smsNotification;
        const optOutAt = userResults[i].data()?.optOutAt;
        const topicId = topic.id;
        const moduleName = moduleTopics[topicId];

        if (!optOutAt) {
          // Trigger the module unlock email
          // MODULE_UNLOCK - firstName, moduleName, dashboardLink
          sendGridPromises.push(
            sendSendGridEmail(email, MODULE_UNLOCK, {
              firstName: emailRefs[i].firstName,
              moduleName: moduleName,
              dashboardLink: DASHBOARD_LINK,
            } as EmailParams),
          );

          // Trigger the SMS notification to parents with a status callback
          if (metaData && smsNotification) {
            let smsTemplate = metaData.smsTemplates['weekly_module_access'];
            smsTemplate = smsTemplate.replace('{{Name}}', emailRefs[i].firstName);
            smsTemplate = smsTemplate.replace('{{moduleTitle}}', moduleName);
            twilioPromises.push(
              client.messages.create({
                body: smsTemplate,
                from: TWILIO_NUMBER,
                statusCallback: TWILIO_STATUS_CALLBACK,
                to: formatPhone(phone),
              }),
            );
          }
        }

        unlockPromises.push(db.doc(`users/${emailRefs[i].uid}/topics/${topicId}`).update({ locked: false }));
      }
    }

    await Promise.allSettled(sendGridPromises);
    await Promise.allSettled(twilioPromises);
    await Promise.allSettled(unlockPromises);
    await Promise.allSettled(deletePromises);
  } catch (error) {
    functions.logger.error('moduleUnlock Error: ', error);
  }
});
