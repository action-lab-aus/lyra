import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const db = admin.firestore();
const FieldValue = admin.firestore.FieldValue;
import { MembershipData } from './utils/types';
import { findIndex } from 'lodash';

/**
 * Record membership information for a specific user
 *
 * @param data Membership data including user id, admin email, facebook user name, and group id
 * @param context Firebase HTTPS callable function context
 *
 * @returns Response data with status - status code & message - response message
 */
export const recordMembership = functions.https.onCall(async (data: MembershipData, context) => {
  const isAdmin = context.auth?.token.admin;
  const { email, code, name, adminEmail, group } = data;

  if (!email) return { status: 403, message: `Missing the 'email' field for the request...` };
  if (!code) return { status: 403, message: `Missing the 'code' field for the request...` };
  if (!name) return { status: 403, message: `Missing the 'name' field for the request...` };
  if (!adminEmail) return { status: 403, message: `Missing the 'admin' field for the request...` };
  if (!group) return { status: 403, message: `Missing the 'group' field for the request...` };
  if (!isAdmin) return { status: 401, message: `Unauthorized` };

  try {
    const timestamp = admin.firestore.Timestamp.now();

    const user = await admin.auth().getUserByEmail(email);
    const uid = user.uid;

    if (uid.substring(uid.length - 6) !== code) return { status: 201, message: getVerificationResult(2) };

    // Check whether the log exist or not
    const userGroups = (await db.doc(`membership/${uid}`).get()).data();

    // If exists, add a new membership record
    if (userGroups) {
      const currentGroups = userGroups.groups;
      const idx = findIndex(currentGroups, (g: any) => {
        return g.group === group;
      });

      if (idx < 0)
        await db
          .collection('membership')
          .doc(uid)
          .update({
            groups: FieldValue.arrayUnion({
              facebookName: name,
              joinedAt: timestamp,
              admin: adminEmail,
              group: group,
            }),
          });
    }
    // Otherwise, set the membership array with the new record
    else
      await db
        .collection('membership')
        .doc(uid)
        .set({
          groups: [
            {
              facebookName: name,
              joinedAt: timestamp,
              admin: adminEmail,
              group: group,
            },
          ],
        });

    return { status: 200, message: getVerificationResult(1) };
  } catch (error) {
    if (error.code === 'auth/user-not-found') return { status: 201, message: getVerificationResult(0) };
    else {
      functions.logger.error(error);
      return { status: 400, message: `Error: ${error}` };
    }
  }
});

const getVerificationResult = (result: 0 | 1 | 2): string => {
  switch (result) {
    case 0:
      return 'The provided email is not a registered PiP+ email. Please double-check the input or decline the membership request.';
    case 1:
      return 'Verification is successful and the PSG joining information has been recorded in the system. Please approve the membership request in the Facebook Group.';
    case 2:
      return 'The provided verification token does not match the registered PiP+ email account. Please double-check the input or decline the membership request.';
  }
};
