import * as functions from 'firebase-functions';
const environment = functions.config();
import * as admin from 'firebase-admin';
const db = admin.firestore();
import * as _ from 'lodash';
import axios from 'axios';
const PAGE_ID = environment.facebook.page_id;
const PAGE_TOKEN = environment.facebook.page_token;
import { Group } from './utils/types';

/**
 * Firebase HTTPS function to add new Facebook Groups and update group details to Firestore
 * when identified from Facebook Graph API (Convert to a Pub/Sub Function for Live Deployment)
 *
 * @param request Firebase function HTTPS request data
 * @param response Firebase function HTTPS response data
 */

export const addNewGroups = functions.pubsub.schedule('every 12 hours').onRun(async context => {
  try {
    const allGroupsRes = await axios.get(
      `https://graph.facebook.com/v11.0/${PAGE_ID}/groups?fields=id,created_time,email,icon,member_count,member_request_count,name,privacy,updated_time&access_token=${PAGE_TOKEN}`,
    );
    const groups = allGroupsRes.data.data as Group[];

    const currentGroupKeys = (await db.collection('groups').get()).docs;
    const currentIds = currentGroupKeys.map(item => item.id);

    const promises: any = [];
    groups.forEach(group => {
      // If it is a new group, set the new group under the `groups` collection
      if (!currentIds.includes(group.id))
        promises.push(
          db
            .collection('groups')
            .doc(group.id)
            .set(group),
        );
      // Otherwise, update the group details
      else
        promises.push(
          db
            .collection('groups')
            .doc(group.id)
            .update({
              icon: group.icon,
              member_count: group.member_count,
              member_request_count: group.member_request_count,
              name: group.name,
              privacy: group.privacy,
              updated_time: group.updated_time,
            }),
        );
    });

    await Promise.all(promises);
  } catch (error) {
    functions.logger.error('addNewGroups Error: ', error);
  }
});
