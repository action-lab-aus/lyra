# PiP+ Firebase Functions

This repository contains the Firebase Cloud Functions which act as the serverless backend for PiP+ infrastructure. These functions utilise external APIs (e.g. SendGrid), storing information in [Firestore](https://firebase.google.com/docs/firestore), and trigger notifications and cron jobs when needed. The implementation of functions is stored under the `/functions` directory.

# Building, Testing, and Deploying

## Prerequisites

- [Firebase Project](https://console.firebase.google.com/u/0/) with **Blaze Plan (Pay as you go)**
- [SendGrid API](https://app.sendgrid.com/)

## 1. Create a Firebase Project

Create a Firebase Project on the [Cloud Console](https://console.firebase.google.com/u/0/) with Firestore, Storage, Authentication enabled. You need to upgrade the project to **Blaze Plan (Pay as you go)** to enable
Firebase Functions.

## 2. Install Firebase CLI

Running and deploying Firebase services requires the installation of the [**Firebase CLI**](https://firebase.google.com/docs/cli).

Run the following npm command to install the CLI or update to the latest CLI version.

```
npm install -g firebase-tools
```

If using Windows, Git Bash doesn't work - Windows Powershell seems to work best. If you're using Windows Powershell, you will probably have to run the following command each session you want to use the Firebase CLI:

```
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass
```

Run the following command to sign in to Google.

```
firebase login
```

## 3. Set up a SendGrid Account & Domain Authentication

Please go to [SendGrid Console](https://app.sendgrid.com/) to create an account for this project. Follow the instruction to [Set up Domain Authentication](https://docs.sendgrid.com/ui/account-and-settings/how-to-set-up-domain-authentication) to allow SendGrid to send emails on your behalf.

## 4. Create SendGrid Dynamic Emailing Templates

PiP+ involves various forms of email notifications. To streamline the process of sending automatic emails, please follow the instructions here to [Create Dynamic Emailing Templates](https://docs.sendgrid.com/ui/sending-email/how-to-send-an-email-with-dynamic-transactional-templates) on the SendGrid console and record the Dynamic Template ID.

### 🔔🔔🔔 List of Dynamic Emailing Templates 🔔🔔🔔

- Password Reset Email
- Survey Completion Email (to RA)
- Intervention Group Access Email
- Module Unlock Email
- Module Completion Email
- Factsheet Unlock Email
- Final Factsheet Email
- Factsheet Completion Email

## 5. Configure Environment Variables for Firebase Functions

Run `firebase use --add` to define the project alias to be used for the Firebase Function (set the alias to be **default**). Find and choose the Firebase Project you have created in Step 1.

Create a `.runtimeconfig.json` file under the root directory of the function repository. Copy the following JSON object into the file and update it with the credentials you received from the previous steps.

```JSON
{
  "sendgrid": {
    "password_rest": "DYNAMIC_TEMPLATE_ID",
    "survey_completion": "DYNAMIC_TEMPLATE_ID", // Done
    "group_access": "DYNAMIC_TEMPLATE_ID", // Done
    "module_access": "DYNAMIC_TEMPLATE_ID",
    "module_completion": "DYNAMIC_TEMPLATE_ID",
    "factsheet_access": "DYNAMIC_TEMPLATE_ID",
    "factsheet_final": "DYNAMIC_TEMPLATE_ID",
    "factsheet_completion": "DYNAMIC_TEMPLATE_ID",
    "api_key": "SENDGRID_API_KEY",
    "sender": "SENDER_EMAIL",
    "trail_email": "PIP+_TRAIL_EMAIL"
  },
  "facebook": {
    "page_id": "FACEBOOK_PAGE_ID",
    "page_token": "FACEBOOK_PAGE_ACCESS_TOKEN"
  }
}
```

Set environment configuration for your project by using the `firebase functions:config:set` command in the Firebase CLI. More details can be found on [Environment Configuration](https://firebase.google.com/docs/functions/config-env).

```
firebase functions:config:set sendgrid.password_rest="DYNAMIC_TEMPLATE_ID"
```

## 6. Run Firebase Functions Locally

Once the .runtimeconfig.json file is prepared under the `/functions` folder, Functions can be tested locally using the command:

```
firebase emulators:start --only functions
```

## 7. Deploy Firebase Functions & Rules

To deploy the Firebase Function:

```
(All Changes) firebase deploy
(Only Functions and Config Changes) firebase deploy  --only functions
(Config Changes and Specific Functions) firebase deploy --only functions:function1,functions:function2
```
