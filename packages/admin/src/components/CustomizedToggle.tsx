import * as React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import Typography from '@material-ui/core/Typography';
import { colors } from '../styles/variables';

const useStyles = makeStyles(() =>
  createStyles({
    toggleButton: {
      color: 'white',
      backgroundColor: colors.brand,
      '&:hover': {
        backgroundColor: '#0a9396',
      },
      '&.Mui-selected:hover': {
        backgroundColor: '#0a9396',
      },
      '&.Mui-selected': {
        color: 'white',
        backgroundColor: '#38a3a5',
      },
    },
    toggleText: {
      fontSize: '11px',
    },
  }),
);

export interface CustomizedToggleProps {
  toggleLabel: string[];
  toggleValue: string;
  handleToggleChange: (toggle: string) => void;
}

function CustomizedToggle(props: CustomizedToggleProps) {
  const classes = useStyles();

  return (
    <ToggleButtonGroup
      size="small"
      value={props.toggleValue}
      onChange={(_event, value) => props.handleToggleChange(value)}
      exclusive
      aria-label="user-status-toggle">
      {props.toggleLabel.map(item => (
        <ToggleButton className={classes.toggleButton} value={item} key={item} aria-label="bold">
          <Typography className={classes.toggleText}>{item}</Typography>
        </ToggleButton>
      ))}
    </ToggleButtonGroup>
  );
}

export default CustomizedToggle;
