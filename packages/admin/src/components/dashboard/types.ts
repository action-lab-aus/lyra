export interface GroupData {
  id: string;
  memberCount: number;
  memberRequestCount: number;
  name: string;
  privacy: 'OPEN' | 'CLOSED';
  noOfPosts: number;
  noOfComments: number;
  icon: string;
  createdAt: string;
}

export interface GroupCardProps {
  cardData: GroupData;
}

export interface ActiveUserPieProps {
  data: number[];
  title: string;
  subtitle: string;
  highlight: number;
  labels: string[];
  loading: boolean;
}

export interface TopicBarProps {
  data: any[];
  title: string;
  subtitle?: string;
  tooltip: string;
  labels: string[];
  tagChange?: number;
  loading: boolean;
  horizontal: boolean;
}

export interface TopicMap {
  [index: string]: number;
}

export interface TagData {
  data: number[];
}
