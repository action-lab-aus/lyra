export interface TagCardProps {
  id: string;
  type: 'post' | 'comment';
  msg: string;
  saveName: (id: string, name: string) => void;
}
