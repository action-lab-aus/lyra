export function getPostLink(groupId: string, postId: string) {
  return `https://www.facebook.com/groups/${groupId}/posts/${postId}`;
}
