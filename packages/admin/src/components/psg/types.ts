export interface PostData {
  id: string;
  message: string;
  userName: string;
  updatedAt: string;
  noOfComments: number;
  noOfReactions: number;
  topics: string[];
  comments: CommentData[];
  reactions: ReactionData[];
  link: string;
}

export interface CommentData {
  id: string;
  message: string;
  userName: string;
  updatedAt: string;
  noOfComments: number;
  noOfReactions: number;
  topics: string[];
  link: string;
}

export interface ReactionData {
  userName: string;
  type: string;
}

export interface HeadCell {
  disablePadding: boolean;
  id: keyof PostData;
  label: string;
  numeric: boolean;
}

export interface CommentHeadCell {
  disablePadding: boolean;
  id: keyof CommentData;
  label: string;
  numeric: boolean;
}

export interface EnhancedTableToolbarProps {
  download: () => void;
}

export type Order = 'asc' | 'desc';

export interface PSGProps {
  location: any;
}

export interface ChipStatus {
  value: string;
  added: boolean;
}

export interface AddTagDialogProps {
  open: boolean;
  row: PostData | CommentData | null;
  topics: string[];
  selected: string[];
  handleTagClose: () => void;
  updateChipStatus: (item: ChipStatus) => void;
  addNewTopic: (str: string) => void;
}

export interface ChipsArrayProps {
  data: ChipStatus[] | null;
  added: boolean;
  updateChipStatus: (item: ChipStatus) => void;
}

export interface StaticChipsArrayProps {
  data: string[];
}

export interface CommentCellProps {
  open: string[];
  groupId: string | null;
  row: PostData;
  comments: CommentData[] | undefined;
  onClickCommentTag: (commentRow: CommentData) => void;
  onClickComment: (row: CommentData) => void;
  getSelectedTopics: (id: string) => string[];
  getFbUserName: (id: string) => string;
}

export interface AddCommentDialogProps {
  open: boolean;
  row: PostData | CommentData | null;
  topics: string[];
  supports: any;
  supportRecords: CheckBoxes;
  loading: boolean;
  handleCommentClose: () => void;
  handleCommentSend: (str: string) => void;
  updateSupport: (checkboxStatus: CheckBoxes) => void;
}

export interface CheckBoxes {
  [index: string]: boolean;
}

export interface SupportCheckBoxProps {
  supports: string[];
  checkbox: CheckBoxes;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export interface SupportGuidesProps {
  type: 'informational' | 'emotional' | 'esteem' | 'network';
  content: string;
}
