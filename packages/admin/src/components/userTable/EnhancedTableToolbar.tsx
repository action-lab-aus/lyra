import * as React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { EnhancedTableToolbarProps } from './types';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import PageviewIcon from '@material-ui/icons/Pageview';
import GetAppIcon from '@material-ui/icons/GetApp';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    title: {
      flex: '1 1 100%',
      fontWeight: 'bold',
      fontSize: '18px',
    },
    button: {
      color: '#fff',
      width: '320px',
      fontSize: '12px',
      marginRight: theme.spacing(2),
    },
    label: {
      '& input::placeholder': {
        fontSize: '14px',
      },
    },
  }),
);

function EnhancedTableToolbar(props: EnhancedTableToolbarProps) {
  const classes = useToolbarStyles();

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.updateSearchStr(event.target.value);
  };

  const onClick = () => {
    props.download();
  };

  return (
    <Toolbar className={classes.root}>
      {
        <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
          PiP+ User Data
        </Typography>
      }
      {props.lastSeen && (
        <Button
          variant="contained"
          color="primary"
          size="small"
          onClick={props.downloadOther}
          className={classes.button}
          endIcon={<PageviewIcon />}>
          {`${props.lastSeen.signedUpUsers ? props.lastSeen.signedUpUsers.length : 0} Users without Profile`}
        </Button>
      )}
      {
        <TextField
          id="search-input"
          onChange={onChange}
          placeholder="Search"
          classes={{ root: classes.label }}
          InputProps={{
            style: { fontSize: '14px' },
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />
      }
      {
        <Tooltip title="Export">
          <IconButton color="primary" onClick={onClick}>
            <GetAppIcon />
          </IconButton>
        </Tooltip>
      }
      {
        <Tooltip title="Verify Membership">
          <IconButton color="primary" onClick={props.onClickVerify}>
            <VerifiedUserIcon />
          </IconButton>
        </Tooltip>
      }
    </Toolbar>
  );
}

export default EnhancedTableToolbar;
