import * as React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { OverrideStatusCellProps } from '../types';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles(() =>
  createStyles({
    chip: {
      color: 'white',
      fontSize: '12px',
      backgroundColor: '#d00000',
    },
  }),
);

function OverrideStatusCell(props: OverrideStatusCellProps) {
  const classes = useStyles();

  return props.override ? <Chip className={classes.chip} size="small" color="primary" label="ARCHIVED" /> : <div />;
}

export default OverrideStatusCell;
