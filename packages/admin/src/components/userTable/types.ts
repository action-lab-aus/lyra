import * as admin from 'firebase-admin';

export interface Data {
  id: string;
  email: string;
  flagged: boolean;
  phone: string;
  name: string;
  userType: string;
  pradasComplete: number;
  followupSurveyCompletion: number;
  moduleCompletion: ModuleCompletion[];
  status: string;
  emailFlag: string;
  smsFlag: string;
  smsNotification: boolean;
  createdAt: string;
  surveyCompletedAt: string;
  daysOfReg: number;
  profile: UserProfile;
}

export type Gender = 'male' | 'female' | 'non-binary';

export type UserProfileStage = 'survey' | 'topic' | 'followup';

export type UserProfile = {
  email: string;
  flagged?: boolean;
  userCategory: 'parent' | 'professional' | 'ineligible-under-12' | 'ineligible-over-17';
  userFirstname: string;
  userSurname: string;
  userAge: string;
  userGender: Gender;
  userPostcode: string;
  userPhone: string;
  userPhoneAlt?: string;
  userEducation?: string;
  userContactEmail?: string;
  familyEthnicity?: string;
  familyRelationship?: string;
  familyIncome?: string;
  familyCovidImpact?: string;
  childName: string;
  childDob: string;
  childGrade: number;
  childGender: Gender;
  childRelationship: string;
  childOtherParentsJoined: string;
  discoverChannels?: string;
  discoverReason?: string;
  discoverFutureResearch?: string;
  currentStage?: UserProfileStage;
  topicSuggestion?: string[];
  optOutAt?: admin.firestore.Timestamp;
  surveyCompletedAt?: admin.firestore.Timestamp;
  smsNotification: boolean;
  emailFlag?: string;
  smsFlag?: string;
  status: string;
  surveyCompletion: number;
  followupSurveyCompletion?: number;
  moduleCompletion: ModuleCompletion[];
  followupCompletedAt?: admin.firestore.Timestamp;
  createdAt: admin.firestore.Timestamp;
  isFacebookUser?: boolean;
};

export interface ActiveUserData {
  uid: string;
  lastSeen: string;
  diff?: number;
}

export interface ModuleCompletion {
  id?: string;
  value: number;
  label: string;
  completedAt?: any;
}

export interface MembershipData {
  admin: string;
  facebookName: string;
  group: string;
  joinedAt: any;
}

export interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
  numeric: boolean;
}

export interface EnhancedTableToolbarProps {
  lastSeen: any;
  updateSearchStr: (str: string) => void;
  download: () => void;
  downloadOther: () => void;
  onClickVerify: () => void;
}

interface EmailTemplates {
  label: string;
  content: string;
}

export interface EmailFollowupDialogProps {
  open: boolean;
  row: Data | null;
  emailLoading: boolean;
  templates: EmailTemplates[];
  handleEmailClose: () => void;
  handleEmailSend: (str: string) => void;
}

export interface SMSDialogProps {
  open: boolean;
  row: Data | null;
  smsLoading: boolean;
  messages: SMSMessages[] | undefined;
  needResolve: number;
  handleSmsClose: () => void;
  handleSmsSend: (str: string) => void;
  handleSmsResolved: () => void;
}

export interface SMSMessages {
  createdAt: any;
  from: 'parent' | 'admin';
  message: string;
  replied: boolean | null;
}

export interface FollowupMessages {
  type: 'Call' | 'Email' | 'SMS' | 'Manual';
  startTime: any;
  endTime?: any;
  admin: string;
  notes: string;
  status: 'done' | 'running' | 'ended' | 'sent';
}

export interface UserLogsCellProps {
  open: string[];
  row: Data;
  logs: FollowupMessages[] | undefined;
  loading: boolean;
  manualLog: boolean;
  handleNoteSubmit: (str: string, uid: string, startTime: any) => void;
  deleteManualLog: (uid: string, logs: FollowupMessages[] | undefined) => void;
  saveManualLog: (uid: string, content: string) => void;
}

export interface FollowupNotesMap {
  startTime: number;
  note: string;
}

export interface ModuleProgressCellProps {
  open: string[];
  row: Data;
}

export interface UserProfileCellProps {
  open: string[];
  row: Data;
}

export interface ModuleBarProps {
  data: object[];
  labels: string[];
  tagChange: number;
}

export interface UserStatusChipProps {
  status: string;
}

export interface OverrideStatusCellProps {
  override: boolean;
}

export interface UserDialogProps {
  open: boolean;
  row: Data | null;
  followupLoading: boolean;
  emailLoading: boolean;
  phoneLoading: boolean;
  userStatus: 'opt-out' | 'stop followup' | 'contact completed' | '';
  handleFollowupClose: () => void;
  optOutUser: (optOutNote: string) => void;
  addStopFollowupUser: (followupNote: string) => void;
  addContactCompletedUser: (contactNote: string) => void;
  updateUserDetail: (type: string, value: string) => void;
  updateSMSNotification: (value: boolean) => void;
}

export interface ResponseData {
  status: number;
  message: string;
}

export interface VerifyDialogProps {
  open: boolean;
  verifyLoading: boolean;
  handleVerifyClose: () => void;
  recordPSGMembership: (email: string, code: string, name: string, group: string) => Promise<string>;
  verifyPSGToken: (email: string, token: string) => Promise<0 | 1 | 2>;
}

export interface VerificationStepperProps {
  verifyLoading: boolean;
  recordPSGMembership: (email: string, code: string, name: string, group: string) => Promise<string>;
  verifyPSGToken: (email: string, token: string) => Promise<0 | 1 | 2>;
}

export interface LastSeenToggleProps {
  lastSeenToggle: string;
  handleLastSeenChange: (lastSeen: string) => void;
}

export interface LastFollowupToggleProps {
  lastFollowupToggle: string;
  handleLastFollowupChange: (lastFollowup: string) => void;
}

export interface CustomSwitchProps {
  toggle: boolean;
  label: string;
  handleToggle: () => void;
}

export type Order = 'asc' | 'desc';
