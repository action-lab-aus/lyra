import * as React from 'react';
import { StaticChipsArrayProps } from './psg/types';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      flexWrap: 'wrap',
      listStyle: 'none',
      padding: theme.spacing(0.5),
      margin: 0,
    },
    chip: {
      margin: theme.spacing(0.5),
      fontSize: '12px',
      color: 'white',
    },
  }),
);

function StaticChipsArray(props: StaticChipsArrayProps) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {props.data &&
        props.data.map(item => {
          return (
            <li key={item}>
              <Chip size="small" color="primary" label={item} className={classes.chip} />
            </li>
          );
        })}
    </div>
  );
}

export default StaticChipsArray;
