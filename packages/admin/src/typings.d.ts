interface CSSModule {
  [className: string]: string;
}

declare module '*.module.scss' {
  const cssModule: CSSModule;
  export = cssModule;
}

declare module '*.module.css' {
  const cssModule: CSSModule;
  export = cssModule;
}

declare module '*.png' {
  const value: any;
  export default value;
}

declare module 'typography-theme-fairy-gates';
