import Typography from 'typography';
import fairyGateTheme from 'typography-theme-fairy-gates';

fairyGateTheme.overrideThemeStyles = () => ({
  a: {
    color: '#0461BE',
  },
  img: {
    marginBottom: '0',
  },
  p: {
    margin: '0',
  },
});

const typography = new Typography(fairyGateTheme);
export const { scale, rhythm, options } = typography;
export default typography;
