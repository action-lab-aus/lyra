import * as admin from 'firebase-admin';

/**
 * Convert a Unix timstamp into a formatted date-time string
 * @param timestamp Input Unix timestamp
 * @returns A formatted date-time string as dd/mm/yyyy hh:mm:ss
 */
export const formattedDateTime = (timestamp: number): string => {
  const date = new Date(timestamp);
  return (
    date.getDate() +
    '/' +
    (date.getMonth() + 1) +
    '/' +
    date.getFullYear() +
    ' ' +
    date
      .getHours()
      .toString()
      .padStart(2, '0') +
    ':' +
    date
      .getMinutes()
      .toString()
      .padStart(2, '0') +
    ':' +
    date
      .getSeconds()
      .toString()
      .padStart(2, '0')
  );
};

/**
 * Convert a Facebook timestamp into a formatted date-time string
 * @param timestamp Input timestamp
 * @returns A formatted date-time string as dd/mm/yyyy hh:mm:ss
 */
export const formatFacebookTimestamp = (timestamp: string): string => {
  const tmp = timestamp.split('T');
  const date = tmp[0].split('-');
  const time = tmp[1].split('+')[0];
  return date[2] + '/' + date[1] + '/' + date[0] + ' ' + time;
};

/**
 * Format Firestore timestamp into a date string
 * @param timestamp Input timestamp
 * @returns A formatted date-time string as yyyy-mm-ddThh:mm:ss
 */
export const formatDate = (timestamp: admin.firestore.Timestamp): string => {
  const tzoffset = new Date().getTimezoneOffset() * 60000;
  const date = new Date(timestamp.seconds * 1000 - tzoffset).toISOString().slice(0, -1);
  return date.split('.')[0];
};
