import {
  Data,
  ModuleCompletion,
  FollowupMessages,
  MembershipData,
  ActiveUserData,
} from '../components/userTable/types';
import { CommentData, PostData } from '../components/psg/types';
import { findIndex } from 'lodash';
import xlsx from 'xlsx';

// User Data
interface UserData {
  id: string;
  isFacebookUser?: boolean;
  email: string;
  phone: string;
  name: string;
  userType: string;
  pradasComplete: number;
  lastSeen: string | null;
  status: string;
  createdAt: string;
}

// Module Data
interface ModuleData {
  id: string;
  label: string;
  value: number;
  completedAt: string;
}

// Followup Logs Data
interface LogData {
  id: string;
  type: string;
  startTime: string;
  endTime: string;
  admin: string;
  notes: string;
  status: string;
}

// Membership Data
interface MemberData {
  id: string;
  admin: string;
  facebookName: string;
  group: string;
  joinedAt: string;
  groupName: string;
}

// Post Data
interface PostDataExport {
  id: string;
  message: string;
  userName: string;
  createdAt: string;
  noOfComments: number;
  noOfReactions: number;
  topics: string;
}

// Comment Data
interface CommentDataExport {
  id: string;
  message: string;
  postId: string;
  userName: string;
  createdAt: string;
  topics: string;
}

/**
 * Export user-related data into a CSV file and trigger downloading automatically
 *
 * @param rows User-related data
 * @param followupLogs Followup logs data
 * @param lastSeen User last seen data
 * @param membership User PSG membership data
 * @param metaData User related metadata (stop followup information)
 */
export function exportUserCSVFile(
  rows: (UserData | Data)[],
  followupLogs: any,
  lastSeen: any,
  membership: any,
  metaData: any,
  groups: any,
): void {
  const tmpRows = [...rows];
  const users: UserData[] = [];
  const modules: ModuleData[] = [];
  const logs: LogData[] = [];
  const members: MemberData[] = [];
  const profiles: any[] = [];

  (tmpRows as Data[]).map((row: Data) => {
    const lastSeenIdx = findIndex(lastSeen.activeUsers, (entry: ActiveUserData) => {
      return entry.uid === row.id;
    });
    const lastSeenData: ActiveUserData = lastSeen.activeUsers[lastSeenIdx];

    // Get the last seen timestamp and active status
    let status = 'inactive';
    let lastSeenTimestamp = null;
    if (lastSeenData) {
      if (lastSeenData.lastSeen) lastSeenTimestamp = JSON.stringify(lastSeenData.lastSeen);

      if (lastSeenData.diff) {
        const diff = lastSeenData.diff;
        if (diff < 7) status = 'active';
        else if (diff >= 7 && diff < 14) status = '> 7 Days';
        else if (diff >= 14 && diff < 21) status = '> 14 Days';
      }
    }

    if (metaData && metaData.stopFollowupUsers) {
      if (metaData.stopFollowupUsers.indexOf(row.id) >= 0) status += ' (Admin Override: Archived)';
    }

    users.push({
      id: row.id,
      isFacebookUser: row.profile.isFacebookUser,
      email: row.email,
      phone: row.phone,
      name: row.name,
      userType: row.userType,
      pradasComplete: row.pradasComplete,
      lastSeen: lastSeenTimestamp,
      status: status,
      createdAt: row.createdAt,
    });

    profiles.push({ id: row.id, ...row.profile });

    if (row.moduleCompletion) {
      row.moduleCompletion.forEach((m: ModuleCompletion) => {
        modules.push({
          id: row.id,
          label: m.label,
          value: m.value,
          completedAt: m.completedAt ? m.completedAt.seconds : '',
        });
      });
    }
  });

  if (followupLogs) {
    Object.keys(followupLogs).forEach(key => {
      followupLogs[key].logs.forEach((log: FollowupMessages) => {
        logs.push({
          id: key,
          type: log.type,
          startTime: log.startTime ? log.startTime.seconds : '',
          endTime: log.endTime ? log.endTime.seconds : '',
          admin: log.admin,
          notes: log.notes,
          status: log.status,
        });
      });
    });
  }

  if (membership && groups) {
    Object.keys(membership).forEach(key => {
      membership[key].groups.forEach((g: MembershipData) => {
        members.push({
          id: key,
          admin: g.admin,
          facebookName: g.facebookName,
          group: g.group,
          joinedAt: g.joinedAt.seconds,
          groupName: groups[g.group].name,
        });
      });
    });
  }

  const exportedFileName = `UserProgramData_${Date.now()}.xlsx`;

  // Create an .xlsx workbook
  const workbook = xlsx.utils.book_new();

  // Append sheets to the workbook
  const userSheet = xlsx.utils.json_to_sheet(users);
  xlsx.utils.book_append_sheet(workbook, userSheet, 'user_data');

  const profileSheet = xlsx.utils.json_to_sheet(profiles);
  xlsx.utils.book_append_sheet(workbook, profileSheet, 'user_profile_data');

  const moduleSheet = xlsx.utils.json_to_sheet(modules);
  xlsx.utils.book_append_sheet(workbook, moduleSheet, 'module_data');

  const logSheet = xlsx.utils.json_to_sheet(logs);
  xlsx.utils.book_append_sheet(workbook, logSheet, 'followup_log_data');

  const memberSheet = xlsx.utils.json_to_sheet(members);
  xlsx.utils.book_append_sheet(workbook, memberSheet, 'membership_data');

  // Client-side download for the workbook
  xlsx.writeFile(workbook, exportedFileName);
}

/**
 * Export all users who have signed up for PiP+ but hasn't created their profile yet
 *
 * @param lastSeen Last seen users
 */
export function exportOtherUserCSVFile(lastSeen: any): void {
  const users = lastSeen.signedUpUsers;
  const exportedFileName = `UsersWithoutProfile_${Date.now()}.xlsx`;
  // Create an .xlsx workbook
  const workbook = xlsx.utils.book_new();

  // Append sheets to the workbook
  const userSheet = xlsx.utils.json_to_sheet(users);
  xlsx.utils.book_append_sheet(workbook, userSheet, 'users_without_profile');

  // Client-side download for the workbook
  xlsx.writeFile(workbook, exportedFileName);
}

/**
 * Export group-related data into a CSV file and trigger downloading automatically
 *
 * @param rows Post-related data
 * @param groupId Facebook group ID
 * @param topicRecords Topics data for posts and comments
 * @param fbNames Facebook Name Mapping
 */
export function exportGroupCSVFile(
  rows: (PostDataExport | PostData)[],
  groupId: string | null,
  topicRecords: any,
  fbNames: any,
): void {
  const tmpRows = [...rows];
  const posts: PostDataExport[] = [];
  const comments: CommentDataExport[] = [];

  (tmpRows as PostData[]).map((row: PostData) => {
    posts.push({
      id: row.id,
      message: JSON.stringify(row.message),
      userName: row.userName || (fbNames[row.id] && fbNames[row.id].userName),
      createdAt: row.updatedAt,
      noOfComments: row.noOfComments,
      noOfReactions: row.noOfReactions,
      topics:
        topicRecords && Object.keys(topicRecords).indexOf(row.id) >= 0
          ? JSON.stringify(topicRecords[row.id].topics.join(','))
          : '',
    });

    if (row.comments) {
      row.comments.forEach((c: CommentData) => {
        comments.push({
          id: c.id,
          message: JSON.stringify(c.message),
          postId: row.id,
          userName: c.userName || (fbNames[row.id] && fbNames[row.id].userName),
          createdAt: c.updatedAt,
          topics:
            topicRecords && Object.keys(topicRecords).indexOf(c.id) >= 0
              ? JSON.stringify(topicRecords[c.id].topics.join(','))
              : '',
        });
      });
    }
  });

  const exportedFileName = `GroupData_${groupId}_${Date.now()}.xlsx`;

  // Create an .xlsx workbook
  const workbook = xlsx.utils.book_new();

  // Append sheets to the workbook
  const postSheet = xlsx.utils.json_to_sheet(posts);
  xlsx.utils.book_append_sheet(workbook, postSheet, 'post_data');

  const commentSheet = xlsx.utils.json_to_sheet(comments);
  xlsx.utils.book_append_sheet(workbook, commentSheet, 'comment_data');

  // Client-side download for the workbook
  xlsx.writeFile(workbook, exportedFileName);
}
