import * as React from 'react';
import Page from '../components/Page';
import Container from '../components/Container';
import IndexLayout from '../layouts';
import { PrivateRoute } from '../components/private';
import { Tag } from '../components/tag';

const TagPage = () => (
  <IndexLayout>
    <PrivateRoute>
      <Page>
        <Container>
          <Tag />
        </Container>
      </Page>
    </PrivateRoute>
  </IndexLayout>
);

export default TagPage;
