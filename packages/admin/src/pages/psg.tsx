import * as React from 'react';
import Page from '../components/Page';
import Container from '../components/Container';
import IndexLayout from '../layouts';
import { PrivateRoute } from '../components/private';
import { PSG } from '../components/psg';

const PSGDashboard = ({ location }: any) => (
  <IndexLayout>
    <PrivateRoute>
      <Page>
        <Container>
          <PSG location={location} />
        </Container>
      </Page>
    </PrivateRoute>
  </IndexLayout>
);

export default PSGDashboard;
