exports.onCreateWebpackConfig = ({ stage, loaders, actions, getConfig }) => {
  if (stage === 'build-html') {
    actions.setWebpackConfig({
      externals: getConfig().externals.concat(function(context, request, callback) {
        const regex = /^@?firebase(\/(.+))?/;
        // Exclude firebase products from being bundled, so they will be loaded using require() at runtime.
        if (regex.test(request)) {
          return callback(null, 'commonjs ' + request);
        }
        callback();
      }),
      module: {
        rules: [
          {
            test: /react-apexcharts/,
            use: loaders.null(),
          },
          {
            test: /apexcharts/,
            use: loaders.null(),
          },
          {
            test: /twilio-client/,
            use: loaders.null(),
          },
        ],
      },
    });
  }
};
