const activeEnv = process.env.GATSBY_ACTIVE_ENV || process.env.NODE_ENV || 'development';

console.log(`Using environment config: '${activeEnv}'`);
require('dotenv').config({
  path: `.env.${activeEnv}`,
});

module.exports = {
  siteMetadata: {
    title: 'PiP+ Admin',
    description: 'PiP+ Admin Dashbord',
    keywords: 'PiP+, Parenting, Gatsby, React',
    siteUrl: 'https://actionlab.dev',
    author: {
      name: 'Action Lab',
      url: 'https://actionlab.dev',
      email: 'peter.chen@monash.edu',
    },
  },
  plugins: [
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    'gatsby-plugin-emotion',
    'gatsby-plugin-typescript',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    'gatsby-plugin-offline',
    'gatsby-plugin-material-ui',
    'gatsby-plugin-react-helmet',
  ],
};
