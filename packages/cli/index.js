#! /usr/bin/env node
const { program } = require('commander');
const { initializeApp, applicationDefault } = require('firebase-admin/app');
const { getFirestore } = require('firebase-admin/firestore');
const { getAuth } = require('firebase-admin/auth');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const range = require('lodash/range');

const modules = {
  m1: 'Connect',
  m2: 'Parenting through the pandemic',
  m3: 'Raising good kids into great adults',
  m4: 'Nurture roots & inspire wings',
  m5: 'Calm versus conflict',
  m6: 'Good friends = supportive relationships',
  m7: 'Good health habits for good mental health',
  m8: 'Partners in problem solving',
  m9: 'From surviving to thriving',
  m10: "When things aren't okay",
};

async function* batchQuery(queryMaker, batchSize = 20) {
  let cursor = null;
  let batch = 0;
  do {
    let usersRef = queryMaker();
    if (cursor !== null) {
      usersRef = usersRef.startAfter(cursor);
    }
    const snapshot = await usersRef.limit(batchSize).get();
    cursor = snapshot.docs.length === batchSize ? snapshot.docs[batchSize - 1] : null;
    let start = batch * batchSize;
    console.log(`>> batch #${batch}: (${start} - ${start + snapshot.docs.length})`);
    yield snapshot;
    batch++;
  } while (cursor !== null);
}

const app = initializeApp({ credential: applicationDefault() });

/**
 * Export user id who completed both baseline and followup surveys to a CSV file
 *
 * @param {output file} csvFile
 */
async function exportUsersCompletedBothK6(csvFile) {
  const firestore = getFirestore(app);
  const csvWriter = createCsvWriter({
    path: csvFile,
    header: [
      { id: 'uid', title: 'UID' },
      { id: 'surveyId', title: 'Baseline survey id' },
      { id: 's3k6Progress', title: 'Baseline K6 Progress' },
      { id: 'followupId', title: 'Followup survey id' },
      { id: 'f3k6Progress', title: 'Followup K6 Progress' },
    ],
  });
  const userBatchs = batchQuery(() => {
    return firestore.collection(`users`).where('userCategory', '==', 'parent');
  }, 64);

  for await (let users of userBatchs) {
    const records = await Promise.all(
      users.docs.map(async doc => {
        const uid = doc.id;
        const baselineSnapshot = await firestore.doc(`/users/${uid}/surveys/s3-k6`).get();
        const followupSnapshot = await firestore.doc(`/users/${uid}/surveys/f3-k6`).get();
        if (!baselineSnapshot.exists) {
          return null;
        }
        if (!followupSnapshot.exists) {
          return null;
        }

        const { _completed: s_completed, _step: s_step, _total: s_total } = baselineSnapshot.data();

        if (!s_total) {
          return null;
        }

        if (s_completed / s_total < 1) {
          return null;
        }

        const { _completed: f_completed, _step: f_step, _total: f_total } = followupSnapshot.data();

        if (!f_total) {
          return null;
        }

        if (f_completed / f_total < 1) {
          return null;
        }

        return {
          uid,
          surveyId: 's3-k6',
          progress: s_completed / s_total,
          surveyId: 'f3-k6',
          progress: f_completed / f_total,
        };
      }),
    );
    await csvWriter.writeRecords(records.filter(record => record !== null));
  }
}

// const surveyIDs = [
//   's1-confidence',
//   's2-pradas',
//   's3-k6',
//   's4-rcads',
//   'f1-evaluation',
//   'f2-pradas',
//   'f3-k6',
//   'f4-rcads',
// ];

const commonHeaders = [
  { id: 'uid', title: 'UID' },
  { id: 'surveyID', title: 'Survey id' },
  { id: 'progress', title: 'Progress' },
];
const confidenceHeader = [...commonHeaders, ...range(1, 12).map(i => ({ id: `s1#q${i}`, title: `s1#q${i}` }))];
const pradasHeaders = [
  ...commonHeaders,
  ...range(1, 10).map(i => ({ id: `S${i}#PSES`, title: `S${i}#PSES` })),
  ...range(1, 9).map(i => ({ id: `S1#Q${i}`, title: `S1#Q${i}` })),
  ...range(1, 9).map(i => ({ id: `S2#Q${i}`, title: `S2#Q${i}` })),
  ...range(1, 7).map(i => ({ id: `S3#Q${i}`, title: `S3#Q${i}` })),
  ...range(1, 10).map(i => ({ id: `S4#Q${i}`, title: `S4#Q${i}` })),
  ...range(1, 9).map(i => ({ id: `S5#Q${i}`, title: `S5#Q${i}` })),
  ...range(1, 13).map(i => ({ id: `S6#Q${i}`, title: `S6#Q${i}` })),
  ...range(1, 11).map(i => ({ id: `S7#Q${i}`, title: `S7#Q${i}` })),
  ...range(1, 10).map(i => ({ id: `S8#Q${i}`, title: `S8#Q${i}` })),
  ...range(1, 10).map(i => ({ id: `S9#Q${i}`, title: `S9#Q${i}` })),
];

const k6Headers = [
  ...commonHeaders,
  ...range(1, 7).map(i => ({ id: `s1#q${i}`, title: `s1#q${i}` })),
  ...range(1, 7).map(i => ({ id: `s2#q${i}`, title: `s2#q${i}` })),
];

const evaluationHeader = [
  ...commonHeaders,
  ...range(1, 13).map(i => ({ id: `s0#q${i}`, title: `s0#q${i}` })),
  ...range(1, 12).map(i => ({ id: `s1#q${i}`, title: `s1#q${i}` })),
  ...range(0, 7).map(i => ({ id: `s0#q4${nextChar('a', i)}`, title: `s0#q4${nextChar('a', i)}` })),
  { id: 's0q7a', title: 's0q7a' },
  { id: 's0q10a', title: 's0q10a' },
];

function nextChar(c, i) {
  return String.fromCharCode(c.charCodeAt(0) + i);
}

const rcadsHeaders = [...commonHeaders, ...range(1, 26).map(i => ({ id: `s1#q${i}`, title: `s1#q${i}` }))];

const allHeaders = {
  's1-confidence': confidenceHeader,
  's2-pradas': pradasHeaders,
  's3-k6': k6Headers,
  's4-rcads': rcadsHeaders,
  'f1-evaluation': evaluationHeader,
  'f2-pradas': pradasHeaders,
  'f3-k6': k6Headers,
  'f4-rcads': rcadsHeaders,
};

/**
 * Export user survey data to a CSV file
 *
 * @param {survey Id} surveyId
 * @param {output file} csvFile
 */
async function exportUserSurveys(surveyId, csvFile) {
  const surveyIds = Object.keys(allHeaders);
  if (surveyIds.indexOf(surveyId) < 0) {
    console.error(`${surveyId}: Invalid survey Id`);
    return;
  }

  const headers = allHeaders[surveyId];
  const firestore = getFirestore(app);
  const csvWriter = createCsvWriter({
    path: csvFile,
    header: headers,
  });

  const userBatchs = batchQuery(() => {
    return firestore.collection(`users`).where('userCategory', '==', 'parent');
  }, 64);

  for await (let users of userBatchs) {
    const records = await Promise.all(
      users.docs.map(async doc => {
        const uid = doc.id;
        const surveySnapshot = await firestore.doc(`/users/${uid}/surveys/${surveyId}`).get();
        if (!surveySnapshot.exists) {
          return null;
        }

        const { _completed, _step, _total, ...rest } = surveySnapshot.data();

        if (!_total) {
          return null;
        }

        return {
          uid,
          surveyID: surveyId,
          progress: _completed / _total,
          ...rest,
        };
      }),
    );
    await csvWriter.writeRecords(records.filter(record => record !== null));
  }
}

/**
 * Export user profile data to a CSV file
 *
 * @param {output file} csvFile
 */
async function exportUserProfile(csvFile) {
  const firestore = getFirestore(app);
  const csvWriter = createCsvWriter({
    path: csvFile,
    header: [
      { id: 'uid', title: 'UID' },
      { id: 'familyCovidImpact', title: 'familyCovidImpact' },
    ],
  });
  const userBatchs = batchQuery(() => {
    return firestore.collection(`users`).where('userCategory', '==', 'parent');
  }, 64);

  for await (let users of userBatchs) {
    const allRecords = await Promise.all(
      users.docs.map(async userDoc => {
        const uid = userDoc.id;
        const { familyCovidImpact } = userDoc.data();
        const userRecord = {
          uid,
          familyCovidImpact,
        };
        return [userRecord];
      }),
    );
    await csvWriter.writeRecords(allRecords.flat());
  }
}

/**
 * Export user topics data to a CSV file
 *
 * @param {output file} csvFile
 */
async function exportUserTopics(csvFile) {
  const firestore = getFirestore(app);
  const csvWriter = createCsvWriter({
    path: csvFile,
    header: [
      { id: 'uid', title: 'UID' },
      { id: 'displayName', title: 'Name' },
      { id: 'topicId', title: 'Module id' },
      { id: 'topicName', title: 'Module name' },
      { id: 'seq', title: 'Module order' },
      { id: 'recommended', title: 'Recommended' },
      { id: 'mandatory', title: 'Selected' },
      { id: 'locked', title: 'Locked' },
      { id: 'lastVisited', title: 'Last visited entry' },
      { id: 'progress', title: 'Progress' },
    ],
  });
  const userBatchs = batchQuery(() => {
    return firestore.collection(`users`).where('userCategory', '==', 'parent');
  }, 64);

  for await (let users of userBatchs) {
    const allRecords = await Promise.all(
      users.docs.map(async userDoc => {
        const uid = userDoc.id;
        const { userFirstname, userSurname } = userDoc.data();
        const displayName = `${userSurname}, ${userFirstname}`;
        const userRecord = {
          uid,
          displayName,
        };

        const topics = await firestore.collection(`/users/${uid}/topics`).get();
        if (topics.docs.length === 0) {
          return [userRecord];
        }

        return topics.docs.reduce((records, topicDoc) => {
          const topicId = topicDoc.id;
          const { seq, tag, mandatory, locked, lastVisited, progress } = topicDoc.data();
          return [
            ...records,
            {
              ...userRecord,
              topicId,
              topicName: modules[topicId],
              seq,
              recommended: tag === 'Recommended',
              mandatory,
              locked,
              lastVisited,
              progress: progress ? progress.completed / progress.total : 0,
            },
          ];
        }, []);
      }),
    );
    await csvWriter.writeRecords(allRecords.flat());
  }
}

function admin(email) {
  const auth = getAuth(app);
  return {
    async getUserRecord() {
      return await auth.getUserByEmail(email);
    },
    async assignAdmin() {
      const userRecord = await this.getUserRecord();
      await auth.setCustomUserClaims(userRecord.uid, { admin: true });
    },
    async verifyEmail() {
      const userRecord = await this.getUserRecord();
      await auth.updateUser(userRecord.uid, { emailVerified: true });
    },
    async updatePassword(newPassword) {
      const userRecord = await this.getUserRecord();
      await auth.updateUser(userRecord.uid, { password: newPassword });
    },
  };
}

program
  .command('user-both-k6')
  .description('Export users who completed both baseline and followup k6 surveys in csv format')
  .requiredOption('-o, --output <path>', 'output file')
  .action(options => {
    (async () => {
      try {
        await exportUsersCompletedBothK6(options.output);
      } catch (e) {
        console.log(e);
      }
    })();
  });

program
  .command('user-surveys')
  .description('Export user survey data in csv format')
  .requiredOption('-i --surveyid <surveyId>', 'survey id')
  .requiredOption('-o, --output <path>', 'output file')
  .action(options => {
    (async () => {
      try {
        await exportUserSurveys(options.surveyid, options.output);
      } catch (e) {
        console.log(e);
      }
    })();
  });

program
  .command('user-topics')
  .description('Export user topics data in csv format')
  .requiredOption('-o, --output <path>', 'output file')
  .action(options => {
    (async () => {
      try {
        await exportUserTopics(options.output);
      } catch (e) {
        console.log(e);
      }
    })();
  });

program
  .command('user-profile')
  .description('Export user profile data in csv format')
  .requiredOption('-o, --output <path>', 'output file')
  .action(options => {
    (async () => {
      try {
        await exportUserProfile(options.output);
      } catch (e) {
        console.log(e);
      }
    })();
  });

program
  .command('assign-admin <email>')
  .description('assign admin claims to a user account')
  .action(email => {
    (async () => {
      try {
        await admin(email).assignAdmin();
        console.log('success');
      } catch (e) {
        console.error(e);
      }
    })();
  });

program
  .command('show-user <email>')
  .description('display full details of a user account')
  .action(email => {
    (async () => {
      try {
        const userRecord = await admin(email).getUserRecord();
        console.log(userRecord);
      } catch (e) {
        console.error(e);
      }
    })();
  });

program
  .command('verify-email <email>')
  .description('verifiy user email on behalf of a user account')
  .action(email => {
    (async () => {
      try {
        await admin(email).verifyEmail();
        console.log(`email <${email}> verified`);
      } catch (e) {
        console.error(e);
      }
    })();
  });

program
  .command('update-password <email>')
  .description('update user password on behalf of a user account')

  .requiredOption('-p, --password <password>', 'new password')
  .action((email, options) => {
    (async () => {
      try {
        await admin(email).updatePassword(options.password);
        console.log('password updated');
      } catch (e) {
        console.log(e);
      }
    })();
  });

program.parse();
