import { PluginOptions } from '@babel/core';
import { CreatePagesArgs, CreateSchemaCustomizationArgs, CreateWebpackConfigArgs, SourceNodesArgs } from 'gatsby';
import path from 'path';
import * as schema from './schema';

export default {
  sourceNodes: async (args: SourceNodesArgs, options: PluginOptions) => {
    await schema.souceNodes(args);
  },

  createSchemaCustomization: (args: CreateSchemaCustomizationArgs, options: PluginOptions) => {
    schema.createSchemaCustomization(args);
  },

  createPages: async ({ actions, graphql }: CreatePagesArgs): Promise<void> => {
    const { createPage } = actions;

    // create Survey pages
    const result = await graphql<{
      allSurvey: {
        nodes: Array<{
          id: string;
          scope: 'baseline' | 'followup';
        }>;
      };
    }>(`
      query {
        allSurvey {
          nodes {
            id
            scope
          }
        }
      }
    `);
    result.data.allSurvey.nodes.forEach((node) => {
      createPage({
        path: `/surveys/${node.id}`,
        component: path.resolve(`./src/templates/SurveyPageTemplate.tsx`),
        context: {
          id: node.id,
          scope: node.scope,
        },
      });
    });
  },

  onCreateWebpackConfig: ({ stage, loaders, actions }: CreateWebpackConfigArgs) => {
    if (stage === 'build-html' || stage === 'develop-html') {
      actions.setWebpackConfig({
        module: {
          rules: [
            {
              test: /firebase/,
              use: loaders.null(),
            },
          ],
        },
      });
    }
  },
};
