import { CreateSchemaCustomizationArgs } from 'gatsby';
import { resolveNav } from './resolveNav';
import { resolveTopicCover, resolveSurveyCover } from './resolveFiles';

export function createSchemaCustomization(args: CreateSchemaCustomizationArgs) {
  const { actions, schema } = args;

  const Topic = schema.buildObjectType({
    name: 'Topic',
    interfaces: ['Node'],
    fields: {
      title: 'String!',
      path: 'String!',
      desc: 'String',
      cover: {
        type: 'File',
        resolve: resolveTopicCover,
      },
      nav: {
        type: 'TopicNav',
        resolve: resolveNav,
      },
    },
  });

  const TopicNav = schema.buildObjectType({
    name: 'TopicNav',
    fields: {
      defaultEntry: 'TopicEntry',
      sequence: '[TopicEntry]',
      hierarchy: '[TopicEntry]',
    },
  });

  const TopicEntry = schema.buildObjectType({
    name: 'TopicEntry',
    fields: {
      title: 'String!',
      path: 'String!',
      key: 'String!',
      optional: 'Boolean',
      goals: 'Boolean',
      activityKeys: '[String]',
      children: '[TopicEntry]',
    },
  });

  const Survey = schema.buildObjectType({
    name: 'Survey',
    interfaces: ['Node'],
    fields: {
      title: 'String!',
      cover: {
        type: 'File',
        resolve: resolveSurveyCover,
      },
      tint: 'String!',
      sections: '[SurveySection]!',
    },
  });

  const SurveySection = schema.buildObjectType({
    name: 'SurveySection',
    fields: {
      key: 'String!',
      title: 'String!',
      questions: '[SurveyItem]',
      type: 'SurveyItemType',
      size: 'String',
      required: 'Boolean',
      options: '[String]',
      scales: '[String]',
    },
  });

  const SurveyItem = schema.buildObjectType({
    name: 'SurveyItem',
    fields: {
      key: 'String!',
      type: 'SurveyItemType',
      options: '[String]',
      scales: '[String]',
      size: 'String',
      required: 'Boolean',
      cond: 'String',
      label: 'String',
      helperText: 'String',
      inputLabel: 'String',
      content: 'String',
      variant: 'String',
    },
  });

  const SurveyItemType = schema.buildEnumType({
    name: 'SurveyItemType',
    values: {
      rating: {
        value: 'rating',
      },
      text: {
        value: 'text',
      },
      select: {
        value: 'select',
      },
      radio: {
        value: 'radio',
      },
      paragraph: {
        value: 'paragraph',
      },
      checkgroup: {
        value: 'checkgroup',
      },
    },
  });

  const SurveyRatingScale = schema.buildEnumType({
    name: 'SurveyRatingScale',
    values: {
      confident: {
        value: 'confident',
      },
    },
  });

  actions.createTypes([
    Topic,
    TopicNav,
    TopicEntry,
    Survey,
    SurveySection,
    SurveyItem,
    SurveyItemType,
    SurveyRatingScale,
  ]);
}
