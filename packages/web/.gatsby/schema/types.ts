export type Topic = {
  id: string;
  title: string;
  name: string;
  path: string;
  cover?: string;
  desc?: String;
  nav?: TopicNav;
};

export type TopicNav = {
  defaultEntry: TopicEntry;
  sequence: TopicEntry[];
  hierarchy: TopicEntry[];
};

export type TopicEntry = {
  title: string;
  path: string;
  children?: TopicEntry[];
  key: string;
  activityKeys?: string[];
  goals: boolean;
  optional: boolean;
};

export type Frontmatter = {
  title?: string;
  optional?: string;
  goals?: string;
};

export type SurveyQuestionBase = {
  id: string;
  title: string;
};

export type RatingQuestion = SurveyQuestionBase & {
  type: 'rating';
  scale: string;
};

export type SurveyQuestion = RatingQuestion;

export type SurveySections = {
  intro?: string[];
  questions: SurveyQuestion[];
};

export type Survey = {
  id: string;
  title: string;
  cover: string;
  scope: 'baseline' | 'followup';
  scales?: Record<string, string[]>;
  sections: Array<SurveySections>;
};
