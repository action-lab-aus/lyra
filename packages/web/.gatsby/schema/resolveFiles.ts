import path from 'path';

export async function resolveSurveyCover(source: any, args: any, context: any) {
  const { cover } = source;
  if (!cover) return null;
  return await context.nodeModel.runQuery({
    type: 'File',
    firstOnly: true,
    query: {
      filter: {
        relativePath: { eq: cover },
        sourceInstanceName: { eq: 'surveys' },
      },
    },
  });
}

export async function resolveTopicCover(source: any, args: any, context: any) {
  const { cover, name } = source;
  if (!cover) return null;
  return await context.nodeModel.runQuery({
    type: 'File',
    firstOnly: true,
    query: {
      filter: {
        relativePath: { eq: path.join(name, cover) },
      },
    },
  });
}
