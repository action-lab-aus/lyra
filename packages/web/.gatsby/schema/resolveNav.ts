import { Frontmatter, TopicEntry, TopicNav } from './types';
import { sortedIndexBy } from 'lodash';
import { titleCase } from 'title-case';

type Mdx = {
  frontmatter: Frontmatter;
  exports: {
    activityKeys: string[] | null;
  };
  __gatsby_resolved: {
    slug: string;
  };
};

export async function resolveNav(source: any, args: any, context: any): Promise<TopicNav | void> {
  const name = source.path.slice(1);
  const result: Array<Mdx> = await context.nodeModel.runQuery({
    type: 'Mdx',
    query: {
      filter: {
        slug: { regex: `/^${name}\\//` },
      },
    },
  });

  if (result) {
    const hierarchy = result.reduce<TopicEntry[]>((contents, mdx) => {
      const slug: string = mdx.__gatsby_resolved.slug;
      const frontmatter: Frontmatter = mdx.frontmatter;
      const activityKeys = mdx.exports.activityKeys;

      const segments = slug.split('/');

      const reduceContents = (previous: TopicEntry[], depth: number = 0): TopicEntry[] => {
        const insert = (items: TopicEntry[], item: TopicEntry) => {
          const index = sortedIndexBy(items, item, (item) => item.path);
          items.splice(index, 0, item);
          return items;
        };

        const next = (items: TopicEntry[], at: number) => {
          const item = items[at];
          items.splice(at, 1, { ...item, children: reduceContents(item.children || [], depth + 1) });
          return items;
        };

        const key = segments[depth + 1];
        if (segments.length === depth + 2) {
          // leaf
          return insert(previous, {
            title: frontmatter?.title || titleCase(segments.slice(-1)[0]),
            optional: Boolean(frontmatter.optional),
            goals: Boolean(frontmatter.goals),
            path: `/${segments.join('/')}`,
            key,
            activityKeys,
          });
        }

        const currentPath = `/${segments.slice(0, depth + 2).join('/')}`;
        let pos = previous.findIndex((item) => item.path === currentPath);
        return pos < 0
          ? insert(previous, {
              title: titleCase(segments.slice(depth + 1, depth + 2)[0]),
              path: currentPath,
              optional: false,
              goals: false,
              key,
              activityKeys,
              children: reduceContents([], depth + 1),
            })
          : next(previous, pos);
      };
      return reduceContents(contents);
    }, []);

    if (hierarchy.length > 0) {
      const flat = (list: TopicEntry[], entry: TopicEntry): TopicEntry[] => {
        return entry.children ? entry.children.reduce(flat, list) : [...list, entry];
      };
      const sequence = hierarchy.reduce<TopicEntry[]>(flat, []);
      return {
        defaultEntry: sequence[0],
        sequence,
        hierarchy,
      };
    }
  }
}
