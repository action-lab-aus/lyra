import { SourceNodesArgs } from 'gatsby';
import path from 'path';
import { Survey, Topic } from './types';
import * as fs from 'fs/promises';
import { Dirent } from 'fs';
import { titleCase } from 'title-case';

async function sourceTopicNodes(args: SourceNodesArgs, contentRoot: string) {
  const createTopicsFromFS = async (contentRoot: string) => {
    const dirEntries = await fs.readdir(contentRoot, { withFileTypes: true });
    const toTopicData = async (entry: Dirent): Promise<Topic> => {
      const entryName = entry.name;
      let topic: Partial<Topic> = {
        title: titleCase(entryName),
        name: entryName,
        path: `/${entryName}`,
      };

      try {
        const metaPath = [contentRoot, entryName, 'topic.json'].join('/');
        const content = await fs.readFile(metaPath, { encoding: 'utf-8' });
        const data = JSON.parse(content) as Partial<Topic>;
        topic = { ...topic, ...data };
      } catch (error) {
        // ignore error if meta file didn't exist
      }
      return topic as Topic;
    };

    return await Promise.all(dirEntries.filter((e) => e.isDirectory()).map(toTopicData));
  };

  const { actions, createNodeId, createContentDigest, reporter } = args;
  reporter.info(`Start create topics from file system`);
  const topics = await createTopicsFromFS(contentRoot);
  topics.forEach((data: Topic) => {
    const json = JSON.stringify(data);
    const meta = {
      id: data.id || createNodeId(`topic-${data.path}`),
      parant: null,
      children: [],
      internal: {
        type: 'Topic',
        content: json,
        contentDigest: createContentDigest(json),
      },
    };
    actions.createNode({ ...data, ...meta });
  });
}

async function sourceSurveyNodes(args: SourceNodesArgs, surveyRoot: string) {
  const createSurveysFromFS = async (surveyRoot: string): Promise<Survey[]> => {
    const entries = await fs.readdir(surveyRoot, { withFileTypes: true });
    const toSurvey = async (entry: Dirent): Promise<Survey> => {
      const surveyJsonFile = [surveyRoot, entry.name].join('/');
      const content = await fs.readFile(surveyJsonFile, { encoding: 'utf-8' });
      const survey = JSON.parse(content) as Survey;
      return survey;
    };
    return await Promise.all(entries.filter((e) => e.isFile() && e.name.match(/.+\.json$/)).map(toSurvey));
  };

  const { actions, createContentDigest, reporter } = args;
  reporter.info(`Start create surveys from file system`);
  const surveys = await createSurveysFromFS(surveyRoot);
  surveys.forEach((survey: Survey) => {
    const json = JSON.stringify(survey);
    const node = {
      parant: null,
      children: [],
      internal: {
        type: 'Survey',
        content: json,
        contentDigest: createContentDigest(json),
      },
      ...survey,
    };
    actions.createNode(node);
  });
}

export async function souceNodes(args: SourceNodesArgs, options: Record<string, any> = {}) {
  await sourceTopicNodes(args, path.resolve(__dirname, '../../src/contents'));
  await sourceSurveyNodes(args, path.resolve(__dirname, '../../src/surveys'));
}
