import { GatsbyConfig } from 'gatsby';
import path from 'path';

export default {
  siteMetadata: {
    title: 'Partners in Parenting Plus',
    titleTemplate: '%s',
    description:
      'Evidenced based parenting program for raising teenagers in ways that may reduce their risk of depression & anxiety problems.',
    url: process.env.SITE_URL,
    image: '/images/shared/meta.png',
  },
  plugins: [
    {
      resolve: `gatsby-plugin-force-file-loader`,
      options: {
        rules: ['images' /* Matches Gatsby default rules for images */],
      },
    },
    {
      resolve: 'gatsby-plugin-root-import',
      options: {
        app: path.join(__dirname, '../src/app'),
        components: path.join(__dirname, '../src/components'),
        helpers: path.join(__dirname, '../src/helpers'),
        pages: path.join(__dirname, '../src/pages'),
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `contents`,
        path: path.join(__dirname, '../src/contents'),
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `surveys`,
        path: path.join(__dirname, '../src/surveys'),
      },
    },
    {
      resolve: 'gatsby-plugin-page-creator',
      options: {
        path: path.join(__dirname, '../src/contents'),
      },
    },
    {
      resolve: 'gatsby-plugin-image',
    },
    {
      resolve: 'gatsby-plugin-sharp',
    },
    {
      resolve: 'gatsby-transformer-sharp',
    },
    {
      resolve: 'gatsby-plugin-react-helmet',
    },
    {
      resolve: 'gatsby-plugin-mdx',
      options: {
        defaultLayouts: {
          contents: require.resolve('../src/components/content/ContentPage'),
          default: require.resolve('../src/components/layout/SitePage'),
        },
        gatsbyRemarkPlugins: [
          {
            resolve: 'gatsby-remark-mermaid',
          },
          {
            resolve: 'gatsby-remark-prismjs',
          },
          {
            resolve: 'gatsby-remark-autolink-headers',
            options: {
              icon: false,
            },
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-material-ui`,
    },
  ],
} as GatsbyConfig;
