import React from 'react';
import { WrapRootElementNodeArgs } from 'gatsby';
import PageProvider from '../src/PageProvider';

export const wrapWithProvider = ({ element }: WrapRootElementNodeArgs): React.ReactElement => {
  return <PageProvider>{element}</PageProvider>;
};
