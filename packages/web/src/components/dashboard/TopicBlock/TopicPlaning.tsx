import React from 'react';
import { getSrc } from 'gatsby-plugin-image';
import { Typography, Accordion, AccordionSummary, AccordionDetails, Box, Chip, Paper, Switch } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { blue } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { ConfirmButton } from 'components/ConfirmButton';
import { TopicPlanItem, UserSurvey } from 'app';
import { getTopicSuggestion } from './helpers';
import { DashboardTopic } from '../types';
import { headings } from '../';

const useStyles = makeStyles((theme) =>
  createStyles({
    info: {
      padding: theme.spacing(2),
      ...theme.typography.body1,
      backgroundColor: blue[50],
    },

    summary: {
      display: 'flex',
      alignItems: 'center',
    },

    box: {
      flex: 1,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      [theme.breakpoints.down('xs')]: {
        flexDirection: 'column',
        alignItems: 'flex-start',
      },
    },

    cover: {
      width: 160,
      marginRight: theme.spacing(2),
      [theme.breakpoints.down('xs')]: {
        display: 'none',
      },
    },
  }),
);

export type TopicPlaningProps = {
  userSurveys: Record<string, UserSurvey>;
  topics: Array<DashboardTopic>;
  onCreatePlan: (plan: TopicPlanItem[]) => void;
};

export function TopicPlaning(props: TopicPlaningProps) {
  const { userSurveys, onCreatePlan } = props;
  const classes = useStyles();
  const topics = React.useMemo<Record<string, DashboardTopic>>(() => {
    return props.topics.reduce<Record<string, DashboardTopic>>((topicRecords, topic) => {
      return { ...topicRecords, [topic.id]: topic };
    }, {});
  }, [props.topics]);

  const [selected, setSelected] = React.useState<string | null>(null);

  const [plan, setPlan] = React.useState<TopicPlanItem[]>(() => {
    return getTopicSuggestion(userSurveys).map<TopicPlanItem>((entry, idx) => {
      const { topicId, recommend } = entry;
      return {
        topicId,
        seq: idx + 1,
        mandatory: recommend,
        tag: recommend ? 'Recommended' : undefined,
      };
    });
  });

  const mandatoryCount = React.useMemo(
    () => plan.reduce<number>((count, plan) => (count += plan.mandatory ? 1 : 0), 0),
    [plan],
  );

  const handleToggle = (topicId: string) => (e: React.MouseEvent<HTMLButtonElement>) => {
    setPlan(
      plan.map<TopicPlanItem>((item) => (item.topicId === topicId ? { ...item, mandatory: !item.mandatory } : item)),
    );
    e.stopPropagation();
  };

  return (
    <>
      <Typography variant="h6" gutterBottom>
        {headings.topic}
      </Typography>

      <Paper className={classes.info} elevation={0}>
        Welcome to your personalised program. Based on your survey responses, we have recommended and pre-selected
        specific modules just for you. These modules correspond to the feedback you have just received, and are designed
        to support you in making changes recommended in your feedback. However, this is your program, so feel free to
        choose other modules. To select or deselect a module, toggle or untoggle the switch. Before confirming your
        modules, click the arrow next to each title to read more about it. Once you're happy with your selection, click
        "Confirm" to start your program.
      </Paper>

      <Box mt={2}>
        {plan.map((item) => {
          const topic = topics[item.topicId];
          return (
            <Accordion
              key={item.topicId}
              expanded={selected === item.topicId}
              onChange={(e, expand) => {
                setSelected(expand ? item.topicId : null);
              }}>
              <AccordionSummary expandIcon={<ExpandMoreIcon />} classes={{ content: classes.summary }}>
                <Switch checked={item.mandatory} onClick={handleToggle(item.topicId)} name={item.topicId} />
                <Box className={classes.box}>
                  <Typography component="div">{topic.title}</Typography>
                  {item.tag && <Chip label={item.tag} size="small" color="primary" variant="outlined" />}
                </Box>
              </AccordionSummary>
              <AccordionDetails>
                <Box display="flex" justifyContent="flex-start" alignItems="flex-start">
                  {topic.cover && <img src={getSrc(topic.cover)} alt={topic.title} className={classes.cover} />}
                  <Typography variant="body1" color="textSecondary" style={{ flex: 1 }}>
                    {topic.desc}
                  </Typography>
                </Box>
              </AccordionDetails>
            </Accordion>
          );
        })}
      </Box>

      {mandatoryCount === 0 && (
        <Box my={1}>
          <Alert severity="info">
            You have not selected any modules. Please select at least one module to continue.
          </Alert>
        </Box>
      )}
      <Box display="flex" justifyContent="center" mt={4}>
        <ConfirmButton onConfirm={() => onCreatePlan(plan)} disabled={mandatoryCount === 0}>
          You have selected {mandatoryCount} modules. Please click <strong>OK</strong> to continue, or{' '}
          <strong>Cancel</strong> to edit your selection.
        </ConfirmButton>
      </Box>
    </>
  );
}
