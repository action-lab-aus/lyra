import { UserSurvey } from 'app';
import sum from 'lodash/sum';
import { calcScores as scores1 } from '../../feeback/sections/Section1';
import { calcScores as scores2 } from '../../feeback/sections/Section2';
import { calcScores as scores3 } from '../../feeback/sections/Section3';
import { calcRecommendScores as scores4 } from '../../feeback/sections/Section4';
import { calcScores as scores5 } from '../../feeback/sections/Section5';
import { calcScores as scores6 } from '../../feeback/sections/Section6';
import { calcScores as scores7 } from '../../feeback/sections/Section7';
import { calcScores as scores8 } from '../../feeback/sections/Section8';
import { calcScores as scores9 } from '../../feeback/sections/Section9';
import { calcScore as calcConfidenceScore } from '../../feeback/S1';

const pradasEntries: Array<{
  topicId: string;
  threshold: number;
  sortScore?: number;
  scores: (userSurvey: UserSurvey) => Array<number>;
}> = [
  {
    //1. Connect - map scores of PRADAS S1
    topicId: 'm1',
    threshold: 7,
    sortScore: -4,
    scores: scores1,
    //S1-Confidence < 42/44 & s1#q5 != "Very confident" => suggest
  },
  {
    // 2. NEW COVID module
    topicId: 'm2',
    threshold: 0,
    sortScore: -3,
    scores: () => [0],
    // S1-Confidence < 42/44 => suggest
  },
  {
    // 3. Raising good kids into great adults: establishing family rules, - map scores of PRADAS S4
    topicId: 'm3',
    threshold: 2,
    sortScore: -2,
    scores: scores4,
  },
  {
    // 4. Nurture roots & inspire wings - map scores of PRADAS S2
    topicId: 'm4',
    threshold: 7,
    scores: scores2,
  },
  {
    // 5. Calm versus conflict, - map scores of PRADAS S5
    topicId: 'm5',
    threshold: 7,
    scores: scores5,
    // S1-Confidence < 42/44 & s1#q8 != "Very confident" => suggest & sortScore
  },
  {
    // 6. Good friends = supportive relationships, - map scores of PRADAS S3
    topicId: 'm6',
    threshold: 5,
    scores: scores3,
  },
  {
    // 7. Good health habits for good mental health - map scores of PRADAS S6
    topicId: 'm7',
    threshold: 0,
    scores: scores6,
  },
  {
    // 8. Partners in problem solving - map scores of PRADAS S7
    topicId: 'm8',
    threshold: 9,
    scores: scores7,
  },
  {
    // 9. From surviving to thriving: Helping your teenager deal with anxiety - map scores of PRADAS S8
    topicId: 'm9',
    threshold: 8,
    scores: scores8,
  },
  {
    // 10. When things aren’t okay: Getting professional help - map scores of PRADAS S9
    topicId: 'm10',
    threshold: 8,
    scores: scores9,
  },
];

export type Entry = {
  topicId: string;
  recommend: boolean;
  sortScore: number;
};

export function getTopicSuggestion(userSurveys: Record<string, UserSurvey>): Entry[] {
  const { 's2-pradas': s2Pradas, 's1-confidence': s1Confidence } = userSurveys;
  const recommend = (entries: Entry[], topicId: string) =>
    entries.map<Entry>((entry) => (entry.topicId === topicId ? { ...entry, recommend: true } : entry));

  // sort by score and recommendation
  let entries: Entry[] = pradasEntries
    .map((entry) => {
      const scores = entry.scores(s2Pradas);
      const score = sum(scores);
      const recommend = score < entry.threshold;
      const sortScore = entry.sortScore || score / scores.length;
      return {
        topicId: entry.topicId,
        recommend,
        sortScore,
      };
    })
    .sort((a, b) => a.sortScore - b.sortScore);

  // recommend m7 by scores6
  const scores = scores6(s2Pradas);
  if (sum(scores.slice(0, 8)) < 7 || sum(scores.slice(8, 12)) < 4) {
    entries = recommend(entries, 'm7');
  }

  // update recommend flag by confidence
  const confidenceScore = calcConfidenceScore(s1Confidence);
  if (confidenceScore < 42) {
    entries = recommend(entries, 'm2');

    if (s1Confidence['s1#q5'] !== 'Very confident') {
      entries = recommend(entries, 'm1');
    }

    if (s1Confidence['s1#q8'] !== 'Very confident') {
      entries = recommend(entries, 'm5');
    }
  }

  // console.log(entries);
  return entries;
}
