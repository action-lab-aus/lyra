import React from 'react';
import { navigate } from 'gatsby';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import {
  Box,
  Button,
  Card,
  CardHeader,
  CardActions,
  CardContent,
  CardMedia,
  Chip,
  IconButton,
  Menu,
  MenuItem,
  Typography,
  LinearProgress,
} from '@material-ui/core';
import { createStyles, makeStyles, styled } from '@material-ui/core/styles';
import { blueGrey, pink } from '@material-ui/core/colors';

import LockIcon from '@material-ui/icons/Lock';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { TopicItem } from './Topics';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      position: 'relative',
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    header: {
      minHeight: 56,
    },

    headerAction: {
      alignSelf: 'center',
    },

    content: {
      flexGrow: 1,
      textAlign: 'center',
    },

    media: {
      position: 'relative',
      backgroundColor: blueGrey[50],
    },

    placeholder: {
      position: 'relative',
      width: '100%',
      paddingTop: '80%',
    },

    statusContainer: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      display: 'block',
    },

    actions: {
      justifyContent: 'center',
      margin: theme.spacing(0, 0, 2, 0),
    },

    locked: {
      color: blueGrey[300],
    },
    unlocked: {
      color: blueGrey[100],
    },
  }),
);

export type TopicCardProps = {
  topicItem: TopicItem;
  onUnlock: (topicId: string) => void;
};

export function TopicCard(props: TopicCardProps) {
  const classes = useStyles();
  const { topicItem, onUnlock } = props;
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const { topic, tag, locked, progress, lastVisited } = topicItem;
  const avatar = locked ? <LockIcon className={classes.locked} /> : <LockOpenIcon className={classes.unlocked} />;
  const progressValue = progress && progress.total > 0 ? (100 * progress.completed) / progress.total : 0;
  const entryPath = lastVisited ? `/${topic.name}/${lastVisited}` : topic.nav.defaultEntry.path;
  const entryLabel = progressValue === 100 ? 'Enter' : lastVisited ? 'Continue' : 'Start';

  const action = (
    <IconButton aria-label="menu" onClick={(e) => setAnchorEl(e.currentTarget)}>
      <MoreVertIcon />
    </IconButton>
  );

  const cover = () => {
    const coverImageData = topic.cover && getImage(topic.cover);
    return coverImageData ? (
      <GatsbyImage image={coverImageData} alt={topic.title} />
    ) : (
      <div className={classes.placeholder} />
    );
  };

  const handleUnlock = () => {
    setAnchorEl(null);
    onUnlock(topic.id);
  };

  return (
    <Card className={classes.root}>
      {progressValue === 100 && <ConerLabel>Completed</ConerLabel>}
      <CardHeader
        classes={{
          root: classes.header,
          action: classes.headerAction,
        }}
        avatar={avatar}
        action={locked && action}
        title={topic.title}
        titleTypographyProps={{ variant: 'subtitle1' }}
      />
      <LinearProgress variant="determinate" value={progressValue} />
      <CardMedia className={classes.media}>
        {cover()}
        <div className={classes.statusContainer}>
          <Box p={1} display="flex" justifyContent="space-between" alignItems="center">
            {tag && <Chip color="primary" label={tag} size="small" />}
          </Box>
        </div>
      </CardMedia>
      <CardContent className={classes.content}>
        <Typography variant="body2" color="textSecondary" paragraph>
          {topic.desc}
        </Typography>
      </CardContent>
      <CardActions className={classes.actions}>
        <Button color="secondary" variant="contained" disabled={locked} onClick={() => navigate(entryPath)}>
          {entryLabel}
        </Button>
      </CardActions>
      <Menu open={Boolean(anchorEl)} anchorEl={anchorEl} onClose={() => setAnchorEl(null)}>
        <MenuItem onClick={handleUnlock}>Unlock module</MenuItem>
      </Menu>
    </Card>
  );
}

const ConerLabel = styled('label')(({ theme }) => {
  return {
    ...theme.typography.caption,
    position: 'absolute',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    bottom: 20,
    right: -35,
    transform: 'rotate(-45deg)',
    padding: '5px 30px',
    textTransform: 'uppercase',
  };
});
