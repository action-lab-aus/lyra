import React from 'react';
import { useDispatch } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import { Typography, Button } from '@material-ui/core';
import { TopicPlanItem, UserProfile, UserSurvey, UserTopic } from 'app';
import { createTopicPlan, startTopic } from 'app/profileSlice';
import { headings } from '../index';
import { DashboardTopic } from '../types';
import { Welcome } from '../Welcome';
import { Topics, TopicItem } from './Topics';
import { TopicPlaning } from './TopicPlaning';
import cover from './topic_cover.png';

export type TopicBlockStage = 'welcome' | 'plan' | 'entries';

export type TopicBlockProps = {
  user: UserProfile;
  userTopics: Record<string, UserTopic>;
  userSurveys: Record<string, UserSurvey>;
  topics: Array<DashboardTopic>;
  baselineCompleted: boolean;
  onShowPSG: () => void;
};

export function TopicBlock(props: TopicBlockProps) {
  const { user, userTopics, userSurveys, topics, baselineCompleted, onShowPSG } = props;
  const shouldShowPsg = user.userCategory === 'parent' && !isEmpty(userTopics);

  const dispatch = useDispatch();
  const psgEnabled = React.useMemo(() => shouldShowPsg, []);

  React.useEffect(() => {
    if (shouldShowPsg && psgEnabled !== shouldShowPsg) {
      onShowPSG();
    }
  }, [shouldShowPsg]);

  const { stage, items } = React.useMemo(() => {
    const topicStage = () => {
      const currentStage = user.currentStage;
      if (currentStage === 'topic' || currentStage === 'followup') {
        return isEmpty(userTopics) ? 'plan' : 'entries';
      } else {
        return 'welcome';
      }
    };

    // calculate topic section state
    const items = topics.reduce<TopicItem[]>((items, topic) => {
      const {
        mandatory,
        locked,
        seq = Number.MAX_VALUE,
        tag,
        progress,
        lastVisited,
      } = userTopics[topic.id] || {
        mandatory: false,
        locked: true,
      };
      return [...items, { topic, mandatory, locked, seq, tag, progress, lastVisited }];
    }, []);

    return {
      stage: topicStage(),
      items,
    };
  }, [user, userTopics, userSurveys]);

  const handleStartTopic = () => {
    // create suggestion
    dispatch(startTopic());
  };

  const handleCreateTopicPlan = (plan: TopicPlanItem[]) => {
    dispatch(createTopicPlan(plan));
  };

  switch (stage) {
    case 'welcome':
      return (
        <Welcome img={cover} title={headings.topic}>
          <Typography variant="subtitle1" color="textSecondary" align="justify" gutterBottom paragraph>
            After you've completed the first online survey, you'll have access to your personalised parenting program.
            You can select up to 10 modules, based on our suggestions and your preferences. We'll suggest you complete
            one module per week, but you can also work through these at your own pace. Click start to select your
            modules and start your program.
          </Typography>
          <Button variant="contained" color="secondary" disabled={!baselineCompleted} onClick={handleStartTopic}>
            Start
          </Button>
        </Welcome>
      );

    case 'plan':
      return <TopicPlaning topics={topics} userSurveys={userSurveys} onCreatePlan={handleCreateTopicPlan} />;

    default:
      return <Topics topicItems={items} />;
  }
}
