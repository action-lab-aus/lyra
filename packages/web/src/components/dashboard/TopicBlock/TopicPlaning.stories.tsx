import React from 'react';
import { Story, Meta } from '@storybook/react';
import { TopicPlaning, TopicPlaningProps } from './TopicPlaning';
import { topics } from 'components/helpers/mocks';
import { Container } from '@material-ui/core';
import * as mocks from 'components/helpers/mocks';

const templates = mocks.templates;

export default {
  title: 'components/dashboard/TopicBlock/TopicPlaning',
  component: TopicPlaning,
  argTypes: { onCreatePlan: { action: 'onCreatePlan' } },
} as Meta;

const Template: Story<TopicPlaningProps> = (args) => (
  <Container maxWidth="lg">
    <TopicPlaning {...args} />
  </Container>
);

export const Example = Template.bind({});
Example.args = {
  userSurveys: {
    's1-confidence': templates.userSurveys.s1Confidence(),
    's2-pradas': { ...templates.userSurveys.s2Pradas(), 'S4#Q1': 'Yes', 'S4#Q2': 'Yes' },
  },
  topics: topics,
};
