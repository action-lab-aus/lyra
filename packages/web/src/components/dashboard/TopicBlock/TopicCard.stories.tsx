import React from 'react';
import { Story, Meta } from '@storybook/react';
import { Box, Container } from '@material-ui/core';
import { ThemeDecorator } from 'components/helpers';
import { TopicCard, TopicCardProps } from './TopicCard';
import { TopicItem } from './Topics';
import { topics } from 'components/helpers/mocks';

export default {
  title: 'components/dashboard/TopicBlock/TopicCard',
  component: TopicCard,
  decorators: [ThemeDecorator],
  argTypes: {
    onFeedback: { action: 'onFeedback' },
    onUnlock: { action: ' onUnlock' },
  },
} as Meta;

const topicItem: TopicItem = {
  seq: 0,
  topic: topics[0],
  mandatory: true,
  progress: { completed: 0, total: 10 },
  locked: true,
};

const Template: Story<TopicCardProps> = (args) => (
  <Container maxWidth="lg">
    <Box mt={2} width={389}>
      <TopicCard {...args} />
    </Box>
  </Container>
);

export const Locked = Template.bind({});
Locked.args = {
  topicItem,
};

export const Unlocked = Template.bind({});
Unlocked.args = {
  topicItem: { ...topicItem, locked: false },
};

export const InProgress = Template.bind({});
InProgress.args = {
  topicItem: { ...topicItem, locked: false, progress: { completed: 2, total: 10 }, lastVisited: 'some-content' },
};

export const Completed = Template.bind({});
Completed.args = {
  topicItem: { ...topicItem, locked: false, progress: { completed: 10, total: 10 } },
};
