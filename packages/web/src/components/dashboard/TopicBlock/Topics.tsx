import React from 'react';
import { useDispatch } from 'react-redux';
import { Box, Typography, Grid, Collapse, IconButton } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { UserTopic } from 'app';
import { unlockTopic } from 'app/profileSlice';
import { headings } from '../index';
import { DashboardTopic } from '../types';
import { TopicCard } from './TopicCard';

/**
 * Topics component
 */

const useStyles = makeStyles((theme) =>
  createStyles<string, { collapseOptional: boolean }>({
    root: {
      margin: theme.spacing(2, 0),
    },

    expand: ({ collapseOptional }) => {
      return {
        transform: collapseOptional ? 'rotate(0deg)' : 'rotate(180deg)',
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
        }),
      };
    },

    expandBox: {
      position: 'relative',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      margin: theme.spacing(2, 0),
    },
  }),
);

export type TopicItem = Omit<UserTopic, 'entries'> & {
  topic: DashboardTopic;
};

export type TopicsProps = {
  topicItems: TopicItem[];
};

export function Topics(props: TopicsProps) {
  const { topicItems } = props;

  const dispatch = useDispatch();

  const [collapseOptional, setCollapseOptional] = React.useState(true);

  const classes = useStyles({ collapseOptional });

  const { mandatory, optional } = React.useMemo(() => {
    const [mandatory, optional] = topicItems.reduce<[Array<TopicItem>, Array<TopicItem>]>(
      (entries, entry) => {
        const [mandatory, optional] = entries;
        return entry.mandatory ? [[...mandatory, entry], optional] : [mandatory, [...optional, entry]];
      },
      [[], []],
    );
    return {
      mandatory: mandatory.sort((a, b) => a.seq - b.seq),
      optional: optional.sort((a, b) => a.seq - b.seq),
    };
  }, [topicItems]);

  const handleUnlock = (topicId: string) => {
    dispatch(unlockTopic(topicId));
  };

  return (
    <div className={classes.root}>
      <Typography variant="subtitle1" color="textSecondary" gutterBottom>
        {headings.topic}
      </Typography>

      {mandatory.length > 0 && (
        <Box my={2}>
          <Grid container spacing={4}>
            {mandatory.map((entry) => (
              <Grid item key={entry.topic.id} xs={12} sm={6} md={4}>
                <TopicCard topicItem={entry} onUnlock={handleUnlock} />
              </Grid>
            ))}
          </Grid>
        </Box>
      )}

      {optional.length > 0 && (
        <Box my={2}>
          <Box className={classes.expandBox}>
            <IconButton
              className={classes.expand}
              onClick={() => setCollapseOptional(!collapseOptional)}
              aria-expanded={collapseOptional}
              aria-label="show more">
              <ExpandMoreIcon />
            </IconButton>
            <Typography variant="h6">
              <b>PRESS TO SEE/HIDE OTHER AVAILABLE MODULES</b>
            </Typography>
          </Box>

          <Collapse in={!collapseOptional}>
            <Grid container spacing={2}>
              {optional.map((entry) => (
                <Grid item key={entry.topic.id} xs={12} sm={6} md={4}>
                  <TopicCard topicItem={entry} onUnlock={handleUnlock} />
                </Grid>
              ))}
            </Grid>
          </Collapse>
        </Box>
      )}
    </div>
  );
}
