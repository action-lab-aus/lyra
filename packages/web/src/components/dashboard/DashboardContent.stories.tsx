import React from 'react';
import update from 'immutability-helper';
import { Story, Meta } from '@storybook/react';
import { RootState } from 'app';
import { ThemeDecorator, AppProvider } from 'components/helpers';
import * as mocks from 'components/helpers/mocks';
import { DashboardContent } from './DashboardContent';
import firebase from 'firebase';
import moment from 'moment';

const templates = mocks.templates;

export default {
  title: 'components/dashboard/DashboardContent',
  component: DashboardContent,
  decorators: [ThemeDecorator],
} as Meta;

const Template: Story<{ state: RootState }> = (args) => {
  return (
    <AppProvider {...args}>
      <DashboardContent topics={mocks.topics} surveys={mocks.dashboardSurveys} />
    </AppProvider>
  );
};

export const NotFetched = Template.bind({});
NotFetched.args = {
  state: mocks.rootState().authenticated().profile({ fetched: false }).state,
};

export const Initial = Template.bind({});
Initial.args = {
  state: mocks
    .rootState()
    .authenticated()
    .profile({
      fetched: true,
      data: {
        surveys: {},
        topics: {},
        user: templates.user(),
      },
    }).state,
};

export const SurveyStarted = Template.bind({});
SurveyStarted.args = {
  state: update(Initial.args.state, {
    profile: {
      data: {
        user: {
          $merge: {
            currentStage: 'survey',
          },
        },
        surveys: {
          $set: {
            's1-confidence': { _total: 0, _completed: 0, _step: 0 },
          },
        },
      },
    },
  }),
};

export const SurveyInProgress = Template.bind({});
SurveyInProgress.args = {
  state: update(SurveyStarted.args.state, {
    profile: {
      data: {
        surveys: {
          $set: {
            's1-confidence': templates.userSurveys.s1Confidence(),
            's2-pradas': templates.userSurveys.s2Pradas(),
          },
        },
      },
    },
  }),
};

export const SurveyCompleted = Template.bind({});
SurveyCompleted.args = {
  state: update(SurveyInProgress.args.state, {
    profile: {
      data: {
        surveys: {
          $set: {
            's1-confidence': templates.userSurveys.s1Confidence(),
            's2-pradas': templates.userSurveys.s2Pradas(),
            's3-k6': templates.userSurveys.s3K6(),
            's4-rcads': templates.userSurveys.s4Rcads(),
          },
        },
      },
    },
  }),
};

export const TopicPlan = Template.bind({});
TopicPlan.args = {
  state: update(SurveyCompleted.args.state, {
    profile: {
      data: {
        user: {
          $merge: {
            currentStage: 'topic',
          },
        },
      },
    },
  }),
};

export const TopicStarted = Template.bind({});
TopicStarted.args = {
  state: update(TopicPlan.args.state, {
    profile: {
      data: {
        topics: {
          $set: templates.userTopics({
            m2: { mandatory: true, locked: false },
            m4: { mandatory: true, tag: 'Recommend' },
            m5: { mandatory: true },
            m7: { mandatory: true },
            m8: { mandatory: true },
          }),
        },
      },
    },
  }),
};

export const FollowupDateNotReached = Template.bind({});
FollowupDateNotReached.args = {
  state: update(TopicStarted.args.state, {
    profile: {
      data: {
        user: {
          $merge: {
            surveyCompletedAt: firebase.firestore.Timestamp.fromDate(moment().subtract(89, 'days').toDate()),
          },
        },
      },
    },
  }),
};

export const FollowupDateReached = Template.bind({});
FollowupDateReached.args = {
  state: update(FollowupDateNotReached.args.state, {
    profile: {
      data: {
        user: {
          $merge: {
            surveyCompletedAt: firebase.firestore.Timestamp.fromDate(moment().subtract(91, 'days').toDate()),
          },
        },
      },
    },
  }),
};

export const FollowupStarted = Template.bind({});
FollowupStarted.args = {
  state: update(FollowupDateReached.args.state, {
    profile: {
      data: {
        user: {
          $merge: {
            currentStage: 'followup',
          },
        },
        surveys: {
          $merge: {
            'f1-evaluation': templates.userSurveys.s1Confidence(),
          },
        },
      },
    },
  }),
};
