import React from 'react';
import { navigate } from 'gatsby-link';
import { Box, Typography, Grid, Button } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { SurveyCard } from './SurveyCard';
import { DashboardSurvey } from '../types';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      margin: theme.spacing(6, 0),
    },
  }),
);

export type SurveyItemData = {
  survey: DashboardSurvey;
  progress: number | null;
};

export type SurveysProps = {
  title: string;
  entries: SurveyItemData[];
  completed: boolean;
};

export function Surveys(props: SurveysProps) {
  const { title, entries, completed } = props;
  const classes = useStyles();
  const totalProgress = React.useMemo<number>(
    () => entries.reduce((total, entry) => total + (entry.progress || 0), 0),
    [entries],
  );

  const handleContinue = () => {
    const incompleteEntry = entries.find((entry) => entry.progress !== 1);
    if (incompleteEntry) {
      navigate(`/surveys/${incompleteEntry.survey.id}`);
    }
  };

  return (
    <section className={classes.root} role="presentation" aria-label="surveys">
      <Typography variant="subtitle1" color="textSecondary" gutterBottom>
        {title}
      </Typography>

      <Grid container spacing={4} role="list">
        {entries.map((entry, index) => {
          const { survey, progress } = entry;
          return (
            <Grid key={survey.id} item xs={12} sm={6} md={4} lg={3} role="listItem">
              <SurveyCard
                survey={survey}
                progress={progress}
                onClick={() => navigate(`/surveys/${survey.id}`)}
                transitionDelay={100 + 200 * index}
              />
            </Grid>
          );
        })}
      </Grid>

      {!completed && (
        <Box display="flex" justifyContent="center" mt={4}>
          <Button variant="contained" color="secondary" onClick={handleContinue}>
            {totalProgress > 0 ? 'Continue' : 'Start'}
          </Button>
        </Box>
      )}
    </section>
  );
}
