import React from 'react';
import { useDispatch } from 'react-redux';
import { Button, Typography } from '@material-ui/core';
import { UserProfile, UserSurvey } from 'app';
import { startSurvey } from 'app/profileSlice';
import { headings } from '../index';
import { DashboardSurvey } from '../types';
import { Welcome } from '../Welcome';
import { Surveys } from './Surveys';
import cover from './survey_cover.png';
import { useSurveyItems } from './useSurveyItems';

export type SurveyStage = 'welcome' | 'entries';

export type BaselineBlockProps = {
  user: UserProfile;
  baseline: Array<DashboardSurvey>;
  userSurveys: Record<string, UserSurvey>;
};

export function BaselineBlock(props: BaselineBlockProps) {
  const { user, userSurveys, baseline } = props;
  const dispatch = useDispatch();
  const { total, items } = useSurveyItems(baseline, user, userSurveys);
  const stage = user.currentStage ? 'entries' : 'welcome';

  return (
    <div id="baseline">
      {stage === 'welcome' ? (
        // Survey block
        <Welcome img={cover} title={headings.survey}>
          <Typography variant="subtitle1" color="textSecondary" align="justify" gutterBottom paragraph>
            Welcome to the PiP+ program! The first stage of the program includes an initial online assessment, during
            which you'll receive personalised feedback. After the surveys, you'll have access to your tailored program
            right away. Click the button to get started.
          </Typography>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              dispatch(startSurvey(items[0].survey.id, 'survey'));
            }}>
            Start
          </Button>
        </Welcome>
      ) : (
        <Surveys title={headings.survey} entries={items} completed={total === items.length} />
      )}
    </div>
  );
}
