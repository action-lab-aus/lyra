import React from 'react';
import { useDispatch } from 'react-redux';
import { Typography, Button } from '@material-ui/core';
import { startSurvey } from 'app/profileSlice';
import { headings } from '../index';
import { Welcome } from '../Welcome';
import cover from './followup_cover.png';
import { UserProfile, UserSurvey } from 'app';
import { DashboardSurvey } from '../types';
import moment from 'moment';
import { Surveys } from './Surveys';
import { useSurveyItems } from './useSurveyItems';

export type FollowupBlockProps = {
  user: UserProfile;
  followup: Array<DashboardSurvey>;
  userSurveys: Record<string, UserSurvey>;
};

const unlockFollowupSurvey = Number(process.env.GATSBY_DAYS_UNLOCK_FOLLOWUP || 90);
const isDaysAfter = (date: Date) => moment().subtract(unlockFollowupSurvey, 'days').isAfter(date);
const unlockDate = (from: Date) => moment(from).add(unlockFollowupSurvey, 'days').fromNow();

export function FollowupBlock(props: FollowupBlockProps) {
  const { user, userSurveys, followup } = props;
  const dispatch = useDispatch();
  const { surveyCompletedAt } = user;
  const enabled = surveyCompletedAt && isDaysAfter(surveyCompletedAt.toDate());
  const { total, items } = useSurveyItems(followup, user, userSurveys);

  return (
    <div id="followup">
      {user.currentStage !== 'followup' ? (
        <Welcome img={cover} title={headings.followup}>
          <Typography variant="subtitle1" color="textSecondary" align="left" gutterBottom paragraph>
            Many parents find it helpful to reflect on their parenting again after they've completed the program. About
            3-months after your first survey, we'll invite you to complete the same surveys again, plus tell us what you
            thought of the PiP program. Your responses will help us to improve the program in future, and contribute to
            important research on parenting during the pandemic.
          </Typography>
          {surveyCompletedAt && (
            <Button
              variant="contained"
              color="secondary"
              onClick={() => {
                dispatch(startSurvey(items[0].survey.id, 'followup'));
              }}
              disabled={!enabled}>
              {enabled ? 'Start' : `Unlocks ${unlockDate(surveyCompletedAt.toDate())}`}
            </Button>
          )}
        </Welcome>
      ) : (
        <Surveys title={headings.followup} entries={items} completed={total === items.length} />
      )}
    </div>
  );
}
