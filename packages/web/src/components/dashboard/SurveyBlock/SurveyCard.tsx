import React from 'react';
import clsx from 'clsx';
import { Box, ButtonBase, ButtonBaseProps, Typography } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import CheckedIcon from '@material-ui/icons/Check';
import LockIcon from '@material-ui/icons/Lock';
import { DashboardSurvey } from '../types';
import { Ring } from 'components';
import { getSrc } from 'gatsby-plugin-image';

/**
 * Survey card component
 */
const useStyles = makeStyles((theme) =>
  createStyles<string, { tint: string; enabled: boolean }>({
    root: {
      position: 'relative',
      width: '100%',
      paddingTop: '50%',
    },
    button: {
      position: 'absolute',
      top: 0,
      width: '100%',
      height: '100%',
      borderRadius: 8,
      overflow: 'hidden',
      display: 'flex',
      flexDirection: 'column',
      color: '#FFF',
      boxShadow: theme.shadows[1],
    },

    mask: ({ enabled, tint }) => ({
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      opacity: enabled ? 1 : 0.5,
      backgroundColor: enabled ? tint : '#666',

      '& > img': {
        position: 'absolute',
        display: 'block',
        left: 10,
        bottom: '-20%',
        height: '95%',
        filter: enabled ? 'none' : 'grayscale(100%)',
        transform: 'translateY(100%)',
        opacity: 0,
        transition: theme.transitions.create(['transform', 'opacity'], { duration: 1000 }),

        '&.slideIn': {
          transform: 'translateY(0)',
          opacity: 1,
        },
        '&.s2-pradas, &.f2-pradas': {
          left: -25,
          bottom: -5,
          height: '50%',
        },
      },
    }),

    content: {
      position: 'absolute',
      top: 20,
      left: 60,
      right: 20,
      bottom: 30,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
      justifyContent: 'center',
      color: '#FFF',
    },

    title: {
      padding: theme.spacing(2, 2, 2, 4),
    },
  }),
);

export type SurveyCardProps = {
  survey: DashboardSurvey;
  progress: number | null;
  transitionDelay: number;
} & Pick<ButtonBaseProps, 'onClick'>;

export function SurveyCard(props: SurveyCardProps) {
  const { survey, progress, transitionDelay, ...buttonProps } = props;
  const cover = getSrc(survey.cover);
  const classes = useStyles({ enabled: progress !== null, tint: survey.tint });
  const [slideIn, setSlideIn] = React.useState(false);
  React.useEffect(() => {
    setTimeout(() => setSlideIn(true), transitionDelay);
  }, []);

  const inner = (): React.ReactNode => {
    switch (progress) {
      case 1:
        return <CheckedIcon />;
      case null:
        return <LockIcon />;
      default:
        return `${Math.round(progress * 100)}%`;
    }
  };

  return (
    <div className={classes.root}>
      <ButtonBase
        {...buttonProps}
        className={classes.button}
        disabled={progress === null}
        aria-label={survey.title}
        focusRipple>
        <div className={classes.mask}>{cover && <img className={clsx(survey.id, { slideIn })} src={cover} />}</div>
        <section className={classes.content}>
          <Typography variant="subtitle1" color="inherit" align="right">
            {survey.title}
          </Typography>
          <Box mt={1}>
            <Ring percent={progress || 0} size="small">
              {inner()}
            </Ring>
          </Box>
        </section>
      </ButtonBase>
    </div>
  );
}
