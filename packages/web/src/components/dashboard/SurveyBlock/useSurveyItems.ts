import React from 'react';
import { useDispatch } from 'react-redux';
import update from 'immutability-helper';
import { substituteWithUserVocabulary } from 'components/helpers';
import { UserProfile, UserSurvey } from 'app';
import { DashboardSurvey } from '../types';
import { SurveyItemData } from './Surveys';
import { startSurvey } from 'app/profileSlice';

type ItemsAndTotal = {
  total: number;
  items: Array<SurveyItemData>;
};

export function useSurveyItems(surveys: DashboardSurvey[], user: UserProfile, userSurveys: Record<string, UserSurvey>) {
  const dispatch = useDispatch();

  const { items, total } = React.useMemo<ItemsAndTotal>(() => {
    const substitute = substituteWithUserVocabulary(user);
    const getProgress = (userSurvey?: UserSurvey): number | null => {
      if (!userSurvey) return null;
      const { _total = 0, _completed = 0 } = userSurvey;
      return _total > 0 ? _completed / _total : 0;
    };

    // calculate survey section state
    return surveys.reduce<ItemsAndTotal>(
      (surveySectionState, survey) => {
        const item = {
          progress: getProgress(userSurveys[survey.id]),
          survey: substitute(survey, ['title']),
        };
        return update(surveySectionState, {
          total: { $set: surveySectionState.total + (item.progress || 0) },
          items: { $push: [item] },
        });
      },
      {
        total: 0,
        items: [],
      },
    );
  }, [surveys, user, userSurveys]);

  // effect to enable next survey
  React.useEffect(() => {
    const nextOpen = items.find(({ progress }, index) => {
      return progress === null && index > 0 && items[index - 1].progress === 1;
    });

    if (nextOpen) {
      dispatch(startSurvey(nextOpen.survey.id));
    }
  }, [items]);

  return { items, total };
}
