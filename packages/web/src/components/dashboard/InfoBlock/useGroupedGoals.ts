import React from 'react';
import { allGoals } from 'components/content/goals';
import { DashboardTopic } from '..';
import { UserTopic } from 'app';

type UserGoal = {
  topicId: string;
  goalId: string;
  topicLabel: string;
  title: React.ReactNode;
  content: React.ReactNode;
  completed: boolean;
};

type GroupedUserGoals = {
  completed: UserGoal[];
  todo: UserGoal[];
};

export function useGroupedGoals(topics: DashboardTopic[], userTopics: Record<string, Pick<UserTopic, 'goals'>>) {
  const topicTitles = React.useMemo(() => {
    return topics.reduce<Record<string, string>>((titles, topic) => ({ ...titles, [topic.id]: topic.title }), {});
  }, [topics]);

  return React.useMemo(() => {
    return Object.entries(userTopics).reduce<GroupedUserGoals>(
      (grouped, [topicId, userTopic]) => {
        const goals = userTopic.goals;
        const topicLabel = topicTitles[topicId];
        if (!goals) {
          return grouped;
        }
        return Object.entries(goals).reduce<GroupedUserGoals>((grouped, [goalId, completed]) => {
          const goal = allGoals[goalId];
          const userGoal: UserGoal = {
            topicId,
            goalId,
            topicLabel,
            title: goal.title,
            content: goal.content,
            completed,
          };
          return completed
            ? { completed: [...grouped.completed, userGoal], todo: grouped.todo }
            : { completed: grouped.completed, todo: [...grouped.todo, userGoal] };
        }, grouped);
      },
      {
        completed: [],
        todo: [],
      },
    );
  }, [userTopics, topicTitles]);
}
