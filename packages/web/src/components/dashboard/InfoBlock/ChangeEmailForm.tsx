import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Grid, Typography, Snackbar } from '@material-ui/core';
import { selectAuth } from 'app';
import { changeEmail } from 'app/authSlice';
import { WaitButton } from 'components';
import { createValidator, Form, mustBeEmail, mustMatch, TextField } from 'components/form';
import { Card } from './UserSettingsDialog';
import { Alert, AlertTitle } from '@material-ui/lab';

type FormData = {
  email: string;
  verify: string;
};

const validateChangePassword = createValidator<FormData>({
  email: mustBeEmail(),
  verify: mustMatch('email'),
});

export type FormProps = {
  onClose: () => void;
};

export function ChangeEmailForm(props: FormProps) {
  const { onClose } = props;
  const dispatch = useDispatch();
  const { status, userInfo } = useSelector(selectAuth);
  const [warning, setWarning] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (status?.error && !warning) {
      setWarning(true);
    }
  }, [status]);

  const handleSubmit = (data: FormData) => {
    dispatch(changeEmail(data.email, onClose));
  };

  return (
    <Card>
      <Typography variant="h6" color="secondary">
        Change email address
      </Typography>
      <Typography variant="subtitle1" color="textSecondary">
        To change your email, please type in your new email and verify the email.
      </Typography>

      <Form<FormData>
        onSubmit={handleSubmit}
        validate={validateChangePassword}
        initial={{ email: userInfo?.email || '', verify: '' }}>
        <Box mt={2}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Alert severity="info">
                <AlertTitle>Important message!</AlertTitle>
                Changing your email address will affect your sign in. You will receive an email verification link via
                your new email address shortly. Please verify your new email address and use it to sign in next time.
              </Alert>
            </Grid>

            <Grid item xs={12} sm={6}>
              <TextField name="email" variant="filled" label="New email" fullWidth required />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField name="verify" variant="filled" label="Verify your new email" fullWidth required />
            </Grid>
          </Grid>
        </Box>
        <Box mt={2} textAlign="center">
          <WaitButton type="submit" color="primary" variant="outlined" wait={status?.type === 'request'}>
            Submit
          </WaitButton>
        </Box>
      </Form>

      <Snackbar open={warning} autoHideDuration={6000} onClose={() => setWarning(false)}>
        <Alert onClose={() => setWarning(false)} severity="error">
          {status?.error}
        </Alert>
      </Snackbar>
    </Card>
  );
}
