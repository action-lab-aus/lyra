import React from 'react';
import isEmpty from 'lodash/isEmpty';
import { useDispatch, useSelector } from 'react-redux';
import { Avatar, Box, Button, ButtonBase, Paper, Popover, Typography, IconButton } from '@material-ui/core';
import { makeStyles, createStyles, WithStyles, withStyles } from '@material-ui/core/styles';
import WarningIcon from '@material-ui/icons/Warning';
import { cyan, blueGrey, red } from '@material-ui/core/colors';
import { AuthState, RootState, UserProfile, UserTopic } from 'app';
import { updatePhotoURL } from 'app/authSlice';
import { getAvatar, getDisplayName } from 'app/helpers';
import { avatars } from 'components/layout';
import { FacebookButton } from 'components/psg';
import { UserSettingsDialog } from './UserSettingsDialog';
import { EmailVerifyDialog } from './EmailVerifyDialog';
import { MyGoalsDialog } from './MyGoalsDialog';
import { DashboardTopic } from '..';

const primary = cyan[600];

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      backgroundColor: primary,
      color: 'rgba(255,255,255,.9)',
      padding: theme.spacing(4, 2),
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },

    avatarbtn: {
      marginBottom: theme.spacing(2),
      borderRadius: '50%',
      overflow: 'hidden',
    },

    avatar: {
      width: 100,
      height: 100,
      backgroundColor: cyan[400],
    },
  }),
);

export type UserInfoProps = {
  user: UserProfile;
  topics: Array<DashboardTopic>;
  onShowPSG: () => void;
};

export function UserInfo(props: UserInfoProps) {
  const classes = useStyles();
  const { user, onShowPSG, topics } = props;

  const { userInfo, userTopics } = useSelector<
    RootState,
    Pick<AuthState, 'userInfo'> & { userTopics?: Record<string, UserTopic> }
  >((state) => {
    const userInfo = state.auth.userInfo;
    const userTopics = state.profile.data?.topics;
    return { userInfo, userTopics };
  });

  const goalsEnabled = React.useMemo(() => !isEmpty(userTopics), [userTopics]);
  const psgEnabled = React.useMemo(() => user.userCategory === 'parent' && goalsEnabled, [user, goalsEnabled]);

  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const [showSettings, setShowSettings] = React.useState<boolean>(false);
  const [showMyGoals, setShowMyGoals] = React.useState<boolean>(false);
  const [showVerifyEmail, setShowVerifyEmail] = React.useState<boolean>(false);

  const handleSetAvatar = (url: string) => {
    dispatch(updatePhotoURL(url));
    setAnchorEl(null);
  };
  return (
    <Paper className={classes.root}>
      <ButtonBase className={classes.avatarbtn} onClick={(e) => setAnchorEl(e.currentTarget)}>
        <Avatar className={classes.avatar} src={getAvatar(user, userInfo)}></Avatar>
      </ButtonBase>
      <AvatarPopupover anchorEl={anchorEl} onClose={() => setAnchorEl(null)} onChange={handleSetAvatar} />

      <Typography variant="body2" color="inherit" style={{ color: 'rgba(255,255,255,.6)' }} gutterBottom>
        {userInfo?.email}
      </Typography>

      {userInfo?.email && !userInfo?.emailVerified && (
        <IconButton style={{ color: red[800] }} onClick={() => setShowVerifyEmail(true)}>
          <WarningIcon fontSize="large" />
        </IconButton>
      )}

      <Typography variant="subtitle1" color="inherit" gutterBottom>
        {getDisplayName(user)}
      </Typography>
      <Box mt={2} display="flex" flexDirection="column">
        <Button variant="contained" color="primary" onClick={() => setShowMyGoals(true)} style={{ marginBottom: 10 }}>
          My Goals
        </Button>

        <Button variant="contained" color="primary" onClick={() => setShowSettings(true)} style={{ marginBottom: 10 }}>
          Settings
        </Button>

        {psgEnabled && <FacebookButton onClick={onShowPSG}>Go to Facebook community</FacebookButton>}
        {goalsEnabled && (
          <MyGoalsDialog
            open={showMyGoals}
            onClose={() => setShowMyGoals(false)}
            userTopics={userTopics!}
            topics={topics}
          />
        )}
        <UserSettingsDialog user={user} open={showSettings} onClose={() => setShowSettings(false)} />
        <EmailVerifyDialog open={showVerifyEmail} onClose={() => setShowVerifyEmail(false)} />
      </Box>
    </Paper>
  );
}

const AvatarPopupover = withStyles((theme) =>
  createStyles({
    root: {
      padding: theme.spacing(2),
      width: 195,
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
    },

    avatar: {
      width: 48,
      height: 48,
      margin: theme.spacing(1),
      backgroundColor: blueGrey[100],
    },
  }),
)((props: { anchorEl: HTMLElement | null; onClose: () => void; onChange: (url: string) => void } & WithStyles) => {
  const { classes, anchorEl, onClose, onChange } = props;
  return (
    <Popover
      id="avatar-menu"
      open={Boolean(anchorEl)}
      anchorEl={anchorEl}
      onClose={onClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}>
      <div className={classes.root}>
        {[...avatars.female, ...avatars['non-binary'], ...avatars.male].map((url, index) => {
          return (
            <ButtonBase key={index} onClick={() => onChange(url)}>
              <Avatar className={classes.avatar} variant="rounded" src={url}></Avatar>
            </ButtonBase>
          );
        })}
      </div>
    </Popover>
  );
});
