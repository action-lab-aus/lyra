import React from 'react';
import { Box, Button, Grid, Typography } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { createValidator, Form, TextField } from 'components/form';
import { Card } from './UserSettingsDialog';
import { revokeConsent } from 'app/authSlice';
import { useDispatch, useSelector } from 'react-redux';
import { selectProfile } from 'app';
import { WaitButton } from 'components/WaitButton';
import { Link } from 'components/Link';

export type FormProps = {
  onClose: () => void;
};

type FormData = {
  optout: string;
};

const validate = createValidator({
  optout: (value: string) => {
    return value === 'OPTOUT' ? null : 'You must type OPTOUT in this field to confirm your intention';
  },
});

export function RevokeConsentForm(props: FormProps) {
  const dispatch = useDispatch();
  const { status } = useSelector(selectProfile);

  const handleSubmit = () => {
    dispatch(revokeConsent());
  };

  return (
    <Card>
      <Typography variant="h6" color="secondary">
        Opt out
      </Typography>
      <Typography variant="subtitle1" color="textSecondary">
        You always have the right to withdraw from the PiP+ program and research project. If you have any questions or
        feedback, please don't hesitate to get in touch with us first (see <Link href="/contact">Contact Us</Link>).
      </Typography>
      <Form<FormData> onSubmit={handleSubmit} validate={validate} initial={{ optout: '' }}>
        <Box mt={2}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Alert severity="warning">
                <AlertTitle>Warning</AlertTitle>
                By opting out, your PiP+ account will be suspended immediately. You will no longer have access to the
                program, and we won't contact you again. If you prefer to retain access to the program, but receive
                fewer emails or cancel SMS notifications, please contact us so we can arrange this. If you wish to
                continue, please type OPTOUT in capital letters in the box below.
              </Alert>
            </Grid>
            <Grid item xs={12}>
              <TextField name="optout" variant="filled" label="OPTOUT" fullWidth required />
            </Grid>
          </Grid>
        </Box>

        <Box mt={2} textAlign="center">
          <WaitButton type="submit" color="primary" variant="contained" wait={status?.type === 'request'}>
            OPT OUT
          </WaitButton>
        </Box>
      </Form>
    </Card>
  );
}
