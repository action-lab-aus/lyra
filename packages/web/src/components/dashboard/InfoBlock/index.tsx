import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { UserProfile, UserSurvey, UserTopic } from 'app';
import { DashboardSurvey, DashboardTopic } from '../types';
import { UserInfo, UserInfoProps } from './UserInfo';
import { Summary } from './Summary';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      margin: theme.spacing(4, 0),
    },
    gridItem: {
      display: 'flex',
    },
  }),
);

export type InfoBlockProps = {
  user: UserProfile;
  userTopics: Record<string, UserTopic>;
  userSurveys: Record<string, UserSurvey>;
  topics: Array<DashboardTopic>;
  baseline: Array<DashboardSurvey>;
  followup: Array<DashboardSurvey>;
} & Pick<UserInfoProps, 'onShowPSG'>;

export function InfoBlock(props: InfoBlockProps) {
  const classes = useStyles();
  return (
    <section className={classes.root} role="presentation" aria-label="info">
      <Typography variant="subtitle1" color="textSecondary" gutterBottom>
        Progress summary
      </Typography>
      <Grid container spacing={2} justify="center">
        <Grid item xs={12} md={4} className={classes.gridItem}>
          <UserInfo user={props.user} onShowPSG={props.onShowPSG} topics={props.topics} />
        </Grid>
        <Grid item xs={12} md={8} className={classes.gridItem}>
          <Summary {...props} />
        </Grid>
      </Grid>
    </section>
  );
}
