import React from 'react';
import clsx from 'clsx';
import {
  Stepper,
  Step,
  StepLabel,
  StepIconProps,
  Avatar,
  StepConnector as MuiStepConnector,
  Box,
  Typography,
  Grid,
  Hidden,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { createStyles, makeStyles, styled, withStyles, WithStyles } from '@material-ui/core/styles';
import cyan from '@material-ui/core/colors/cyan';
import blueGrey from '@material-ui/core/colors/blueGrey';
import pink from '@material-ui/core/colors/pink';
import CheckedIcon from '@material-ui/icons/Check';
import { Progress, UserProfile, UserSurvey, UserTopic } from 'app';
import { Ring } from 'components';
import { DashboardSurvey, DashboardTopic } from '../types';
import { headings } from '../index';
import { useSurveyProgress, useTopicProgress } from './hooks';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      position: 'relative',
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      overflow: 'hidden',
      borderRadius: theme.shape.borderRadius,
      boxShadow: theme.shadows[1],
      backgroundColor: '#FFF',
    },
    stepLabel: {
      [theme.breakpoints.down('xs')]: {
        fontSize: '0.72rem',
      },
    },
  }),
);

const stages = ['survey', 'topic', 'followup'];

export type SummaryProps = {
  user: UserProfile;
  userTopics: Record<string, UserTopic>;
  userSurveys: Record<string, UserSurvey>;
  topics: Array<DashboardTopic>;
  baseline: Array<DashboardSurvey>;
  followup: Array<DashboardSurvey>;
};
export function Summary(props: SummaryProps) {
  const classes = useStyles();
  const { user, userSurveys, userTopics, baseline, followup, topics } = props;
  const baselineProgress = useSurveyProgress(baseline, userSurveys);
  const followupProgress = useSurveyProgress(followup, userSurveys);
  const topicProgress = useTopicProgress(topics, userTopics);

  return (
    <section className={classes.root}>
      <nav aria-label="steps">
        <Stepper
          activeStep={user.currentStage && stages.indexOf(user.currentStage)}
          connector={<StepConnector />}
          alternativeLabel>
          <Step key={0}>
            <StepLabel StepIconComponent={StepIcon} classes={{ label: classes.stepLabel }}>
              {headings.survey}
            </StepLabel>
          </Step>
          <Step key={1}>
            <StepLabel StepIconComponent={StepIcon} classes={{ label: classes.stepLabel }}>
              {headings.topic}{' '}
            </StepLabel>
          </Step>
          <Step key={2}>
            <StepLabel StepIconComponent={StepIcon} classes={{ label: classes.stepLabel }}>
              {headings.followup}{' '}
            </StepLabel>
          </Step>
        </Stepper>
      </nav>

      <Box flexGrow={1} bgcolor="#FFF" p={2}>
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <RingColumn label="Baseline assessment" progress={baselineProgress} />
          </Grid>
          <Grid item xs={4}>
            <RingColumn label="Parenting program" progress={topicProgress} />
          </Grid>
          <Grid item xs={4}>
            <RingColumn label="Follow-up assessment" progress={followupProgress} />
          </Grid>
        </Grid>
      </Box>
      <Hidden smDown>
        <Box height={60} bgcolor={blueGrey[500]} color="rgba(255,255,255,.9)" px={2} py={1} justifySelf="flex-end">
          <Typography variant="subtitle1" color="inherit">
            Your progress summary
          </Typography>
          <Typography variant="body2" style={{ color: 'rgba(255,255,255,.7)' }}>
            Get to know the road map of your program. Please check back from time to time.
          </Typography>
        </Box>
      </Hidden>
    </section>
  );
}

const StepIcon = withStyles((theme) =>
  createStyles({
    root: {},
    active: {
      color: 'rgba(255,255,255,.9)',
      backgroundColor: pink[600],
    },
    completed: {
      backgroundColor: cyan[600],
      color: '#FFF',
    },
  }),
)((props: StepIconProps & WithStyles<string>) => {
  const { icon, active, completed, classes } = props;
  return (
    <Avatar
      className={clsx(classes.root, {
        [classes.active as string]: active,
        [classes.completed as string]: completed,
      })}>
      {completed ? <CheckedIcon /> : icon}
    </Avatar>
  );
});

const StepConnector = styled(MuiStepConnector)({
  top: 20,
  left: 'calc(-50% + 30px)',
  right: 'calc(50% + 30px)',
});

const RingColumn = withStyles((theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    label: {
      marginTop: theme.spacing(2),
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
  }),
)((props: { label: string; progress: Progress } & WithStyles<string>) => {
  const { classes, label, progress } = props;
  const { total, completed } = progress;
  const percent = total === 0 ? 0 : Math.round((100 * completed) / total);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('xs'));

  return (
    <div className={classes.root}>
      <Ring percent={percent / 100} size={matches ? 'medium' : 'large'} trackColor={blueGrey[50]}>
        <Box display="flex" flexDirection="column" alignItems="center">
          <Typography variant="body1">{percent}%</Typography>
          <Typography variant="caption" color="textSecondary">
            {completed}/{total}
          </Typography>
        </Box>
      </Ring>

      <Typography className={classes.label} variant="subtitle1" color="textSecondary">
        {label}
      </Typography>
    </div>
  );
});
