import React from 'react';
import { Story, Meta } from '@storybook/react';
import { MyGoalsDialog, MyGoalsProps } from './MyGoalsDialog';
import { AppProvider } from 'components/helpers';
import { topics } from '../../helpers/mocks';

export default {
  title: 'components/dashboard/InfoBlock/MyGoalsDialog',
  component: MyGoalsDialog,
} as Meta;

const Template: Story<MyGoalsProps> = (args) => {
  return (
    <AppProvider>
      <MyGoalsDialog {...args} open={true} topics={topics} />;
    </AppProvider>
  );
};

export const Example = Template.bind({});
Example.args = {
  userTopics: {
    m1: {
      goals: {
        'm1-connect-goal1': true,
      },
    },
    m3: {
      goals: {
        'm3-family-rules-goal1': false,
      },
    },
    m4: {
      goals: {
        'm4-nurture-goal2': false,
      },
    },
  },
};

export const LenthyDialogContent = Template.bind({});
LenthyDialogContent.args = {
  userTopics: {
    m1: {
      goals: {
        'm1-connect-goal1': true,
        'm1-connect-goal2': true,
        'm1-connect-goal3': true,
        'm1-connect-goal4': true,
      },
    },
    m3: {
      goals: {
        'm3-family-rules-goal1': false,
        'm3-family-rules-goal2': false,
        'm3-family-rules-goal3': false,
        'm3-family-rules-goal4': false,
      },
    },
    m4: {
      goals: {
        'm4-nurture-goal2': false,
      },
    },
  },
};
