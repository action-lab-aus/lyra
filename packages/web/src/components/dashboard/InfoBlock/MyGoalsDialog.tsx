import React from 'react';
import { useDispatch } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import {
  Box,
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogProps,
  DialogTitle,
  Divider,
  List,
  ListItem,
  Typography,
} from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import CheckBoxIcon from '@material-ui/icons/CheckBoxOutlined';
import { UserTopic } from 'app';
import { updateUserTopic } from 'app/profileSlice';
import { DashboardTopic } from '../types';
import { useGroupedGoals } from './useGroupedGoals';

const useStyles = makeStyles(
  createStyles({
    listItemTextWithAction: {
      paddingRight: '120px',
    },
  }),
);

export type MyGoalsProps = Pick<DialogProps, 'open' | 'onClose'> & {
  topics: Array<DashboardTopic>;
  userTopics: Record<string, Pick<UserTopic, 'goals'>>;
};

export function MyGoalsDialog(props: MyGoalsProps) {
  const { open, onClose, userTopics, topics } = props;
  const classes = useStyles();
  const dispatch = useDispatch();
  const { completed, todo } = useGroupedGoals(topics, userTopics);
  const markCompleted = (topicId: string, goalId: string) => {
    const userGoals = userTopics[topicId].goals;
    dispatch(updateUserTopic(topicId, { goals: { ...userGoals, [goalId]: true } }));
  };

  const hasTodo = !isEmpty(todo);
  const hasCompleted = !isEmpty(completed);

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>My Goals</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {hasTodo || hasCompleted
            ? "Below are a list of goals you have selected. You can mark your goals as complete any time. Once you've completed a goal, you may return to the module and select another goal if you wish. We suggest you focus on one goal per week."
            : "You haven't selected any goals yet. As you work through each module, you'll be asked to select a goal to work on. Your goals will display here."}
        </DialogContentText>

        {hasTodo && (
          <List>
            {todo.map((userGoal) => {
              return (
                <ListItem key={userGoal.goalId} classes={{ secondaryAction: classes.listItemTextWithAction }}>
                  <Box>
                    <Typography variant="subtitle1">
                      {userGoal.title} <Chip label={userGoal.topicLabel} size="small" />
                    </Typography>
                    <Typography variant="body2" color="textSecondary" gutterBottom>
                      {userGoal.content}
                    </Typography>
                    <Button
                      variant="contained"
                      color="primary"
                      size="small"
                      onClick={() => markCompleted(userGoal.topicId, userGoal.goalId)}>
                      Mark as complete
                    </Button>
                  </Box>
                </ListItem>
              );
            })}
          </List>
        )}

        {hasCompleted && (
          <>
            <Divider />
            <Box mt={2}>
              <Typography variant="h6" color="textSecondary">
                Completed Goals
              </Typography>
              <List>
                {completed.map((userGoal) => {
                  return (
                    <ListItem key={userGoal.goalId}>
                      <Box display="flex">
                        <CheckBoxIcon color="primary" />
                        <Box ml={2}>
                          <Typography variant="subtitle1">
                            {userGoal.title} <Chip label={userGoal.topicLabel} size="small" />
                          </Typography>
                          <Typography variant="body2" color="textSecondary" gutterBottom>
                            {userGoal.content}
                          </Typography>
                        </Box>
                      </Box>
                    </ListItem>
                  );
                })}
              </List>
            </Box>
          </>
        )}
      </DialogContent>
      {onClose && (
        <DialogActions>
          <Button onClick={(e) => onClose(e, 'escapeKeyDown')}>Close</Button>
        </DialogActions>
      )}
    </Dialog>
  );
}
