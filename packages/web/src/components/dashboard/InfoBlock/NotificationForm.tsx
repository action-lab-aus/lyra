import React from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import { Form } from 'components/form';
import { Card } from './UserSettingsDialog';
import { updateProfile } from 'app/profileSlice';
import { useDispatch, useSelector } from 'react-redux';
import { selectProfile, UserProfile } from 'app';
import { WaitButton } from 'components/WaitButton';
import { Checkbox } from '../../questions/Checkbox';

export type FormProps = {
  user: UserProfile;
  onClose: () => void;
};

type FormData = Pick<UserProfile, 'smsNotification'>;

export function NotificationForm(props: FormProps) {
  const { user, onClose } = props;
  const dispatch = useDispatch();
  const { status } = useSelector(selectProfile);
  const [smsNotification, setSmsNotification] = React.useState(user.smsNotification);

  const onChange = (value: boolean) => {
    setSmsNotification(value);
  };

  const handleSubmit = () => {
    const user = { smsNotification };
    dispatch(updateProfile(user, onClose));
  };

  return (
    <Card>
      <Typography variant="h6" color="secondary">
        Manage notifications
      </Typography>
      <Typography variant="subtitle1" color="textSecondary">
        You'll get email alerts when you have new module or survey available. If you've provided a mobile number, we'll
        also send you SMS notifications. Parents who have completed PiP in the past have found these helpful. If you
        prefer <i>not</i> to receive SMS, please update your preference below.
      </Typography>
      <Form<FormData> onSubmit={handleSubmit} initial={{ smsNotification }}>
        <Box mt={2}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Checkbox
                id="emailNotification"
                value={true}
                label="Receive Email Notifications"
                disabled={true}
                onChange={(value) => onChange(value)}
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              <Checkbox
                id="smsNotification"
                value={smsNotification}
                label="Receive SMS Notifications"
                disabled={false}
                onChange={(value) => onChange(value)}
              />
            </Grid>
          </Grid>
        </Box>

        <Box mt={2} textAlign="center">
          <WaitButton type="submit" color="primary" variant="outlined" wait={status?.type === 'request'}>
            Update
          </WaitButton>
        </Box>
      </Form>
    </Card>
  );
}
