import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Grid, Typography } from '@material-ui/core';
import { selectProfile, UserProfile } from 'app';
import { updateProfile } from 'app/profileSlice';
import { WaitButton } from 'components';
import { createValidator, Form, TextField, Select, notEmpty, mustBeAuNumber } from 'components/form';
import { Card } from './UserSettingsDialog';

type FormData = Pick<
  UserProfile,
  'userFirstname' | 'userSurname' | 'userPhone' | 'userPhoneAlt' | 'userGender' | 'childName' | 'childGender'
>;

const validate = createValidator<FormData>({
  userFirstname: notEmpty(),
  userSurname: notEmpty(),
  userGender: notEmpty('Please select one'),
  userPhone: mustBeAuNumber(),
  userPhoneAlt: mustBeAuNumber(),
  childName: notEmpty(),
  childGender: notEmpty('Please select one'),
});

const genderOptions = [
  {
    label: 'Male',
    value: 'male',
  },
  {
    label: 'Female',
    value: 'female',
  },
  {
    label: 'Non-binary/gender diverse',
    value: 'non-binary',
  },
];

export type FormProps = {
  user: UserProfile;
  onClose: () => void;
};

export function UserProfileForm(props: FormProps) {
  const { user, onClose } = props;
  const dispatch = useDispatch();
  const { userFirstname, userSurname, userPhone, userPhoneAlt, userGender, childName, childGender } = user;
  const { status } = useSelector(selectProfile);

  const handleSubmit = (data: FormData) => {
    const user = Object.entries(data).reduce<Partial<UserProfile>>((user, [key, value]) => {
      return { ...user, [key]: value === undefined ? null : value };
    }, {});
    dispatch(updateProfile(user, onClose));
  };

  return (
    <Card>
      <Typography variant="h6" color="secondary">
        Update user profile
      </Typography>
      <Typography variant="subtitle1" color="textSecondary">
        You can update your profile details.
      </Typography>
      <Form<FormData>
        onSubmit={handleSubmit}
        validate={validate}
        initial={{ userFirstname, userSurname, userPhone, userPhoneAlt, userGender, childName, childGender }}>
        <Box mt={2}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={4}>
              <TextField name="userFirstname" variant="filled" label="Firstname" fullWidth required />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField name="userSurname" variant="filled" label="Surname" fullWidth required />
            </Grid>
            <Grid item xs={12} sm={4}>
              <Select name="userGender" variant="filled" label="Gender" options={genderOptions} fullWidth required />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                name="userPhone"
                variant="filled"
                label="Prefered phone"
                fullWidth
                required
                helperText="A valid Australia mobile number"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField name="userPhoneAlt" variant="filled" label="Alternative phone" fullWidth />
            </Grid>

            <Grid item xs={12} sm={6}>
              <TextField name="childName" variant="filled" label="Child name" fullWidth required />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Select
                name="childGender"
                variant="filled"
                label="Child Gender"
                options={genderOptions}
                fullWidth
                required
              />
            </Grid>
          </Grid>
        </Box>

        <Box mt={2} display="flex" justifyContent="center">
          <WaitButton type="submit" color="primary" variant="outlined" wait={status?.type === 'request'}>
            Update
          </WaitButton>
        </Box>
      </Form>
    </Card>
  );
}
