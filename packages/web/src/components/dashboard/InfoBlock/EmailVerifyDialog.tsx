import React from 'react';
import { Dialog, DialogProps, Slide } from '@material-ui/core';
import { TransitionProps } from '@material-ui/core/transitions';
import { VerifyEmailForm } from './VerifyEmailForm';

const Transition = React.forwardRef(
  (props: { children?: React.ReactElement } & TransitionProps, ref: React.Ref<unknown>) => (
    <Slide direction="up" ref={ref} {...props} />
  ),
);

export type UserSettingsProps = {
  onClose: () => void;
} & Pick<DialogProps, 'open'>;

export function EmailVerifyDialog(props: UserSettingsProps) {
  const { open, onClose } = props;

  return (
    <Dialog open={open} onClose={onClose} TransitionComponent={Transition}>
      <VerifyEmailForm onClose={onClose} />
    </Dialog>
  );
}
