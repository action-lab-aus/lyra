import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Grid, Typography } from '@material-ui/core';
import { selectAuth } from 'app';
import { changePassword } from 'app/authSlice';
import { WaitButton } from 'components';
import { createValidator, Form, mustMatch, passwordPolicy, TextField } from 'components/form';
import { Card } from './UserSettingsDialog';

type FormData = {
  password: string;
  verify: string;
};

const validateChangePassword = createValidator<FormData>({
  password: passwordPolicy(),
  verify: mustMatch('password'),
});

export type FormProps = {
  onClose: () => void;
};

export function ChangePasswordForm(props: FormProps) {
  const { onClose } = props;
  const dispatch = useDispatch();
  const { status } = useSelector(selectAuth);

  const handleSubmit = (data: FormData) => {
    dispatch(changePassword(data.password, onClose));
  };

  return (
    <Card>
      <Typography variant="h6" color="secondary">
        Change password
      </Typography>
      <Typography variant="subtitle1" color="textSecondary">
        To change your password, please type in your new password and verify the password.
      </Typography>

      <Form<FormData> onSubmit={handleSubmit} validate={validateChangePassword} initial={{ password: '', verify: '' }}>
        <Box mt={2}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField type="password" name="password" variant="filled" label="New password" fullWidth required />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                type="password"
                name="verify"
                variant="filled"
                label="Verify your new password"
                fullWidth
                required
              />
            </Grid>
          </Grid>
        </Box>
        <Box mt={2} textAlign="center">
          <WaitButton type="submit" color="primary" variant="outlined" wait={status?.type === 'request'}>
            Submit
          </WaitButton>
        </Box>
      </Form>
    </Card>
  );
}
