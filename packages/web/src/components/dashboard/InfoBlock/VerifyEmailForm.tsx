import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Grid, Typography } from '@material-ui/core';
import { selectAuth } from 'app';
import { verifyEmail } from 'app/authSlice';
import { WaitButton } from 'components';
import { Alert, AlertTitle } from '@material-ui/lab';

export type FormProps = {
  onClose: () => void;
};

export function VerifyEmailForm(props: FormProps) {
  const { onClose } = props;
  const dispatch = useDispatch();

  const { status, userInfo } = useSelector(selectAuth);

  const handleSubmit = () => {
    if (userInfo?.email) dispatch(verifyEmail(userInfo?.email, onClose));
  };

  return (
    <Box my={4} mx={3}>
      <Typography variant="h6" color="secondary">
        Verify email address
      </Typography>

      <Typography variant="subtitle1" color="textSecondary">
        Your email address has <strong>NOT</strong> been verified yet. Please verify your email to continue accessing
        the PiP+ program.
      </Typography>

      <Box mt={2}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Alert severity="info">
              <AlertTitle>Can't find the verification email?</AlertTitle>
              If you can't find the email we sent you when you first signed up, try checking your junk or spam mail.
              Alternatively, you can click the button below to send a new email.
            </Alert>
          </Grid>
        </Grid>
      </Box>

      <Box mt={2} display="flex" justifyContent="flex-end">
        <WaitButton
          type="submit"
          color="primary"
          variant="outlined"
          onClick={handleSubmit}
          wait={status?.type === 'request'}>
          Verify
        </WaitButton>
      </Box>
    </Box>
  );
}
