import React from 'react';
import { Progress, UserSurvey, UserTopic } from 'app';
import { DashboardSurvey, DashboardTopic } from '../types';
import { byCond } from 'components/questions';

export function useTopicProgress(topics: Array<DashboardTopic>, userTopics: Record<string, UserTopic>): Progress {
  return React.useMemo(() => {
    return topics.reduce<Progress>(
      (progress, topic) => {
        const userTopic = userTopics[topic.id];
        if (!userTopic?.mandatory) {
          return progress;
        }

        const topicTotal = topic.nav.sequence.reduce<number>((total, entry) => {
          if (entry.optional) {
            return total;
          } else {
            // visited + activities
            return total + (entry.activityKeys ? entry.activityKeys.length + 1 : 1);
          }
        }, 1);

        return {
          total: progress.total + topicTotal,
          completed: progress.completed + (userTopic?.progress?.completed || 0),
        };
      },
      {
        total: 0,
        completed: 0,
      },
    );
  }, [topics, userTopics]);
}

export function useSurveyProgress(surveys: Array<DashboardSurvey>, userSurveys: Record<string, UserSurvey>): Progress {
  return React.useMemo(() => {
    return surveys.reduce<Progress>(
      (progress, survey) => {
        const userSurvey = userSurveys[survey.id];
        const total = survey.sections.reduce<number>((total, section) => {
          const questions = userSurvey ? section.questions.filter(byCond(userSurvey)) : section.questions;
          return questions.reduce((total, question) => {
            const questionRequired = Boolean(question.required === null ? section.required : question.required);
            const required = question.type === 'paragraph' ? false : questionRequired;
            return required ? total + 1 : total;
          }, total);
        }, progress.total);

        return {
          total,
          completed: progress.completed + (userSurvey?._completed || 0),
        };
      },
      {
        total: 0,
        completed: 0,
      },
    );
  }, [surveys, userSurveys]);
}
