import React from 'react';
import {
  AppBar,
  Grid,
  Container,
  Dialog,
  DialogProps,
  IconButton,
  Slide,
  Toolbar,
  Typography,
  Paper,
  Button,
} from '@material-ui/core';
import { TransitionProps } from '@material-ui/core/transitions';
import { createStyles, makeStyles, styled } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import { UserProfile } from 'app';
import { ChangePasswordForm } from './ChangePasswordForm';
import { UserProfileForm } from './UserProfileForm';
import { RevokeConsentForm } from './RevokeConsentForm';
import { ChangeEmailForm } from './ChangeEmailForm';
import { NotificationForm } from './NotificationForm';

const useStyles = makeStyles((theme) =>
  createStyles({
    appbar: {
      position: 'relative',
      marginBottom: theme.spacing(2),
    },
    toolbar: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    paper: {
      backgroundColor: theme.palette.background.default,
    },
    article: {
      paddingBottom: theme.spacing(8),
    },
  }),
);

const Transition = React.forwardRef(
  (props: { children?: React.ReactElement } & TransitionProps, ref: React.Ref<unknown>) => (
    <Slide direction="up" ref={ref} {...props} />
  ),
);

export type UserSettingsProps = {
  user: UserProfile;
  onClose: () => void;
} & Pick<DialogProps, 'open'>;

export function UserSettingsDialog(props: UserSettingsProps) {
  const { open, onClose } = props;
  const { user } = props;
  const classes = useStyles();

  return (
    <Dialog
      open={open}
      onClose={onClose}
      TransitionComponent={Transition}
      classes={{ paperFullScreen: classes.paper }}
      fullScreen>
      <AppBar>
        <Toolbar className={classes.toolbar}>
          <Typography variant="h6">User Settings</Typography>
          <IconButton onClick={onClose} edge="end" color="inherit" aria-label="close">
            <CloseIcon />
          </IconButton>
        </Toolbar>
      </AppBar>

      <article className={classes.article}>
        <Toolbar />
        <Container maxWidth="md">
          <UserProfileForm user={user} onClose={onClose} />
          <ChangeEmailForm onClose={onClose} />
          <ChangePasswordForm onClose={onClose} />
          <NotificationForm user={user} onClose={onClose} />
          <RevokeConsentForm onClose={onClose} />

          <Grid item xs={12} container justify="flex-end">
            <Button onClick={onClose} variant="contained" color="primary">
              Close
            </Button>
          </Grid>
        </Container>
      </article>
    </Dialog>
  );
}

export const Card = styled(Paper)(({ theme }) => ({
  padding: theme.spacing(2),
  margin: theme.spacing(2, 0),
}));
