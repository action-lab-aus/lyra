import React from 'react';
import { Paper, Grid, Typography, Box } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
  createStyles({
    paper: {
      display: 'block',
      padding: theme.spacing(2),
      margin: theme.spacing(6, 0),
    },
    imgContainer: {
      overflow: 'hidden',
      '& > img': {
        width: '100%',
        height: '100%',
      },
    },
    box: {
      display: 'flex',
      alignItems: 'flex-end',
      flexDirection: 'column',
      padding: theme.spacing(4),
      [theme.breakpoints.down('xs')]: {
        padding: theme.spacing(1),
      },
    },
  }),
);

export type WelcomeProps = {
  title: string;
  img: string;
  children: React.ReactNode;
};

export function Welcome(props: WelcomeProps) {
  const { title, img, children } = props;

  const classes = useStyles();
  return (
    <React.Fragment>
      <div id="topicCard" />
      <Paper className={classes.paper} role="banner" aria-label={title}>
        <Typography variant="h6" gutterBottom color="secondary">
          {title}
        </Typography>
        <Grid container spacing={4} justify="center" alignItems="center">
          <Grid item xs={12} sm={5} md={6} className={classes.imgContainer}>
            <img src={img} alt={title} />
          </Grid>
          <Grid item xs={12} sm={7} md={6}>
            <Box className={classes.box}>{children}</Box>
          </Grid>
        </Grid>
      </Paper>
    </React.Fragment>
  );
}
