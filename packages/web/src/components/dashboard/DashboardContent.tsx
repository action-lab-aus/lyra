import React from 'react';
import { useSelector } from 'react-redux';
import { Typography, Box, Breadcrumbs, Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import blueGrey from '@material-ui/core/colors/blueGrey';
import { Link } from 'components';
import { AppPage } from 'components/layout';
import { RootState, useProfile } from 'app';
import { surveysCompleted } from 'app/helpers';
import { DashboardTopic, DashboardSurvey } from './types';
import { InfoBlock } from './InfoBlock';
import { TopicBlock } from './TopicBlock';
import { BaselineBlock, FollowupBlock } from './SurveyBlock';
import { PSGDialog } from 'components/psg';

export function DashboardContent(props: { surveys: DashboardSurvey[]; topics: DashboardTopic[] }) {
  const { topics } = props;
  const [baseline, followup] = React.useMemo(() => {
    return props.surveys.reduce<[DashboardSurvey[], DashboardSurvey[]]>(
      (results, survey) => {
        const [baseline, followup] = results;
        return survey.scope === 'baseline' ? [[...baseline, survey], followup] : [baseline, [...followup, survey]];
      },
      [[], []],
    );
  }, [props.surveys]);

  const profile = useProfile();
  const uid = useSelector<RootState, string>((state) => state.auth.userInfo?.uid!);
  const [warning, setWarning] = React.useState<boolean>(false);
  const [showPsg, setShowPsg] = React.useState<boolean>(false);

  // effect to show warning
  React.useEffect(() => {
    if (profile.status?.error && !warning) {
      setWarning(true);
    }
  }, [profile.status]);

  // no data
  if (profile.data === null) {
    return null;
  }

  const { user, surveys: userSurveys, topics: userTopics } = profile.data;
  const baselineCompleted = surveysCompleted(
    baseline.map((b) => b.id),
    userSurveys,
  );

  return (
    <AppPage title="Dashboard" bgcolor={blueGrey[50]}>
      <Box mt={2}>
        <Breadcrumbs aria-label="breadcrumb">
          <Link color="inherit" href="/">
            Home
          </Link>
          <Typography color="textPrimary">Dashboard</Typography>
        </Breadcrumbs>

        <InfoBlock
          topics={topics}
          baseline={baseline}
          followup={followup}
          user={user}
          userTopics={userTopics}
          userSurveys={userSurveys}
          onShowPSG={() => setShowPsg(true)}
        />

        <BaselineBlock baseline={baseline} user={user} userSurveys={userSurveys} />

        <TopicBlock
          topics={topics}
          user={user}
          userSurveys={userSurveys}
          userTopics={userTopics}
          baselineCompleted={baselineCompleted}
          onShowPSG={() => setShowPsg(true)}
        />

        <FollowupBlock followup={followup} user={user} userSurveys={userSurveys} />

        <Snackbar open={warning} autoHideDuration={6000} onClose={() => setWarning(false)}>
          <Alert onClose={() => setWarning(false)} severity="error">
            {profile.status?.error}
          </Alert>
        </Snackbar>
        <PSGDialog open={showPsg} onClose={() => setShowPsg(false)} code={uid.slice(-6)} />
      </Box>
    </AppPage>
  );
}

export default DashboardContent;
