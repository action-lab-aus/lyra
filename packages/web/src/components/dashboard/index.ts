export { default as DashboardContent } from './DashboardContent';

export * from './types';

export const headings = {
  survey: 'Baseline assessment and feedback',
  topic: 'Personalised parenting program',
  followup: 'Follow-up assessment and feedback',
};
