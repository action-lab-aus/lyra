import React from 'react';
import { getAllByRole, getByRole, render, screen } from '@testing-library/react';
import { RootState } from 'app';
import { AppProvider } from 'components/helpers';
import * as mocks from 'components/helpers/mocks';
import { DashboardContent } from '../DashboardContent';
import { DashboardSurvey } from '../types';

const Dashboard = (props: { state: RootState }) => {
  return (
    <AppProvider state={props.state}>
      <DashboardContent topics={mocks.topics} surveys={mocks.dashboardSurveys} />
    </AppProvider>
  );
};

describe('DashboardContent', () => {
  test('when fetching user profile', () => {
    const state = mocks
      .rootState()
      .authenticated()
      .profile({
        fetched: false,
        status: { type: 'request' },
        data: null,
      }).state;
    render(<Dashboard state={state} />);
  });

  test('when visit for the first time with empty state', () => {
    const state = mocks
      .rootState()
      .authenticated()
      .profile({
        fetched: true,
        data: {
          user: mocks.templates.user(),
          topics: {},
          surveys: {},
        },
      }).state;

    render(<Dashboard state={state} />);

    const summary = screen.getByRole('navigation', { name: 'steps' });
    const surveyWelcome = screen.getByRole('banner', { name: 'Baseline assessment and feedback' });
    const topicsWelcome = screen.getByRole('banner', { name: 'Personalised parenting program' });
    const followupWelcome = screen.getByRole('banner', { name: 'Follow-up assessment and feedback' });

    expect(summary).toBeInTheDocument();
    expect(surveyWelcome).toBeInTheDocument();

    expect(getByRole(surveyWelcome, 'button', { name: 'Start' })).toBeEnabled();
    expect(topicsWelcome).toBeInTheDocument();
    expect(getByRole(topicsWelcome, 'button', { name: 'Start' })).toBeDisabled();
    expect(followupWelcome).toBeInTheDocument();
  });

  describe('when survey', () => {
    let state: RootState;
    beforeEach(() => {
      state = mocks
        .rootState()
        .authenticated()
        .profile({
          fetched: true,
          data: {
            user: mocks.templates.user({
              currentStage: 'survey',
            }),
            surveys: {
              's1-confidence': { _total: 0, _completed: 0, _step: 0 },
            },
            topics: {},
          },
        }).state;
    });

    test('just started', () => {
      render(<Dashboard state={state} />);
      const { user } = state.profile.data!;

      // assert summary
      const summary = screen.getByRole('navigation', { name: 'steps' });
      expect(summary).toBeInTheDocument();

      // assert surveys
      const surveysWelcome = screen.queryByRole('banner', { name: 'Baseline assessment and feedback' });
      const surveys = screen.getByRole('presentation', { name: 'surveys' });
      const surveyList = getByRole(surveys, 'list');
      expect(surveysWelcome).not.toBeInTheDocument();
      expect(surveys).toBeInTheDocument();
      expect(getByRole(surveys, 'button', { name: 'Start' })).toBeEnabled();
      expect(getAllByRole(surveyList, 'listItem').length).toEqual(4);
      expect(getByRole(surveyList, 'button', { name: 'Your parenting confidence' })).toBeEnabled();
      expect(getByRole(surveyList, 'button', { name: 'Your parenting' })).toBeDisabled();
      expect(getByRole(surveyList, 'button', { name: 'Your mental wellbeing' })).toBeDisabled();
      expect(getByRole(surveyList, 'button', { name: `${user?.childName}'s mental wellbeing` })).toBeDisabled();

      // assert topics
      const topicsWelcome = screen.getByRole('banner', { name: 'Personalised parenting program' });
      expect(topicsWelcome).toBeInTheDocument();
      expect(getByRole(topicsWelcome, 'button', { name: 'Start' })).toBeDisabled();

      //assert follow ups
      const followupWelcome = screen.getByRole('banner', { name: 'Follow-up assessment and feedback' });
      expect(followupWelcome).toBeInTheDocument();
    });

    test('in progress', () => {});

    test('completed', () => {});
  });

  describe('when topic', () => {
    let state: RootState;
    beforeEach(() => {
      state = mocks
        .rootState()
        .authenticated()
        .profile({
          fetched: true,
          data: {
            user: mocks.templates.user({
              currentStage: 'topic',
              topicSuggestion: ['m1', 'm2', 'm3'],
            }),
            surveys: {
              's1-confidence': mocks.templates.userSurveys.s1Confidence(),
              's2-pradas': mocks.templates.userSurveys.s2Pradas(),
              's3-k6': mocks.templates.userSurveys.s3K6(),
              's4-rcads': mocks.templates.userSurveys.s4Rcads(),
            },
            topics: {},
          },
        }).state;
    });

    test('in planning stage', () => {});
    test('just started', () => {});
    test('in progress', () => {});
  });
});
