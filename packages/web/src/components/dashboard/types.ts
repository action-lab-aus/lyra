import { FileNode } from 'gatsby-plugin-image/dist/src/components/hooks';

type QuestionType = 'rating' | 'text' | 'select' | 'radio' | 'paragraph';

export type DashboardTopic = {
  id: string;
  name: string;
  path: string;
  title: string;
  desc: string;
  cover: FileNode | null;
  nav: {
    defaultEntry: {
      path: string;
    };
    sequence: Array<{
      key: string;
      optional: boolean;
      activityKeys: string[];
    }>;
  };
};

export type DashboardQuestionItem = {
  key: string;
  type: QuestionType;
  required: boolean | null;
  cond: string | null;
};

export type DashboardSurvey = {
  id: string;
  title: string;
  scope: 'baseline' | 'followup';
  cover: FileNode;
  tint: string;
  sections: Array<{
    key: string;
    type: QuestionType;
    required: boolean | null;
    questions: Array<DashboardQuestionItem>;
  }>;
};
