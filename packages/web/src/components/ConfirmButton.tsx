import React from 'react';
import {
  Dialog,
  DialogContentText,
  Button,
  DialogTitle,
  DialogContent,
  DialogActions,
  ButtonProps,
} from '@material-ui/core';

export type ConfirmButtonProps = {
  title?: string;
  children: React.ReactNode;
  onConfirm: () => void;
} & Pick<ButtonProps, 'disabled'>;

export function ConfirmButton(props: ConfirmButtonProps) {
  const { title, children, onConfirm, disabled } = props;
  const [open, setOpen] = React.useState(false);
  const handleClose = (confirmed?: boolean) => {
    setOpen(false);
    if (confirmed) {
      onConfirm();
    }
  };

  return (
    <>
      <Button variant="contained" color="secondary" onClick={() => setOpen(true)} disabled={disabled}>
        {title || 'Confirm'}
      </Button>
      <Dialog onClose={() => handleClose()} open={open}>
        <DialogTitle>Please confirm</DialogTitle>
        <DialogContent>
          <DialogContentText>{children}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleClose()}> Cancel</Button>
          <Button variant="contained" color="primary" onClick={() => handleClose(true)}>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
