import React from 'react';

export type AudioProps = {
  urls: string[];
};

export function Audio(props: AudioProps) {
  const { urls } = props;
  const [seq, setSeq] = React.useState(0);

  const handleEnded = () => {
    if (seq < urls.length) {
      setSeq(seq + 1);
    } else {
      setSeq(0);
    }
  };

  return (
    <figure>
      <audio id="feedback-audio" src={urls[seq]} onEnded={handleEnded} controls autoPlay></audio>
    </figure>
  );
}
