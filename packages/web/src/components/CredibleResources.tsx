import React from 'react';
import { Typography } from '@material-ui/core';
import { ExtLink } from 'components';

export const CredibleResources = (props: { subheading: boolean }) => {
  return (
    <div>
      <Typography component="span" paragraph variant={props.subheading ? 'h5' : 'body1'}>
        Credible sources about COVID-19
      </Typography>

      <Typography component="span" paragraph>
        <ul>
          <li>
            <ExtLink href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019">
              World Health Organization
            </ExtLink>
          </li>
          <li>
            <ExtLink href="https://www.australia.gov.au/">
              Australian government coronavirus (COVID-19) information
            </ExtLink>
          </li>
          <li>
            Government information for each state:
            <ul>
              <li>
                <ExtLink href="https://www.coronavirus.vic.gov.au/">Victoria </ExtLink>
              </li>
              <li>
                <ExtLink href="https://www.nsw.gov.au/covid-19">New South Wales</ExtLink>
              </li>
              <li>
                <ExtLink href="https://www.qld.gov.au/health/conditions/health-alerts/coronavirus-covid-19/current-status/urgent-covid-19-update">
                  Queensland
                </ExtLink>
              </li>
              <li>
                <ExtLink href="https://www.covid19.act.gov.au/">Australian Capital Territory</ExtLink>
              </li>
              <li>
                <ExtLink href="https://www.coronavirus.tas.gov.au/">Tasmania</ExtLink>
              </li>
              <li>
                <ExtLink href="https://www.wa.gov.au/organisation/department-of-the-premier-and-cabinet/covid-19-coronavirus-latest-updates">
                  Western Australia
                </ExtLink>
              </li>
              <li>
                <ExtLink href="https://www.covid-19.sa.gov.au/">South Australia</ExtLink>
              </li>
              <li>
                <ExtLink href="https://coronavirus.nt.gov.au/">Northern Territory</ExtLink>
              </li>
            </ul>
          </li>
        </ul>
      </Typography>
    </div>
  );
};
