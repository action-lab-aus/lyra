import React from 'react';
import { Button, ButtonProps } from '@material-ui/core';
import { createMuiTheme, ThemeProvider, withStyles, WithStyles } from '@material-ui/core/styles';
import FacebookIcon from '@material-ui/icons/Facebook';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#4267B2',
    },
  },
});

export const FacebookButton = withStyles((theme) => ({
  root: {},
  icon: {
    marginRight: theme.spacing(1),
  },
}))((props: Omit<ButtonProps, 'variant'> & WithStyles) => {
  const { classes, children, ...btnProps } = props;
  return (
    <ThemeProvider theme={theme}>
      <Button className={classes.root} {...btnProps} variant="contained" color="primary">
        <FacebookIcon className={classes.icon} /> <span>{children}</span>
      </Button>
    </ThemeProvider>
  );
});
