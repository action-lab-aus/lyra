import React from 'react';
import { Container, Grid, Dialog, DialogContentText, DialogProps, Typography, Button } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { ExtLink } from 'components';
import privacy from './imgs/privacy.png';

const useStyles = makeStyles((theme) =>
  createStyles({
    article: {
      backgroundColor: '#FFF8F1',
      padding: theme.spacing(4),
      paddingBottom: theme.spacing(2),
      [theme.breakpoints.down('sm')]: {
        padding: theme.spacing(2),
      },
    },
  }),
);

export type DataSecurityDialogProps = DialogProps & {
  open: boolean;
  onClose: () => void;
};

export function DataSecurityDialog(props: DataSecurityDialogProps) {
  const classes = useStyles();
  const { open, onClose } = props;

  return (
    <Dialog onClose={onClose} open={open}>
      <Container className={classes.article}>
        <DialogContentText component="span">
          <Grid container spacing={2}>
            <Grid item xs={12} container alignContent="center">
              <Grid item xs={12}>
                <img src={privacy} alt="privacy" width="56px" height="56px" />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="h5" color="primary" style={{ marginBottom: '12px' }}>
                  What happens to your data?
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1" paragraph>
                  The information posted to this Facebook group will be extracted by our research team and stored
                  securely, according to strict privacy policies. Posts, comments and social reactions (e.g., likes)
                  will be stored separately to your personal information (i.e. Facebook name) and are only linked by ID
                  number. Only authorised researchers can access this information.
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1" paragraph>
                  Also, because this is a Facebook group, Facebook will have access to the type of data that they
                  routinely collect as part of your day to day interactions with Facebook (e.g. browsing, posts,
                  comments and reactions). More information about Facebook’s data policy is available via this{' '}
                  <ExtLink href="https://www.facebook.com/about/privacy">link on Facebook</ExtLink>. Note that the
                  research team is <b>not</b> collaborating with Facebook for this research project, nor will we be
                  providing them with any additional data.
                </Typography>
              </Grid>

              <Grid item xs={12} container justify="flex-end">
                <Button onClick={onClose} variant="contained" color="primary">
                  Close
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </DialogContentText>
      </Container>
    </Dialog>
  );
}
