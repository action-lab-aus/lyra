import React from 'react';
import {
  AppBar,
  Container,
  Grid,
  Box,
  Paper,
  Button,
  Dialog,
  DialogContentText,
  DialogProps,
  IconButton,
  Toolbar,
  Typography,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import FacebookIcon from '@material-ui/icons/Facebook';
import CloseIcon from '@material-ui/icons/Close';
import { ExtLink } from 'components';
import psg from './imgs/psg.png';
import learning from './imgs/learning.png';
import chat from '../landing/imgs/chat.png';
import { DataSecurityDialog } from './DataSecurityDialog';

const psgLink = process.env.GATSBY_PSGLINK;

const useStyles = makeStyles((theme) =>
  createStyles({
    toolbar: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    title: {
      fontWeight: 'bold',
      marginBottom: theme.spacing(2),
      [theme.breakpoints.down('sm')]: {
        marginBottom: theme.spacing(0),
      },
    },
    code: {
      ...theme.typography.h5,
      padding: theme.spacing(1, 4),
      border: '1px dashed #666',
      borderRadius: '12px',
      textAlign: 'center',
      backgroundColor: '#fff',
    },
    list: {
      padding: 0,
      listStylePosition: 'inside',
    },
    article: {
      marginTop: theme.spacing(12),
      paddingBottom: theme.spacing(8),
    },
    joinPaper: {
      backgroundColor: '#e9f7fa',
      padding: theme.spacing(2, 4),
      [theme.breakpoints.down('sm')]: {
        padding: theme.spacing(2, 2),
      },
    },
    fbBotton: {
      backgroundColor: '#1AA9BB',
      color: '#fff',
      borderRadius: '8px',
    },
    image: {
      width: '70%',
      height: '70%',
      objectFit: 'cover',
    },
    orderThird: {
      order: 3,
      [theme.breakpoints.down('sm')]: {
        order: 4,
      },
    },
    orderFourth: {
      order: 4,
      [theme.breakpoints.down('sm')]: {
        order: 3,
      },
    },
    bannerBtn: {
      color: '#fff',
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
    },
  }),
);

export type PSGDialogProps = DialogProps & {
  open: boolean;
  onClose: () => void;
  code: string;
};

export function PSGDialog(props: PSGDialogProps) {
  const classes = useStyles();
  const { code, open, onClose } = props;
  const [dataOpen, setDataOpen] = React.useState(false);

  const onDataClose = () => {
    setDataOpen(false);
  };

  return (
    <Dialog onClose={onClose} open={open} fullScreen>
      <AppBar>
        <Toolbar className={classes.toolbar}>
          <Typography variant="h6">PiP+ Online Community</Typography>
          <IconButton onClick={onClose} edge="end" color="inherit" aria-label="close">
            <CloseIcon />
          </IconButton>
        </Toolbar>
      </AppBar>

      <Container className={classes.article}>
        <DialogContentText component="span">
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Alert severity="info" variant="filled">
                <Typography paragraph>
                  <strong>You don't have to do this alone.</strong>
                </Typography>
                <Typography paragraph>
                  Consider joining our PiP+ online community to connect with other parents. You are welcome to join now,
                  or you can come back at any time via your dashboard.
                </Typography>

                <Grid item xs={12} container justify="flex-end">
                  <Button size="medium" onClick={onClose} className={classes.bannerBtn}>
                    Not Now
                  </Button>
                </Grid>
              </Alert>
            </Grid>

            <Grid item xs={12} container justify="flex-end" style={{ order: 1 }}>
              <Grid item xs={12}>
                <Paper className={classes.joinPaper} elevation={0}>
                  <Grid item xs={12} style={{ marginTop: '18px' }}>
                    <Typography variant="h5" color="primary">
                      How do you join?
                    </Typography>
                  </Grid>

                  <Grid item xs={12}>
                    <Typography component="span" variant="subtitle1" className={classes.list}>
                      <ol style={{ padding: 0, margin: 0 }}>
                        <li>
                          Visit the <strong>PiP+ community</strong> Facebook group by clicking the button below
                        </li>
                        <li>
                          Answer the membership form questions, including the email you used to register to the program,
                          and the code below. We ask these to keep this group a safe space for PiP+ parents.
                        </li>
                        <li>Agree to the community guidelines</li>
                        <li>
                          <strong>Copy the verification code below</strong> and paste it into Facebook group’s
                          membership form
                        </li>
                      </ol>
                    </Typography>
                  </Grid>

                  <Box my={1}>
                    <Typography variant="h6">Verification Code:</Typography>
                  </Box>

                  <Box display="flex" alignItems="center" mb={2}>
                    <Typography className={classes.code}>{code}</Typography>
                  </Box>

                  <Box mb={2}>
                    <Button
                      onClick={() => onClose()}
                      className={classes.fbBotton}
                      href={psgLink!}
                      target="_blank"
                      rel="noopener">
                      <FacebookIcon fontSize="large" style={{ marginRight: 6 }} />
                      <Typography variant="body2">Go to PiP+ online community</Typography>
                    </Button>
                  </Box>
                </Paper>
              </Grid>
            </Grid>

            <Grid item xs={12} md={6} container alignContent="center" style={{ order: 2 }}>
              <Grid item xs={12}>
                <Typography variant="h5" color="primary">
                  Get support from other PiP+ parents
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">
                  You are not alone in this journey—Join our Facebook PiP+ community to get to know other parents who
                  are also raising teenagers, and learn from one another’s experiences. Our staff moderate and
                  facilitate the discussions in this group to keep the community safe and supportive to all parents.
                </Typography>
              </Grid>

              <Grid item xs={12} style={{ marginTop: '18px' }}>
                <Typography variant="h5" color="primary">
                  What can you find inside?
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography component="span" variant="subtitle1" className={classes.list}>
                  <ul style={{ padding: 0, margin: 0 }}>
                    <li>Evidence-based parenting strategies and expert advice</li>
                    <li>Facilitated discussions around popular topics (e.g., coping with COVID)</li>
                    <li>Personal parenting stories, advice, and questions from other parents</li>
                    <li>Fun and reflective activities to support your personal wellbeing</li>
                  </ul>
                </Typography>
              </Grid>
            </Grid>

            <Grid item xs={12} md={6} style={{ order: 3 }}>
              <Box display="flex" justifyContent="center">
                <img src={chat} alt="Chat" className={classes.image} />
              </Box>
            </Grid>

            <Grid item xs={12} md={6} className={classes.orderThird}>
              <Box display="flex" justifyContent="center" my={2}>
                <img src={psg} alt="Peer Support Groups" className={classes.image} />
              </Box>
            </Grid>

            <Grid item xs={12} md={6} container alignContent="center" className={classes.orderFourth}>
              <Grid item xs={12}>
                <Typography variant="h5" color="primary">
                  This a safe and supportive community
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">
                  For your safety, and to ensure you get the support you need, all posts are checked and moderated by
                  members of the PiP+ team during business hours. Our team’s work hours are 9am-5pm AEST, Monday to
                  Friday, so posts made outside of these times may not appear until the next working day.
                </Typography>
              </Grid>
            </Grid>

            <Grid item xs={12} md={6} container alignContent="center" style={{ order: 6 }}>
              <Grid item xs={12}>
                <Typography variant="h5" color="primary">
                  We are still learning...
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">
                  The PiP+ community is a new feature of the program that we are still testing to see how we can best
                  support parents. To do this, we will be looking at things like the number of parents who contribute to
                  the groups, number of posts and comments, and what members discuss. This will form part of the overall
                  PiP research project that you initially signed up for. To know how we learn from your data and how we
                  keep them safe and secure, read our{' '}
                  <ExtLink style={{ cursor: 'pointer' }} onClick={() => setDataOpen(true)}>
                    {' '}
                    data policy
                  </ExtLink>
                  .
                </Typography>
              </Grid>
            </Grid>

            <Grid item xs={12} md={6} style={{ order: 7 }}>
              <Box display="flex" justifyContent="center">
                <img src={learning} alt="Responsive" className={classes.image} />
              </Box>
            </Grid>

            <Grid item xs={12} container justify="flex-end" style={{ order: 8 }}>
              <Grid item xs={12} md={6}>
                <Paper className={classes.joinPaper} elevation={0}>
                  <Grid item xs={12} style={{ marginTop: '18px' }}>
                    <Typography variant="h5" color="primary">
                      How do you join?
                    </Typography>
                  </Grid>

                  <Grid item xs={12}>
                    <Typography component="span" variant="subtitle1" className={classes.list}>
                      <ol style={{ padding: 0, margin: 0 }}>
                        <li>
                          Visit the <strong>PiP+ community</strong> Facebook group by clicking the button below
                        </li>
                        <li>
                          Answer the membership form questions, including the email you used to register to the program,
                          and the code below. We ask these to keep this group a safe space for PiP+ parents.
                        </li>
                        <li>Agree to the community guidelines</li>
                        <li>
                          <strong>Copy the verification code below</strong> and paste it into Facebook group’s
                          membership form
                        </li>
                      </ol>
                    </Typography>
                  </Grid>

                  <Box my={1}>
                    <Typography variant="h6">Verification Code:</Typography>
                  </Box>

                  <Box display="flex" alignItems="center" mb={2}>
                    <Typography className={classes.code}>{code}</Typography>
                  </Box>

                  <Box mb={2}>
                    <Button
                      onClick={() => onClose()}
                      className={classes.fbBotton}
                      href={psgLink!}
                      target="_blank"
                      rel="noopener">
                      <FacebookIcon fontSize="large" style={{ marginRight: 6 }} />
                      <Typography variant="body2">Go to PiP+ online community</Typography>
                    </Button>
                  </Box>
                </Paper>
              </Grid>
            </Grid>

            <Grid item xs={12} container justify="flex-end" style={{ order: 9 }}>
              <Button onClick={onClose} variant="contained" color="primary">
                Close
              </Button>
            </Grid>
          </Grid>
        </DialogContentText>
      </Container>

      <DataSecurityDialog onClose={onDataClose} open={dataOpen} />
    </Dialog>
  );
}
