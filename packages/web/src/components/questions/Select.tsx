import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { TextField, FormControl, InputLabel, Select as MuiSelect, MenuItem, Box, Typography } from '@material-ui/core';
import { SelectOption } from '.';

const useStyles = makeStyles((theme) =>
  createStyles({
    helperText: (props: SelectProps) => ({
      ...theme.typography.caption,
      color: props.error ? theme.palette.error.main : theme.palette.text.secondary,
      marginTop: 3,
      marginLeft: 14,
      marginRight: 14,
    }),
    formControl: {
      flex: 1,
    },
    other: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: theme.spacing(0.5),
    },
  }),
);

export type SelectProps = {
  id: string;
  items: Array<string | SelectOption>;
  value: string | null | string[];
  onChange: (value: string | null) => void;
  title?: string;
  label?: string;
  inputLabel?: string;
  helperText?: string;
  error?: boolean;
  required?: boolean;
};

export function Select(props: SelectProps) {
  const { label, title, id, value, inputLabel, onChange, helperText, required, error } = props;
  const classes = useStyles(props);

  const options = props.items.map<SelectOption>((item) =>
    typeof item === 'string' ? { value: item, label: item } : item,
  );

  const inputItem = options.find((item) => item.value === '_input');

  const selectedValue = React.useMemo(() => {
    const selectedItem = options.find((item) => item.value === value);
    if (selectedItem) {
      return selectedItem.value;
    }

    if (value === null) {
      return null;
    }

    return inputItem ? '_input' : value;
  }, [options, value, inputItem]);

  const handleSelectChange = (selected: string) => {
    onChange(selected === '_input' ? '' : selected);
  };

  const handleInputChange = (value: string) => {
    onChange(value);
  };

  const renderOptions = (options: SelectOption[]) => {
    return options.map((item) => (
      <MenuItem key={item.value} value={item.value}>
        {item.label}
      </MenuItem>
    ));
  };

  return (
    <div>
      {title && (
        <Typography variant="body1" gutterBottom color="textSecondary">
          {title}
        </Typography>
      )}
      <Box display="flex">
        <FormControl variant="filled" className={classes.formControl} error={error}>
          {label && (
            <InputLabel id={id} required={required}>
              {label}
            </InputLabel>
          )}
          <MuiSelect
            labelId={id}
            value={selectedValue || ''}
            onChange={(e) => handleSelectChange(e.target.value as string)}>
            {renderOptions(options)}
          </MuiSelect>
        </FormControl>
        {selectedValue === '_input' && inputItem && (
          <TextField
            className={classes.other}
            label={inputLabel || inputItem.label}
            variant="filled"
            value={value}
            onChange={(e) => handleInputChange(e.target.value)}
            error={error}
          />
        )}
      </Box>
      {helperText && <Typography className={classes.helperText}>{helperText}</Typography>}
    </div>
  );
}
