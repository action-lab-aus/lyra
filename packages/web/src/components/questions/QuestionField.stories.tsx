import React from 'react';
import { Story, Meta } from '@storybook/react';
import { Box, Container } from '@material-ui/core';
import { QuestionField, QuestionFieldProps } from './QuestionField';
import { QuestionItem } from '.';

export default {
  title: 'components/questions/QuestionField',
  component: QuestionField,
  argTypes: { onChange: { action: 'onChange' } },
} as Meta;

const Template: Story<QuestionFieldProps> = (args) => (
  <Container>
    <Box p={2}>
      <QuestionField {...args} />
    </Box>
  </Container>
);

export const Text = Template.bind({});
Text.args = {
  question: {
    type: 'text',
    label: 'Text Input',
    title: 'Text title',
  } as QuestionItem,
  data: {},
  errors: {},
};

export const Checkgroup = Template.bind({});
Checkgroup.args = {
  question: {
    id: 'q1',
    key: 'q1',
    type: 'checkgroup',
    title: 'Multiple selection',
    options: ['Option A', 'Option B', 'Option C', 'Option D', 'Option E'],
  } as QuestionItem,
  data: {
    q1: ['Option C', 'Option D'],
  },
  errors: {},
};
