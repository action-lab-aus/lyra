import React from 'react';
import isEmpty from 'lodash/isEmpty';
import { Button, Grid, GridSize, Step, StepContent, StepLabel, Stepper, Typography } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { ItemSize, QuestionSection, QuestionItem } from './types';
import { QuestionField } from './QuestionField';
import { notEmpty, ValidateFn } from 'components/form';
import { byCond } from './byCond';

const useStyles = makeStyles((theme) =>
  createStyles({
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    actionsContainer: {
      margin: theme.spacing(2, 0),
    },
  }),
);

const getGridSize = (size: ItemSize) => {
  const breakPointGridSizes: Record<ItemSize, Record<string, GridSize>> = {
    small: {
      xs: 12,
      sm: 6,
      md: 4,
    },
    medium: {
      xs: 12,
      md: 6,
    },
    standard: {
      xs: 12,
    },
  };
  return breakPointGridSizes[size];
};

type Errors = Record<string, string>;

export type QuestionStepperProps = {
  sections: QuestionSection[];
  step: number;
  setStep: (step: number) => void;
  data: Record<string, any>;
  onChange: (data: Record<string, any>) => void;
  finishLabel?: string;
};

export function QuestionStepper(props: QuestionStepperProps) {
  const { sections, data, onChange, step, setStep, finishLabel } = props;
  const classes = useStyles();
  const [errors, setErrors] = React.useState<Errors>({});

  const updateField = (field: string, value: any) => {
    onChange({ ...data, [field]: value });
  };

  const handleNext = () => {
    // validate
    const section = sections[step];
    const errors = section.questions.filter(byCond(data)).reduce<Errors>((errors, question) => {
      if (question.type == 'paragraph') {
        return errors;
      }

      const validateFn = createValidateFn(question);
      if (!validateFn) {
        return errors;
      }

      const value = data[question.key];
      const error = validateFn(value, data);
      return error !== null ? { ...errors, [question.key]: error } : errors;
    }, {});

    const element = document.querySelector(`[aria-label="${Object.keys(errors)[0]}"`);
    if (element) element.scrollIntoView({ block: 'center', behavior: 'smooth' });
    else {
      if (step === sections.length - 1) {
        const feedbackElem = document.querySelector(`[aria-label="breadcrumb-feedback"`);
        if (feedbackElem) feedbackElem.scrollIntoView({ block: 'end', behavior: 'smooth' });
      }
    }

    setErrors(errors);
    if (isEmpty(errors)) {
      setStep(step + 1);
    }
  };

  const handleBack = () => {
    setStep(step - 1);
  };

  return (
    <Stepper activeStep={step} orientation="vertical">
      {sections.map((section, index) => {
        return (
          <Step key={index} arie-label={section.title}>
            <StepLabel>
              <Typography variant="h6" gutterBottom>
                {section.title}
              </Typography>
            </StepLabel>
            <StepContent>
              <Grid container spacing={2}>
                {section.questions.filter(byCond(data)).map((question) => {
                  const gridSizes = getGridSize(question.size || 'standard');
                  return (
                    <Grid key={question.key} item {...gridSizes}>
                      <QuestionField question={question} data={data} errors={errors} onChange={updateField} />
                    </Grid>
                  );
                })}
              </Grid>
              <div className={classes.actionsContainer}>
                <div>
                  {index > 0 && (
                    <Button disabled={step === 0} onClick={handleBack} className={classes.button}>
                      Back
                    </Button>
                  )}
                  <Button variant="contained" color="primary" onClick={handleNext} className={classes.button}>
                    {step === sections.length - 1 ? finishLabel || 'Finish' : 'Next'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        );
      })}
    </Stepper>
  );
}

const createValidateFn = (question: QuestionItem): ValidateFn<{}> | null => {
  const validates = [question.required && notEmpty('This field is required'), question.validate].filter((v) =>
    Boolean(v),
  ) as ValidateFn<{}>[];

  if (validates.length > 0) {
    return (value: any, data: {}) => {
      let error: string | null = null;
      for (let validate of validates) {
        error = validate(value, data);
        if (error !== null) break;
      }
      return error;
    };
  } else {
    return null;
  }
};
