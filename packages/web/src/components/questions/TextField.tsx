import React from 'react';
import {
  debounce,
  FormControl,
  FormLabel,
  TextField as MuiTextField,
  TextFieldProps as MuiTextFieldProps,
} from '@material-ui/core';

export type TextFieldProps = Omit<MuiTextFieldProps, 'onChange'> & {
  title?: string;
  onChange: (value: string) => void;
  controlled?: boolean;
};

export function TextField(props: TextFieldProps) {
  const { id, title, onChange, controlled, value, ...other } = props;
  const onContentChange = React.useMemo(() => debounce(onChange, 1000), [onChange]);
  const [content, setContent] = React.useState(value);

  const handleContentChange = (value: string) => {
    setContent(value);
    onContentChange(value);
  };

  return (
    <FormControl id={id} component="fieldset" fullWidth>
      {title && (
        <FormLabel component="legend" style={{ marginBottom: 12 }}>
          {title}
        </FormLabel>
      )}
      {controlled ? (
        <MuiTextField value={value} onChange={(e) => onChange(e.target.value)} {...other} />
      ) : (
        <MuiTextField value={content} onChange={(e) => handleContentChange(e.target.value)} {...other} />
      )}
    </FormControl>
  );
}
