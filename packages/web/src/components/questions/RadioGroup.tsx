import React from 'react';
import { Box, FormControl, FormLabel, Typography } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import { SelectOption } from '.';

const useStyles = makeStyles((theme) =>
  createStyles({
    label: {
      lineHeight: 1.5,
    },
    helperText: (props: { error?: boolean }) => ({
      ...theme.typography.caption,
      color: props.error ? theme.palette.error.main : theme.palette.text.secondary,
      marginTop: 3,
      marginLeft: 14,
      marginRight: 14,
    }),
  }),
);

export type RadioGroupProps = {
  label: string;
  id: string;
  value: string | null;
  options: Array<string | SelectOption>;
  onChange: (value: any) => void;
  helperText?: string;
  error?: boolean;
};

export function RadioGroup(props: RadioGroupProps) {
  const { label, id, value, onChange, error, helperText } = props;
  const options = props.options.map<SelectOption>((option) =>
    typeof option === 'string' ? { value: option, label: option } : option,
  );

  const classes = useStyles({ error });

  return (
    <FormControl component="fieldset">
      {label && (
        <Typography variant="body1" color="textSecondary" gutterBottom>
          {label}
        </Typography>
      )}
      <Box mt={1}>
        <ToggleButtonGroup
          aria-label={id}
          value={value}
          onChange={(e, value) => onChange(value)}
          size="small"
          exclusive>
          {options.map((option) => (
            <ToggleButton key={option.value} value={option.value}>
              {option.label}
            </ToggleButton>
          ))}
        </ToggleButtonGroup>
      </Box>
      {helperText && <Typography className={classes.helperText}>{helperText}</Typography>}
    </FormControl>
  );
}
