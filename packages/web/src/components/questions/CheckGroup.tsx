import React, { ChangeEvent } from 'react';
import { FormControl, FormLabel, FormGroup, FormControlLabel, Checkbox, FormHelperText } from '@material-ui/core';
import { SelectOption } from './types';

export type CheckGroupProps = {
  id: string;
  options: Array<string | SelectOption>;
  value: Array<string | number> | null;
  onChange: (value: Array<string | number> | null) => void;
  title?: string;
  label?: string;
  helperText?: string;
  error?: boolean;
  required?: boolean;
};

export function CheckGroup(props: CheckGroupProps) {
  const { id, options, onChange, title, helperText, error, required } = props;
  const items = options.map<SelectOption>((option) =>
    typeof option === 'string' ? { value: option, label: option } : option,
  );

  const value = props.value || [];

  const handleChange = (optionValue: string | number) => (e: ChangeEvent, checked: boolean) => {
    if (checked) {
      onChange([...value, optionValue]);
    } else {
      onChange(value.filter((v) => v !== optionValue));
    }
  };

  const isChecked = (optionValue: string | number) => {
    return value.indexOf(optionValue) >= 0;
  };

  return (
    <FormControl id={id} component="fieldset" required={required}>
      {title && <FormLabel component="legend"> {title}</FormLabel>}
      <FormGroup>
        {items.map((item) => {
          const { label, value } = item;
          const checked = isChecked(value);
          return (
            <FormControlLabel
              key={value}
              control={<Checkbox checked={checked} onChange={handleChange(value)} name={String(value)} />}
              label={label}
            />
          );
        })}
      </FormGroup>
      {helperText && <FormHelperText error={error}>{helperText}</FormHelperText>}
    </FormControl>
  );
}
