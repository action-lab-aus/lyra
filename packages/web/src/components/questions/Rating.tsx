import React from 'react';
import MuiRating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import { Chip, FormControl, FormLabel, Typography } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles((theme) =>
  createStyles({
    label: {
      lineHeight: 1.5,
    },
    helperText: (props: { error?: boolean }) => ({
      ...theme.typography.caption,
      color: props.error ? theme.palette.error.main : theme.palette.text.secondary,
      marginTop: 3,
      marginLeft: 14,
      marginRight: 14,
    }),
  }),
);

export type RatingProps = {
  label: string;
  name: string;
  value: string | null;
  options: string[];
  onChange: (value: string | null) => void;
  helperText?: string;
  error?: boolean;
};

export function Rating(props: RatingProps) {
  const { label, name, options, value, onChange, error, helperText } = props;
  const [hover, setHover] = React.useState<number>(-1);
  const rating = value ? options.indexOf(value) + 1 : null;
  const classes = useStyles({ error });

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('xs'));

  const getText = () => {
    if (hover > 0) {
      return options[hover - 1];
    }

    if (rating && rating > 0) {
      return options[rating - 1];
    }
    return null;
  };

  const text = getText();

  const handleChange = (v: number | null) => {
    const value = v ? options[v - 1] : null;
    onChange(value);
  };

  return (
    <FormControl component="fieldset">
      <FormLabel className={classes.label} component="legend">
        {label}
      </FormLabel>
      <Box display="flex" alignItems="center" minHeight={32}>
        <MuiRating
          name={name}
          value={rating}
          precision={1}
          size={matches ? 'small' : 'medium'}
          max={options.length}
          onChange={(e, v) => handleChange(v)}
          onChangeActive={(e, newHover) => setHover(newHover)}
        />
        <Box ml={2}>{text && <Chip label={text} size="small" />}</Box>
      </Box>
      {helperText && <Typography className={classes.helperText}>{helperText}</Typography>}
    </FormControl>
  );
}
