import React from 'react';
import { Typography } from '@material-ui/core';
import { Rating } from './Rating';
import { Select } from './Select';
import { RadioGroup } from './RadioGroup';
import { Checkbox } from './Checkbox';
import { CheckGroup } from './CheckGroup';
import { QuestionItem } from './types';
import { TextField } from './TextField';
import { textHighlight } from '../../theme';

export type QuestionFieldProps = {
  question: QuestionItem;
  data: Record<string, any>;
  errors: any;
  onChange: (key: string, value: any) => void;
};

export function QuestionField(props: QuestionFieldProps) {
  const { question: item, errors, data: userSurvey, onChange } = props;
  const key = item.key;

  const values = (key: string) => {
    const v = userSurvey[key];
    return v === undefined ? null : v;
  };

  switch (item.type) {
    case 'text': {
      const value = values(key) || '';
      const error = errors[key];

      return (
        <TextField
          key={key}
          id={key}
          controlled={item.controlled}
          title={item.title}
          variant="filled"
          label={item.label}
          onChange={(value) => onChange(key, value)}
          value={value}
          helperText={error || item.helperText}
          error={Boolean(error)}
          required={Boolean(item.required)}
          fullWidth
        />
      );
    }

    case 'select': {
      const value = values(key);
      const error = errors[key];

      return (
        <Select
          id={key}
          label={item.label}
          title={item.title}
          items={item.options}
          onChange={(value) => onChange(key, value)}
          value={value}
          helperText={error || item.helperText}
          error={Boolean(error)}
          inputLabel={item.inputLabel}
          required={Boolean(item.required)}
        />
      );
    }

    case 'radio': {
      const value = values(key);
      const error = errors[key];
      return (
        <RadioGroup
          id={key}
          label={item.label}
          options={item.options}
          onChange={(value) => onChange(key, value)}
          value={value}
          helperText={error || item.helperText}
          error={Boolean(error)}
        />
      );
    }

    case 'checkbox': {
      const value = values(key);
      return (
        <Checkbox
          id={key}
          value={value}
          label={item.label}
          disabled={item.disabled}
          onChange={(value) => onChange(key, value)}
        />
      );
    }

    case 'rating': {
      const value = values(key);
      const error = errors[key];
      return (
        <Rating
          key={key}
          name={key}
          label={item.label}
          options={item.scales}
          value={value}
          onChange={(value) => onChange(key, value)}
          helperText={error || item.helperText}
          error={Boolean(error)}
        />
      );
    }

    case 'checkgroup': {
      const value = values(key);
      const error = errors[key];
      return (
        <CheckGroup
          id={key}
          key={key}
          title={item.title}
          options={item.options}
          value={value}
          onChange={(value) => onChange(key, value)}
          helperText={error || item.helperText}
          error={Boolean(error)}
        />
      );
    }

    default: {
      return typeof item.content === 'string' ? (
        <Typography
          variant="subtitle1"
          style={item.variant === 'bold' ? { fontWeight: 900, color: textHighlight } : {}}>
          {item.content}
        </Typography>
      ) : (
        <React.Fragment key={key}>{item.content}</React.Fragment>
      );
    }
  }
}
