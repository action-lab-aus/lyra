import { QuestionItem } from './types';
import { DashboardQuestionItem } from '../dashboard/types';

function evaluateCondExpression(expression: string, context: object) {
  const [key, op, value] = expression.split(' ');

  const include = (value: string | string[] | undefined, data: string) => {
    if (!value) {
      return false;
    }
    const values = Array.isArray(value) ? value : [value];
    return values.includes(data);
  };

  switch (op) {
    case '==': {
      return context[key] === value;
    }

    case 'has': {
      const values = Array.isArray(context[key]) ? context[key] : [context[key]];
      return include(context[key], value);
    }

    default:
      return false;
  }
}

export const byCond =
  (data: Record<string, any> = {}) =>
  (question: QuestionItem | DashboardQuestionItem) => {
    const cond = question.cond;

    if (!cond) {
      return true;
    }

    const multi = cond.match(/\((.+)\) && \((.+)\)/);
    if (!multi) {
      return evaluateCondExpression(cond, data);
    }

    const [, group1, group2] = multi;
    return evaluateCondExpression(group1, data) && evaluateCondExpression(group2, data);
  };
