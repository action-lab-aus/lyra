import { ValidateFn } from 'components/form';

export type SelectOption = { label: string; value: string | number };

export type ItemSize = 'small' | 'medium' | 'standard';

export type QuestionItemBase = {
  key: string;
  cond?: string;
  size?: ItemSize;
  required?: boolean;
  validate?: ValidateFn<{}>;
};

export type SelectItem = QuestionItemBase & {
  type: 'select';
  options: Array<SelectOption | string>;
  label?: string;
  title?: string;
  helperText?: string;
  inputLabel?: string;
  variant?: 'multiple';
};

export type CheckGroupItem = QuestionItemBase & {
  type: 'checkgroup';
  options: Array<SelectOption | string>;
  title?: string;
  label?: string;
  helperText?: string;
};

export type RadioItem = Omit<SelectItem, 'type' | 'inputLable'> & {
  type: 'radio';
  label: string;
};

export type TextDateItem = QuestionItemBase & {
  type: 'text';
  label?: string;
  title?: string;
  helperText?: string;
  controlled?: boolean;
};

export type RatingItem = QuestionItemBase & {
  type: 'rating';
  label: string;
  scales: string[];
  helperText?: string;
};

export type CheckboxItem = QuestionItemBase & {
  type: 'checkbox';
  label: string;
  disabled: boolean;
};

export type ParagraphItem = QuestionItemBase & {
  type: 'paragraph';
  content: string | React.ReactNode;
  variant?: 'bold' | 'italic';
};

export type QuestionItem =
  | TextDateItem
  | SelectItem
  | RatingItem
  | RadioItem
  | ParagraphItem
  | CheckboxItem
  | CheckGroupItem;

export type QuestionSection = {
  key: string;
  title: string;
  questions: QuestionItem[];
};
