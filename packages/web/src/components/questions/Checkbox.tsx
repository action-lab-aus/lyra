import React from 'react';
import { Checkbox as MuiCheckbox } from '@material-ui/core';
import { FormControlLabel } from '@material-ui/core';

export type CheckboxProps = {
  id: string;
  label: string;
  value: any;
  disabled: boolean;
  onChange: (value: boolean) => void;
};

export function Checkbox(props: CheckboxProps) {
  const { label, value, disabled, onChange } = props;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
    onChange(checked);
  };

  return (
    <FormControlLabel
      disabled={disabled}
      control={
        <MuiCheckbox
          checked={value === undefined ? true : disabled ? true : value}
          onChange={handleChange}
          name={label}
        />
      }
      label={label}
    />
  );
}
