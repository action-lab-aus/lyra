import React from 'react';
import { Story, Meta } from '@storybook/react';
import { RootState, UserInfo } from 'app';
import { AppPage } from 'components/layout';
import { AppProvider, ThemeDecorator } from 'components/helpers';
import * as mocks from 'components/helpers/mocks';
import { CreateProfileContent } from './CreateProfileContent';

export default {
  title: 'components/settings/CreateProfileContent',
  component: CreateProfileContent,
  decorators: [ThemeDecorator],
} as Meta;

const Template: Story<{ state: RootState }> = (args) => {
  return (
    <AppProvider {...args}>
      <AppPage title="Create Profile ">
        <CreateProfileContent />
      </AppPage>
    </AppProvider>
  );
};

export const Example = Template.bind({});
Example.args = {
  state: mocks.rootState().authenticated().state,
};

export const FacebookUser = Template.bind({});
FacebookUser.args = {
  state: mocks.rootState().authenticated({
    authenticated: true,
    userInfo: {
      providerData: [{ providerId: 'facebook.com', uid: '123567890ABCD', email: 'user@example.com' } as UserInfo],
    },
  }).state,
};

export const Completed = Template.bind({});
Completed.args = {
  state: mocks
    .rootState()
    .authenticated()
    .profile({
      fetched: true,
      status: null,
      data: {
        user: mocks.templates.user(),
        topics: {},
        surveys: {},
      },
    }).state,
};
