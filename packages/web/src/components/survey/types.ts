import { QuestionSection, QuestionItem, ItemSize, SelectOption } from 'components/questions';

export type Survey = {
  id: string;
  title: string;
  scope: 'baseline' | 'followup';
  cover: any;
  tint: string;
  sections: QuestionSection[];
};

export type InheritableSurvey = Omit<Survey, 'sections'> & {
  sections: Array<
    Omit<QuestionSection, 'questions'> & {
      type: 'rating' | 'text' | 'select' | 'radio' | 'paragraph' | null;
      size: ItemSize | null;
      required: boolean | null;
      scales: string[] | null;
      options: Array<SelectOption | string> | null;
      questions: Partial<QuestionItem>[];
    }
  >;
};
