import React from 'react';
import { Story, Meta } from '@storybook/react';
import update from 'immutability-helper';
import { RootState } from 'app';
import { AppPage } from 'components/layout';
import { AppProvider } from 'components/helpers';
import * as mocks from 'components/helpers/mocks';
import { SurveyContent, SurveyContentProps } from './SurveyContent';
import { InheritableSurvey } from './types';

// @ts-ignore
import s1 from '../../surveys/s1-confidence.json';
// @ts-ignore
import s2 from '../../surveys/s2-pradas.json';
// @ts-ignore
import s3 from '../../surveys/s3-k6.json';
// @ts-ignore
import s4 from '../../surveys/s4-rcads.json';
// @ts-ignore
import f1 from '../../surveys/f1-evaluation.json';

export default {
  title: 'components/survey/SurveyContent',
  component: SurveyContent,
  argTypes: {},
} as Meta;

const Template: Story<SurveyContentProps & { state: RootState }> = (args) => {
  const { state, survey } = args;
  const { user, surveys: userSurveys } = state.profile.data!;
  return (
    <AppProvider state={state}>
      <AppPage title="Survey content">
        <SurveyContent survey={survey} user={user} userSurveys={userSurveys} />
      </AppPage>
    </AppProvider>
  );
};

const templates = mocks.templates;
const profile = {
  fetched: true,
  data: {
    user: templates.user(),
    surveys: {},
    topics: {},
  },
};

export const S1Confidence = Template.bind({});
S1Confidence.args = {
  survey: s1 as InheritableSurvey,
  state: mocks
    .rootState()
    .authenticated()
    .profile(
      update(profile, {
        data: {
          surveys: {
            $set: {
              's1-confidence': templates.userSurveys.empty(),
            },
          },
        },
      }),
    ).state,
};

export const S1ConfidenceCompleted = Template.bind({});
S1ConfidenceCompleted.args = {
  ...S1Confidence.args,
  state: mocks
    .rootState()
    .authenticated()
    .profile(
      update(profile, {
        data: {
          surveys: {
            $set: {
              's1-confidence': templates.userSurveys.s1Confidence(),
            },
          },
        },
      }),
    ).state,
};

export const S2PRADAS = Template.bind({});
S2PRADAS.args = {
  survey: s2 as InheritableSurvey,
  state: mocks
    .rootState()
    .authenticated()
    .profile(
      update(profile, {
        data: {
          surveys: {
            $set: {
              's2-pradas': templates.userSurveys.empty(),
            },
          },
        },
      }),
    ).state,
};

export const S2PRADASCompleted = Template.bind({});
S2PRADASCompleted.args = {
  survey: s2 as InheritableSurvey,
  state: mocks
    .rootState()
    .authenticated()
    .profile(
      update(profile, {
        data: {
          surveys: {
            $set: {
              's2-pradas': templates.userSurveys.s2Pradas(),
            },
          },
        },
      }),
    ).state,
};

export const S3K6 = Template.bind({});
S3K6.args = {
  //@ts-ignore
  survey: s3 as InheritableSurvey,
  state: mocks
    .rootState()
    .authenticated()
    .profile(
      update(profile, {
        data: {
          surveys: {
            $set: {
              's3-k6': templates.userSurveys.empty(),
            },
          },
        },
      }),
    ).state,
};

export const S3K6Completed = Template.bind({});
S3K6Completed.args = {
  //@ts-ignore
  survey: s3 as InheritableSurvey,
  state: mocks
    .rootState()
    .authenticated()
    .profile(
      update(profile, {
        data: {
          surveys: {
            $set: {
              's3-k6': templates.userSurveys.s3K6(),
            },
          },
        },
      }),
    ).state,
};

export const S4RCADS = Template.bind({});
S4RCADS.args = {
  //@ts-ignore
  survey: s4 as InheritableSurvey,
  state: mocks
    .rootState()
    .authenticated()
    .profile(
      update(profile, {
        data: {
          surveys: {
            $set: {
              's4-rcads': templates.userSurveys.empty(),
            },
          },
        },
      }),
    ).state,
};

export const S4RCADSCompleted = Template.bind({});
S4RCADSCompleted.args = {
  //@ts-ignore
  survey: s4 as InheritableSurvey,
  state: mocks
    .rootState()
    .authenticated()
    .profile(
      update(profile, {
        data: {
          surveys: {
            $set: {
              's4-rcads': templates.userSurveys.s4Rcads(),
            },
          },
        },
      }),
    ).state,
};

export const F1Evaluation = Template.bind({});
F1Evaluation.args = {
  //@ts-ignore
  survey: f1 as InheritableSurvey,
  state: mocks
    .rootState()
    .authenticated()
    .profile(
      update(profile, {
        data: {
          surveys: {
            $set: {
              'f1-evaluation': {
                's0#q4': 'Yes',
                's0#q10': [
                  'I saw less of my teen due to changed living arrangements (e.g. my teen was living elsewhere)',
                ],
              },
            },
          },
        },
      }),
    ).state,
};
