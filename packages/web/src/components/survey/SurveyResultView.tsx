import React from 'react';
import { navigate } from 'gatsby';
import {
  Box,
  Button,
  Card,
  CardHeader,
  CardActions,
  CardContent,
  IconButton,
  Typography,
  makeStyles,
  createStyles,
  Collapse,
  Chip,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { UserProfile, UserSurvey } from 'app';
import { QuestionItem } from 'components/questions';
import { Survey } from './types';
import { Feedback } from '../feeback';
import { SurveyEntry } from './SurveyContent';

const surveyDebug = process.env.GATSBY_SURVEY_DEBUG || false;

const useStyles = makeStyles((theme) =>
  createStyles<string, { collapseResult: boolean }>({
    feedbackCard: {
      marginTop: theme.spacing(4),
    },

    expand: ({ collapseResult }) => {
      return {
        transform: collapseResult ? 'rotate(0deg)' : 'rotate(180deg)',
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
        }),
      };
    },

    expandBox: {
      position: 'relative',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      margin: theme.spacing(2, 0),
    },

    resultCard: {
      marginTop: theme.spacing(2, 0, 4, 0),
    },
  }),
);

export type SurveyResultViewProps = {
  survey: Survey;
  user: UserProfile;
  userSurvey: UserSurvey;
  updateUserSurvey: (data: Partial<UserSurvey>) => void;
  next: SurveyEntry | null;
  prev: SurveyEntry | null;
};

export function SurveyResultView(props: SurveyResultViewProps) {
  const { survey, user, userSurvey, updateUserSurvey, prev, next } = props;
  const [collapseResult, setCollapseResult] = React.useState(true);
  const classes = useStyles({ collapseResult });

  const handleRestart = () => {
    updateUserSurvey({ _step: 0 });
  };

  const handleClose = () => {
    navigate(`/dashboard#${survey.scope}`);
  };

  return (
    <Box pb={4}>
      <Card className={classes.feedbackCard}>
        <CardHeader
          title="Feedback"
          subheader={
            <span>
              Based on <em>{survey.title}</em> result you've submitted
            </span>
          }
          // action={
          //   <IconButton aria-label="close" onClick={handleClose}>
          //     <CloseIcon />
          //   </IconButton>
          // }
        />
        <CardContent>
          <Feedback surveyId={survey.id} user={user} userSurvey={userSurvey} />
        </CardContent>

        <Box mt={2} p={2} display="flex" justifyContent="space-between">
          <Button
            variant="contained"
            color="primary"
            onClick={() => prev && navigate(`/surveys/${prev.id}`)}
            disabled={prev === null}>
            Prev
          </Button>
          {surveyDebug && (
            <Button variant="contained" color="primary" onClick={handleRestart}>
              Redo Survey
            </Button>
          )}
          {next !== null ? (
            <Button variant="contained" color="primary" onClick={() => navigate(`/surveys/${next.id}`)}>
              Next
            </Button>
          ) : (
            <Button variant="contained" color="primary" onClick={handleClose}>
              {survey.scope === 'baseline' ? 'Continue to modules' : 'Back to dashboard'}
            </Button>
          )}
        </Box>
      </Card>

      {surveyDebug && (
        <>
          <Box className={classes.expandBox}>
            <IconButton
              className={classes.expand}
              onClick={() => setCollapseResult(!collapseResult)}
              aria-expanded={collapseResult}
              aria-label="show more">
              <ExpandMoreIcon />
            </IconButton>
            <Typography variant="button" color="textSecondary">
              Press to {collapseResult ? 'see' : 'hide'} <em>{survey.title}</em> results
            </Typography>
          </Box>

          <Collapse in={!collapseResult}>
            <Card className={classes.resultCard}>
              <CardHeader title={survey.title} />
              <CardContent>
                {survey.sections.map((section) => (
                  <section key={section.key}>
                    <Typography variant="h6" gutterBottom>
                      {section.title}
                    </Typography>
                    {section.questions.map((question) => (
                      <ResultField key={question.key} item={question} value={userSurvey[question.key]} />
                    ))}
                  </section>
                ))}
              </CardContent>
            </Card>
          </Collapse>
        </>
      )}
    </Box>
  );
}

function ResultField(props: { item: QuestionItem; value?: string }) {
  const { item, value } = props;
  if (item.type === 'paragraph') {
    return (
      <Typography key={item.key} variant="subtitle1" paragraph>
        {item.content}
      </Typography>
    );
  }

  return (
    <Box key={item.key} my={2}>
      <Typography variant="subtitle1" color="textSecondary">
        {
          // @ts-ignore
          item.label || item.title
        }
      </Typography>
      {value ? <Chip label={value} /> : <Typography>Not answered</Typography>}
    </Box>
  );
}
