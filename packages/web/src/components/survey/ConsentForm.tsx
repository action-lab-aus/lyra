import React from 'react';
import { Paper, Box, Button, Typography, styled } from '@material-ui/core';

export type ConsentFormProps = {
  onChange: (consent: boolean) => void;
};

const Heading = styled('div')(({ theme }) => {
  return {
    ...theme.typography.subtitle1,
    margin: theme.spacing(1, 0),
  };
});

const P = styled('div')(({ theme }) => {
  return {
    ...theme.typography.body1,
    color: theme.palette.text.secondary,
    margin: theme.spacing(1, 0),
  };
});

export function ConsentForm(props: ConsentFormProps) {
  return (
    <Paper>
      <Box p={4}>
        <Typography variant="h5" gutterBottom>
          Consent
        </Typography>
        <Box p={2} height={300} border="1px solid #CCC" overflow="scroll" my={2}>
          <Heading>
            Project: Increasing Healthy Eating and Active Living in the Child Welfare Sector (HEALing Matters)
          </Heading>
          <P>
            You are invited to take part in this study. Please read this Explanatory Statement in full before deciding
            whether or not to participate in this research. If you would like further information regarding any aspect
            of this project, you are encouraged to contact the researchers via the email address listed above.
          </P>
          <Heading>What does the research involve? </Heading>
          <P>
            The overall aim of the research project is to improve the health of our most vulnerable and disadvantaged
            youth, by evaluating the effectiveness of the healthy eating, active living matters (HEALing Matters)
            program in residential care units/houses across Victoria, Australia. The HEALing Matters program involves
            residential carers completing a six-month online training program. Residential care workers who are
            participating in HEALing Matters online training are invited to take part in the evaluation of the program
            by completing a set of surveys before, during, and after HEALing Matters online training.
          </P>
          <P>
            Residential carers will be asked to complete a 15 to 20 minute online survey directly before starting
            HEALing Matters online training, and again after completing the six core modules included in the online
            training. They survey includes questions relating to your background, such as your age and how long you have
            been a residential care worker, and questions relating to your perception of the HEALing Matters program,
            and nutrition and physical activity practices in your residential unit/house.{' '}
          </P>
          <P>
            In addition to the two online surveys before and after HEALing Matters online training, residential carers
            will be asked to complete a brief online formative assessment after completing each core training module. In
            all, there will be six formative assessments to complete, each taking approximately 5 to 10 minutes to
            complete. Questions include information relating to your knowledge/skills for each module, and your
            confidence and motivation to apply the training in each module. Additional training modules also include a
            brief online formative assessment. Participants will be assigned a unique study ID to complete all surveys
            and formative assessments.{' '}
          </P>
          <Heading>Why were you chosen for this research?</Heading>
          <P>
            You are invited to take part in this research project being conducted by researchers from Monash University.
            This is because you are a residential care worker who will complete the HEALing Matters online training
            program.
          </P>
          <Heading>Source of funding</Heading>
          <P>Department of Health and Human Services</P>
          <Heading>Consenting to participate in the project and withdrawing from the research</Heading>
          <P>
            If you decide that you would like to participate, you must read this explanatory statement and proceed to
            the next step of the HEALing Matters online training. You do not need to sign a consent form. If you do not
            wish to be involved in the research, you will need to select the box at the bottom of this page to opt-out
            of the research. You do not have to take part in this research if you don’t want to.{' '}
          </P>
          <P>
            If you decide you no longer wish to participate, you are free to withdraw from the research at any stage.
            Your decision whether or not to withdraw from the research will not affect your relationship with your
            organisation or Monash University in any way. If you withdraw from the research your responses to surveys,
            formative assessments and interviews/focus groups will not be used.
          </P>
          <Heading>Possible benefits and risks to participants </Heading>
          <P>
            Participants may not benefit directly from participation in this research. However, participants will know
            that they are contributing to the evaluation of the HEALing Matters program, which will benefit the health
            and well-being of young people living in residential care.{' '}
          </P>
          <P>
            It is not anticipated that there will be any risks of harms associated with participation in this project,
            except for the inconvenience of completing the surveys and formative assessments.{' '}
          </P>
          <Heading>Services on offer if adversely affected </Heading>
          <P>
            Should participants become distressed when completing any aspect of the research, participants are
            encouraged to contact the research team, who will ensure that the participant is referred to appropriate
            supports as necessary. Participants can also access the following services:
          </P>
          <P>Lifeline Australia 13 11 14</P>
          <P>BeyondBlue Support Service 1300 224 636</P>
          <Heading>Confidentiality</Heading>
          <P>
            To ensure the confidentiality of participants, surveys will be coded with a unique ID and contact details
            will be stored separately from survey/formative assessments. Only group data will be reported in results.
            Findings will be published in peer-review journals and presented at research conferences.
          </P>
          <Heading>Storage of data</Heading>
          <P>
            Information will be stored in hard copy (paper copy) and computer files. Computer files will be password
            protected. All written/completed questionnaires with code numbers will be stored in a locked filing cabinet
            in a research office at Monash University. After 6 years, all paper and computer files will be destroyed.
          </P>
          <Heading>Results</Heading>
          <P>
            Participants who are interested in research findings can contacting Professor Helen Skouteris for a copy of
            the results.
          </P>
          <Heading>Complaints</Heading>
          <P>
            Should you have any concerns or complaints about the conduct of the project, you are welcome to contact the
            Executive Officer, Monash University Human Research Ethics (MUHREC):
          </P>

          <Box my={1}>
            <Typography variant="body2">Executive Officer</Typography>
            <Typography variant="body2">Monash University Human Research Ethics Committee (MUHREC)</Typography>
            <Typography variant="body2">Room 111, Chancellery Building E,</Typography>
          </Box>

          <Box my={1}>
            <Typography variant="body2">24 Sports Walk, Clayton Campus</Typography>
            <Typography variant="body2">Research Office</Typography>
            <Typography variant="body2">Monash University VIC 3800</Typography>
          </Box>

          <Box my={1}>
            <Typography variant="body2">
              <strong>Tel</strong>: +61 3 9905 2052
            </Typography>
            <Typography variant="body2">
              <strong>Email</strong>: muhrec@monash.edu
            </Typography>
            <Typography variant="body2">
              <strong>Fax</strong>: +61 3 9905 3831{' '}
            </Typography>
          </Box>
        </Box>
        <Typography>
          I have been asked to take part in this Monash University research project as part of my participation in the
          HEALing Matters program. I have read and understood the Explanatory Statement and hereby consent to
          participate in this project.
        </Typography>
        <Box display="flex" justifyContent="center" mt={2}>
          <Button variant="contained" color="primary" onClick={() => props.onChange(true)}>
            Agree
          </Button>
        </Box>
      </Box>
    </Paper>
  );
}
