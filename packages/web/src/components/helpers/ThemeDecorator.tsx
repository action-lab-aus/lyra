import { ThemeProvider } from '@material-ui/core';
import React from 'react';
import { theme } from '../../theme';

export const ThemeDecorator = (Story: React.ComponentType) => (
  <ThemeProvider theme={theme}>
    <Story />
  </ThemeProvider>
);
