import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { LocationProvider, createMemorySource, createHistory } from '@reach/router';
import { RootState } from 'app';
import { initialState } from './mocks';
import { ThemeProvider } from '@material-ui/core';
import { theme } from '../../theme';

const middleware =
  ({ dispatch, getState }) =>
  (next) =>
  (action) => {
    if (typeof action === 'function') {
      console.log(`invoked thunk ${action.name}`);
    } else {
      next(action);
    }
  };

const mockStore = configureStore([middleware]);

export type AppProviderProps = {
  path?: string;
  state?: RootState;
  children: React.ReactNode;
};

export const AppProvider = (props: AppProviderProps) => {
  const { path = '/', state = initialState, children } = props;
  const source = createMemorySource(path);
  const history = createHistory(source);
  history.listen(() => console.log('message arrived at router', source.location));
  const store = mockStore(state);
  return (
    <LocationProvider history={history}>
      <ThemeProvider theme={theme}>
        <Provider store={store}>{children}</Provider>
      </ThemeProvider>
    </LocationProvider>
  );
};
