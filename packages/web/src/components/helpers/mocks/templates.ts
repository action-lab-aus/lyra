import range from 'lodash/range';
import { UserSurvey, UserProfile, UserTopic } from 'app';
import { AgreeScales, ConfidentScales, FreqScales, OccasionScales, SimpleFreqScales } from 'components/feeback/helpers';

const createUserSurvey = (
  sectionSeq: number,
  questionRange: [number, number],
  value: string,
  keyFn?: (sectionSeq: number, questionSeq: number) => string,
) => {
  const [from, to] = questionRange;
  return range(from, to).reduce<UserSurvey>(
    (userSurvey, questionSeq) => {
      const key = keyFn ? keyFn(sectionSeq, questionSeq) : `s${sectionSeq}#q${questionSeq}`;
      return { ...userSurvey, [key]: value };
    },
    {
      _total: 11,
      _completed: 11,
      _step: 5,
    },
  );
};

export const templates = {
  user(overrides: Partial<UserProfile> = {}): UserProfile {
    return {
      userCategory: 'parent',
      userFirstname: 'John',
      userSurname: 'Smith',
      userAge: '39',
      userGender: 'male',
      userPostcode: '3000',
      userPhone: '0403456789',
      childName: 'Tim',
      childDob: '06/09/2005',
      childGrade: 12,
      childGender: 'male',
      childRelationship: 'Father',
      childOtherParentsJoined: 'No',
      smsNotification: false,
      ...overrides,
    };
  },

  userSurveys: {
    empty() {
      return {
        _total: 0,
        _completed: 0,
        _step: 0,
      } as UserSurvey;
    },

    s1Confidence(questionRange: [number, number] = [1, 12]) {
      return createUserSurvey(1, questionRange, ConfidentScales[0]);
    },

    s2Pradas(sectionRange: [number, number] = [1, 10]) {
      const [from, to] = sectionRange;
      const pradasSections: Array<[[number, number], string]> = [
        [[1, 9], FreqScales[0]],
        [[1, 9], FreqScales[0]],
        [[1, 7], FreqScales[0]],
        [[1, 10], FreqScales[0]],
        [[1, 9], FreqScales[0]],
        [[1, 13], FreqScales[0]],
        [[1, 11], FreqScales[0]],
        [[1, 10], FreqScales[0]],
        [[1, 10], FreqScales[0]],
      ];
      return range(from, to).reduce<UserSurvey>(
        (userSurvey, sectionSeq) => {
          const [questionRange, defaultValue] = pradasSections[sectionSeq - 1];
          return {
            ...userSurvey,
            ...createUserSurvey(
              sectionSeq,
              questionRange,
              defaultValue,
              (sectionSeq: number, questionSeq: number) => `S${sectionSeq}#Q${questionSeq}`,
            ),
          };
        },
        { _total: 11, _completed: 11, _step: 0 },
      );
    },

    // s3K6(questionRange: [number, number] = [1, 7]) {
    //   return createUserSurvey(1, questionRange, OccasionScales[0]);
    // },

    s3K6(sectionRange: [number, number] = [1, 3]) {
      const [from, to] = sectionRange;
      const pradasSections: Array<[[number, number], string]> = [
        [[1, 7], OccasionScales[0]],
        [[1, 6], AgreeScales[0]],
      ];
      return range(from, to).reduce<UserSurvey>(
        (userSurvey, sectionSeq) => {
          const [questionRange, defaultValue] = pradasSections[sectionSeq - 1];
          return {
            ...userSurvey,
            ...createUserSurvey(
              sectionSeq,
              questionRange,
              defaultValue,
              (sectionSeq: number, questionSeq: number) => `s${sectionSeq}#q${questionSeq}`,
            ),
          };
        },
        { _total: 11, _completed: 11, _step: 0 },
      );
    },

    s4Rcads(questionRange: [number, number] = [1, 26]) {
      return createUserSurvey(1, questionRange, SimpleFreqScales[0]);
    },
  },

  userTopics(overrides: Record<string, Partial<UserTopic>> = {}) {
    return range(1, 10).reduce<Record<string, UserTopic>>((userTopics, seq) => {
      const key = `m${seq}`;
      return {
        ...userTopics,
        [key]: {
          mandatory: false,
          locked: true,
          seq: seq,
          total: 10,
          comleted: 0,
          entries: {},
          ...(overrides[key] || {}),
        } as UserTopic,
      };
    }, {});
  },
};
