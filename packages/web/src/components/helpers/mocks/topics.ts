import { DashboardTopic } from 'components/dashboard';

// @ts-ignore
export const topics = [
  {
    id: 'm1',
    title: 'Connect',
    name: 'm1-connect',
    path: '/m1-connect',
    desc: '‘Connect’ is about empowering you to change your behaviour so you can positively influence your teenager. We’ll talk about ways to build your relationship and communication with your teenager. Connect is the foundational module of the program, so we encourage you to complete it if it’s recommended for you.',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/5434ef99af69630405d90c22e039f937/45758/m1.png',
              srcSet:
                '/static/5434ef99af69630405d90c22e039f937/4d865/m1.png 175w,\n/static/5434ef99af69630405d90c22e039f937/0ac2f/m1.png 350w,\n/static/5434ef99af69630405d90c22e039f937/45758/m1.png 700w',
              sizes: '(min-width: 700px) 700px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/5434ef99af69630405d90c22e039f937/cf941/m1.webp 175w,\n/static/5434ef99af69630405d90c22e039f937/4732e/m1.webp 350w,\n/static/5434ef99af69630405d90c22e039f937/50544/m1.webp 700w',
                type: 'image/webp',
                sizes: '(min-width: 700px) 700px, 100vw',
              },
            ],
          },
          width: 700,
          height: 560,
        },
      },
    },
    nav: {
      defaultEntry: {
        path: '/m1-connect/00-home',
      },
      sequence: [
        {
          key: '00-home',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '01-checking-in',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '02-good-relationship',
          activityKeys: ['textStepperGoodRelationship'],
          optional: false,
          goals: false,
        },
        {
          key: '03-ups-and-downs',
          activityKeys: ['textRevealAdolescentBrain'],
          optional: false,
          goals: false,
        },
        {
          key: '04-pandemic-emotions',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '05-three-strategies',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '06-show-affection',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '07-things-you-can-do',
          activityKeys: [
            'textRevealHugs',
            'textRevealBrag',
            'textRevealHighFive',
            'textRevealPat',
            'textRevealSqueeze',
            'textRevealRuffle',
            'textRevealInvite',
            'textRevealDrape',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '08-things-you-can-say',
          activityKeys: [
            'textRevealEncouragement',
            'textStepperEncouragement',
            'textRevealAcknowledgement',
            'textStepperAcknowledgement',
            'textRevealAffection',
            'textStepperAffection',
            'textRevealAppreciation',
            'textStepperAppreciation',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '09-be-genuine',
          activityKeys: ['textRevealBeGenuine', 'textRevealSmile', 'textRevealLaugh'],
          optional: false,
          goals: false,
        },
        {
          key: '10-take-time-to-talk',
          activityKeys: ['imageStepperTalk'],
          optional: false,
          goals: false,
        },
        {
          key: '11-conversation',
          activityKeys: ['imageStepperConversation'],
          optional: false,
          goals: false,
        },
        {
          key: '12-talking-to-brickwall',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '13-talking-tough-stuff',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '14-identify-validate-understand',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '15-things-to-avoid',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '16-your-response-matters',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '17-video-activity',
          activityKeys: ['quizIvu1', 'quizIvu2', 'quizIvu3'],
          optional: false,
          goals: false,
        },
        {
          key: '18-pandemic-support',
          activityKeys: [
            'textRevealAsk',
            'textRevealShow',
            'textRevealPatient',
            'textRevealReassuring',
            'textRevealMonitor',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '19-pandemic-bounceback',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '20-goals',
          activityKeys: null,
          optional: false,
          goals: true,
        },
        {
          key: '21-quiz',
          activityKeys: ['quizQuestion1', 'quizQuestion2', 'quizQuestion3', 'quizQuestion4', 'quizQuestion5'],
          optional: false,
          goals: false,
        },
        {
          key: '22-dont-blame-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
      ],
    },
  },
  {
    id: 'm10',
    title: "When things aren't okay",
    name: 'm10-seeking-help',
    path: '/m10-seeking-help',
    desc: 'It can be hard for parents to tell whether changes in their teenager’s behaviour are a normal part of adolescence, or a more serious problem like depression or anxiety. In this module, we’ll take a closer look at depression and clinical anxiety, including when and how to seek professional help.',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/150f25ad74e29ea9af44a33ffbef6472/45758/m10.png',
              srcSet:
                '/static/150f25ad74e29ea9af44a33ffbef6472/4d865/m10.png 175w,\n/static/150f25ad74e29ea9af44a33ffbef6472/0ac2f/m10.png 350w,\n/static/150f25ad74e29ea9af44a33ffbef6472/45758/m10.png 700w',
              sizes: '(min-width: 700px) 700px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/150f25ad74e29ea9af44a33ffbef6472/cf941/m10.webp 175w,\n/static/150f25ad74e29ea9af44a33ffbef6472/4732e/m10.webp 350w,\n/static/150f25ad74e29ea9af44a33ffbef6472/50544/m10.webp 700w',
                type: 'image/webp',
                sizes: '(min-width: 700px) 700px, 100vw',
              },
            ],
          },
          width: 700,
          height: 560,
        },
      },
    },
    nav: {
      defaultEntry: {
        path: '/m10-seeking-help/00-home',
      },
      sequence: [
        {
          key: '00-home',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '01-checking-in',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '02-mental-health',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '03-risk-factors',
          activityKeys: ['textStepperFactors'],
          optional: false,
          goals: false,
        },
        {
          key: '04-depression-signs',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '05-anxiety-signs',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '06-what-is-normal',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '07-what-should-I-do',
          activityKeys: ['textRevealEncourage', 'textRevealListen'],
          optional: false,
          goals: false,
        },
        {
          key: '08-teen-seek-help',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '09-where-is-help',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '10-previous-depression',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '11-goals',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '12-quiz',
          activityKeys: ['quizQuestion1', 'quizQuestion2', 'quizQuestion3', 'quizQuestion4', 'quizQuestion5'],
          optional: false,
          goals: false,
        },
        {
          key: '13-dont-blame-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
      ],
    },
  },
  {
    id: 'm2',
    title: 'Parenting through the pandemic',
    name: 'm2-parenting-in-pandemic',
    path: '/m2-parenting-in-pandemic',
    desc: 'The COVID-19 pandemic has brought with it many new challenges for parents and families. This module aims to support you and your teen to navigate the ‘new normal’ and respond to the challenges of the pandemic with confidence.',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#f8f8f8',
          images: {
            fallback: {
              src: '/static/07ebc51ee91f74868d51b4e3ea0fb71b/45758/m2.png',
              srcSet:
                '/static/07ebc51ee91f74868d51b4e3ea0fb71b/4d865/m2.png 175w,\n/static/07ebc51ee91f74868d51b4e3ea0fb71b/0ac2f/m2.png 350w,\n/static/07ebc51ee91f74868d51b4e3ea0fb71b/45758/m2.png 700w,\n/static/07ebc51ee91f74868d51b4e3ea0fb71b/0ba9e/m2.png 1400w',
              sizes: '(min-width: 700px) 700px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/07ebc51ee91f74868d51b4e3ea0fb71b/cf941/m2.webp 175w,\n/static/07ebc51ee91f74868d51b4e3ea0fb71b/4732e/m2.webp 350w,\n/static/07ebc51ee91f74868d51b4e3ea0fb71b/50544/m2.webp 700w,\n/static/07ebc51ee91f74868d51b4e3ea0fb71b/22c2f/m2.webp 1400w',
                type: 'image/webp',
                sizes: '(min-width: 700px) 700px, 100vw',
              },
            ],
          },
          width: 700,
          height: 560,
        },
      },
    },
    nav: {
      defaultEntry: {
        path: '/m2-parenting-in-pandemic/00-home',
      },
      sequence: [
        {
          key: '00-home',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '01-care-for-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '02-checking-in',
          activityKeys: ['confidenceParentingInPandemic'],
          optional: false,
          goals: false,
        },
        {
          key: '03-self-care',
          activityKeys: ['currentCareActions'],
          optional: false,
          goals: false,
        },
        {
          key: '04-ask-support',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '05-discuss',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '06-find-info',
          activityKeys: [
            'pandemicFindInfo1',
            'pandemicFindInfo2',
            'pandemicFindInfo3',
            'pandemicFindInfo4',
            'pandemicFindInfo5',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '07-have-conversation',
          activityKeys: ['ButtonDialog_copingTipsDialog'],
          optional: false,
          goals: false,
        },
        {
          key: '08-address-misconceptions',
          activityKeys: [
            'ButtonDialog_sample1',
            'ButtonDialog_sample2',
            'ButtonDialog_sample3',
            'ButtonDialog_sample4',
            'ButtonDialog_sample5',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '09-follow-restrictions',
          activityKeys: [
            'quizFollowRestrictions1',
            'quizFollowRestrictions2',
            'quizFollowRestrictions3',
            'quizFollowRestrictions4',
            'quizFollowRestrictions5',
            'quizFollowRestrictions6',
            'quizFollowRestrictions7',
            'quizFollowRestrictions8',
            'quizFollowRestrictions9',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '10-new-normal',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '11-coping-worries',
          activityKeys: [
            'pandemicCopingWorries1',
            'pandemicCopingWorries2',
            'pandemicCopingWorries3',
            'pandemicCopingWorries4',
            'pandemicReflection',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '12-easing-restrictions',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '13-connection',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '14-routines',
          activityKeys: [
            'quizRoutine1',
            'quizRoutine2',
            'quizRoutine3',
            'quizRoutine4',
            'quizRoutine5',
            'quizRoutine6',
            'quizRoutine7',
            'quizRoutine8',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '15-silver-linings',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '16-expectations',
          activityKeys: ['ButtonDialog_expectationDialog'],
          optional: false,
          goals: false,
        },
        {
          key: '17-optional-topics',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '18-work-from-home',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '19-online-learning',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '20-support-online-learning',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '21-facetoface-school',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '22-loved-one',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '23-loved-one-covid',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '24-loved-one-death',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '25-signs-grief',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '26-support-grief',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '27-family-violence',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '28-risk-family-violence',
          activityKeys: null,
          optional: true,
          goals: false,
        },
        {
          key: '29-seek-help',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '30-goals',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '31-quiz',
          activityKeys: [
            'pandemicQuizQuestion1',
            'pandemicQuizQuestion2',
            'pandemicQuizQuestion3',
            'pandemicQuizQuestion4',
            'pandemicQuizQuestion5',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '32-dont-blame-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
      ],
    },
  },
  {
    id: 'm3',
    title: 'Raising good kids into great adults',
    name: 'm3-family-rules',
    path: '/m3-family-rules',
    desc: 'Teenagers need the adults around them to set clear boundaries and expectations for their behaviour. This module will help you to establish clear expectations that your teen is likely to follow.',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#0878c8',
          images: {
            fallback: {
              src: '/static/a1af8ea67b49f9e6a85846cd4fe92592/45758/m3.png',
              srcSet:
                '/static/a1af8ea67b49f9e6a85846cd4fe92592/4d865/m3.png 175w,\n/static/a1af8ea67b49f9e6a85846cd4fe92592/0ac2f/m3.png 350w,\n/static/a1af8ea67b49f9e6a85846cd4fe92592/45758/m3.png 700w',
              sizes: '(min-width: 700px) 700px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/a1af8ea67b49f9e6a85846cd4fe92592/cf941/m3.webp 175w,\n/static/a1af8ea67b49f9e6a85846cd4fe92592/4732e/m3.webp 350w,\n/static/a1af8ea67b49f9e6a85846cd4fe92592/50544/m3.webp 700w',
                type: 'image/webp',
                sizes: '(min-width: 700px) 700px, 100vw',
              },
            ],
          },
          width: 700,
          height: 560,
        },
      },
    },
    nav: {
      defaultEntry: {
        path: '/m3-family-rules/00-home',
      },
      sequence: [
        {
          key: '00-home',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '01-checking-in',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '02-why-family-rules',
          activityKeys: ['textStepperFamilyRules'],
          optional: false,
          goals: false,
        },
        {
          key: '03-what-rules',
          activityKeys: ['textRevealSafety', 'textRevealDailyRoutines', 'textRevealTreatEachOther'],
          optional: false,
          goals: false,
        },
        {
          key: '04-who-makes-rules',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '05-how-to-rules',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '06-apply-rules',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '07-review-rules',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '08-rules-as-foundations',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '09-reward-good-behaviour',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '10-reflect-on-rules',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '11-goals',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '12-quiz',
          activityKeys: ['quizQuestion1', 'quizQuestion2', 'quizQuestion3', 'quizQuestion4', 'quizQuestion5'],
          optional: false,
          goals: false,
        },
        {
          key: '13-dont-blame-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
      ],
    },
  },
  {
    id: 'm4',
    title: 'Nurture roots & inspire wings',
    name: 'm4-nurture',
    path: '/m4-nurture',
    desc: '‘Nurture roots’ is all about finding a balanced way to stay involved in your teenager’s life while also allowing them to ‘leave the nest’ and develop age-appropriate independence.',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/02679d06070d0c95090cefe6c814ca4f/45758/m4.png',
              srcSet:
                '/static/02679d06070d0c95090cefe6c814ca4f/4d865/m4.png 175w,\n/static/02679d06070d0c95090cefe6c814ca4f/0ac2f/m4.png 350w,\n/static/02679d06070d0c95090cefe6c814ca4f/45758/m4.png 700w',
              sizes: '(min-width: 700px) 700px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/02679d06070d0c95090cefe6c814ca4f/cf941/m4.webp 175w,\n/static/02679d06070d0c95090cefe6c814ca4f/4732e/m4.webp 350w,\n/static/02679d06070d0c95090cefe6c814ca4f/50544/m4.webp 700w',
                type: 'image/webp',
                sizes: '(min-width: 700px) 700px, 100vw',
              },
            ],
          },
          width: 700,
          height: 560,
        },
      },
    },
    nav: {
      defaultEntry: {
        path: '/m4-nurture/00-home',
      },
      sequence: [
        {
          key: '00-home',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '01-checking-in',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '02-balancing-act',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '03-how-to-stay-connected',
          activityKeys: [
            'textRevealOneOnOne',
            'textRevealTakeInterest',
            'textRevealDinnerTogether',
            'textRevealKnowFriends',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '04-know-your-teen',
          activityKeys: ['quizTeen1', 'quizTeen2', 'quizTeen3', 'quizTeen4', 'quizTeen5'],
          optional: false,
          goals: false,
        },
        {
          key: '05-ways-to-connect',
          activityKeys: ['textRevealOneHow', 'textRevealIncludeHow', 'textRevealActivitiesHow'],
          optional: false,
          goals: false,
        },
        {
          key: '06-the-together-list',
          activityKeys: ['textStepperMoreIdeas'],
          optional: false,
          goals: false,
        },
        {
          key: '07-encourage-independence',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '08-responsibilities',
          activityKeys: ['textStepperWhatShouldTeenagerDo'],
          optional: false,
          goals: false,
        },
        {
          key: '09-other-activities',
          activityKeys: ['textStepperWhatElse'],
          optional: false,
          goals: false,
        },
        {
          key: '10-am-i-over-involved',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '11-goals',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '12-quiz',
          activityKeys: ['quizQuestion1', 'quizQuestion2', 'quizQuestion3', 'quizQuestion4', 'quizQuestion5'],
          optional: false,
          goals: false,
        },
        {
          key: '13-dont-blame-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
      ],
    },
  },
  {
    id: 'm5',
    title: 'Calm versus conflict',
    name: 'm5-conflict',
    path: '/m5-conflict',
    desc: 'Parents play an important role in helping their teenagers learn to manage conflict. ‘Calm versus conflict’ aims to help you manage conflict at home, to create a safe and supportive environment for your teenager.',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#f8f8f8',
          images: {
            fallback: {
              src: '/static/ae267c782ee2db00bd04df97d9016473/45758/m5.png',
              srcSet:
                '/static/ae267c782ee2db00bd04df97d9016473/4d865/m5.png 175w,\n/static/ae267c782ee2db00bd04df97d9016473/0ac2f/m5.png 350w,\n/static/ae267c782ee2db00bd04df97d9016473/45758/m5.png 700w,\n/static/ae267c782ee2db00bd04df97d9016473/0ba9e/m5.png 1400w',
              sizes: '(min-width: 700px) 700px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/ae267c782ee2db00bd04df97d9016473/cf941/m5.webp 175w,\n/static/ae267c782ee2db00bd04df97d9016473/4732e/m5.webp 350w,\n/static/ae267c782ee2db00bd04df97d9016473/50544/m5.webp 700w,\n/static/ae267c782ee2db00bd04df97d9016473/22c2f/m5.webp 1400w',
                type: 'image/webp',
                sizes: '(min-width: 700px) 700px, 100vw',
              },
            ],
          },
          width: 700,
          height: 560,
        },
      },
    },
    nav: {
      defaultEntry: {
        path: '/m5-conflict/00-home',
      },
      sequence: [
        {
          key: '00-home',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '01-checking-in',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '02-manage-disagreements',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '03-conflict-partner',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '04-establish-ground-rules',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '05-ground-rules',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '06-ground-rules-cont',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '07-remain-calm',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '08-siblings',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '09-assertive-communication',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '10-communication-styles',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '11-communication-styles-cont',
          activityKeys: ['imageRevealPassive', 'imageRevealAssertive', 'imageRevealAggressive'],
          optional: false,
          goals: false,
        },
        {
          key: '12-carry-on-loving-them',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '13-goals',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '14-quiz',
          activityKeys: ['quizQuestion1', 'quizQuestion2', 'quizQuestion3', 'quizQuestion4', 'quizQuestion5'],
          optional: false,
          goals: false,
        },
        {
          key: '15-dont-blame-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
      ],
    },
  },
  {
    id: 'm6',
    title: 'Good friends = supportive relationships',
    name: 'm6-friends',
    path: '/m6-friends',
    desc: 'As teenagers begin to place greater emphasis on their peers, parents still play an important role in helping their teens to develop social skills. This module helps you to support your teen to develop social skills across a variety of settings.',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/616c3b56f9dec1e6b1739a21679f8cd2/45758/m6.png',
              srcSet:
                '/static/616c3b56f9dec1e6b1739a21679f8cd2/4d865/m6.png 175w,\n/static/616c3b56f9dec1e6b1739a21679f8cd2/0ac2f/m6.png 350w,\n/static/616c3b56f9dec1e6b1739a21679f8cd2/45758/m6.png 700w',
              sizes: '(min-width: 700px) 700px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/616c3b56f9dec1e6b1739a21679f8cd2/cf941/m6.webp 175w,\n/static/616c3b56f9dec1e6b1739a21679f8cd2/4732e/m6.webp 350w,\n/static/616c3b56f9dec1e6b1739a21679f8cd2/50544/m6.webp 700w',
                type: 'image/webp',
                sizes: '(min-width: 700px) 700px, 100vw',
              },
            ],
          },
          width: 700,
          height: 560,
        },
      },
    },
    nav: {
      defaultEntry: {
        path: '/m6-friends/00-home',
      },
      sequence: [
        {
          key: '00-home',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '01-checking-in',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '02-friendships',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '03-social-situations',
          activityKeys: ['textStepperSocialSituations'],
          optional: false,
          goals: false,
        },
        {
          key: '04-you-and-their-friends',
          activityKeys: ['quizChoose1', 'quizChoose2', 'quizChoose3', 'quizChoose4'],
          optional: false,
          goals: false,
        },
        {
          key: '05-friends-range-of-ages',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '06-good-social-skills',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '07-friendship-troubles',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '08-navigate-problems',
          activityKeys: ['textRevealDealingBully', 'textRevealManagingCyberBully', 'textRevealSupportingLGBT'],
          optional: false,
          goals: false,
        },
        {
          key: '09-goals',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '10-quiz',
          activityKeys: ['quizQuestion1', 'quizQuestion2', 'quizQuestion3', 'quizQuestion4', 'quizQuestion5'],
          optional: false,
          goals: false,
        },
        {
          key: '11-dont-blame-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
      ],
    },
  },
  {
    id: 'm7',
    title: 'Good health habits for good mental health',
    name: 'm7-health-habits',
    path: '/m7-health-habits',
    desc: 'Getting enough sleep, eating well, being physically active, and not using alcohol or drugs can protect our physical and mental health. This module is all about helping you to support your teenager to develop good health habits.',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/6d3905cdf0132649a77513e0a0f09854/45758/m7.png',
              srcSet:
                '/static/6d3905cdf0132649a77513e0a0f09854/4d865/m7.png 175w,\n/static/6d3905cdf0132649a77513e0a0f09854/0ac2f/m7.png 350w,\n/static/6d3905cdf0132649a77513e0a0f09854/45758/m7.png 700w',
              sizes: '(min-width: 700px) 700px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/6d3905cdf0132649a77513e0a0f09854/cf941/m7.webp 175w,\n/static/6d3905cdf0132649a77513e0a0f09854/4732e/m7.webp 350w,\n/static/6d3905cdf0132649a77513e0a0f09854/50544/m7.webp 700w',
                type: 'image/webp',
                sizes: '(min-width: 700px) 700px, 100vw',
              },
            ],
          },
          width: 700,
          height: 560,
        },
      },
    },
    nav: {
      defaultEntry: {
        path: '/m7-health-habits/00-home',
      },
      sequence: [
        {
          key: '00-home',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '01-checking-in',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '02-encourage-healthy-habits',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '03-healthy-diet',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '04-how-to-healthy-diet',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '05-daily-exercise',
          activityKeys: ['textStepperSplitExercise'],
          optional: false,
          goals: false,
        },
        {
          key: '06-build-into-life',
          activityKeys: ['textStepperDailyExercise'],
          optional: false,
          goals: false,
        },
        {
          key: '07-screentime',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '08-screentime-quiz',
          activityKeys: [
            'quizTeenager1',
            'quizTeenager2',
            'quizTeenager3',
            'quizTeenager4',
            'quizTeenager5',
            'quizTeenager6',
            'textRevealPandemicScreenTime',
          ],
          optional: false,
          goals: false,
        },
        {
          key: '09-healthy-screen-use',
          activityKeys: ['textRevealScreen1', 'textRevealScreen2', 'textRevealScreen3'],
          optional: false,
          goals: false,
        },
        {
          key: '10-sleep-habits',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '11-what-if-sleep-problems',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '12-no-alcohol-drugs',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '13-what-if-alchohol-drugs',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '14-goals',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '15-quiz',
          activityKeys: ['quizQuestion1', 'quizQuestion2', 'quizQuestion3', 'quizQuestion4', 'quizQuestion5'],
          optional: false,
          goals: false,
        },
        {
          key: '16-dont-blame-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
      ],
    },
  },
  {
    id: 'm8',
    title: 'Partners in problem solving',
    name: 'm8-problems',
    path: '/m8-problems',
    desc: 'Problems are a part of life. As teenagers mature, they need to learn strategies to tackle the problems they will face throughout their life. ‘Partners in problem solving’ focusses on how you as a parent can support your teenager to develop good problem-solving skills.',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/9d649ef9175756227fa5081cead55cfc/45758/m8.png',
              srcSet:
                '/static/9d649ef9175756227fa5081cead55cfc/4d865/m8.png 175w,\n/static/9d649ef9175756227fa5081cead55cfc/0ac2f/m8.png 350w,\n/static/9d649ef9175756227fa5081cead55cfc/45758/m8.png 700w',
              sizes: '(min-width: 700px) 700px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/9d649ef9175756227fa5081cead55cfc/cf941/m8.webp 175w,\n/static/9d649ef9175756227fa5081cead55cfc/4732e/m8.webp 350w,\n/static/9d649ef9175756227fa5081cead55cfc/50544/m8.webp 700w',
                type: 'image/webp',
                sizes: '(min-width: 700px) 700px, 100vw',
              },
            ],
          },
          width: 700,
          height: 560,
        },
      },
    },
    nav: {
      defaultEntry: {
        path: '/m8-problems/00-home',
      },
      sequence: [
        {
          key: '00-home',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '01-checking-in',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '02-deal-with-problems',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '03-problem-solving',
          activityKeys: ['quizDef1', 'quizDef2', 'quizDef3', 'quizDef4', 'quizDef5'],
          optional: false,
          goals: false,
        },
        {
          key: '04-brainstorm-solutions',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '05-evaluate-solutions',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '06-decide-solution',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '07-evaluate-outcome',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '08-stress-management',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '09-signs-of-stress',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '10-pressures-expectations',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '11-navigate-problems',
          activityKeys: ['textRevealDealingBully', 'textRevealManagingCyberBully', 'textRevealSupportingLGBT'],
          optional: false,
          goals: false,
        },
        {
          key: '12-goals',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '13-quiz',
          activityKeys: ['quizQuestion1', 'quizQuestion2', 'quizQuestion3', 'quizQuestion4', 'quizQuestion5'],
          optional: false,
          goals: false,
        },
        {
          key: '14-dont-blame-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
      ],
    },
  },
  {
    id: 'm9',
    title: 'From surviving to thriving',
    name: 'm9-anxiety',
    path: '/m9-anxiety',
    desc: 'We all feel anxious at times, and it’s important for teenagers to learn to manage their everyday anxiety. ‘From surviving to thriving: Helping your teenager deal with anxiety’ will talk about ways that you can support your teenager to cope with the fears and worries they will inevitably face.',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/914ff3ab653356329416ef7f052e1643/45758/m9.png',
              srcSet:
                '/static/914ff3ab653356329416ef7f052e1643/4d865/m9.png 175w,\n/static/914ff3ab653356329416ef7f052e1643/0ac2f/m9.png 350w,\n/static/914ff3ab653356329416ef7f052e1643/45758/m9.png 700w',
              sizes: '(min-width: 700px) 700px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/914ff3ab653356329416ef7f052e1643/cf941/m9.webp 175w,\n/static/914ff3ab653356329416ef7f052e1643/4732e/m9.webp 350w,\n/static/914ff3ab653356329416ef7f052e1643/50544/m9.webp 700w',
                type: 'image/webp',
                sizes: '(min-width: 700px) 700px, 100vw',
              },
            ],
          },
          width: 700,
          height: 560,
        },
      },
    },
    nav: {
      defaultEntry: {
        path: '/m9-anxiety/00-home',
      },
      sequence: [
        {
          key: '00-home',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '01-checking-in',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '02-anxiety',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '03-parental-accommodation',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '04-reflection',
          activityKeys: ['quizTeen1', 'quizTeen2', 'quizTeen3', 'quizTeen4', 'quizTeen5'],
          optional: false,
          goals: false,
        },
        {
          key: '05-support',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '06-facing-fears',
          activityKeys: ['textReveal1', 'textReveal2', 'textReveal3'],
          optional: false,
          goals: false,
        },
        {
          key: '07-other-tips',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '08-manage-own-anxiety',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '09-goals',
          activityKeys: null,
          optional: false,
          goals: false,
        },
        {
          key: '10-quiz',
          activityKeys: ['quizQuestion1', 'quizQuestion2', 'quizQuestion3', 'quizQuestion4', 'quizQuestion5'],
          optional: false,
          goals: false,
        },
        {
          key: '11-dont-blame-yourself',
          activityKeys: null,
          optional: false,
          goals: false,
        },
      ],
    },
  },
] as DashboardTopic[];
