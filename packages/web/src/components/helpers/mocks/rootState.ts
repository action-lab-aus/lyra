import update from 'immutability-helper';
import { AuthState, ProfileState, RootState } from 'app';

export const initialState: RootState = {
  auth: {
    authenticated: null,
    userInfo: null,
    status: null,
  },
  profile: {
    fetched: false,
    status: null,
    data: null,
  },
};

export function rootState() {
  let currentState = initialState;
  return {
    get state() {
      return currentState;
    },

    authenticated(
      auth: Partial<AuthState> = { authenticated: true, userInfo: { uid: '123567890ABCD', email: 'user@example.com' } },
    ) {
      currentState = update(currentState, {
        auth: { $merge: auth },
      });
      return this;
    },

    profile(profile: Partial<ProfileState>) {
      currentState = update(currentState, {
        profile: { $merge: profile },
      });
      return this;
    },
  };
}
