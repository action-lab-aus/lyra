import { DashboardSurvey } from 'components/dashboard';

export const dashboardSurveys = [
  {
    id: 'f1-evaluation',
    title: 'Your evaluation of PiP+',
    tint: '#6AA0EF',
    scope: 'followup',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/03e1df71120fb1b9e80c18d3c1eeac6e/eac02/s1.png',
              srcSet:
                '/static/03e1df71120fb1b9e80c18d3c1eeac6e/cf993/s1.png 47w,\n/static/03e1df71120fb1b9e80c18d3c1eeac6e/a51b0/s1.png 94w,\n/static/03e1df71120fb1b9e80c18d3c1eeac6e/eac02/s1.png 188w',
              sizes: '(min-width: 188px) 188px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/03e1df71120fb1b9e80c18d3c1eeac6e/c3ccb/s1.webp 47w,\n/static/03e1df71120fb1b9e80c18d3c1eeac6e/971b9/s1.webp 94w,\n/static/03e1df71120fb1b9e80c18d3c1eeac6e/309c6/s1.webp 188w',
                type: 'image/webp',
                sizes: '(min-width: 188px) 188px, 100vw',
              },
            ],
          },
          width: 188,
          height: 412,
        },
      },
    },
    sections: [
      {
        key: 's1',
        type: 'rating',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'q1',
            type: null,
            required: null,
          },
          {
            key: 'q2',
            type: null,
            required: null,
          },
          {
            key: 'q3',
            type: null,
            required: null,
          },
          {
            key: 'q4',
            type: 'radio',
            required: null,
          },
          {
            key: 'q4a',
            type: null,
            required: null,
          },
          {
            key: 'q4b',
            type: null,
            required: null,
          },
          {
            key: 'q4c',
            type: 'radio',
            required: null,
          },
          {
            key: 'q5',
            type: 'radio',
            required: null,
          },
          {
            key: 'q6',
            type: 'radio',
            required: null,
          },
          {
            key: 'q7',
            type: null,
            required: null,
          },
          {
            key: 'q8',
            type: null,
            required: null,
          },
          {
            key: 'q9',
            type: 'radio',
            required: null,
          },
          {
            key: 'q10',
            type: 'radio',
            required: null,
          },
          {
            key: 'q11',
            type: 'radio',
            required: false,
          },
          {
            key: 'q12',
            type: 'radio',
            required: false,
          },
        ],
      },
      {
        key: 's2',
        type: 'rating',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'intro2',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'q1',
            type: null,
            required: null,
          },
          {
            key: 'q2',
            type: null,
            required: null,
          },
          {
            key: 'q3',
            type: null,
            required: null,
          },
          {
            key: 'q4',
            type: null,
            required: null,
          },
          {
            key: 'q5',
            type: null,
            required: null,
          },
          {
            key: 'q6',
            type: null,
            required: null,
          },
          {
            key: 'q7',
            type: null,
            required: null,
          },
          {
            key: 'q8',
            type: null,
            required: null,
          },
          {
            key: 'q9',
            type: null,
            required: null,
          },
          {
            key: 'q10',
            type: null,
            required: null,
          },
          {
            key: 'q11',
            type: null,
            required: null,
          },
        ],
      },
    ],
  },
  {
    id: 'f2-pradas',
    title: 'Your parenting',
    tint: '#FF8256',
    scope: 'followup',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/3c9ca4b32902cd14585ca94c9dd95423/fb459/s2.png',
              srcSet:
                '/static/3c9ca4b32902cd14585ca94c9dd95423/a4963/s2.png 114w,\n/static/3c9ca4b32902cd14585ca94c9dd95423/a33bc/s2.png 227w,\n/static/3c9ca4b32902cd14585ca94c9dd95423/fb459/s2.png 454w',
              sizes: '(min-width: 454px) 454px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/3c9ca4b32902cd14585ca94c9dd95423/8ce3b/s2.webp 114w,\n/static/3c9ca4b32902cd14585ca94c9dd95423/84658/s2.webp 227w,\n/static/3c9ca4b32902cd14585ca94c9dd95423/e330b/s2.webp 454w',
                type: 'image/webp',
                sizes: '(min-width: 454px) 454px, 100vw',
              },
            ],
          },
          width: 454,
          height: 156,
        },
      },
    },
    sections: [
      {
        key: 'S1',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S2',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S3',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S4',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'note',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q2',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q3',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q4',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q5',
            type: 'select',
            required: null,
          },
          {
            key: 'divider',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'Q9',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S5',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q7',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S6',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'divider',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q9',
            type: null,
            required: null,
          },
          {
            key: 'Q10',
            type: null,
            required: null,
          },
          {
            key: 'Q11',
            type: null,
            required: null,
          },
          {
            key: 'Q12',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S7',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'divider',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'Q9',
            type: null,
            required: null,
          },
          {
            key: 'Q10',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S8',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro-1',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'intro-2',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'divider',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q7',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q8',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q9',
            type: 'radio',
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S9',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'divider-1',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'divider-2',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'Q9',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
    ],
  },
  {
    id: 'f3-k6',
    title: 'Your mental wellbeing',
    tint: '#AE6AEF',
    scope: 'followup',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/c24591b2d9df2197f6cc604d84d1dbfb/88722/s3.png',
              srcSet:
                '/static/c24591b2d9df2197f6cc604d84d1dbfb/a10b9/s3.png 37w,\n/static/c24591b2d9df2197f6cc604d84d1dbfb/f3923/s3.png 74w,\n/static/c24591b2d9df2197f6cc604d84d1dbfb/88722/s3.png 148w',
              sizes: '(min-width: 148px) 148px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/c24591b2d9df2197f6cc604d84d1dbfb/d9f9b/s3.webp 37w,\n/static/c24591b2d9df2197f6cc604d84d1dbfb/45dce/s3.webp 74w,\n/static/c24591b2d9df2197f6cc604d84d1dbfb/29551/s3.webp 148w',
                type: 'image/webp',
                sizes: '(min-width: 148px) 148px, 100vw',
              },
            ],
          },
          width: 148,
          height: 340,
        },
      },
    },
    sections: [
      {
        key: 's1',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro1',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'intro2',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'q1',
            type: null,
            required: null,
          },
          {
            key: 'q2',
            type: null,
            required: null,
          },
          {
            key: 'q3',
            type: null,
            required: null,
          },
          {
            key: 'q4',
            type: null,
            required: null,
          },
          {
            key: 'q5',
            type: null,
            required: null,
          },
          {
            key: 'q6',
            type: null,
            required: null,
          },
        ],
      },
      {
        key: 's2',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro1',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'q1',
            type: 'rating',
            required: null,
          },
          {
            key: 'q2',
            type: null,
            required: null,
          },
          {
            key: 'q3',
            type: null,
            required: null,
          },
          {
            key: 'q4',
            type: null,
            required: null,
          },
          {
            key: 'q5',
            type: null,
            required: null,
          },
          {
            key: 'q6',
            type: null,
            required: null,
          },
        ],
      },
    ],
  },
  {
    id: 'f4-rcads',
    title: "${childName}'s mental wellbeing",
    tint: '#EF6A6E',
    scope: 'followup',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/95fd65b6497b0b167720226672217e79/fd57a/s4.png',
              srcSet:
                '/static/95fd65b6497b0b167720226672217e79/446ed/s4.png 40w,\n/static/95fd65b6497b0b167720226672217e79/28c0d/s4.png 79w,\n/static/95fd65b6497b0b167720226672217e79/fd57a/s4.png 158w',
              sizes: '(min-width: 158px) 158px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/95fd65b6497b0b167720226672217e79/4b83e/s4.webp 40w,\n/static/95fd65b6497b0b167720226672217e79/72e1e/s4.webp 79w,\n/static/95fd65b6497b0b167720226672217e79/97b75/s4.webp 158w',
                type: 'image/webp',
                sizes: '(min-width: 158px) 158px, 100vw',
              },
            ],
          },
          width: 158,
          height: 336,
        },
      },
    },
    sections: [
      {
        key: 's1',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'q1',
            type: null,
            required: null,
          },
          {
            key: 'q2',
            type: null,
            required: null,
          },
          {
            key: 'q3',
            type: null,
            required: null,
          },
          {
            key: 'q4',
            type: null,
            required: null,
          },
          {
            key: 'q5',
            type: null,
            required: null,
          },
          {
            key: 'q6',
            type: null,
            required: null,
          },
          {
            key: 'q7',
            type: null,
            required: null,
          },
          {
            key: 'q8',
            type: null,
            required: null,
          },
          {
            key: 'q9',
            type: null,
            required: null,
          },
          {
            key: 'q10',
            type: null,
            required: null,
          },
          {
            key: 'q11',
            type: null,
            required: null,
          },
          {
            key: 'q12',
            type: null,
            required: null,
          },
          {
            key: 'q13',
            type: null,
            required: null,
          },
          {
            key: 'q14',
            type: null,
            required: null,
          },
          {
            key: 'q15',
            type: null,
            required: null,
          },
          {
            key: 'q16',
            type: null,
            required: null,
          },
          {
            key: 'q17',
            type: null,
            required: null,
          },
          {
            key: 'q18',
            type: null,
            required: null,
          },
          {
            key: 'q19',
            type: null,
            required: null,
          },
          {
            key: 'q20',
            type: null,
            required: null,
          },
          {
            key: 'q21',
            type: null,
            required: null,
          },
          {
            key: 'q22',
            type: null,
            required: null,
          },
          {
            key: 'q23',
            type: null,
            required: null,
          },
          {
            key: 'q24',
            type: null,
            required: null,
          },
          {
            key: 'q25',
            type: null,
            required: null,
          },
        ],
      },
    ],
  },
  {
    id: 's1-confidence',
    title: 'Your parenting confidence',
    tint: '#6AA0EF',
    scope: 'baseline',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/03e1df71120fb1b9e80c18d3c1eeac6e/eac02/s1.png',
              srcSet:
                '/static/03e1df71120fb1b9e80c18d3c1eeac6e/cf993/s1.png 47w,\n/static/03e1df71120fb1b9e80c18d3c1eeac6e/a51b0/s1.png 94w,\n/static/03e1df71120fb1b9e80c18d3c1eeac6e/eac02/s1.png 188w',
              sizes: '(min-width: 188px) 188px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/03e1df71120fb1b9e80c18d3c1eeac6e/c3ccb/s1.webp 47w,\n/static/03e1df71120fb1b9e80c18d3c1eeac6e/971b9/s1.webp 94w,\n/static/03e1df71120fb1b9e80c18d3c1eeac6e/309c6/s1.webp 188w',
                type: 'image/webp',
                sizes: '(min-width: 188px) 188px, 100vw',
              },
            ],
          },
          width: 188,
          height: 412,
        },
      },
    },
    sections: [
      {
        key: 's1',
        type: 'rating',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'intro2',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'q1',
            type: null,
            required: null,
          },
          {
            key: 'q2',
            type: null,
            required: null,
          },
          {
            key: 'q3',
            type: null,
            required: null,
          },
          {
            key: 'q4',
            type: null,
            required: null,
          },
          {
            key: 'q5',
            type: null,
            required: null,
          },
          {
            key: 'q6',
            type: null,
            required: null,
          },
          {
            key: 'q7',
            type: null,
            required: null,
          },
          {
            key: 'q8',
            type: null,
            required: null,
          },
          {
            key: 'q9',
            type: null,
            required: null,
          },
          {
            key: 'q10',
            type: null,
            required: null,
          },
          {
            key: 'q11',
            type: null,
            required: null,
          },
        ],
      },
    ],
  },
  {
    id: 's2-pradas',
    title: 'Your parenting',
    tint: '#FF8256',
    scope: 'baseline',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/3c9ca4b32902cd14585ca94c9dd95423/fb459/s2.png',
              srcSet:
                '/static/3c9ca4b32902cd14585ca94c9dd95423/a4963/s2.png 114w,\n/static/3c9ca4b32902cd14585ca94c9dd95423/a33bc/s2.png 227w,\n/static/3c9ca4b32902cd14585ca94c9dd95423/fb459/s2.png 454w',
              sizes: '(min-width: 454px) 454px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/3c9ca4b32902cd14585ca94c9dd95423/8ce3b/s2.webp 114w,\n/static/3c9ca4b32902cd14585ca94c9dd95423/84658/s2.webp 227w,\n/static/3c9ca4b32902cd14585ca94c9dd95423/e330b/s2.webp 454w',
                type: 'image/webp',
                sizes: '(min-width: 454px) 454px, 100vw',
              },
            ],
          },
          width: 454,
          height: 156,
        },
      },
    },
    sections: [
      {
        key: 'S1',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S2',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S3',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S4',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'note',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q2',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q3',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q4',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q5',
            type: 'select',
            required: null,
          },
          {
            key: 'divider',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'Q9',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S5',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q7',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S6',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'divider',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q9',
            type: null,
            required: null,
          },
          {
            key: 'Q10',
            type: null,
            required: null,
          },
          {
            key: 'Q11',
            type: null,
            required: null,
          },
          {
            key: 'Q12',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S7',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'divider',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'Q9',
            type: null,
            required: null,
          },
          {
            key: 'Q10',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S8',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro-1',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'intro-2',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'divider',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q7',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q8',
            type: 'radio',
            required: null,
          },
          {
            key: 'Q9',
            type: 'radio',
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
      {
        key: 'S9',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q1',
            type: null,
            required: null,
          },
          {
            key: 'Q2',
            type: null,
            required: null,
          },
          {
            key: 'Q3',
            type: null,
            required: null,
          },
          {
            key: 'Q4',
            type: null,
            required: null,
          },
          {
            key: 'divider-1',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q5',
            type: null,
            required: null,
          },
          {
            key: 'Q6',
            type: null,
            required: null,
          },
          {
            key: 'divider-2',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'Q7',
            type: null,
            required: null,
          },
          {
            key: 'Q8',
            type: null,
            required: null,
          },
          {
            key: 'Q9',
            type: null,
            required: null,
          },
          {
            key: 'PSES',
            type: 'rating',
            required: null,
          },
        ],
      },
    ],
  },
  {
    id: 's3-k6',
    title: 'Your mental wellbeing',
    tint: '#AE6AEF',
    scope: 'baseline',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/c24591b2d9df2197f6cc604d84d1dbfb/88722/s3.png',
              srcSet:
                '/static/c24591b2d9df2197f6cc604d84d1dbfb/a10b9/s3.png 37w,\n/static/c24591b2d9df2197f6cc604d84d1dbfb/f3923/s3.png 74w,\n/static/c24591b2d9df2197f6cc604d84d1dbfb/88722/s3.png 148w',
              sizes: '(min-width: 148px) 148px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/c24591b2d9df2197f6cc604d84d1dbfb/d9f9b/s3.webp 37w,\n/static/c24591b2d9df2197f6cc604d84d1dbfb/45dce/s3.webp 74w,\n/static/c24591b2d9df2197f6cc604d84d1dbfb/29551/s3.webp 148w',
                type: 'image/webp',
                sizes: '(min-width: 148px) 148px, 100vw',
              },
            ],
          },
          width: 148,
          height: 340,
        },
      },
    },
    sections: [
      {
        key: 's1',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro1',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'intro2',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'q1',
            type: null,
            required: null,
          },
          {
            key: 'q2',
            type: null,
            required: null,
          },
          {
            key: 'q3',
            type: null,
            required: null,
          },
          {
            key: 'q4',
            type: null,
            required: null,
          },
          {
            key: 'q5',
            type: null,
            required: null,
          },
          {
            key: 'q6',
            type: null,
            required: null,
          },
        ],
      },
      {
        key: 's2',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro1',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'q1',
            type: 'rating',
            required: null,
          },
          {
            key: 'q2',
            type: null,
            required: null,
          },
          {
            key: 'q3',
            type: null,
            required: null,
          },
          {
            key: 'q4',
            type: null,
            required: null,
          },
          {
            key: 'q5',
            type: null,
            required: null,
          },
          {
            key: 'q6',
            type: null,
            required: null,
          },
        ],
      },
    ],
  },
  {
    id: 's4-rcads',
    title: "${childName}'s mental wellbeing",
    tint: '#EF6A6E',
    scope: 'baseline',
    cover: {
      childImageSharp: {
        gatsbyImageData: {
          layout: 'constrained',
          backgroundColor: '#080808',
          images: {
            fallback: {
              src: '/static/95fd65b6497b0b167720226672217e79/fd57a/s4.png',
              srcSet:
                '/static/95fd65b6497b0b167720226672217e79/446ed/s4.png 40w,\n/static/95fd65b6497b0b167720226672217e79/28c0d/s4.png 79w,\n/static/95fd65b6497b0b167720226672217e79/fd57a/s4.png 158w',
              sizes: '(min-width: 158px) 158px, 100vw',
            },
            sources: [
              {
                srcSet:
                  '/static/95fd65b6497b0b167720226672217e79/4b83e/s4.webp 40w,\n/static/95fd65b6497b0b167720226672217e79/72e1e/s4.webp 79w,\n/static/95fd65b6497b0b167720226672217e79/97b75/s4.webp 158w',
                type: 'image/webp',
                sizes: '(min-width: 158px) 158px, 100vw',
              },
            ],
          },
          width: 158,
          height: 336,
        },
      },
    },
    sections: [
      {
        key: 's1',
        type: 'radio',
        required: true,
        questions: [
          {
            key: 'intro',
            type: 'paragraph',
            required: null,
          },
          {
            key: 'q1',
            type: null,
            required: null,
          },
          {
            key: 'q2',
            type: null,
            required: null,
          },
          {
            key: 'q3',
            type: null,
            required: null,
          },
          {
            key: 'q4',
            type: null,
            required: null,
          },
          {
            key: 'q5',
            type: null,
            required: null,
          },
          {
            key: 'q6',
            type: null,
            required: null,
          },
          {
            key: 'q7',
            type: null,
            required: null,
          },
          {
            key: 'q8',
            type: null,
            required: null,
          },
          {
            key: 'q9',
            type: null,
            required: null,
          },
          {
            key: 'q10',
            type: null,
            required: null,
          },
          {
            key: 'q11',
            type: null,
            required: null,
          },
          {
            key: 'q12',
            type: null,
            required: null,
          },
          {
            key: 'q13',
            type: null,
            required: null,
          },
          {
            key: 'q14',
            type: null,
            required: null,
          },
          {
            key: 'q15',
            type: null,
            required: null,
          },
          {
            key: 'q16',
            type: null,
            required: null,
          },
          {
            key: 'q17',
            type: null,
            required: null,
          },
          {
            key: 'q18',
            type: null,
            required: null,
          },
          {
            key: 'q19',
            type: null,
            required: null,
          },
          {
            key: 'q20',
            type: null,
            required: null,
          },
          {
            key: 'q21',
            type: null,
            required: null,
          },
          {
            key: 'q22',
            type: null,
            required: null,
          },
          {
            key: 'q23',
            type: null,
            required: null,
          },
          {
            key: 'q24',
            type: null,
            required: null,
          },
          {
            key: 'q25',
            type: null,
            required: null,
          },
        ],
      },
    ],
  },
] as DashboardSurvey[];
