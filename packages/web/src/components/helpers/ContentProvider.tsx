import React from 'react';
import { ContentContext } from 'components/content/ContentContext';
import { Box, Container } from '@material-ui/core';
import { UserGoals } from 'app';

export type ContentProviderProps = {
  data?: Record<string, any>;
  userGoals?: UserGoals;
  children: React.ReactElement;
};

export const ContentProvider = (props: ContentProviderProps) => {
  const [data, setData] = React.useState(props.data || {});
  const [userGoals, setUserGoals] = React.useState(props.userGoals || null);

  return (
    <ContentContext.Provider
      value={{
        getContent: (contentId: string) => {
          return data[contentId];
        },
        setContent: (contentId: string, value: any) => {
          setData({ ...data, [contentId]: value });
        },
        getGoals: () => userGoals,
        setGoals: (userGoals: UserGoals) => setUserGoals(userGoals),
      }}>
      <Box my={2}>
        <Container maxWidth="md">{props.children}</Container>
      </Box>
    </ContentContext.Provider>
  );
};
