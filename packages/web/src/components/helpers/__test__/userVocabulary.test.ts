import { UserProfile } from 'app';
import { userVocabulary } from '../userVocabulary';
import update from 'immutability-helper';

describe(userVocabulary, () => {
  let userTemplate: Pick<UserProfile, 'childGender' | 'childName'>;

  beforeEach(() => {
    userTemplate = {
      childGender: 'female',
      childName: 'Mary',
    };
  });

  test('it has childname', () => {
    let user = userTemplate;
    let vocabulary = userVocabulary(user);
    expect(vocabulary.childName).toEqual('Mary');

    user = update(userTemplate, { childName: { $set: 'Jack' } });
    vocabulary = userVocabulary(user);
    expect(vocabulary.childName).toEqual('Jack');
  });

  test('it has pronounes', () => {
    let user = userTemplate;
    let vocabulary = userVocabulary(user);
    expect(vocabulary.they).toEqual('she');
    expect(vocabulary.them).toEqual('her');
    expect(vocabulary.their).toEqual('her');
    expect(vocabulary.themselves).toEqual('herself');

    user = update(userTemplate, { childGender: { $set: 'male' } });
    vocabulary = userVocabulary(user);
    expect(vocabulary.they).toEqual('he');
    expect(vocabulary.them).toEqual('him');
    expect(vocabulary.their).toEqual('his');
    expect(vocabulary.themselves).toEqual('himself');

    user = update(userTemplate, { childGender: { $set: 'non-binary' } });
    vocabulary = userVocabulary(user);
    expect(vocabulary.they).toEqual('they');
    expect(vocabulary.them).toEqual('them');
    expect(vocabulary.their).toEqual('their');
    expect(vocabulary.themselves).toEqual('themselves');
  });

  test('it has third person verbs', () => {
    let user = userTemplate;
    let vocabulary = userVocabulary(user);
    expect(vocabulary.trust).toEqual('trusts');
    expect(vocabulary.appear).toEqual('appears');

    user = update(userTemplate, { childGender: { $set: 'non-binary' } });
    vocabulary = userVocabulary(user);
    expect(vocabulary.trust).toEqual('trust');
    expect(vocabulary.appear).toEqual('appear');
  });
});
