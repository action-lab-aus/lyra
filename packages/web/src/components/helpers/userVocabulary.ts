import { UserProfile } from 'app';

const pronouns: Record<string, [string, string, string, string]> = {
  male: ['he', 'his', 'him', 'himself'],
  female: ['she', 'her', 'her', 'herself'],
  'non-binary': ['they', 'their', 'them', 'themselves'],
};

const thirdPersonSingularVerbs: Record<string, string | [string, string]> = {
  trust: 'trusts',
  appear: 'appears',
  have: 'has',
  are: 'is',
  realise: 'realises',
  see: 'sees',
  seem: 'seems',
  need: 'needs',
  shut: 'shuts',
  take: 'takes',
  want: 'wants',
  learn: 'learns',
  ask: 'asks',
  find: 'finds',
  think: 'thinks',
  face: 'faces',
  dont: ["don't", "doesn't"],
  hasnt: ["haven't", "hasn't"],
};

const names = {
  topicName: "Parenting through the pandemic: Navigating the 'new normal'",
};

export function userVocabulary(user: Pick<UserProfile, 'childName' | 'childGender'>): Record<string, string> {
  const { childName = 'Your teenager', childGender = 'non-binary' } = user;
  const [they, their, them, themselves] = pronouns[childGender];
  const verbIndex = childGender === 'non-binary' ? 0 : 1;
  const verbs = Object.entries(thirdPersonSingularVerbs).reduce<Record<string, string>>((verbs, [key, value]) => {
    const map: [string, string] = typeof value === 'string' ? [key, value] : value;
    return { ...verbs, [key]: map[verbIndex] };
  }, {});
  return { childName, they, their, them, themselves, ...verbs, ...names };
}

export function substituteWithUserVocabulary(user: UserProfile) {
  const vocabulary = userVocabulary(user);
  const regex = new RegExp(`\\$\\{(${Object.keys(vocabulary).join('|')})\\}`, 'g');
  const replaceWithVocabulary = (value: any) => {
    return typeof value === 'string' ? value.replace(regex, (match, group) => vocabulary[group] || group) : value;
  };
  return function <T extends object>(target: T, fields: string[]): T {
    return fields.reduce<T>((target, field) => ({ ...target, [field]: replaceWithVocabulary(target[field]) }), target);
  };
}
