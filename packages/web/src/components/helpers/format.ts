import { UserProfile } from 'app';

export function getDisplayName(user: UserProfile) {
  return `${user.userFirstname} ${user.userSurname}`;
}
