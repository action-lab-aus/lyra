import React from 'react';
import { Button, createStyles, IconButton, makeStyles } from '@material-ui/core';
import PlayIcon from '@material-ui/icons/VolumeUp';
import PauseIcon from '@material-ui/icons/Pause';
import range from 'lodash/range';

const barHeight = 25;
const useStyles = makeStyles((theme) =>
  createStyles<string, { active: boolean }>({
    root: {
      display: 'flex',
      alignItems: 'center',
    },

    bars: ({ active }) => ({
      position: 'relative',
      height: barHeight,
      width: 40,
      marginLeft: theme.spacing(2),
      transform: `scaleX(${active ? 1 : 0})`,
      transition: 'transform 0.5s',
    }),

    bar: {
      background: '#E20074',
      bottom: 1,
      height: 3,
      position: 'absolute',
      width: 2,
      animation: '$sound 0ms -800ms linear infinite alternate',
    },

    '@keyframes sound': {
      '0%': {
        opacity: 0.25,
        height: 3,
      },
      '100%': {
        opacity: 1,
        height: barHeight,
      },
    },
  }),
);
export type AudioButtonProps = {
  active: boolean;
  onActive: (value: boolean) => void;
};

export function AudioButton(props: AudioButtonProps) {
  const { active, onActive } = props;
  const classes = useStyles({ active });
  return (
    <div className={classes.root}>
      <IconButton size="small" onClick={() => onActive(!active)}>
        {active ? <PauseIcon /> : <PlayIcon />}
      </IconButton>
      <div className={classes.bars}>
        {range(0, 10).map((i) => (
          <div
            key={i}
            className={classes.bar}
            style={{ left: 1 + i * 4, animationDuration: `${400 + ((Math.random() * 100) % 100)}ms` }}
          />
        ))}
      </div>
    </div>
  );
}
