import { Button } from '@material-ui/core';
import React from 'react';
import { AudioButton } from './AudioButton';

export const AudioSectionContext = React.createContext<{
  activeKey: string | null;
}>({
  activeKey: null,
});

export type AudioSectionProps = {
  audios: Array<[string, string]>;
  children: React.ReactNode;
  active: boolean;
  onActive: (value: boolean) => void;
};

export function AudioSection(props: AudioSectionProps) {
  const { audios, children, active, onActive } = props;
  const [seq, setSeq] = React.useState(0);
  const audioRef = React.useRef<HTMLAudioElement | null>(null);
  const [audioKey, audioUrl] = audios[seq];
  const activeKey = active ? audioKey : null;

  React.useEffect(() => {
    if (active) {
      audioRef.current?.play();
    } else {
      audioRef.current?.pause();
    }
  }, [active]);

  const handleEnded = () => {
    if (seq < audios.length - 1) {
      setSeq(seq + 1);
    } else {
      setSeq(0);
      onActive && onActive(false);
    }
  };

  return (
    <section>
      <AudioSectionContext.Provider value={{ activeKey }}>{children}</AudioSectionContext.Provider>
      <audio
        preload={'auto'}
        ref={audioRef}
        src={audioUrl}
        onEnded={handleEnded}
        controls={false}
        autoPlay={Boolean(active)}
      />
      <AudioButton active={active} onActive={onActive} />
    </section>
  );
}
