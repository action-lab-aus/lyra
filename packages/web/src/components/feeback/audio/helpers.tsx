import React from 'react';
import { AudioSectionContext } from './AudioSection';
import { Typography as MuiTypography, styled } from '@material-ui/core';

export type AudioLine = {
  text: React.ReactNode;
  audio?: string;
};

export function audioLine(text: React.ReactElement, audio?: string): AudioLine {
  return {
    audio,
    text: React.cloneElement(text, { key: audio, audioKey: audio }),
  };
}

export function decorate<Component extends React.ElementType>(
  Component: Component,
): React.ComponentType<React.ComponentProps<Component> & { audioKey?: string }> {
  return (props: React.ComponentProps<Component> & { audioKey?: string }) => {
    const { audioKey, ...others } = props;
    const { activeKey } = React.useContext(AudioSectionContext);
    const style = { backgroundColor: audioKey && audioKey === activeKey ? '#f1eca6' : 'transparent' };
    //@ts-ignore
    return <Component key={audioKey} style={style} {...others} />;
  };
}

export const Typography = decorate(MuiTypography);

export const ListItem = decorate(
  //@ts-ignore
  styled('li')(({ theme }) => ({
    ...theme.typography.body1,
    marginBottom: theme.spacing(2),
  })),
);

export const Section = decorate('section');
