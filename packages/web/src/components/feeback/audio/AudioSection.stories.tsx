import React from 'react';
import { Story, Meta } from '@storybook/react';
import { AudioSection, audioLine, AudioLine, Typography, ListItem } from '.';
import { Box, Container } from '@material-ui/core';
import { getPublicAudioUrl } from '../helpers';

function ExampleSection() {
  const [active, setActive] = React.useState<boolean>(false);

  const listItems: AudioLine[] = [
    audioLine(
      <ListItem>
        Verbally expressing your affection for childName more often, such as telling them that you love them. Adapt the
        way you show affection according to childName’s age and maturity.
      </ListItem>,
      '2_white_1_a',
    ),
    audioLine(
      <ListItem>
        Making time each day to ask childName about their day and what they have been doing, regardless of their{' '}
        response.
      </ListItem>,
      '2_white_2_a',
    ),
    audioLine(
      <ListItem>
        Trying to start conversations with childName when they appear most open to conversation. While it may be hard to
        tell when they are open to talking, try to chat when they seem most responsive to you. For example, this could
        be in the car, at the shops, or before bed.
      </ListItem>,
      '2_white_3_a',
    ),
    audioLine(
      <ListItem>Showing respect for childName’s opinions, even if you don’t agree with them.</ListItem>,
      '2_white_4_b',
    ),
    audioLine(
      <ListItem>
        Letting childName know that you are there for them whenever they need it and that they can talk to you about
        anything, even difficult issues.
      </ListItem>,
      '2_white_5_a',
    ),
    audioLine(
      <ListItem>
        Choosing somewhere private to talk with childName about sensitive issues. Having other people around may make it
        difficult for them to be honest with you.
      </ListItem>,
      '2_white_6_b',
    ),
    audioLine(
      <ListItem>
        Showing your concern when childName is feeling a strong emotion by asking about their feelings (e.g. “You look
        worried, is there something on your mind?”). Encourage them to talk with you about their emotions, and take the
        time to listen.
      </ListItem>,
      '2_white_7_a',
    ),
    audioLine(
      <ListItem>
        Not telling childName to toughen up when they are upset. Responding in this way can lead childName to believe
        that their emotions are wrong and they are bad for having them. Instead, let them know that you are concerned
        and offer to chat with them about what’s going on.
      </ListItem>,
      '2_white_8_b',
    ),
  ].filter((item, index) => index % 2 === 0);

  const audios = (['2_heading', '2_body', ...listItems.map((item) => item.audio)] as string[]).map<[string, string]>(
    (file) => [file, getPublicAudioUrl(file)],
  );

  return (
    <Container maxWidth="md">
      <AudioSection
        audios={audios}
        active={active}
        onActive={(active) => {
          setActive(active);
        }}>
        <Box display="flex" alignItems="center" justifyContent="space-between">
          <Typography audioKey="2_heading" variant="h6" gutterBottom>
            Your Relationship with teenager
          </Typography>
        </Box>
        <Typography audioKey="2_body" paragraph>
          Your care and support for teenager helps to reduce their risk of depression and clinical anxiety.
        </Typography>
        <ul>{listItems.map((item) => item.text)}</ul>
      </AudioSection>
    </Container>
  );
}

export default {
  title: 'components/feedback/audio/AudioSection',
  component: ExampleSection,
} as Meta;

const Template: Story = (...args) => <ExampleSection />;

export const Example = Template.bind({});
