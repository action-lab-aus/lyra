import React from 'react';
import { Accordion, AccordionDetails, AccordionSummary, Box, Typography } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { UserProfile, UserSurvey } from 'app';
import { Link } from 'components';
import { userVocabulary } from 'components/helpers';
import {
  Section1,
  Section2,
  Section3,
  Section4,
  Section5,
  Section6,
  Section7,
  Section8,
  Section9,
  SectionProps,
} from './sections';

export type S2Props = {
  user: UserProfile;
  userSurvey: UserSurvey;
  lean?: boolean;
};

export default function S2(props: S2Props) {
  const { user, userSurvey, lean = false } = props;
  const vocabulary = React.useMemo(() => userVocabulary(user), [user]);
  const [active, setActive] = React.useState<string | null>(null);
  const { childName } = vocabulary;
  const sections: Array<[string, string, React.ComponentType<SectionProps>]> = [
    ['S1', `Your Relationship with ${childName}`, Section1],
    ['S2', `Your Involvement in ${childName}’s Life`, Section2],
    ['S3', `${childName}’s Relationships with Others`, Section3],
    ['S4', 'Your Family Rules', Section4],
    ['S5', 'Your Home Environment', Section5],
    ['S6', 'Health Habits', Section6],
    ['S7', `Dealing with Problems in ${childName}’s Life`, Section7],
    ['S8', 'Coping with Anxiety', Section8],
    ['S9', 'Getting Help When Needed', Section9],
  ];

  const handleActive = (key: string) => (value: boolean) => setActive(value ? key : null);
  const isActive = (key: string) => key === active;
  return (
    <article>
      {!lean && <Header childName={childName} />}
      <Box my={2}>
        {sections.map(([key, title, Section]) => (
          <Accordion key={key}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography variant="subtitle1">{title}</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Section
                userSurvey={userSurvey}
                vocabulary={vocabulary}
                active={isActive(key)}
                onActive={handleActive(key)}
              />
            </AccordionDetails>
          </Accordion>
        ))}
      </Box>
      {!lean && <Footer childName={childName} />}
    </article>
  );
}

export const Header = ({ childName }: { childName: string }) => {
  return (
    <section>
      <Typography variant="h5" color="primary" gutterBottom>
        Your Personalised Parenting Tips
      </Typography>

      <Typography paragraph>
        We can now provide you with feedback about your role in reducing {childName}’s risk of developing depression and
        clinical anxiety. We also provide you with some practical strategies that you could use to further support{' '}
        {childName}.
      </Typography>

      <Typography variant="h6" gutterBottom>
        What next?
      </Typography>

      <Typography paragraph>
        This feedback message contains a number of strategies that may be useful for you and {childName}. This may seem
        like a lot of information to take in at once. If you’re not sure where to start, you may like to have a quick
        read over it and pick one or two sections to focus on first. You may prefer to start with the changes that you
        feel would make the most difference to you and {childName}, or those that would be easiest to implement. You
        don’t need to try all of these strategies at once. Remember, change can take time and patience. You might
        already feel that you do some of the things recommended to you. If this is the case, you could consider doing
        them more often. If you feel that there are things you could have done differently as a parent, try not to be
        too hard on yourself. You’ve already taken a positive step by completing the survey, well done!
      </Typography>
    </section>
  );
};

export const Footer = ({ childName }: { childName: string }) => {
  return (
    <section>
      <Typography variant="h5" color="primary" gutterBottom>
        Don’t Blame Yourself
      </Typography>

      <Typography paragraph>
        If you feel that there are things you could have done differently as a parent, it’s important not to be too hard
        on yourself. If, despite your best efforts, {childName} does develop depression or anxiety, you should not view
        it as a failure on your part. Any teenager can develop these problems, even in happy, well-adjusted families.
        Remember that it is important to take care of yourself, and seek professional help if you think you need it.
      </Typography>

      <Typography paragraph>
        You can find more information <Link href="/resources">here</Link>.
      </Typography>
    </section>
  );
};
