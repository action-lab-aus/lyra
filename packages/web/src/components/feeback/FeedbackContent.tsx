import React from 'react';
import { navigate } from 'gatsby-link';
import isEmpty from 'lodash/isEmpty';
import { useSelector } from 'react-redux';
import { Typography, Container, Box, Paper, Button } from '@material-ui/core';
import { createStyles, makeStyles, WithStyles, withStyles } from '@material-ui/core/styles';
import { RootState } from 'app';
import { userVocabulary } from 'components/helpers';
import { feedbacks } from './Feedback';
import { Header, Footer } from './S2';

const useStyles = makeStyles((theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(2),
    },
  }),
);

export function FeedbackContent(props: { surveyIds: string[] }) {
  const { surveyIds } = props;
  const classes = useStyles();
  const profileData = useSelector((state: RootState) => state.profile.data);
  if (!profileData || isEmpty(profileData.surveys)) {
    return <NoFeedback />;
  }
  const { user, surveys } = profileData;
  const vocabulary = React.useMemo(() => userVocabulary(user), [user]);
  const { childName } = vocabulary;

  const segments = surveyIds.reduce<Array<React.ReactNode>>((segments, surveyId) => {
    const userSurvey = surveys[surveyId];
    const Feedback = feedbacks[surveyId];
    if (!userSurvey) {
      return segments;
    }
    const { _completed, _total } = userSurvey;
    return _completed > 0 && _completed === _total
      ? [...segments, <Feedback key={surveyId} user={user} userSurvey={userSurvey} lean />]
      : segments;
  }, []);

  if (segments.length === 0) {
    return <NoFeedback />;
  }

  return (
    <Container>
      <Box mt={2} pb={6}>
        <Paper className={classes.paper}>
          <Header childName={childName} />
          {segments}
          <Footer childName={childName} />
          <Box my={2} display="flex" justifyContent="center">
            <Button variant="contained" color="primary" onClick={() => navigate('/dashboard')}>
              Go to dashboard
            </Button>
          </Box>
        </Paper>
      </Box>
    </Container>
  );
}

const NoFeedback = withStyles((theme) =>
  createStyles({
    paper: {
      marginTop: theme.spacing(2),
      padding: theme.spacing(4),
    },
  }),
)((props: WithStyles) => {
  return (
    <Paper className={props.classes.paper}>
      <Typography variant="h5" color="textSecondary" align="center" gutterBottom>
        Feedback not available
      </Typography>
      <Typography variant="h6" color="textSecondary" align="center">
        Your feedback will be available when you've completed at least one section of the baseline survey. Please check
        back when you have completed the surveys to see your tailored feedback.
      </Typography>
    </Paper>
  );
});
