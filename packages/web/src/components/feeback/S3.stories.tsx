import React from 'react';
import { Box, Container } from '@material-ui/core';
import { Story, Meta } from '@storybook/react';
import S3, { S3Props } from './S3';
import { UserProfile, UserSurvey } from 'app';
import * as mocks from 'components/helpers/mocks';

export default {
  title: 'components/feedback/S3',
  component: S3,
} as Meta;

const Template: Story<S3Props> = (args) => {
  return (
    <Box mt={2}>
      <Container maxWidth="md">
        <S3 {...args} />
      </Container>
    </Box>
  );
};

const userSurvey = mocks.templates.userSurveys.s3K6();

const userProfile: UserProfile = mocks.templates.user({
  childName: 'Marry',
  childGender: 'female',
  childGrade: 12,
});

export const FemaleLowDistress = Template.bind({});
FemaleLowDistress.args = {
  userSurvey: {
    ...userSurvey,
    's1#q1': 'None of the time',
    's1#q2': 'None of the time',
    's1#q3': 'A little of the time',
    's1#q4': 'A little of the time',
    's1#q5': 'A little of the time',
    's1#q6': 'A little of the time',
  },
  user: userProfile,
};

export const FemaleModerateDistress = Template.bind({});
FemaleModerateDistress.args = {
  userSurvey: {
    ...userSurvey,
    's1#q1': 'None of the time',
    's1#q2': 'None of the time',
    's1#q3': 'Most of the time',
    's1#q4': 'Most of the time',
    's1#q5': 'Most of the time',
    's1#q6': 'Most of the time',
  },
  user: FemaleLowDistress.args.user,
};

export const FemaleHighDistress = Template.bind({});
FemaleHighDistress.args = {
  userSurvey: {
    ...userSurvey,
    's1#q1': 'Most of the time',
    's1#q2': 'Most of the time',
    's1#q3': 'Most of the time',
    's1#q4': 'Most of the time',
    's1#q5': 'Most of the time',
    's1#q6': 'Most of the time',
  },
  user: FemaleLowDistress.args.user,
};

export const MaleLowDistress = Template.bind({});
MaleLowDistress.args = {
  userSurvey: FemaleLowDistress.args.userSurvey,
  user: {
    ...userProfile,
    childName: 'Jack',
    childGender: 'male',
  },
};

export const MaleModerateDistress = Template.bind({});
MaleModerateDistress.args = {
  userSurvey: FemaleModerateDistress.args.userSurvey,
  user: MaleLowDistress.args.user,
};

export const MaleHighDistress = Template.bind({});
MaleHighDistress.args = {
  userSurvey: FemaleHighDistress.args.userSurvey,
  user: MaleLowDistress.args.user,
};

export const NonBinaryLowDistress = Template.bind({});
NonBinaryLowDistress.args = {
  userSurvey: FemaleLowDistress.args.userSurvey,
  user: {
    ...userProfile,
    childName: 'Children',
    childGender: 'non-binary',
  },
};

export const NonBinaryModerateDistress = Template.bind({});
NonBinaryModerateDistress.args = {
  userSurvey: FemaleModerateDistress.args.userSurvey,
  user: NonBinaryLowDistress.args.user,
};

export const NonBinaryHighDistress = Template.bind({});
NonBinaryHighDistress.args = {
  userSurvey: FemaleHighDistress.args.userSurvey,
  user: NonBinaryLowDistress.args.user,
};
