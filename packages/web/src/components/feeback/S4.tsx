import React from 'react';
import { UserProfile, UserSurvey, Gender } from 'app';
import { Typography } from '@material-ui/core';
import { userVocabulary } from 'components/helpers/userVocabulary';
import { calcScaleScore, findByConditions, SimpleFreqScales } from './helpers';

type Config = {
  range: number[];
  genderFactors: {
    female: Array<[number, number]>;
    other: Array<[number, number]>;
  };
};

const anxietyConfig: Config = {
  range: [2, 3, 5, 6, 7, 9, 11, 12, 14, 17, 18, 20, 22, 23, 25],
  genderFactors: {
    female: [
      [7.29, 5.25],
      [5.8, 3.91],
      [5.94, 5.27],
      [5.76, 3.97],
    ],
    other: [
      [6.1, 4.15],
      [5.27, 3.95],
      [6.23, 4.56],
      [4.66, 3.58],
    ],
  },
};

const depressionConfig: Config = {
  range: [1, 4, 8, 10, 13, 15, 16, 19, 21, 24],
  genderFactors: {
    female: [
      [3.75, 3.63],
      [3.6, 3.37],
      [3.97, 3.25],
      [4.91, 3.17],
    ],
    other: [
      [3.62, 2.87],
      [3.54, 3.18],
      [5.21, 3.51],
      [3.94, 3.88],
    ],
  },
};

function calcScore(config: Config) {
  const { range, genderFactors } = config;
  const scaleScore = calcScaleScore(
    range.map((i) => `s1#q${i}`),
    {
      [SimpleFreqScales[0]]: 0,
      [SimpleFreqScales[1]]: 1,
      [SimpleFreqScales[2]]: 2,
      [SimpleFreqScales[3]]: 3,
    },
  );

  return (userSurvey: Record<string, any>, gender: Gender, grade: number) => {
    const rawScore = scaleScore(userSurvey);
    const factors = gender === 'female' ? genderFactors.female : genderFactors.other;
    const [factor1, factor2] = factors[Math.floor((grade - 5) / 2)] || factors[0];
    return 50 + ((rawScore - factor1) * 10) / factor2;
  };
}

type Scores = { anxiety: number; depression: number };

export type S4Props = {
  user: UserProfile;
  userSurvey: UserSurvey;
};

export const calcAnxietyScore = calcScore(anxietyConfig);

export const calcDepressionScore = calcScore(depressionConfig);

export default function S4(props: S4Props) {
  const { user, userSurvey } = props;

  const { childName, them, they, are } = React.useMemo(() => userVocabulary(user), [user]);

  const scores = React.useMemo<Scores>(() => {
    return {
      anxiety: calcAnxietyScore(userSurvey, user.childGender, user.childGrade),
      depression: calcDepressionScore(userSurvey, user.childGender, user.childGrade),
    };
  }, [user, userSurvey]);

  return (
    <article>
      <Typography variant="h5" color="primary" gutterBottom>
        {childName}’s mental wellbeing
      </Typography>
      {findByConditions<Scores, React.ReactNode>([
        [
          (scores) => scores.anxiety < 70 && scores.depression < 70,
          <Typography paragraph>
            Thanks for answering the questions about {childName}’s thoughts and feelings. Based on your responses,{' '}
            {childName} does <strong>not</strong> seem to be experiencing significant symptoms of anxiety or depression.
            Keep in mind that these questions only give us a snapshot of {childName}’s mental health, and are not a
            replacement for professional advice. You know your child best – if you are ever worried about {childName}’s
            mental health, we suggest you check in with {them} about how {they} {are} feeling. You can also speak with
            your GP or health professional.
          </Typography>,
        ],
        [
          (scores) => scores.anxiety >= 70 && scores.depression < 70,
          <Typography paragraph>
            Thanks for answering the questions about {childName}’s thoughts and feelings. Based on your responses,{' '}
            {childName} could be experiencing symptoms of anxiety. Keep in mind that these questions only give us a
            snapshot of {childName}’s mental health, and are not a replacement for professional advice. We suggest you
            check in with {childName} about how {they} {are} feeling and also consider seeking professional support.
            We’ve sent you an email with more information – keep an eye out for this in your inbox.
          </Typography>,
        ],
        [
          (scores) => scores.anxiety < 70 && scores.depression >= 70,
          <Typography paragraph>
            Thanks for answering the questions about {childName}’s thoughts and feelings. Based on your responses,{' '}
            {childName} could be experiencing symptoms of depression. Keep in mind that these questions only give us a
            snapshot of {childName}’s mental health, and are not a replacement for professional advice. We suggest you
            check in with {childName} about how {they} {are} feeling and also consider seeking professional support.
            We’ve sent you an email with more information – keep an eye out for this in your inbox.
          </Typography>,
        ],
        [
          (scores) => scores.anxiety >= 70 && scores.depression >= 70,
          <Typography paragraph>
            Thanks for answering the questions about {childName}’s thoughts and feelings. Based on your responses,{' '}
            {childName} could be experiencing symptoms of anxiety and depression. Keep in mind that these questions only
            give us a snapshot of {childName}’s mental health, and are not a replacement for professional advice. We
            suggest you check in with {childName} about how {they} {are} feeling and also consider seeking professional
            support. We’ve sent you an email with more information – keep an eye out for this in your inbox.
          </Typography>,
        ],
      ])(scores)}
    </article>
  );
}
