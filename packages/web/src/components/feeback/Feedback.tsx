import React from 'react';
import { Typography } from '@material-ui/core';
import { UserProfile, UserSurvey } from 'app';
import S1 from './S1';
import S2 from './S2';
import S3 from './S3';
import S4 from './S4';

export const feedbacks = {
  's1-confidence': S1,
  's2-pradas': S2,
  's3-k6': S3,
  's4-rcads': S4,
  'f1-evaluation': S1,
  'f2-pradas': S2,
  'f3-k6': S3,
  'f4-rcads': S4,
};

export type FeedbackProps = {
  user: UserProfile;
  surveyId: string;
  userSurvey: UserSurvey;
};

export function Feedback(props: FeedbackProps) {
  const { user, surveyId, userSurvey } = props;
  const FeedbackComp = feedbacks[surveyId];

  return FeedbackComp ? (
    <FeedbackComp user={user} userSurvey={userSurvey} />
  ) : (
    <Typography color="error">No feedback for {surveyId}</Typography>
  );
}
