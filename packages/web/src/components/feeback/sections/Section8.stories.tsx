import React from 'react';
import { Container, Box } from '@material-ui/core';
import { Meta, Story } from '@storybook/react';
import { UserSurvey } from 'app';
import { SectionProps } from './index';
import { Section8 } from './Section8';
import { FreqScales } from '../helpers';
import { userVocabulary } from 'components/helpers';
import { Article } from '../Article';

export default {
  title: 'components/feedback/sections/Section8',
  component: Section8,
} as Meta;

const vocabulary: Record<string, string> = userVocabulary({
  childName: 'Marry',
  childGender: 'female',
});

// ['S8#Q1', [FreqScales[0], FreqScales[1]]],
// ['S8#Q2', [FreqScales[2], FreqScales[3]]],
// ['S8#Q3', FreqScales[3]],
// ['S8#Q4', FreqScales[0]],
// ['S8#Q5', [FreqScales[2], FreqScales[3]]],
// ['S8#Q6', [FreqScales[2], FreqScales[3]]],
// ['S8#Q7', ['Yes, definitely', 'Yes, partly']],
// ['S8#Q8', ['Yes, definitely', 'Yes, partly']],
// ['S8#Q9', ['Yes, definitely', 'Yes, partly']],
const bestSurvey: UserSurvey = {
  _step: 0,
  _total: 0,
  _completed: 0,
  'S8#Q1': FreqScales[0],
  'S8#Q2': FreqScales[3],
  'S8#Q3': FreqScales[3],
  'S8#Q4': FreqScales[0],
  'S8#Q5': FreqScales[3],
  'S8#Q6': FreqScales[3],
  'S8#Q7': 'Yes, definitely',
  'S8#Q8': 'Yes, definitely',
  'S8#Q9': 'Yes, definitely',
};

const Template: Story<SectionProps> = (args) => {
  const [active, setActive] = React.useState(false);
  return (
    <Container>
      <Box my={2}>
        <Article>
          <Section8 {...args} active={active} onActive={setActive} />
        </Article>
      </Box>
    </Container>
  );
};

export const Low = Template.bind({});
Low.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S8#Q1': FreqScales[3],
    'S8#Q2': FreqScales[0],
    'S8#Q3': FreqScales[0],
    'S8#Q4': FreqScales[3],
    'S8#Q5': FreqScales[0],
    'S8#Q6': FreqScales[0],
    'S8#Q7': 'No',
    'S8#Q8': 'No',
    'S8#Q9': 'No',
  },
};

export const Medium = Template.bind({});
Medium.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S8#Q1': FreqScales[3],
    'S8#Q2': FreqScales[0],
    'S8#Q7': 'No',
    'S8#Q8': 'No',
  },
};

export const High = Template.bind({});
High.args = {
  vocabulary,
  userSurvey: { ...bestSurvey, 'S8#Q1': FreqScales[3] },
};

export const Best = Template.bind({});
Best.args = {
  vocabulary,
  userSurvey: bestSurvey,
};
