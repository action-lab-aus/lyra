import React from 'react';
import sum from 'lodash/sum';
import { Link } from 'components/Link';
import { commonLinks, SectionProps } from '.';
import { calcOptionsScores, findByRanges, FreqScales, getPublicAudioUrl } from '../helpers';
import { AudioSection, AudioLine, ListItem, Typography, audioLine, Section } from '../audio';

export const calcScores = calcOptionsScores([
  ['S7#Q1', FreqScales[3]],
  ['S7#Q2', [FreqScales[2], FreqScales[3]]],
  ['S7#Q3', [FreqScales[2], FreqScales[3]]],
  ['S7#Q4', [FreqScales[0], FreqScales[1]]],
  ['S7#Q5', [FreqScales[2], FreqScales[3]]],
  ['S7#Q6', FreqScales[3]],
  ['S7#Q7', [FreqScales[0], FreqScales[1]]],
  ['S7#Q8', [FreqScales[2], FreqScales[3]]],
  ['S7#Q9', FreqScales[0]],
  ['S7#Q10', FreqScales[3]],
]);

export function Section7(props: SectionProps) {
  const { userSurvey, vocabulary, ...others } = props;
  const { childName, them, their, they, themselves, trust, have, are } = vocabulary;
  const scores = calcScores(userSurvey);

  const lifeGroup = scores.slice(0, 4);
  const listIntro = findByRanges<AudioLine>([
    [
      [0, 5],
      audioLine(
        <Typography paragraph>
          You can help {childName} to do this by demonstrating effective problem‐solving skills yourself, and teaching{' '}
          {them} how to manage problems and stress in {their} own life. Here are some useful strategies:
        </Typography>,
        '8_score_low',
      ),
    ],
    [
      [5, 9],
      audioLine(
        <Section>
          <Typography paragraph>
            You can help {childName} to do this by demonstrating effective problem‐solving skills yourself, and teaching{' '}
            {them} how to manage problems and stress in {their} own life.
          </Typography>
          <Typography paragraph>
            {' '}
            You are doing some of this already. Here are some other things you could do:
          </Typography>
        </Section>,
        '8_score_medium',
      ),
    ],
    [
      [9, Number.MAX_VALUE],
      audioLine(
        <Typography paragraph>
          You are doing a great job of helping {childName} to deal with problems in {their} life, keep it up!
        </Typography>,
        '8_score_high',
      ),
    ],
  ])(sum(scores));

  const listIntroExtra =
    sum(scores) === 9 && lifeGroup.indexOf(0) >= 0
      ? audioLine(
          <Typography paragraph>
            To help {childName} further develop {their} problem solving skills, keep this in mind:
          </Typography>,
          '8_score_extra_high',
        )
      : undefined;

  const lifeItems: AudioLine[] = [
    audioLine(
      <ListItem>
        Encourage {childName} to set realistic goals. You can help {them} to think of different ways {they} can achieve{' '}
        {their} goals, and select the best one.
      </ListItem>,
      '8_life_1_a',
    ),
    audioLine(
      <ListItem>
        Pay attention to {childName}’s behaviour and look for signs of stress. If you notice that {they} {are} stressed
        or upset, help {them} to learn ways to cope with these emotions (e.g. by talking to someone {they} {trust}).
      </ListItem>,
      '8_life_2_a',
    ),
    audioLine(
      <ListItem>
        When talking with {childName} about problems that {they} {have} dealt with, recognise and praise {their}{' '}
        problem‐solving efforts (i.e. what {they} did well when trying to solve the problem) rather than focussing on
        the outcome {they} achieved.
      </ListItem>,
      '8_life_3_a',
    ),
    audioLine(
      <ListItem>Set an example for {childName} by persisting at tasks, even when they are difficult.</ListItem>,
      '8_life_4_c',
    ),
  ].filter((item, index) => lifeGroup[index] === 0);

  const problemGroup = scores.slice(5, 10);
  const problemItems: AudioLine[] = [
    audioLine(
      <ListItem>Give {them} time to talk through the problem before offering to discuss solutions.</ListItem>,
      '8_problems_6_a',
    ),
    audioLine(
      <ListItem>
        Support {childName} to develop the skills to solve the problem {themselves}, rather than trying to solve the
        problem for {them}.
      </ListItem>,
      '8_problems_7_c',
    ),
    audioLine(
      <ListItem>Help {them} to break the problem down into smaller, more manageable steps.</ListItem>,
      '8_problems_8_a',
    ),
    audioLine(
      <ListItem>
        Don’t brush off or dismiss {childName}’s problems or concerns. It is important for you to help {childName} learn
        helpful ways to deal with problems in {their} life. Offer to talk the problem through with {them}, and help{' '}
        {them} to problem‐solve the issue.
      </ListItem>,
      '8_problems_9_b',
    ),
    audioLine(
      <ListItem>
        Encourage {them} to consider the effects of {their} actions on other people.
      </ListItem>,
      '8_problems_10_a',
    ),
  ].filter((item, index) => problemGroup[index] === 0);

  const audioFiles = [
    '8_heading',
    '8_body',
    listIntro?.audio,
    listIntroExtra?.audio,
    ...(lifeItems || []).map((item) => item.audio),
    ...(problemItems.length > 0 ? ['8_problems_start', ...problemItems.map((item) => item.audio)] : []),
    scores[4] === 0 ? '8_5_5_a' : undefined,
    '8_end',
  ] as string[];

  const audios = audioFiles
    .filter((file) => file !== undefined)
    .map<[string, string]>((file) => [file, getPublicAudioUrl(file)]);

  return (
    <AudioSection audios={audios} {...others}>
      <Typography audioKey="8_heading" variant="h6" color="textSecondary" gutterBottom>
        Dealing with Problems in {childName}’s Life
      </Typography>
      <Typography audioKey="8_body" paragraph>
        Learning to deal well with problems can help protect {childName} from developing depression and clinical
        anxiety.
      </Typography>

      {listIntro?.text}

      {listIntroExtra?.text}

      <ul>{(lifeItems || []).map((item) => item.text)}</ul>

      {problemItems.length > 0 && (
        <>
          <Typography audioKey="8_problems_start" variant="h6" gutterBottom>
            When {childName} faces problems in {their} life
          </Typography>
          <ul>{problemItems.map((item) => item.text)}</ul>
        </>
      )}

      {scores[4] === 0 && (
        <Typography audioKey="8_5_5_a" paragraph>
          Teenagers often feel pressure to live up to expectations from a range of sources (e.g. themselves, their
          friends, school, the media, or family members). It is important for you to be aware of these pressures so that
          you can help {childName} deal with them. Have a conversation with {childName} about the pressures on {them}{' '}
          (e.g. pressure to do well at school, or pressure from the media to look a certain way) and how these may be
          affecting {them}. Try not to put too much pressure on {childName} to achieve or perform. Excessive parental
          pressure can increase {their} risk of depression and clinical anxiety.
        </Typography>
      )}

      <Typography audioKey="8_end" paragraph>
        For more strategies to help support {childName}’s problem‐solving skills, check out the{' '}
        <b><i>Partners in problem solving</i></b>{' '}module.
      </Typography>
    </AudioSection>
  );
}
