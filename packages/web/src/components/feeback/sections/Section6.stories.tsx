import React from 'react';
import { Container, Box } from '@material-ui/core';
import { Meta, Story } from '@storybook/react';
import { UserSurvey } from 'app';
import { SectionProps } from './index';
import { Section6 } from './Section6';
import { FreqScales, LikelyScales } from '../helpers';
import { userVocabulary } from 'components/helpers';
import { Article } from '../Article';

export default {
  title: 'components/feedback/sections/Section6',
  component: Section6,
} as Meta;

const vocabulary: Record<string, string> = userVocabulary({
  childName: 'Marry',
  childGender: 'female',
});

// ['S6#Q1', FreqScales[3]],
// ['S6#Q2', [FreqScales[0], FreqScales[1]]],
// ['S6#Q3', FreqScales[3]],
// ['S6#Q4', FreqScales[3]],
// ['S6#Q5', FreqScales[3]],
// ['S6#Q6', FreqScales[0]],
// ['S6#Q7', [FreqScales[2], FreqScales[3]]],
// ['S6#Q8', FreqScales[0]],
// ['S6#Q9', [LikelyScales[2], LikelyScales[3]]],
// ['S6#Q10', [LikelyScales[0], LikelyScales[1]]],
// ['S6#Q11', [LikelyScales[0], LikelyScales[1]]],
// ['S6#Q12', [LikelyScales[2], LikelyScales[3]]],
const bestSurvey: UserSurvey = {
  _step: 0,
  _total: 0,
  _completed: 0,
  'S6#Q1': FreqScales[3],
  'S6#Q2': FreqScales[0],
  'S6#Q3': FreqScales[3],
  'S6#Q4': FreqScales[3],
  'S6#Q5': FreqScales[3],
  'S6#Q6': FreqScales[0],
  'S6#Q7': FreqScales[3],
  'S6#Q8': FreqScales[0],
  'S6#Q9': LikelyScales[3],
  'S6#Q10': LikelyScales[0],
  'S6#Q11': LikelyScales[0],
  'S6#Q12': LikelyScales[3],
};

const Template: Story<SectionProps> = (args) => {
  const [active, setActive] = React.useState(false);
  return (
    <Container>
      <Box my={2}>
        <Article>
          <Section6 {...args} active={active} onActive={setActive} />
        </Article>
      </Box>
    </Container>
  );
};

export const Low = Template.bind({});
Low.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S6#Q1': FreqScales[0],
    'S6#Q2': FreqScales[3],
    'S6#Q3': FreqScales[0],
    'S6#Q4': FreqScales[0],
    'S6#Q5': FreqScales[0],
    'S6#Q6': FreqScales[3],
    'S6#Q7': FreqScales[0],
    'S6#Q8': FreqScales[3],
    'S6#Q9': LikelyScales[0],
    'S6#Q10': LikelyScales[3],
    'S6#Q11': LikelyScales[3],
    'S6#Q12': LikelyScales[0],
  },
};

export const Medium = Template.bind({});
Medium.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S6#Q1': FreqScales[0],
    'S6#Q2': FreqScales[3],
    'S6#Q3': FreqScales[0],
    'S6#Q4': FreqScales[0],
    'S6#Q11': LikelyScales[3],
    'S6#Q12': LikelyScales[0],
  },
};

export const High = Template.bind({});
High.args = {
  vocabulary,
  userSurvey: { ...bestSurvey, 'S6#Q12': LikelyScales[1] },
};

export const Best = Template.bind({});
Best.args = {
  vocabulary,
  userSurvey: bestSurvey,
};
