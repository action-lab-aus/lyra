import { UserSurvey } from 'app';
import { AudioSectionProps } from '../audio';

export type SectionProps = {
  userSurvey: UserSurvey;
  vocabulary: Record<string, string>;
} & Pick<AudioSectionProps, 'active' | 'onActive'>;

export { Section1 } from './Section1';

export { Section2 } from './Section2';

export { Section3 } from './Section3';

export { Section4 } from './Section4';

export { Section5 } from './Section5';

export { Section6 } from './Section6';

export { Section7 } from './Section7';

export { Section8 } from './Section8';

export { Section9 } from './Section9';

export const commonLinks = {
  m1_relationshipRoot: '/m1-teenager-relationship/00-home',
  m2_covidRoot: '/m2-covid/00-home',
  m3_ruleRoot: '/m3-family-rules/00-home',
  m4_balanceRoot: '/m4-balance/00-home',
  m5_conflictRoot: '/m5-solve-conflict/00-home',
  m6_skillRoot: '/m6-teenager-skill/00-home',
  m7_lifestyleRoot: '/m7-healthy-lifestyle/00-home',
  m8_problemRoot: '/m8-teenager-problems/00-home',
  m9_anxietyRoot: '/m9-teenager-anxiety/00-home',
  m10_helpRoot: '/m10-professional-help/00-home',
};
