import React from 'react';
import sum from 'lodash/sum';
import { Link } from 'components';
import { commonLinks, SectionProps } from '.';
import { calcOptionsScores, findByRanges, FreqScales, getPublicAudioUrl } from '../helpers';
import { AudioSection, AudioLine, ListItem, Typography, audioLine, Section } from '../audio';

export const calcScores = calcOptionsScores([
  ['S5#Q1', [FreqScales[2], FreqScales[3]]],
  ['S5#Q2', FreqScales[0]],
  ['S5#Q3', [FreqScales[2], FreqScales[3]]],
  ['S5#Q4', FreqScales[0]],
  ['S5#Q5', FreqScales[0]],
  ['S5#Q6', [FreqScales[3], FreqScales[4]]],
  ['S5#Q7', [FreqScales[0], FreqScales[4]]],
  ['S5#Q8', FreqScales[3]],
]);

export function Section5(props: SectionProps) {
  const { userSurvey, vocabulary, ...others } = props;
  const { childName, them, they, their, themselves, shut } = vocabulary;
  const scores = calcScores(userSurvey);

  const listIntro = findByRanges<AudioLine>([
    [
      [0, 4],
      audioLine(
        <Typography paragraph>
          Being a part of a family where there is frequent or unresolved conflict increases {childName}’s risk of
          depression and clinical anxiety. While you may not be able to avoid conflict altogether, there are a number of
          things that you can do to make the home environment supportive and safe for {childName}, including:
        </Typography>,
        '6_white_low',
      ),
    ],
    [
      [4, 7],
      audioLine(
        <Typography paragraph>
          Being a part of a family where there is frequent or unresolved conflict increases {childName}’s risk of
          depression and clinical anxiety. You already manage conflict at home in a number of ways. There are some other
          things that you could do, including:
        </Typography>,
        '6_white_medium',
      ),
    ],
    [
      7,
      audioLine(
        <Section>
          <Typography paragraph>You are doing a great job of managing conflict at home. Well done!</Typography>
          <Typography paragraph>To further improve in this area, remember this:</Typography>
        </Section>,
        '6_white_high',
      ),
    ],
    [
      [8, Number.MAX_VALUE],
      audioLine(
        <Typography paragraph>You are doing a great job of managing conflict at home. Well done!</Typography>,
        '6_white_best',
      ),
    ],
  ])(sum(scores));

  const listItems: AudioLine[] = [
    audioLine(
      <ListItem>
        {' '}
        If you have an argument or conflict with {childName}, try to problem‐solve the issue together.
      </ListItem>,
      '6_white_1_b',
    ),
    audioLine(
      <ListItem>
        If you are experiencing conflict with {childName}, continue to show {them} affection and keep having normal
        everyday conversations with {them}. Tell {them} that you are still there for {them}, even if {they} {shut} you
        out.
      </ListItem>,
      '6_white_2_c',
    ),
    audioLine(
      <ListItem>
        Try to minimise conflict with {childName} where possible. You can do this by considering which issues are minor
        and can be ignored, and which are important for {childName}’s safety and wellbeing and therefore should be
        addressed.
      </ListItem>,
      '6_white_3_a',
    ),
    audioLine(
      <ListItem>
        If you are not happy with {childName}’s behaviour, don’t criticise {them} in a personal way (e.g. “You are so
        lazy and spoilt”). Instead, talk to {them} specifically about {their} actions (e.g. “You put in the effort for
        sport, but what about your studies? Can we find a way to balance your time better?”). Encourage {them} to think
        of {their} specific actions as good or bad, rather than seeing {themselves} as a good or bad person.
      </ListItem>,
      '6_white_4_b',
    ),
    audioLine(
      <ListItem>
        If you lose your temper with {childName}, acknowledge it and apologise to {them} rather than brushing it off and
        forgetting about it.
      </ListItem>,
      '6_white_5_d',
    ),
    audioLine(
      <ListItem>
        Try not to argue with your partner if {childName} can hear. Frequent and intense conflict between parents
        increases a teenager’s risk of depression and clinical anxiety.
      </ListItem>,
      '6_white_6_b',
    ),
    audioLine(
      <ListItem> If you argue with your partner, do not ask {childName} to choose sides.</ListItem>,
      '6_white_7_d',
    ),
    audioLine(
      <ListItem>
        If you feel angry with others at home, try to take some time to calm down before trying to resolve the issue.
      </ListItem>,
      '6_white_8_c',
    ),
  ].filter((item, index) => scores[index] === 0);

  const audioFiles = [
    '6_heading',
    '6_body',
    listIntro?.audio,
    ...listItems.map((item) => item.audio),
    '6_end',
  ] as string[];

  const audios = audioFiles
    .filter((file) => file !== undefined)
    .map<[string, string]>((file) => [file, getPublicAudioUrl(file)]);

  return (
    <AudioSection audios={audios} {...others}>
      <Typography audioKey="6_heading" variant="h6" color="textSecondary" gutterBottom>
        Your Home Environment
      </Typography>
      <Typography audioKey="6_body" paragraph>
        Having a supportive and safe home environment helps to reduce {childName}’s risk of depression and clinical
        anxiety. This includes minimising conflict at home, and setting an example for {childName} by using positive
        approaches to handling conflict.
      </Typography>

      {listIntro?.text}

      <ul>{listItems.map((item) => item.text)}</ul>

      <Typography audioKey="6_end" paragraph>
        Check out the module{' '}<b><i>Calm versus conflict</i></b> for more strategies to
        help keep your home environment supportive for {childName}.
      </Typography>
    </AudioSection>
  );
}
