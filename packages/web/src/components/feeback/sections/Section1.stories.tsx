import React from 'react';
import { Container, Box } from '@material-ui/core';
import { Meta, Story } from '@storybook/react';
import { UserSurvey } from 'app';
import { SectionProps } from './index';
import { Section1 } from './Section1';
import { FreqScales } from '../helpers';
import { userVocabulary } from 'components/helpers';
import { Article } from '../Article';

export default {
  title: 'components/feedback/sections/Section1',
  component: Section1,
} as Meta;

const vocabulary: Record<string, string> = userVocabulary({
  childName: 'Marry',
  childGender: 'female',
});

// ['S1#Q1', FreqScales[3]],
// ['S1#Q2', [FreqScales[2], FreqScales[3]]],
// ['S1#Q3', [FreqScales[2], FreqScales[3]]],
// ['S1#Q4', FreqScales[0]],
// ['S1#Q5', FreqScales[3]],
// ['S1#Q6', FreqScales[0]],
// ['S1#Q7', FreqScales[3]],
// ['S1#Q8', FreqScales[0]],

const bestScenario: UserSurvey = {
  _step: 0,
  _total: 0,
  _completed: 0,
  'S1#Q1': FreqScales[3],
  'S1#Q2': FreqScales[3],
  'S1#Q3': FreqScales[3],
  'S1#Q4': FreqScales[0],
  'S1#Q5': FreqScales[3],
  'S1#Q6': FreqScales[0],
  'S1#Q7': FreqScales[3],
  'S1#Q8': FreqScales[0],
};

const Template: Story<SectionProps> = (args) => {
  const [active, setActive] = React.useState(false);
  return (
    <Container>
      <Box my={2}>
        <Article>
          <Section1 {...args} active={active} onActive={setActive} />
        </Article>
      </Box>
    </Container>
  );
};

export const LowConfidence = Template.bind({});
LowConfidence.args = {
  vocabulary,
  userSurvey: {
    ...bestScenario,
    'S1#Q1': FreqScales[0],
    'S1#Q2': FreqScales[0],
    'S1#Q3': FreqScales[0],
    'S1#Q4': FreqScales[3],
    'S1#Q5': FreqScales[0],
  },
};

export const MediumConfidence = Template.bind({});
MediumConfidence.args = {
  vocabulary,
  userSurvey: {
    ...bestScenario,
    'S1#Q1': FreqScales[0],
    'S1#Q2': FreqScales[0],
  },
};

export const HighConfidence = Template.bind({});
HighConfidence.args = {
  vocabulary,
  userSurvey: {
    ...bestScenario,
    'S1#Q1': FreqScales[0],
  },
};

export const BestConfidence = Template.bind({});
BestConfidence.args = {
  vocabulary,
  userSurvey: bestScenario,
};
