import React from 'react';
import { Container, Box } from '@material-ui/core';
import { Meta, Story } from '@storybook/react';
import { UserSurvey } from 'app';
import { SectionProps } from './index';
import { Section9 } from './Section9';
import { LikelyScales } from '../helpers';
import { userVocabulary } from 'components/helpers';
import { Article } from '../Article';

export default {
  title: 'components/feedback/sections/Section9',
  component: Section9,
} as Meta;

const vocabulary: Record<string, string> = userVocabulary({
  childName: 'Marry',
  childGender: 'female',
});

// ['S9#Q1', [LikelyScales[2], LikelyScales[3]]],
// ['S9#Q2', [LikelyScales[0], LikelyScales[1]]],
// ['S9#Q3', [LikelyScales[2], LikelyScales[3]]],
// ['S9#Q4', [LikelyScales[2], LikelyScales[3]]],
// ['S9#Q5', [LikelyScales[0], LikelyScales[1]]],
// ['S9#Q6', [LikelyScales[2], LikelyScales[3]]],
// ['S9#Q7', [LikelyScales[2], LikelyScales[3]]],
// ['S9#Q8', [LikelyScales[2], LikelyScales[3]]],
// ['S9#Q9', [LikelyScales[2], LikelyScales[3]]],
const bestSurvey: UserSurvey = {
  _step: 0,
  _total: 0,
  _completed: 0,
  'S9#Q1': LikelyScales[3],
  'S9#Q2': LikelyScales[0],
  'S9#Q3': LikelyScales[3],
  'S9#Q4': LikelyScales[3],
  'S9#Q5': LikelyScales[0],
  'S9#Q6': LikelyScales[3],
  'S9#Q7': LikelyScales[3],
  'S9#Q8': LikelyScales[3],
  'S9#Q9': LikelyScales[3],
};

const Template: Story<SectionProps> = (args) => {
  const [active, setActive] = React.useState(false);
  return (
    <Container>
      <Box my={2}>
        <Article>
          <Section9 {...args} active={active} onActive={setActive} />
        </Article>
      </Box>
    </Container>
  );
};

export const Low = Template.bind({});
Low.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S9#Q1': LikelyScales[0],
    'S9#Q2': LikelyScales[3],
    'S9#Q3': LikelyScales[0],
    'S9#Q4': LikelyScales[0],
    'S9#Q5': LikelyScales[3],
    'S9#Q6': LikelyScales[0],
    'S9#Q7': LikelyScales[0],
    'S9#Q8': LikelyScales[0],
    'S9#Q9': LikelyScales[0],
  },
};

export const Medium = Template.bind({});
Medium.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S9#Q1': LikelyScales[0],
    'S9#Q2': LikelyScales[3],
    'S9#Q7': LikelyScales[0],
    'S9#Q8': LikelyScales[0],
    'S9#Q9': LikelyScales[0],
  },
};

export const High = Template.bind({});
High.args = {
  vocabulary,
  userSurvey: { ...bestSurvey, 'S9#Q1': LikelyScales[0] },
};

export const Best = Template.bind({});
Best.args = {
  vocabulary,
  userSurvey: bestSurvey,
};
