import React from 'react';
import sum from 'lodash/sum';
import { Link } from 'components';
import { commonLinks, SectionProps } from '.';
import { calcOptionsScores, findByRanges, FreqScales, getPublicAudioUrl } from '../helpers';
import { AudioSection, AudioLine, ListItem, Typography, audioLine, Section } from '../audio';

export const calcScores = calcOptionsScores([
  ['S3#Q1', [FreqScales[2], FreqScales[3]]],
  ['S3#Q2', [FreqScales[2], FreqScales[3]]],
  ['S3#Q3', [FreqScales[2], FreqScales[3]]],
  ['S3#Q4', [FreqScales[0], FreqScales[1]]],
  ['S3#Q5', [FreqScales[2], FreqScales[3]]],
  ['S3#Q6', [FreqScales[0], FreqScales[1]]],
]);

export function Section3(props: SectionProps) {
  const { userSurvey, vocabulary, ...others } = props;
  const { childName, their, them } = vocabulary;
  const scores = calcScores(userSurvey);

  const listIntro = findByRanges<AudioLine>([
    [
      [0, 3],
      audioLine(
        <Typography paragraph>To help {childName} in this area, you could try the following strategies:</Typography>,
        '4_white_low',
      ),
    ],
    [
      [3, 5],
      audioLine(
        <Typography paragraph>
          You already help {childName} in this area in several ways. To help build {their} social skills further, you
          could:
        </Typography>,
        '4_white_medium',
      ),
    ],
    [
      5,
      audioLine(
        <Section>
          <Typography paragraph>
            It’s great that you help {childName} to develop {their} social skills and have positive relationships with
            others in lots of ways.
          </Typography>
          <Typography paragraph>To further improve {their} social skills, you could also:</Typography>
        </Section>,
        '4_white_high',
      ),
    ],
    [
      [6, Number.MAX_VALUE],
      audioLine(
        <Typography paragraph>
          It’s great that you help {childName} to develop {their} social skills and have positive relationships with
          others in lots of ways.
        </Typography>,
        '4_white_best',
      ),
    ],
  ])(sum(scores));

  const listItems: AudioLine[] = [
    audioLine(
      <ListItem>
        Encourage {childName} to spend time with {their} friends more often.
      </ListItem>,
      '4_white_1_a',
    ),
    audioLine(<ListItem>Encourage {childName} to do kind things for others.</ListItem>, '4_white_2_a'),
    audioLine(
      <ListItem>Encourage {childName} to participate in a range of social situations.</ListItem>,
      '4_white_3_a',
    ),
    audioLine(
      <ListItem>
        Encourage {childName} to spend time with people of various ages (both young and old), to help {them} develop a
        range of social skills and supportive relationships. These could include friends, relatives (e.g. younger and
        older cousins), neighbours, or other important people in {their} life.
      </ListItem>,
      '4_white_4_c',
    ),
    audioLine(
      <ListItem>Take some time to talk through any social problems {childName} may have.</ListItem>,
      '4_white_5_a',
    ),
    audioLine(
      <ListItem>Encourage {childName} to spend time with both friends and family on weekends.</ListItem>,
      '4_white_6_c',
    ),
  ].filter((item, index) => scores[index] === 0);

  const audioFiles = [
    '4_heading',
    '4_body',
    listIntro?.audio,
    ...listItems.map((item) => item.audio),
    '4_end',
  ] as string[];

  const audios = audioFiles
    .filter((file) => file !== undefined)
    .map<[string, string]>((file) => [file, getPublicAudioUrl(file)]);

  return (
    <AudioSection audios={audios} {...others}>
      <Typography audioKey="4_heading" variant="h6" color="textSecondary" gutterBottom>
        {childName}’s Relationships with Others
      </Typography>

      <Typography audioKey="4_body" paragraph>
        Having good social skills and supportive relationships with a range of people of different ages helps to reduce{' '}
        {childName}’s risk of depression and clinical anxiety.
      </Typography>

      {listIntro?.text}

      <ul>{listItems.map((item) => item.text)}</ul>

      <Typography audioKey="4_end" paragraph>
        If you’d like to learn more about helping {childName} build supportive relationships, check out the module{' '}
        <i>
          <b>Good friends = supportive relationships</b>
        </i>
        .
      </Typography>
    </AudioSection>
  );
}
