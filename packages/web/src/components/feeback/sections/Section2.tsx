import React from 'react';
import sum from 'lodash/sum';
import { Link } from 'components';
import { calcOptionsScores, findByRanges, FreqScales, getPublicAudioUrl } from '../helpers';
import { AudioSection, AudioLine, ListItem, Typography, audioLine } from '../audio';
import { commonLinks, SectionProps } from '.';

export const calcScores = calcOptionsScores([
  ['S2#Q1', FreqScales[3]],
  ['S2#Q2', [FreqScales[2], FreqScales[3]]],
  ['S2#Q3', FreqScales[0]],
  ['S2#Q4', [FreqScales[0], FreqScales[1]]],
  ['S2#Q5', FreqScales[3]],
  ['S2#Q6', [FreqScales[0], FreqScales[1]]],
  ['S2#Q7', [FreqScales[2], FreqScales[3]]],
  ['S2#Q8', FreqScales[3]],
]);

export function Section2(props: SectionProps) {
  const { userSurvey, vocabulary, ...others } = props;
  const { childName, them, they, are, their } = vocabulary;
  const scores = calcScores(userSurvey);

  // list intro
  const listIntro = findByRanges<AudioLine>([
    [
      [0, 4],
      audioLine(
        <Typography paragraph>
          Teenagers benefit most when their parents continue to show interest in their lives without being intrusive,
          and respect their need for growing independence.
        </Typography>,
        '3_white_low',
      ),
    ],
    [
      [4, 7],
      audioLine(
        <Typography paragraph>
          Teenagers benefit most when their parents continue to show interest in their lives without being intrusive,
          and respect their need for growing independence.
        </Typography>,
        '3_white_low',
      ),
    ],
    [
      [7, Number.MAX_VALUE],
      audioLine(<Typography paragraph>You seem to have found a good balance, well done!</Typography>, '3_white_best'),
    ],
  ])(sum(scores));

  // group1
  const group1 = [scores[0], scores[1], scores[3], scores[4]];
  const list1Items: AudioLine[] = [
    audioLine(<ListItem>Eating dinner together as a family more often.</ListItem>, '3_life_1_a'),
    audioLine(
      <ListItem>
        Doing enjoyable one‐on‐one activities with {childName} regularly. This could be anything that you both enjoy ‐
        be creative!
      </ListItem>,
      '3_life_2_a',
    ),
    audioLine(
      <ListItem>
        Showing interest in what {childName} is doing at school. For example, you could ask who {they} spent time with
        at lunch time, or what was the easiest/hardest thing {they} had to do at school that day.
      </ListItem>,
      '3_life_4_c',
    ),
    audioLine(
      <ListItem>
        If {childName} is going out without you, have a casual chat with {them} about what {they} will be doing, where{' '}
        {they} will be, and who {they} will be with.
      </ListItem>,
      '3_life_5_a',
    ),
  ].filter((item, index) => group1[index] === 0);

  const list1Audios = list1Items.map((item) => item.audio);

  // group2
  const group2 = [scores[2], scores[5], scores[6], scores[7]];
  const list2Items: AudioLine[] = [
    audioLine(
      <ListItem>
        Encouraging {childName} to try out extra‐curricular activities (e.g. sports, music, or anything else {they}{' '}
        {are} interested in).
      </ListItem>,
      '3_independence_3_b',
    ),
    audioLine(
      <ListItem>
        Allowing {childName} to become more independent of you over time. Evaluate whether you are taking over things
        too much. For example, you can ask yourself, “Did I really need to step in?” and “What would have been the worst
        thing to happen if I didn’t step in?”
      </ListItem>,
      '3_independence_6_c',
    ),
    audioLine(
      <ListItem>
        Encouraging {childName} to try out a variety of activities, to find out what {they} {are} interested in and what{' '}
        {they} {are} good at. This helps to build {their} self‐confidence.
      </ListItem>,
      '3_independence_7_a',
    ),
    audioLine(
      <ListItem>
        Gradually increasing {childName}’s responsibilities and independence over time to allow {them} to mature.
      </ListItem>,
      '3_independence_8_a',
    ),
  ].filter((item, index) => group2[index] === 0);

  const list2Audios = list2Items.map((item) => item.audio);

  const audioFiles = [
    '3_heading',
    '3_body',
    listIntro?.audio,
    ...(list1Audios.length > 0 ? ['3_life_start', ...list1Audios] : []),
    ...(list2Audios.length > 0 ? ['3_independence_start', ...list2Audios] : []),
    '3_end',
  ] as string[];

  const audios = audioFiles
    .filter((file) => file !== undefined)
    .map<[string, string]>((file) => [file, getPublicAudioUrl(file)]);

  return (
    <AudioSection audios={audios} {...others}>
      <Typography audioKey="3_heading" variant="h6" color="textSecondary" gutterBottom>
        Your Involvement in {childName}’s Life
      </Typography>

      <Typography audioKey="3_body" paragraph>
        It is important to find a balance between being involved in {childName}’s life and giving {them} age‐appropriate
        independence.
      </Typography>

      {listIntro?.text}

      {list1Items.length > 0 && (
        <>
          <Typography audioKey="3_life_start" variant="subtitle1">
            You can be more involved in {childName}’s life by:
          </Typography>
          <ul> {list1Items.map((item) => item.text)}</ul>
        </>
      )}

      {list2Items.length > 0 && (
        <>
          <Typography audioKey="3_independence_start" variant="subtitle1">
            You can further support {childName}’s growing independence by:
          </Typography>
          <ul>{list2Items.map((item) => item.text)}</ul>
        </>
      )}

      <Typography audioKey="3_end" paragraph>
        You can learn more about the balance between being involved in {childName}’s life and supporting {their}{' '}
        developing autonomy in the module{' '}<b><i>Nurture roots and inspire wings</i></b>
        .
      </Typography>
    </AudioSection>
  );
}
