import React from 'react';
import { Container, Box } from '@material-ui/core';
import { Meta, Story } from '@storybook/react';
import { UserSurvey } from 'app';
import { SectionProps } from './index';
import { Section3 } from './Section3';
import { FreqScales } from '../helpers';
import { userVocabulary } from 'components/helpers';
import { Article } from '../Article';

export default {
  title: 'components/feedback/sections/Section3',
  component: Section3,
} as Meta;

const vocabulary: Record<string, string> = userVocabulary({
  childName: 'Marry',
  childGender: 'female',
});

// ['S3#Q1', [FreqScales[2], FreqScales[3]]],
// ['S3#Q2', [FreqScales[2], FreqScales[3]]],
// ['S3#Q3', [FreqScales[2], FreqScales[3]]],
// ['S3#Q4', [FreqScales[0], FreqScales[1]]],
// ['S3#Q5', [FreqScales[2], FreqScales[3]]],
// ['S3#Q6', [FreqScales[0], FreqScales[1]]],

const bestSurvey: UserSurvey = {
  _step: 0,
  _total: 0,
  _completed: 0,
  'S3#Q1': FreqScales[3],
  'S3#Q2': FreqScales[3],
  'S3#Q3': FreqScales[3],
  'S3#Q4': FreqScales[0],
  'S3#Q5': FreqScales[3],
  'S3#Q6': FreqScales[0],
};

const Template: Story<SectionProps> = (args) => {
  const [active, setActive] = React.useState(false);
  return (
    <Container>
      <Box my={2}>
        <Article>
          <Section3 {...args} active={active} onActive={setActive} />
        </Article>
      </Box>
    </Container>
  );
};

export const Low = Template.bind({});
Low.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S3#Q1': FreqScales[0],
    'S3#Q2': FreqScales[0],
    'S3#Q3': FreqScales[0],
    'S3#Q4': FreqScales[3],
  },
};

export const Medium = Template.bind({});
Medium.args = {
  vocabulary,
  userSurvey: { ...bestSurvey, 'S3#Q1': FreqScales[0], 'S3#Q2': FreqScales[0] },
};

export const High = Template.bind({});
High.args = {
  vocabulary,
  userSurvey: { ...bestSurvey, 'S3#Q1': FreqScales[0] },
};

export const Best = Template.bind({});
Best.args = {
  vocabulary,
  userSurvey: bestSurvey,
};
