import React from 'react';
import sum from 'lodash/sum';
import { ExtLink } from 'components';
import { SectionProps } from '.';
import {
  calcOptionsScores,
  filterByConditions,
  findByRanges,
  FreqScales,
  LikelyScales,
  getPublicAudioUrl,
} from '../helpers';
import { AudioSection, AudioLine, ListItem, Typography, audioLine, Section } from '../audio';

export const calcScores = calcOptionsScores([
  ['S6#Q1', FreqScales[3]],
  ['S6#Q2', [FreqScales[0], FreqScales[1]]],
  ['S6#Q3', FreqScales[3]],
  ['S6#Q4', FreqScales[3]],
  ['S6#Q5', FreqScales[3]],
  ['S6#Q6', FreqScales[0]],
  ['S6#Q7', [FreqScales[2], FreqScales[3]]],
  ['S6#Q8', FreqScales[0]],
  ['S6#Q9', [LikelyScales[2], LikelyScales[3]]],
  ['S6#Q10', [LikelyScales[0], LikelyScales[1]]],
  ['S6#Q11', [LikelyScales[0], LikelyScales[1]]],
  ['S6#Q12', [LikelyScales[2], LikelyScales[3]]],
]);

export function Section6(props: SectionProps) {
  const { userSurvey, vocabulary, ...others } = props;
  const { childName, their, they, them, are } = vocabulary;
  const scores = calcScores(userSurvey);

  const group1 = [scores[0], scores[1], scores[2], scores[6], scores[7]];

  const listIntro = findByRanges<AudioLine>([
    [
      [0, 4],
      audioLine(
        <Typography paragraph>
          Here are some strategies you could use to help {childName} develop good health habits:
        </Typography>,
        '7_score_low',
      ),
    ],
    [
      [4, 7],
      audioLine(
        <Typography paragraph>
          You are already helping {childName} to develop some good health habits, well done! To improve {their} health
          habits, here are some strategies you could use:
        </Typography>,
        '7_score_medium',
      ),
    ],
    [
      [7, Number.MAX_VALUE],
      audioLine(
        <Typography paragraph>
          You are doing a great job of encouraging {childName} to develop good health habits – keep up the great work!
        </Typography>,
        '7_score_high',
      ),
    ],
  ])(sum(scores.slice(0, 8)));

  const listIntroExtra =
    sum(scores.slice(0, 8)) === 7 && group1.indexOf(0) >= 0
      ? audioLine(<Typography paragraph>It is also important to remember this:</Typography>, '7_score_extra_high')
      : undefined;

  const list1Items: AudioLine[] = [
    audioLine(
      <ListItem>
        Make sure that {childName} eats a healthy, balanced diet, including a wide variety of nutritious foods. For
        information on the Australian dietary guidelines, click{' '}
        <ExtLink href="https://www.eatforhealth.gov.au/sites/default/files/content/The%20Guidelines/n55f_children_brochure.pdf">
          here
        </ExtLink>
        .
      </ListItem>,
      '7_health_1_c',
    ),
    audioLine(
      <ListItem>
        Limit the amount of unhealthy foods and drinks (e.g. chips, biscuits, chocolates, soft drinks) in the house.
      </ListItem>,
      '7_health_2_d',
    ),
    audioLine(
      <ListItem>
        Set an example for {childName} by having good health habits (i.e. healthy diet, regular exercise and responsible
        use of alcohol) yourself.
      </ListItem>,
      '7_health_3_c',
    ),
    audioLine(
      <ListItem>
        Encourage {childName} to get regular physical exercise, at least most days. If {they} {are} not interested in
        sports, encourage {them} to find other activities, such as going for a walk, riding a bike, or walking to
        school. For more information on the Australian physical activity guidelines for young people, click{' '}
        <ExtLink href="https://www.health.gov.au/sites/default/files/documents/2021/05/24-hour-movement-guidelines-children-and-young-people-5-to-17-years-fact-sheet.pdf">
          here
        </ExtLink>
        .
      </ListItem>,
      '7_health_7_a',
    ),
    audioLine(
      <ListItem>
        Do not allow {childName} to have any alcohol. Research shows that drug and alcohol use is harmful for teenagers’
        physical health and brain development. The National Health and Medical Research Council (NHMRC) recommends that
        teenagers under 15 years do not consume any alcohol, and that alcohol consumption is delayed for as long as
        possible in older teenagers. To find out more about young people and alcohol use, click{' '}
        <ExtLink href="https://www.parentingstrategies.net">here</ExtLink>.
      </ListItem>,
      '7_health_8_d',
    ),
  ].filter((item, index) => group1[index] === 0);

  const sleepItems: AudioLine[] = filterByConditions<Array<0 | 1>, AudioLine>([
    [
      (scores) => scores[3] === 0,
      audioLine(
        <ListItem>
          Going to bed and getting up at roughly the same time each day (no more than a two‐hour difference between
          weekdays and weekends). This helps to regulate {childName}’s internal ‘body clock,’ which will help {them} get
          a better night’s sleep.
        </ListItem>,
        '7_sleep_4_a',
      ),
    ],
    [
      (scores) => scores[4] === 0,
      audioLine(
        <ListItem>
          Not watching TV or using electronic devices in bed before going to sleep. The bright light from electronic
          devices can make it harder for {childName} to get to sleep. Instead, encourage {them} to wind down with
          relaxing activities (e.g. reading, having a shower or bath, or listening to calming music).
        </ListItem>,
        '7_sleep_5_a',
      ),
    ],
    [
      (scores) => scores[3] === 1 && scores[5] === 0,
      audioLine(
        <ListItem>
          Not sleeping‐in on weekends. Instead, encourage {childName} to get up at roughly the same time each day (no
          more than two hours difference between weekdays and weekends). If you think {childName} needs more sleep,
          encourage {them} to go to bed earlier each night. Sleeping in too late on weekends can make it harder for{' '}
          {childName} to wake up on weekday mornings.
        </ListItem>,
        '7_sleep_6_b_4_d',
      ),
    ],
    [
      (scores) => scores[3] === 0 && scores[5] === 0,
      audioLine(
        <ListItem>
          Not sleeping‐in on weekends. If you think {childName} needs more sleep, encourage {them} to go to bed earlier
          each night. Sleeping in too late on weekends can make it harder for {childName} to wake up on weekday
          mornings.
        </ListItem>,
        '7_sleep_6_b_4_a',
      ),
    ],
  ])(scores);

  const drugGroup = scores.slice(8, 12);
  const drugGroupItem =
    sum(drugGroup) === 4
      ? audioLine(
          <Section>
            <Typography audioKey="7_drugs_best" paragraph>
              You already seem to have a good idea of what to do if you find out that {childName} is using alcohol or
              other drugs.
            </Typography>
            <Typography audioKey="7_drugs_best" paragraph>
              For guidelines on parenting strategies to prevent alcohol misuse in teenagers, click{' '}
              <ExtLink href="https://www.parentingstrategies.net">here</ExtLink>.
            </Typography>
          </Section>,
          '7_drugs_best',
        )
      : audioLine(
          <Section>
            <Typography audioKey="7_drugs_low" paragraph>
              It is important that you know how to respond to this situation if it does occur. We suggest you handle it
              by:
            </Typography>
            <ul>
              <ListItem>
                Calmly talking with {childName} about why {they} {are} using the alcohol or drugs.
              </ListItem>
              <ListItem>
                Being careful to express your disappointment with {their} behaviour without being overly critical of{' '}
                {them} as a person.
              </ListItem>
              <ListItem>Enforcing a reasonable consequence, rather than punishing {them} in anger.</ListItem>
              <ListItem>Seeking professional help if you think it is needed.</ListItem>
            </ul>
            <Typography audioKey="7_drugs_low" paragraph>
              For guidelines on parenting strategies to prevent alcohol misuse in teenagers that are supported by
              research evidence and endorsed by national experts, click{' '}
              <ExtLink href="https://www.parentingstrategies.net">here</ExtLink>.
            </Typography>
          </Section>,
          '7_drugs_low',
        );

  const audioFiles = [
    '7_heading',
    '7_body',
    listIntro?.audio,
    listIntroExtra?.audio,
    ...list1Items.map((item) => item.audio),
    ...(sleepItems.length > 0 ? ['7_sleep_start', ...sleepItems.map((item) => item.audio), '7_sleep_end'] : []),
    '7_drugs_start',
    drugGroupItem?.audio,
    '7_end',
  ] as string[];

  const audios = audioFiles
    .filter((file) => file !== undefined)
    .map<[string, string]>((file) => [file, getPublicAudioUrl(file)]);

  return (
    <AudioSection audios={audios} {...others}>
      <Typography audioKey="7_heading" variant="h6" color="textSecondary" gutterBottom>
        Health Habits
      </Typography>
      <Typography audioKey="7_body" paragraph>
        Having a healthy diet and lifestyle is important for {childName}’s mental health and wellbeing. This includes
        eating a balanced diet, exercising regularly, getting enough sleep, and not using alcohol or other drugs.
      </Typography>

      {listIntro?.text}
      {listIntroExtra?.text}

      <ul>{list1Items.map((item) => item.text)}</ul>

      {sleepItems.length > 0 && (
        <>
          <Typography audioKey="7_sleep_start" variant="h6" gutterBottom>
            {childName}’s sleep
          </Typography>

          <Typography audioKey="7_sleep_start" paragraph>
            Getting enough sleep each night can reduce {childName}’s risk of developing depression and clinical anxiety.
            You can improve {childName}’s sleep by encouraging {them} to have healthy sleep habits, including:
          </Typography>

          <ul>{sleepItems.map((item) => item.text)}</ul>

          <Typography audioKey="7_sleep_end" paragraph>
            To read more about teenage sleep, click{' '}
            <ExtLink href="http://raisingchildren.net.au/articles/sleep_early_teens.html/context/1069">here</ExtLink>.
          </Typography>
        </>
      )}

      <Typography audioKey="7_drugs_start" variant="h6" gutterBottom>
        Alcohol or drug use
      </Typography>
      <Typography audioKey="7_drugs_start" paragraph>
        It is not uncommon for teenagers to experiment with alcohol or other drugs.
      </Typography>

      {drugGroupItem.text}

      <Typography audioKey="7_end" paragraph>
        To find out more about supporting {childName}’s health habits, see the module{' '}
        <b>
          <i>Good health habits for good mental health</i>
        </b>
        .
      </Typography>
    </AudioSection>
  );
}
