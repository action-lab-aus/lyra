import React from 'react';
import sum from 'lodash/sum';
import { Link } from 'components';
import { commonLinks, SectionProps } from '.';
import { calcOptionsScores, filterByConditions, findByRanges, FreqScales, getPublicAudioUrl } from '../helpers';
import { AudioSection, AudioLine, ListItem, Typography, audioLine } from '../audio';

export const calcScores = calcOptionsScores([
  ['S8#Q1', [FreqScales[0], FreqScales[1]]],
  ['S8#Q2', [FreqScales[2], FreqScales[3]]],
  ['S8#Q3', FreqScales[3]],
  ['S8#Q4', FreqScales[0]],
  ['S8#Q5', [FreqScales[2], FreqScales[3]]],
  ['S8#Q6', [FreqScales[2], FreqScales[3]]],
  ['S8#Q7', ['Yes, definitely', 'Yes, partly']],
  ['S8#Q8', ['Yes, definitely', 'Yes, partly']],
  ['S8#Q9', ['Yes, definitely', 'Yes, partly']],
]);

export function Section8(props: SectionProps) {
  const { userSurvey, vocabulary, ...others } = props;
  const { childName, they, them, their, themselves, are, take, ask, find } = vocabulary;
  const scores = calcScores(userSurvey);
  const anxietyGroup = [scores[0], scores[1], scores[3], scores[4]];

  const listIntro = findByRanges<AudioLine>([
    [
      [0, 4],
      audioLine(
        <Typography paragraph>
          You can help {childName} manage {their} everyday anxiety by trying the following:
        </Typography>,
        '9_score_low',
      ),
    ],
    [
      [4, 8],
      audioLine(
        <Typography paragraph>
          You are already doing some good things to help {childName} learn how to deal with everyday anxiety.
        </Typography>,
        '9_score_medium',
      ),
    ],
    [
      [8, Number.MAX_VALUE],
      audioLine(
        <Typography paragraph>You are doing a great job of helping {childName} learn to deal with anxiety.</Typography>,
        '9_score_high',
      ),
    ],
  ])(sum(scores));

  const listIntroExtra =
    sum(scores) > 3 && sum(anxietyGroup) !== 4
      ? sum(scores) === 8
        ? audioLine(
            <Typography paragraph>To further help {childName} in this area, keep this in mind:</Typography>,
            '9_score_extra_high',
          )
        : audioLine(<Typography paragraph>You could also try the following:</Typography>, '9_score_extra_medium')
      : null;

  const anxietyItems: AudioLine[] = filterByConditions<Array<0 | 1>, AudioLine>([
    [
      (scores) => scores[0] === 0 || scores[4] === 0,
      audioLine(
        <ListItem>
          Encourage {childName} to face situations that make {them} anxious (unless {their} safety or well‐being is at
          risk). Facing anxiety‐provoking situations is one of the best ways to reduce anxiety, because you find out
          that it wasn’t as bad as you had feared or that you coped with it. If {childName} avoids anxiety‐provoking
          situations, {they} {are} likely to become more anxious, not less. However, make sure that {childName} is
          capable of handling the situation before you encourage {them} to tackle it.
        </ListItem>,
        '9_anxiety_5_a_1_a',
      ),
    ],
    [
      (scores) => scores[1] === 0,
      audioLine(
        <ListItem>
          Reward or praise {childName} if {they} {take} steps to manage {their} anxiety. If you find yourself becoming
          impatient with {childName}’s anxiety, remind yourself of how daunting it can be to face one’s fears.
        </ListItem>,
        '9_anxiety_2_a',
      ),
    ],
    [
      (scores) => scores[3] === 0,
      audioLine(
        <ListItem>
          Try not to step in to help {childName} at the very first sign of any stress or anxiety, as the way you respond
          to {childName}’s anxiety may unintentionally increase {their} anxiety. Instead, let {them} try to manage the
          situation {themselves}, and provide help if {they} {ask} you to or if the anxiety persists.
        </ListItem>,
        '9_anxiety_4_b',
      ),
    ],
  ])(scores);

  const understandGroup = scores.slice(6, 9);
  const understandItems: AudioLine[] = [
    audioLine(<ListItem>All teenagers experience some anxiety.</ListItem>, '9_understand_7_d'),
    audioLine(
      <ListItem>Anxiety is normal and helps us to prepare for situations or perform at our best.</ListItem>,
      '9_understand_8_d',
    ),
    audioLine(
      <ListItem>
        Anxiety can become a problem if it is severe, long‐lasting, or interferes with school or other activities.
      </ListItem>,
      '9_understand_9_d',
    ),
  ].filter((item, index) => understandGroup[index] === 0);

  const manageGroup = [scores[2], scores[5]];
  const manageIntro =
    sum(scores) > 3
      ? audioLine(
          <Typography paragraph>
            You are also doing an excellent job of setting an example for {childName} by showing {them} how you manage
            your own anxiety. Keep up the good work!
          </Typography>,
          '9_extra_high_6_c_3_d',
        )
      : audioLine(
          <Typography paragraph>
            It is great that you already set an example for {childName} by showing {them} how you manage your own
            anxiety. Keep it up!
          </Typography>,
          '9_extra_low_6_c_3_d',
        );
  const manageStart = audioLine(
    <Typography paragraph>
      It is also important that you set an example for {childName} by showing {them} how you can manage your own
      anxiety. To do this, you could try the following:
    </Typography>,
    '9_manage_start',
  );

  const manageItems: AudioLine[] = [
    audioLine(
      <ListItem>
        When you talk to {childName} about things {they} {find} anxiety‐provoking, try to remain calm and relaxed.
      </ListItem>,
      '9_manage_3_a',
    ),
    audioLine(
      <ListItem>
        If you feel anxious yourself, set an example for {childName} by showing {them} how you use strategies to manage
        your anxiety. Seek professional help if you feel you need assistance to manage your own anxiety.
      </ListItem>,
      '9_manage_6_a',
    ),
  ].filter((item, index) => manageGroup[index] === 0);

  const audioFiles = [
    '9_heading',
    '9_body',
    listIntro?.audio,
    anxietyGroup.indexOf(0) >= 0 ? listIntroExtra?.audio : undefined,
    ...(anxietyItems || []).map((item) => item.audio),
    ...(understandItems.length > 0 ? ['9_understand_start', ...understandItems.map((item) => item.audio)] : []),
    sum(manageGroup) === 2 ? manageIntro?.audio : [manageStart.audio, ...manageItems.map((item) => item.audio)],
    '9_end',
  ] as string[];

  const audios = audioFiles
    .filter((file) => file !== undefined)
    .map<[string, string]>((file) => [file, getPublicAudioUrl(file)]);

  return (
    <AudioSection audios={audios} {...others}>
      <Typography audioKey="9_heading" variant="h6" color="textSecondary" gutterBottom>
        Coping with Anxiety
      </Typography>
      <Typography audioKey="9_body" paragraph>
        It is important for {childName} to learn to manage everyday anxiety so that it doesn’t develop into clinical
        anxiety. All young people experience some anxiety, however it can become a problem if it is severe,
        long‐lasting, or interferes with {childName}’s school or other activities.
      </Typography>

      {listIntro?.text}

      {anxietyGroup.indexOf(0) >= 0 && listIntroExtra?.text}

      <ul>{(anxietyItems || []).map((item) => item.text)}</ul>

      {understandItems.length > 0 && (
        <>
          <Typography audioKey="9_understand_start" paragraph>
            It would also be good for you to help {childName} understand that:
          </Typography>
          <ul>{understandItems.map((item) => item.text)}</ul>
        </>
      )}

      {sum(manageGroup) === 2 ? (
        manageIntro?.text
      ) : (
        <>
          {manageStart.text}
          <ul>{manageItems.map((item) => item.text)}</ul>
        </>
      )}

      <Typography audioKey="9_end" paragraph>
        You can learn more about supporting {childName} to manage anxiety in the module{' '}
        <i>
          <b>From thriving to surviving: Helping your teenager deal with anxiety</b>
        </i>
        .
      </Typography>
    </AudioSection>
  );
}
