import React from 'react';
import sum from 'lodash/sum';
import { ExtLink, Link } from 'components';
import { commonLinks, SectionProps } from '.';
import { calcOptionsScores, filterByConditions, findByRanges, getPublicAudioUrl, LikelyScales } from '../helpers';
import { AudioSection, AudioLine, ListItem, Typography, Section, audioLine } from '../audio';

export const calcScores = calcOptionsScores([
  ['S9#Q1', [LikelyScales[2], LikelyScales[3]]],
  ['S9#Q2', [LikelyScales[0], LikelyScales[1]]],
  ['S9#Q3', [LikelyScales[2], LikelyScales[3]]],
  ['S9#Q4', [LikelyScales[2], LikelyScales[3]]],
  ['S9#Q5', [LikelyScales[0], LikelyScales[1]]],
  ['S9#Q6', [LikelyScales[2], LikelyScales[3]]],
  ['S9#Q7', [LikelyScales[2], LikelyScales[3]]],
  ['S9#Q8', [LikelyScales[2], LikelyScales[3]]],
  ['S9#Q9', [LikelyScales[2], LikelyScales[3]]],
]);

export function Section9(props: SectionProps) {
  const { userSurvey, vocabulary, ...others } = props;
  const { childName, their, them, they, themselves, think, are, need } = vocabulary;
  const scores = calcScores(userSurvey);
  const moodGroup = scores.slice(0, 4);

  const listIntro = findByRanges<AudioLine>([
    [
      [0, 8],
      audioLine(
        <Typography paragraph>If you do notice a persistent change in {childName}’s mood or behaviour:</Typography>,
        '10_score_low',
      ),
    ],
    [
      [8, Number.MAX_VALUE],
      audioLine(
        <Typography paragraph>
          You have a good idea of what to do if you think {childName} needs professional help for depression or anxiety,
          which is important in protecting {their} health and wellbeing.
        </Typography>,
        '10_score_high',
      ),
    ],
  ])(sum(scores));

  const moodIntro =
    sum(scores) === 8 && moodGroup.indexOf(0) >= 0
      ? audioLine(
          <Typography paragraph>If you do notice a persistent change in {childName}’s mood or behaviour:</Typography>,
          '10_mood_start',
        )
      : undefined;

  const moodItems: AudioLine[] = [
    audioLine(
      <ListItem>
        Encourage {them} to talk to you about what’s on {their} mind, and really listen to what {they} {are} saying.
      </ListItem>,
      '10_mood_1_a',
    ),
    audioLine(
      <ListItem>
        Don’t dismiss {their} feelings, or tell {them} to ‘snap out of it.’ Instead, show concern and encourage {them}{' '}
        to talk to you about what’s going on for {them}.
      </ListItem>,
      '10_mood_2_c',
    ),
    audioLine(
      <ListItem>
        Try to determine whether the change in mood or behaviour is caused by a temporary situation or a more ongoing
        problem.
      </ListItem>,
      '10_mood_3_a',
    ),
    audioLine(<ListItem>Take {them} to a trained mental health professional.</ListItem>, '10_mood_4_a'),
  ].filter((item, index) => moodGroup[index] === 0);

  const paraItems = filterByConditions<Array<0 | 1>, AudioLine>([
    [
      (scores) => scores[4] === 0,
      audioLine(
        <Section>
          <Typography paragraph>
            While it is important to seek help if you think that {childName} may be experiencing clinical anxiety,
            remember that everyone experiences anxiety sometimes, and that normal anxiety is not necessarily a problem.
            Seeking help too soon may actually increase {childName}’s anxiety. Rather than seeking help at the very
            first sign of anxiety, try to determine whether the problem is serious and persistent, and needs
            professional help. You should seek professional help if:
          </Typography>
          <ul>
            <ListItem>
              <div>Your attempts to help reduce {childName}’s anxiety don’t work.</div>
              <div>Or</div>
            </ListItem>

            <ListItem>
              <div>{childName} experiences symptoms of anxiety for more than 6 months.</div>
              <div>Or</div>
            </ListItem>

            <ListItem>
              Anxiety symptoms begin to take over {childName}’s life or limit {their} activities.
            </ListItem>
          </ul>
        </Section>,
        '10_para_5_d',
      ),
    ],

    [
      (scores) => scores[5] === 0,
      audioLine(
        <Typography paragraph>
          If you think that you may be experiencing problems with depression or clinical anxiety, it is important that
          you look after yourself. You can set an example for {childName} by seeking professional help. Getting
          treatment may not only help you, but also {childName}. By getting treatment yourself, you will be better able
          to support {childName}.
        </Typography>,
        '10_para_6_a',
      ),
    ],

    [
      (scores) => scores[6] === 0,
      audioLine(
        <Section>
          <Typography paragraph>
            To find out more about where to get help for depression or clinical anxiety, check out{' '}
            <Link href="/resources">the Useful Resources here</Link>.
          </Typography>
          <Typography paragraph>You can also talk to your family doctor or local health service.</Typography>
        </Section>,
        '10_para_11_a',
      ),
    ],

    [
      (scores) => scores[7] === 0,
      audioLine(
        <Section>
          <Typography paragraph>
            Provide {childName} with information about where {they} can seek help for depression and clinical anxiety if{' '}
            {they} {need} to. For example, you could talk with {childName} about where {they} could seek help (e.g. the
            school counselor or family doctor), put some information or resources up on the fridge, or give {them} a
            list of local services available.
          </Typography>
          <Typography paragraph>
            You can find some more resources for {childName}{' '}
            <Link href="/resources">here</Link>.
          </Typography>
        </Section>,
        '10_para_12_a',
      ),
    ],

    [
      (scores) => scores[8] === 0,
      audioLine(
        <Typography paragraph>
          Sometimes, it can be difficult for both parents and teenagers to accept that professional help is needed.
          Teenagers may be reluctant to seek help due to concerns about privacy or embarrassment. Encourage {childName}{' '}
          to seek help {themselves} if {they} {think} {they} {need} it. However, if you believe that {childName} needs
          professional help, you should seek help on {their} behalf even if {they} {are} reluctant.
        </Typography>,
        '10_para_13_a',
      ),
    ],
  ])(scores);

  const audioFiles = [
    '10_heading',
    '10_body',
    listIntro?.audio,
    moodIntro?.audio,
    ...moodItems.map((item) => item.audio),
    ...paraItems.map((item) => item.audio),
    '10_end',
  ] as string[];

  const audios = audioFiles
    .filter((file) => file !== undefined)
    .map<[string, string]>((file) => [file, getPublicAudioUrl(file)]);

  return (
    <AudioSection audios={audios} {...others}>
      <Typography audioKey="10_heading" variant="h6" color="textSecondary" gutterBottom>
        Getting Help When Needed
      </Typography>
      <Typography audioKey="10_body" paragraph>
        During adolescence, a lot of changes occur in a person’s emotions, thinking, and behaviour. This can make it
        hard to tell whether a change is due to depression or clinical anxiety, or is part of normal adolescent
        development.
      </Typography>

      {listIntro?.text}

      {moodIntro?.text}

      <ul>{(moodItems || []).map((item) => item.text)}</ul>

      {paraItems.map((item) => item.text)}

      <Typography audioKey="10_end" paragraph>
        If you’d like to learn more about when and where to seek professional help, please see the module{' '}
        <b><i>When things aren’t OK? Seeking professional help</i></b>.
      </Typography>
    </AudioSection>
  );
}
