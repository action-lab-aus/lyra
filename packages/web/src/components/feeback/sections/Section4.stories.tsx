import React from 'react';
import { Container, Box } from '@material-ui/core';
import { Meta, Story } from '@storybook/react';
import { UserSurvey } from 'app';
import { SectionProps } from './index';
import { Section4 } from './Section4';
import { FreqScales } from '../helpers';
import { userVocabulary } from 'components/helpers';
import { Article } from '../Article';

export default {
  title: 'components/feedback/sections/Section4',
  component: Section4,
} as Meta;

const vocabulary: Record<string, string> = userVocabulary({
  childName: 'Marry',
  childGender: 'female',
});

// ['S4#Q1', 'Yes'],
// ['S4#Q2', 'Yes'],
// ['S4#Q3', 'Yes'],
// ['S4#Q4', 'Yes'],
// ['S4#Q5', 'Yes, to adapt to my teenager’s maturity and responsibility.'],
// ['S4#Q6', FreqScales[0]],
// ['S4#Q7', FreqScales[3]],
// ['S4#Q8', FreqScales[0]],
// ['S4#Q9', [FreqScales[2], FreqScales[3]]],
const bestSurvey: UserSurvey = {
  _step: 0,
  _total: 0,
  _completed: 0,
  'S4#Q1': 'Yes',
  'S4#Q2': 'Yes',
  'S4#Q3': 'Yes',
  'S4#Q4': 'Yes',
  'S4#Q5': 'Yes, to adapt to my teenager’s maturity and responsibility.',
  'S4#Q6': FreqScales[0],
  'S4#Q7': FreqScales[3],
  'S4#Q8': FreqScales[0],
  'S4#Q9': FreqScales[3],
};

const Template: Story<SectionProps> = (args) => {
  const [active, setActive] = React.useState(false);
  return (
    <Container>
      <Box my={2}>
        <Article>
          <Section4 {...args} active={active} onActive={setActive} />
        </Article>
      </Box>
    </Container>
  );
};

export const Best = Template.bind({});
Best.args = {
  vocabulary,
  userSurvey: bestSurvey,
};

export const OddLowEvenHigh = Template.bind({});
OddLowEvenHigh.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S4#Q1': 'No',
    'S4#Q3': 'No',
    'S4#Q5': 'Yes, all the time.',
    'S4#Q7': FreqScales[0],
    'S4#Q9': FreqScales[0],
  },
};

export const OddHighEvenLow = Template.bind({});
OddHighEvenLow.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S4#Q2': 'No',
    'S4#Q4': 'No',
    'S4#Q6': FreqScales[3],
    'S4#Q8': FreqScales[3],
  },
};

export const Low = Template.bind({});
Low.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S4#Q1': 'No',
    'S4#Q2': 'No',
    'S4#Q3': 'No',
    'S4#Q4': 'No',
    'S4#Q5': 'No, family rules apply for as long as my teenager is under my care.',
    'S4#Q6': FreqScales[3],
    'S4#Q7': FreqScales[0],
    'S4#Q8': FreqScales[3],
    'S4#Q9': FreqScales[0],
  },
};
