import React from 'react';
import sum from 'lodash/sum';
import { Link } from 'components';
import { calcOptionsScores, findByRanges, FreqScales, getPublicAudioUrl } from '../helpers';
import { AudioSection, AudioLine, ListItem, Typography, audioLine } from '../audio';
import { commonLinks, SectionProps } from '.';

export const calcScores = calcOptionsScores([
  ['S1#Q1', FreqScales[3]],
  ['S1#Q2', [FreqScales[2], FreqScales[3]]],
  ['S1#Q3', [FreqScales[2], FreqScales[3]]],
  ['S1#Q4', FreqScales[0]],
  ['S1#Q5', FreqScales[3]],
  ['S1#Q6', FreqScales[0]],
  ['S1#Q7', FreqScales[3]],
  ['S1#Q8', FreqScales[0]],
]);

export function Section1(props: SectionProps) {
  const { userSurvey, vocabulary, ...others } = props;
  const { childName, them, their, they, appear, are, seem, have, need } = vocabulary;
  const scores = calcScores(userSurvey);

  const listIntro = findByRanges<AudioLine>([
    [
      [0, 4],
      audioLine(
        <Typography paragraph>
          {childName} would benefit from having a closer relationship with you. There are some things you could do to
          improve your relationship, such as:
        </Typography>,
        '2_white_low',
      ),
    ],
    [
      [4, 7],
      audioLine(
        <Typography paragraph>
          You are doing lots of things to build a good relationship with {childName}. There are some other things you
          may also like to consider doing, such as:
        </Typography>,
        '2_white_medium',
      ),
    ],
    [
      7,
      audioLine(
        <Typography paragraph>
          You are doing a great job of building a good relationship with {childName} by being involved in {their} life,
          encouraging communication between the two of you, and supporting {them}. Keep up the good work! You may also
          like to consider:
        </Typography>,
        '2_white_high',
      ),
    ],
    [
      [8, Number.MAX_VALUE],
      audioLine(
        <Typography paragraph>
          You are doing a great job of building a good relationship with {childName} by being involved in {their} life,
          encouraging communication between the two of you, and supporting {them}. Keep up the good work!
        </Typography>,
        '2_white_best',
      ),
    ],
  ])(sum(scores));

  const listItems: AudioLine[] = [
    audioLine(
      <ListItem>
        Verbally expressing your affection for {childName} more often, such as telling {them} that you love {them}.
        Adapt the way you show affection according to {childName}’s age and maturity.
      </ListItem>,
      '2_white_1_a',
    ),
    audioLine(
      <ListItem>
        Making time each day to ask {childName} about {their} day and what {they} {have} been doing, regardless of{' '}
        {their} response.
      </ListItem>,
      '2_white_2_a',
    ),
    audioLine(
      <ListItem>
        Trying to start conversations with {childName} when {they} {appear} most open to conversation. While it may be
        hard to tell when {they} {are} open to talking, try to chat when {they} {seem} most responsive to you. For
        example, this could be in the car, at the shops, or before bed.
      </ListItem>,
      '2_white_3_a',
    ),
    audioLine(
      <ListItem>
        Showing respect for {childName}’s opinions, even if you don’t agree with {them}.
      </ListItem>,
      '2_white_4_b',
    ),
    audioLine(
      <ListItem>
        Letting {childName} know that you are there for {them} whenever {they} {need} it and that {they} can talk to you
        about anything, even difficult issues.
      </ListItem>,
      '2_white_5_a',
    ),
    audioLine(
      <ListItem>
        Choosing somewhere private to talk with {childName} about sensitive issues. Having other people around may make
        it difficult for {them} to be honest with you.
      </ListItem>,
      '2_white_6_b',
    ),
    audioLine(
      <ListItem>
        Showing your concern when {childName} is feeling a strong emotion by asking about {their} feelings (e.g. “You
        look worried, is there something on your mind?”). Encourage {them} to talk with you about {their} emotions, and
        take the time to listen.
      </ListItem>,
      '2_white_7_a',
    ),
    audioLine(
      <ListItem>
        Not telling {childName} to toughen up when {they} {are} upset. Responding in this way can lead {childName} to
        believe that {their} emotions are wrong and {they} {are} bad for having them. Instead, let {them} know that you
        are concerned and offer to chat with {them} about what’s going on.
      </ListItem>,
      '2_white_8_b',
    ),
  ].filter((item, index) => scores[index] === 0);

  const audioFiles = [
    '2_heading',
    '2_body',
    listIntro?.audio,
    ...listItems.map((item) => item.audio),
    '2_end',
  ] as string[];

  const audios = audioFiles
    .filter((file) => file !== undefined)
    .map<[string, string]>((file) => [file, getPublicAudioUrl(file)]);

  return (
    <AudioSection audios={audios} {...others}>
      <Typography audioKey="2_heading" variant="h6" color="textSecondary" gutterBottom>
        Your Relationship with {childName}
      </Typography>

      <Typography audioKey="2_body" paragraph>
        Your care and support for {childName} helps to reduce {their} risk of depression and clinical anxiety.
      </Typography>

      {listIntro && listIntro.text}

      <ul>{listItems.map((item) => item.text)}</ul>

      <Typography audioKey="2_end" paragraph>
        For more strategies to help you build a strong relationship with {childName}, check out the{' '}
        <b><i>Connect</i></b>{' '}module.
      </Typography>
    </AudioSection>
  );
}
