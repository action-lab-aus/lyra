import React from 'react';
import sum from 'lodash/sum';
import { Link } from 'components';
import { calcOptionsScores, findByConditions, FreqScales, getPublicAudioUrl } from '../helpers';
import { AudioSection, AudioLine, ListItem, Typography, audioLine } from '../audio';
import { SectionProps } from '.';

export const calcScores = calcOptionsScores([
  ['S4#Q1', 'Yes'],
  ['S4#Q2', 'Yes'],
  ['S4#Q3', 'Yes'],
  ['S4#Q4', 'Yes'],
  ['S4#Q5', 'Yes, to adapt to my teenager’s maturity and responsibility.'],
  ['S4#Q6', FreqScales[0]],
  ['S4#Q7', FreqScales[3]],
  ['S4#Q8', FreqScales[0]],
  ['S4#Q9', [FreqScales[2], FreqScales[3]]],
]);

export const calcRecommendScores = calcOptionsScores([
  ['S4#Q1', 'Yes'],
  ['S4#Q2', 'Yes'],
]);

export function Section4(props: SectionProps) {
  const { userSurvey, vocabulary, ...others } = props;
  const { childName, their, they, are, dont, them } = vocabulary;
  const scores = calcScores(userSurvey);

  const listRule = findByConditions<Array<number>, AudioLine>([
    [
      (s) => s[0] === 1 && s[1] === 1,
      audioLine(
        <Typography paragraph>
          It is great that you have set specific, defined rules and consequences for {childName}’s behaviour, as this
          helps to reduce {their} risk of depression and clinical anxiety.
        </Typography>,
        '5_rules_2_a_1_a',
      ),
    ],
    [
      (s) => s[0] === 1 && s[1] === 0,
      audioLine(
        <Typography paragraph>
          It is great that you have set specific, defined rules for {childName}’s behaviour. It is also important to set
          clear consequences for when {they} {dont} follow these rules. Having clear rules <em>and</em> consequences
          helps to reduce {their} risk of depression and clinical anxiety.
        </Typography>,
        '5_rules_2_a_1_b',
      ),
    ],
    [
      (s) => s[0] === 0 && s[1] === 1,
      audioLine(
        <Typography paragraph>
          It is great that you have set specific, defined consequences for {childName}’s behaviour. It is also important
          to establish clear rules that are linked to these consequences. Having clear rules <em>and</em> consequences
          helps to reduce {their} risk of depression and clinical anxiety.
        </Typography>,
        '5_rules_2_a_1_b',
      ),
    ],
    [
      (s) => s[0] === 0 && s[1] === 0,
      audioLine(
        <Typography paragraph>
          Establishing clear rules and consequences for {childName}’s behaviour is important in reducing {their} risk of
          depression and clinical anxiety. If clear rules are established from an early age, {childName} is more likely
          to accept the rules than if they are established for the first time when {they} {are} older. You could help{' '}
          {childName} by establishing specific, defined rules for {their} behaviour, and consequences for when {they}{' '}
          {dont} follow the rules.
        </Typography>,
        '5_rules_2_b_1_b',
      ),
    ],
  ])(scores);

  const PositiveFamilyRulesIntro =
    sum(scores.slice(2, 9)) !== 0
      ? sum(scores.slice(0, 2)) !== 0
        ? audioLine(<Typography paragraph>It is also great that:</Typography>, '5_also_2_b_1_a')
        : audioLine(<Typography paragraph>It is great that:</Typography>, '5_also_2_b_1_b')
      : null;

  const FamilyRulesToImproveIntro =
    sum(scores.slice(2, 9)) !== 7
      ? audioLine(
          <Typography paragraph>You could improve the setting of family rules by doing the following:</Typography>,
          '5_white_start',
        )
      : null;

  const q3Item: AudioLine =
    userSurvey['S4#Q3'] === 'Yes'
      ? audioLine(
          <ListItem>
            {childName} is involved in developing the rules for {their} behaviour.
          </ListItem>,
          '5_green_3_a',
        )
      : audioLine(
          <ListItem>
            Involving {childName} in the development of rules for {their} behaviour.
          </ListItem>,
          '5_white_3_c',
        );

  const q4Item: AudioLine =
    userSurvey['S4#Q4'] === 'Yes'
      ? audioLine(
          <ListItem>
            You have talked with {childName} to help {them} understand the reasons behind the family rules (e.g. to
            ensure {their} safety).
          </ListItem>,
          '5_green_4_a',
        )
      : audioLine(
          <ListItem>
            Talking with {childName} to help {them} understand the reasons behind the family rules (e.g. to ensure{' '}
            {their} safety).
          </ListItem>,
          '5_white_4_b',
        );

  const q5Item: AudioLine | null = findByConditions<string, AudioLine>([
    [
      (v) => v === 'Yes, all the time.',
      audioLine(
        <ListItem>
          Keeping family rules stable and consistent, and only changing them to reflect {childName}’s maturity and
          responsibility.
        </ListItem>,
        '5_white_5_a',
      ),
    ],
    [
      (v) => v === 'Yes, to adapt to my teenager’s maturity and responsibility.',
      audioLine(
        <ListItem>You adapt the family rules to reflect {childName}’s maturity and responsibility.</ListItem>,
        '5_green_5_b',
      ),
    ],
    [
      (v) => v === 'No, family rules apply for as long as my teenager is under my care.',
      audioLine(
        <ListItem>
          Reviewing your family rules at regular intervals (e.g. once a year) and adapting them to reflect {childName}
          ’s maturity and responsibility.
        </ListItem>,
        '5_white_5_c',
      ),
    ],
  ])(userSurvey['S4#Q5']);

  const group1Items: AudioLine[] = [
    [
      audioLine(
        <ListItem>Upholding the family rules and consequences, even if {childName} isn’t happy about it.</ListItem>,
        '5_white_6_b',
      ),
      audioLine(
        <ListItem>You uphold the family rules and consequences, even if {childName} isn’t happy about it.</ListItem>,
        '5_green_6_a',
      ),
    ],
    [
      audioLine(
        <ListItem>
          When you enforce consequences, explain to {childName} why {their} behaviour wasn’t acceptable.
        </ListItem>,
        '5_white_7_a',
      ),
      audioLine(
        <ListItem>
          When you enforce consequences, you talk with {childName} about why {their} behaviour was not acceptable.
        </ListItem>,
        '5_green_7_d',
      ),
    ],
    [
      audioLine(<ListItem>Not using consequences that make {childName} feel embarrassed.</ListItem>, '5_white_8_b'),
      audioLine(<ListItem>You don’t use consequences that make {childName} feel embarrassed.</ListItem>, '5_green_8_a'),
    ],
    [
      audioLine(
        <ListItem>
          Noticing when {childName} behaves well, and rewarding {them} with positive consequences (e.g. praise or
          privileges).
        </ListItem>,
        '5_white_9_a',
      ),
      audioLine(
        <ListItem>
          You notice when {childName} behaves well and reward {them} with positive consequences (e.g. praise or
          privileges)
        </ListItem>,
        '5_green_9_c',
      ),
    ],
  ].map((items, index) => items[scores[index + 5] || 0]);

  const listItems = [q3Item, q4Item, q5Item!, ...group1Items];

  const recapItem: AudioLine | null =
    sum(scores.slice(2, 9)) < 6
      ? {
          text: (
            <Typography paragraph>
              {childName} is more likely to follow the family rules if you do these things.
            </Typography>
          ),
          audio: '5_white_end',
        }
      : null;

  const audioFiles = [
    '5_heading',
    listRule?.audio,
    PositiveFamilyRulesIntro?.audio,
    ...listItems.map((item, index) => (scores[index + 2] === 1 ? item?.audio : null)),
    FamilyRulesToImproveIntro?.audio,
    ...listItems.map((item, index) => (scores[index + 2] === 0 ? item?.audio : null)),
    recapItem?.audio,
    '5_end',
  ] as string[];

  const audios = audioFiles
    .filter((file) => file !== undefined)
    .map<[string, string]>((file) => [file, getPublicAudioUrl(file)]);

  return (
    <AudioSection audios={audios} {...others}>
      <Typography audioKey="5_heading" variant="h6" color="textSecondary" gutterBottom>
        Your Family Rules
      </Typography>

      {listRule?.text}
      {PositiveFamilyRulesIntro?.text}
      <ul>{listItems.map((item, index) => (scores[index + 2] === 1 ? item?.text : null))} </ul>
      {FamilyRulesToImproveIntro?.text}
      <ul>{listItems.map((item, index) => (scores[index + 2] === 0 ? item?.text : null))} </ul>

      {recapItem?.text}

      <Typography audioKey="5_end" paragraph>
        For more information about setting family rules, see the module{' '}
        <b>
          <i>Raising good kids into great adults</i>
        </b>
        .
      </Typography>
    </AudioSection>
  );
}
