import React from 'react';
import { Container, Box } from '@material-ui/core';
import { Meta, Story } from '@storybook/react';
import { UserSurvey } from 'app';
import { SectionProps } from './index';
import { Section7 } from './Section7';
import { FreqScales } from '../helpers';
import { userVocabulary } from 'components/helpers';
import { Article } from '../Article';

export default {
  title: 'components/feedback/sections/Section7',
  component: Section7,
} as Meta;

const vocabulary: Record<string, string> = userVocabulary({
  childName: 'Marry',
  childGender: 'female',
});

// ['S7#Q1', FreqScales[3]],
// ['S7#Q2', [FreqScales[2], FreqScales[3]]],
// ['S7#Q3', [FreqScales[2], FreqScales[3]]],
// ['S7#Q4', [FreqScales[0], FreqScales[1]]],
// ['S7#Q5', [FreqScales[2], FreqScales[3]]],
// ['S7#Q6', FreqScales[3]],
// ['S7#Q7', [FreqScales[0], FreqScales[1]]],
// ['S7#Q8', [FreqScales[2], FreqScales[3]]],
// ['S7#Q9', FreqScales[0]],
// ['S7#Q10', FreqScales[3]],
const bestSurvey: UserSurvey = {
  _step: 0,
  _total: 0,
  _completed: 0,
  'S7#Q1': FreqScales[3],
  'S7#Q2': FreqScales[3],
  'S7#Q3': FreqScales[3],
  'S7#Q4': FreqScales[0],
  'S7#Q5': FreqScales[3],
  'S7#Q6': FreqScales[3],
  'S7#Q7': FreqScales[0],
  'S7#Q8': FreqScales[3],
  'S7#Q9': FreqScales[0],
  'S7#Q10': FreqScales[3],
};

const Template: Story<SectionProps> = (args) => {
  const [active, setActive] = React.useState(false);
  return (
    <Container>
      <Box my={2}>
        <Article>
          <Section7 {...args} active={active} onActive={setActive} />
        </Article>
      </Box>
    </Container>
  );
};

export const Low = Template.bind({});
Low.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S7#Q1': FreqScales[0],
    'S7#Q2': FreqScales[0],
    'S7#Q3': FreqScales[0],
    'S7#Q4': FreqScales[3],
    'S7#Q5': FreqScales[0],
    'S7#Q6': FreqScales[0],
    'S7#Q7': FreqScales[3],
    'S7#Q8': FreqScales[0],
    'S7#Q9': FreqScales[3],
    'S7#Q10': FreqScales[0],
  },
};

export const Medium = Template.bind({});
Medium.args = {
  vocabulary,
  userSurvey: {
    ...bestSurvey,
    'S7#Q1': FreqScales[0],
    'S7#Q2': FreqScales[0],
    'S7#Q3': FreqScales[0],
    'S7#Q5': FreqScales[0],
    'S7#Q6': FreqScales[0],
  },
};

export const High = Template.bind({});
High.args = {
  vocabulary,
  userSurvey: { ...bestSurvey, 'S7#Q1': FreqScales[0] },
};

export const Best = Template.bind({});
Best.args = {
  vocabulary,
  userSurvey: bestSurvey,
};
