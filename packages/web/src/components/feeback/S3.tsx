import React from 'react';
import range from 'lodash/range';
import { UserProfile, UserSurvey } from 'app';
import { Link, Typography } from '@material-ui/core';
import { userVocabulary } from 'components/helpers';
import { calcScaleScore, findByRanges, OccasionScales } from './helpers';

export type S3Props = {
  user: UserProfile;
  userSurvey: UserSurvey;
};

export const calcScore = calcScaleScore(
  range(1, 7).map((i) => `s1#q${i}`),
  {
    [OccasionScales[0]]: 0,
    [OccasionScales[1]]: 1,
    [OccasionScales[2]]: 2,
    [OccasionScales[3]]: 3,
    [OccasionScales[4]]: 4,
  },
);

export default function S3(props: S3Props) {
  const { user, userSurvey } = props;
  const { childName } = React.useMemo(() => userVocabulary(user), [user]);
  const score = calcScore(userSurvey);

  return (
    <article>
      <Typography variant="h5" color="primary" gutterBottom>
        Your mental wellbeing
      </Typography>
      {findByRanges([
        [
          [0, 5],
          <Typography paragraph>
            Thanks for letting us know how you’ve been feeling lately. It’s great to see from your responses that you
            seem to be managing quite well emotionally. If this changes, or you feel you need extra support, we
            encourage you to reach out to a trusted friend, family member, or health professional. Check out the{' '}
            <Link href="/resources">Useful Resources</Link>{' '}page at any time for more information.
          </Typography>,
        ],
        [
          [5, 13],
          <Typography paragraph>
            Thanks for letting us know how you’ve been feeling lately. Based on your answers, it looks like you’ve been
            experiencing some distress. We encourage you to seek extra support from a trusted friend or family member,
            or a health professional. After you complete this survey, we’ll send you an email with some suggestions of
            where you can seek professional support. You can also check out the <Link href="/resources">Useful Resources</Link>{' '}page on our website.
            Remember, looking after your own mental wellbeing is important not only for you, but also for {childName}.
          </Typography>,
        ],
        [
          [13, Number.MAX_VALUE],
          <Typography paragraph>
            Thank you for letting us know how you’ve been going lately. We’re sorry to see that you’ve been feeling
            quite distressed. If you haven’t already, we encourage you to seek additional support from a health
            professional. After you complete this survey, we’ll send you an email with suggestions of how you can do
            this. You can also check out the <Link href="/resources">Useful Resources</Link>{' '}page on our website. Remember, looking after your own mental
            wellbeing is important not only for you, but also for {childName}.
          </Typography>,
        ],
      ])(score)}
    </article>
  );
}
