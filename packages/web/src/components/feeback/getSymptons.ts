import { UserProfile, UserSurvey } from 'app';
import { calcScore as s3k6Score } from './S3';
import { calcAnxietyScore, calcDepressionScore } from './S4';

export type SurveySymptons = {
  k6?: 'moderate' | 'high';
  rcads?: 'depression' | 'anxiety' | 'depression and anxiety';
};

export function getSymptons(userProfile: UserProfile, k6Result: UserSurvey, rcadsResult: UserSurvey): SurveySymptons {
  const { childGender = 'male', childGrade = 9 } = userProfile;
  const k6Score = s3k6Score(k6Result);
  const anxietyScore = calcAnxietyScore(rcadsResult, childGender, childGrade);
  const depressionScore = calcDepressionScore(rcadsResult, childGender, childGrade);

  const k6 = () => {
    if (k6Score >= 13) {
      return 'high';
    }
    if (k6Score > 5) {
      return 'moderate';
    }
  };

  const rcads = () => {
    if (anxietyScore > 70 && depressionScore > 70) {
      return 'depression and anxiety';
    }

    if (anxietyScore > 70) {
      return 'anxiety';
    }

    if (depressionScore > 70) {
      return 'depression';
    }
  };

  return {
    k6: k6(),
    rcads: rcads(),
  };
}
