import { UserSurvey } from 'app';
import firebase from 'firebase/app';

type Range<T> = [number | [number, number], T];

type Condition<C, T> = [(data: C) => boolean, T];

export const SimpleFreqScales = ['Never', 'Sometimes', 'Often', 'Always'];
export const FreqScales = ['Never', 'Rarely', 'Sometimes', 'Often', 'Not applicable, I don’t have a partner.'];
export const LikelyScales = ['Very unlikely', 'Unlikely', 'Likely', 'Very likely'];
export const ConfidentScales = ['Not at all confident', 'A little confident', 'Somewhat confident', 'Very confident'];
export const OccasionScales = [
  'None of the time',
  'A little of the time',
  'Some of the time',
  'Most of the time',
  'All of the time',
];
export const AgreeScales = ['Strongly disagree', 'Disagree', 'Neutral', 'Agree', 'Strongly agree'];

export function calcScaleScore(keys: string[], scaleScores: Record<string, number>) {
  return function (userSurvey: Record<string, any>): number {
    return keys.reduce((score, key) => {
      const result = userSurvey[key];
      score += scaleScores[result] || 0;
      return score;
    }, 0);
  };
}

export function calcOptionsScores(scoredOptions: Array<[string, string | string[]]>) {
  return function (userSurvey: Record<string, any>): Array<0 | 1> {
    return scoredOptions.map(([key, scored]) => {
      const value = userSurvey[key];
      const included = Array.isArray(scored) ? scored.indexOf(value) >= 0 : value === scored;
      return included ? 1 : 0;
    });
  };
}

export function findByRanges<T>(ranges: Array<Range<T>>) {
  return function (score: number): T | null {
    const result = ranges.find(([range]) => {
      return typeof range === 'number' ? score === range : score >= range[0] && score < range[1];
    });
    return result ? result[1] : null;
  };
}

export function findByConditions<C, T>(conditions: Array<Condition<C, T>>) {
  return function (data: C): T | null {
    const result = conditions.find(([condition]) => {
      return condition(data);
    });
    return result ? result[1] : null;
  };
}

export function filterByConditions<C, T>(conditions: Array<Condition<C, T>>) {
  return function (data: C): Array<T> {
    return conditions.filter(([condition]) => condition(data)).map(([, item]) => item);
  };
}

export function getPublicAudioUrl(file: string) {
  // If streaming from Google cloud storage
  // const bucketName = 'partnersinparenting-6f42b.appspot.com';
  // const objectName = encodeURIComponent(`audio/clips/nonbinary_${file}.mp3`);
  // return `https://firebasestorage.googleapis.com/v0/b/${bucketName}/o/${objectName}?alt=media`;

  // If streaming from the CDN
  const objectName = encodeURIComponent(`clips/nonbinary_${file}.mp3`);
  return `https://d2hjx33ioxu6g5.cloudfront.net/${objectName}?alt=media`;
}
