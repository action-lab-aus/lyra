import React from 'react';
import { Story, Meta } from '@storybook/react';
import { FeedbackContent } from './FeedbackContent';
import { AppProvider } from 'components/helpers';
import { RootState } from 'app';
import * as mocks from 'components/helpers/mocks';

export default {
  title: 'components/feedback/FeedbackContent',
  component: FeedbackContent,
} as Meta;

const Template: Story<{ state: RootState }> = (args) => (
  <AppProvider {...args}>
    <FeedbackContent surveyIds={['s1-confidence', 's2-pradas', 's3-k6', 's4-rcads']} />
  </AppProvider>
);

export const WithAllFeedback = Template.bind({});
WithAllFeedback.args = {
  state: mocks
    .rootState()
    .authenticated()
    .profile({
      fetched: true,
      data: {
        surveys: {
          's1-confidence': mocks.templates.userSurveys.s1Confidence(),
          's2-pradas': mocks.templates.userSurveys.s2Pradas(),
          's3-k6': mocks.templates.userSurveys.s3K6(),
          's4-rcads': mocks.templates.userSurveys.s4Rcads(),
        },
        topics: {},
        user: mocks.templates.user(),
      },
    }).state,
};

export const WithSomeFeedback = Template.bind({});
WithSomeFeedback.args = {
  state: mocks
    .rootState()
    .authenticated()
    .profile({
      fetched: true,
      data: {
        surveys: {
          's1-confidence': mocks.templates.userSurveys.s1Confidence(),
          's2-pradas': mocks.templates.userSurveys.s2Pradas(),
        },
        topics: {},
        user: mocks.templates.user(),
      },
    }).state,
};

export const NoFeedback = Template.bind({});
NoFeedback.args = {
  state: mocks
    .rootState()
    .authenticated()
    .profile({
      fetched: true,
      data: {
        surveys: {},
        topics: {},
        user: mocks.templates.user(),
      },
    }).state,
};
