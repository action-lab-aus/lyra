import { calcAnxietyScore, calcDepressionScore } from '../S4';
import { SimpleFreqScales } from '../helpers';
import range from 'lodash/range';

const surveyTemplateBothElevated = range(1, 26).reduce((template, i) => {
  return { ...template, [`s1#q${i}`]: SimpleFreqScales[3] };
}, {});

const surveyTemplateNeitherElevated = range(1, 26).reduce((template, i) => {
  return { ...template, [`s1#q${i}`]: SimpleFreqScales[0] };
}, {});

const surveyTemplateDepressionOnlyElevated = [1, 4, 8, 10, 13, 15, 16, 19, 21, 24].reduce((template, i) => {
  return { ...template, [`s1#q${i}`]: SimpleFreqScales[3] };
}, surveyTemplateNeitherElevated);

const surveyTemplateAnxietyOnlyElevated = [2, 3, 5, 6, 7, 9, 11, 12, 14, 17, 18, 20, 22, 23, 25].reduce(
  (template, i) => {
    return { ...template, [`s1#q${i}`]: SimpleFreqScales[3] };
  },
  surveyTemplateNeitherElevated,
);

describe('Calc t-score for survey 4 RCADS Anxiety and Depression', () => {
  describe('Calc t-score for female', () => {
    test('Can calc t-score for female both elevated', () => {
      // anxiety_total_max == 45
      expect(calcAnxietyScore(surveyTemplateBothElevated, 'female', 5)).toBeCloseTo(((45 - 7.29) * 10) / 5.25 + 50);
      expect(calcAnxietyScore(surveyTemplateBothElevated, 'female', 7)).toBeCloseTo(((45 - 5.8) * 10) / 3.91 + 50);
      expect(calcAnxietyScore(surveyTemplateBothElevated, 'female', 9)).toBeCloseTo(((45 - 5.94) * 10) / 5.27 + 50);
      expect(calcAnxietyScore(surveyTemplateBothElevated, 'female', 11)).toBeCloseTo(((45 - 5.76) * 10) / 3.97 + 50);

      // depression_total_max == 30
      expect(calcDepressionScore(surveyTemplateBothElevated, 'female', 5)).toBeCloseTo(((30 - 3.75) * 10) / 3.63 + 50);
      expect(calcDepressionScore(surveyTemplateBothElevated, 'female', 7)).toBeCloseTo(((30 - 3.6) * 10) / 3.37 + 50);
      expect(calcDepressionScore(surveyTemplateBothElevated, 'female', 9)).toBeCloseTo(((30 - 3.97) * 10) / 3.25 + 50);
      expect(calcDepressionScore(surveyTemplateBothElevated, 'female', 11)).toBeCloseTo(((30 - 4.91) * 10) / 3.17 + 50);
    });

    test('Can calc t-score  female , fanxiety only elevated', () => {
      // anxiety_total_max == 45
      expect(calcAnxietyScore(surveyTemplateAnxietyOnlyElevated, 'female', 5)).toBeCloseTo(
        ((45 - 7.29) * 10) / 5.25 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateAnxietyOnlyElevated, 'female', 7)).toBeCloseTo(
        ((45 - 5.8) * 10) / 3.91 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateAnxietyOnlyElevated, 'female', 9)).toBeCloseTo(
        ((45 - 5.94) * 10) / 5.27 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateAnxietyOnlyElevated, 'female', 11)).toBeCloseTo(
        ((45 - 5.76) * 10) / 3.97 + 50,
      );

      // depression_total_min == 0
      expect(calcDepressionScore(surveyTemplateAnxietyOnlyElevated, 'female', 5)).toBeCloseTo(
        ((0 - 3.75) * 10) / 3.63 + 50,
      );
      expect(calcDepressionScore(surveyTemplateAnxietyOnlyElevated, 'female', 7)).toBeCloseTo(
        ((0 - 3.6) * 10) / 3.37 + 50,
      );
      expect(calcDepressionScore(surveyTemplateAnxietyOnlyElevated, 'female', 9)).toBeCloseTo(
        ((0 - 3.97) * 10) / 3.25 + 50,
      );
      expect(calcDepressionScore(surveyTemplateAnxietyOnlyElevated, 'female', 11)).toBeCloseTo(
        ((0 - 4.91) * 10) / 3.17 + 50,
      );
    });

    test('Can calc t-score for female depression only elevated', () => {
      // anxiety_total_min == 0
      expect(calcAnxietyScore(surveyTemplateDepressionOnlyElevated, 'female', 5)).toBeCloseTo(
        ((0 - 7.29) * 10) / 5.25 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateDepressionOnlyElevated, 'female', 7)).toBeCloseTo(
        ((0 - 5.8) * 10) / 3.91 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateDepressionOnlyElevated, 'female', 9)).toBeCloseTo(
        ((0 - 5.94) * 10) / 5.27 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateDepressionOnlyElevated, 'female', 11)).toBeCloseTo(
        ((0 - 5.76) * 10) / 3.97 + 50,
      );

      // depression_total_max == 30
      expect(calcDepressionScore(surveyTemplateDepressionOnlyElevated, 'female', 5)).toBeCloseTo(
        ((30 - 3.75) * 10) / 3.63 + 50,
      );
      expect(calcDepressionScore(surveyTemplateDepressionOnlyElevated, 'female', 7)).toBeCloseTo(
        ((30 - 3.6) * 10) / 3.37 + 50,
      );
      expect(calcDepressionScore(surveyTemplateDepressionOnlyElevated, 'female', 9)).toBeCloseTo(
        ((30 - 3.97) * 10) / 3.25 + 50,
      );
      expect(calcDepressionScore(surveyTemplateDepressionOnlyElevated, 'female', 11)).toBeCloseTo(
        ((30 - 4.91) * 10) / 3.17 + 50,
      );
    });

    test('Can calc t-score for female neither elevated', () => {
      // anxiety_total_min == 0
      expect(calcAnxietyScore(surveyTemplateNeitherElevated, 'female', 5)).toBeCloseTo(((0 - 7.29) * 10) / 5.25 + 50);
      expect(calcAnxietyScore(surveyTemplateNeitherElevated, 'female', 7)).toBeCloseTo(((0 - 5.8) * 10) / 3.91 + 50);
      expect(calcAnxietyScore(surveyTemplateNeitherElevated, 'female', 9)).toBeCloseTo(((0 - 5.94) * 10) / 5.27 + 50);
      expect(calcAnxietyScore(surveyTemplateNeitherElevated, 'female', 11)).toBeCloseTo(((0 - 5.76) * 10) / 3.97 + 50);

      // depression_total_min == 0
      expect(calcDepressionScore(surveyTemplateNeitherElevated, 'female', 5)).toBeCloseTo(
        ((0 - 3.75) * 10) / 3.63 + 50,
      );
      expect(calcDepressionScore(surveyTemplateNeitherElevated, 'female', 7)).toBeCloseTo(((0 - 3.6) * 10) / 3.37 + 50);
      expect(calcDepressionScore(surveyTemplateNeitherElevated, 'female', 9)).toBeCloseTo(
        ((0 - 3.97) * 10) / 3.25 + 50,
      );
      expect(calcDepressionScore(surveyTemplateNeitherElevated, 'female', 11)).toBeCloseTo(
        ((0 - 4.91) * 10) / 3.17 + 50,
      );
    });
  });

  describe('Calc t-score for otehr (male or non-binary)', () => {
    test('Can calc t-score for other both elevated', () => {
      // anxiety_total_max == 45
      expect(calcAnxietyScore(surveyTemplateBothElevated, 'male', 5)).toBeCloseTo(((45 - 6.1) * 10) / 4.15 + 50);
      expect(calcAnxietyScore(surveyTemplateBothElevated, 'male', 7)).toBeCloseTo(((45 - 5.27) * 10) / 3.95 + 50);
      expect(calcAnxietyScore(surveyTemplateBothElevated, 'male', 9)).toBeCloseTo(((45 - 6.23) * 10) / 4.56 + 50);
      expect(calcAnxietyScore(surveyTemplateBothElevated, 'male', 11)).toBeCloseTo(((45 - 4.66) * 10) / 3.58 + 50);

      // depression_total_max == 30
      expect(calcDepressionScore(surveyTemplateBothElevated, 'male', 5)).toBeCloseTo(((30 - 3.62) * 10) / 2.87 + 50);
      expect(calcDepressionScore(surveyTemplateBothElevated, 'male', 7)).toBeCloseTo(((30 - 3.54) * 10) / 3.18 + 50);
      expect(calcDepressionScore(surveyTemplateBothElevated, 'male', 9)).toBeCloseTo(((30 - 5.21) * 10) / 3.51 + 50);
      expect(calcDepressionScore(surveyTemplateBothElevated, 'male', 11)).toBeCloseTo(((30 - 3.94) * 10) / 3.88 + 50);
    });

    test('Can calc t-score for other anxiety only elevated', () => {
      // anxiety_total_max == 45
      expect(calcAnxietyScore(surveyTemplateAnxietyOnlyElevated, 'male', 5)).toBeCloseTo(((45 - 6.1) * 10) / 4.15 + 50);
      expect(calcAnxietyScore(surveyTemplateAnxietyOnlyElevated, 'male', 7)).toBeCloseTo(
        ((45 - 5.27) * 10) / 3.95 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateAnxietyOnlyElevated, 'male', 9)).toBeCloseTo(
        ((45 - 6.23) * 10) / 4.56 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateAnxietyOnlyElevated, 'male', 11)).toBeCloseTo(
        ((45 - 4.66) * 10) / 3.58 + 50,
      );

      // depression_total_min == 0
      expect(calcDepressionScore(surveyTemplateAnxietyOnlyElevated, 'male', 5)).toBeCloseTo(
        ((0 - 3.62) * 10) / 2.87 + 50,
      );
      expect(calcDepressionScore(surveyTemplateAnxietyOnlyElevated, 'male', 7)).toBeCloseTo(
        ((0 - 3.54) * 10) / 3.18 + 50,
      );
      expect(calcDepressionScore(surveyTemplateAnxietyOnlyElevated, 'male', 9)).toBeCloseTo(
        ((0 - 5.21) * 10) / 3.51 + 50,
      );
      expect(calcDepressionScore(surveyTemplateAnxietyOnlyElevated, 'male', 11)).toBeCloseTo(
        ((0 - 3.94) * 10) / 3.88 + 50,
      );
    });

    test('Can calc t-score for other depression only elevated', () => {
      // anxiety_total_min == 0
      expect(calcAnxietyScore(surveyTemplateDepressionOnlyElevated, 'male', 5)).toBeCloseTo(
        ((0 - 6.1) * 10) / 4.15 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateDepressionOnlyElevated, 'male', 7)).toBeCloseTo(
        ((0 - 5.27) * 10) / 3.95 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateDepressionOnlyElevated, 'male', 9)).toBeCloseTo(
        ((0 - 6.23) * 10) / 4.56 + 50,
      );
      expect(calcAnxietyScore(surveyTemplateDepressionOnlyElevated, 'male', 11)).toBeCloseTo(
        ((0 - 4.66) * 10) / 3.58 + 50,
      );

      // depression_total_max == 30
      expect(calcDepressionScore(surveyTemplateDepressionOnlyElevated, 'male', 5)).toBeCloseTo(
        ((30 - 3.62) * 10) / 2.87 + 50,
      );
      expect(calcDepressionScore(surveyTemplateDepressionOnlyElevated, 'male', 7)).toBeCloseTo(
        ((30 - 3.54) * 10) / 3.18 + 50,
      );
      expect(calcDepressionScore(surveyTemplateDepressionOnlyElevated, 'male', 9)).toBeCloseTo(
        ((30 - 5.21) * 10) / 3.51 + 50,
      );
      expect(calcDepressionScore(surveyTemplateDepressionOnlyElevated, 'male', 11)).toBeCloseTo(
        ((30 - 3.94) * 10) / 3.88 + 50,
      );
    });

    test('Can calc t-score for other neither elevated', () => {
      // anxiety_total_min == 0
      expect(calcAnxietyScore(surveyTemplateNeitherElevated, 'male', 5)).toBeCloseTo(((0 - 6.1) * 10) / 4.15 + 50);
      expect(calcAnxietyScore(surveyTemplateNeitherElevated, 'male', 7)).toBeCloseTo(((0 - 5.27) * 10) / 3.95 + 50);
      expect(calcAnxietyScore(surveyTemplateNeitherElevated, 'male', 9)).toBeCloseTo(((0 - 6.23) * 10) / 4.56 + 50);
      expect(calcAnxietyScore(surveyTemplateNeitherElevated, 'male', 11)).toBeCloseTo(((0 - 4.66) * 10) / 3.58 + 50);

      // depression_total_min == 0
      expect(calcDepressionScore(surveyTemplateNeitherElevated, 'male', 5)).toBeCloseTo(((0 - 3.62) * 10) / 2.87 + 50);
      expect(calcDepressionScore(surveyTemplateNeitherElevated, 'male', 7)).toBeCloseTo(((0 - 3.54) * 10) / 3.18 + 50);
      expect(calcDepressionScore(surveyTemplateNeitherElevated, 'male', 9)).toBeCloseTo(((0 - 5.21) * 10) / 3.51 + 50);
      expect(calcDepressionScore(surveyTemplateNeitherElevated, 'male', 11)).toBeCloseTo(((0 - 3.94) * 10) / 3.88 + 50);
    });
  });

  describe('Calc anxiety score elevated', () => {
    let userSurvey = { ...surveyTemplateBothElevated, 's1#q2': SimpleFreqScales[2] };

    test('calculate anxiety score for female', () => {
      expect(Math.round(calcAnxietyScore(userSurvey, 'female', 5))).toBeCloseTo(120);
      expect(Math.round(calcAnxietyScore(userSurvey, 'female', 7))).toBeCloseTo(148);
      expect(Math.round(calcAnxietyScore(userSurvey, 'female', 9))).toBeCloseTo(122);
    });

    test('calculate anxiety score for others', () => {
      expect(Math.round(calcAnxietyScore(userSurvey, 'male', 5))).toBeCloseTo(141);
      expect(Math.round(calcAnxietyScore(userSurvey, 'male', 7))).toBeCloseTo(148);
      expect(Math.round(calcAnxietyScore(userSurvey, 'male', 9))).toBeCloseTo(133);
    });
  });

  describe('Calc anxiety score not elevated', () => {
    let userSurvey = { ...surveyTemplateNeitherElevated, 's1#q2': SimpleFreqScales[2] };

    test('calculate anxiety score for female', () => {
      expect(Math.round(calcAnxietyScore(userSurvey, 'female', 5))).toBeCloseTo(40);
      expect(Math.round(calcAnxietyScore(userSurvey, 'female', 7))).toBeCloseTo(40);
      expect(Math.round(calcAnxietyScore(userSurvey, 'female', 9))).toBeCloseTo(43);
    });

    test('calculate anxiety score for others', () => {
      expect(Math.round(calcAnxietyScore(userSurvey, 'male', 5))).toBeCloseTo(40);
      expect(Math.round(calcAnxietyScore(userSurvey, 'male', 7))).toBeCloseTo(42);
      expect(Math.round(calcAnxietyScore(userSurvey, 'male', 9))).toBeCloseTo(41);
    });

    // test('Can calc score for others', () => {
    //   const survey = {};
    //   const calcAnxietyScore = calcScore(survey);
    // });
  });

  // test('Can calc score for others', () => {
  //   const survey = {};
  //   const calcAnxietyScore = calcScore(survey);
  // });
  describe('Calc depression score elevated', () => {
    let userSurvey = { ...surveyTemplateBothElevated, 's1#q2': SimpleFreqScales[2] };

    xtest('calculate depression score for female', () => {
      expect(Math.round(calcDepressionScore(userSurvey, 'female', 5))).toBeCloseTo(120);
      expect(Math.round(calcDepressionScore(userSurvey, 'female', 7))).toBeCloseTo(125);
      expect(Math.round(calcDepressionScore(userSurvey, 'female', 9))).toBeCloseTo(127);
    });

    xtest('calculate calc depression score for others', () => {
      expect(Math.round(calcDepressionScore(userSurvey, 'male', 5))).toBeCloseTo(138);
      expect(Math.round(calcDepressionScore(userSurvey, 'male', 7))).toBeCloseTo(130);
      expect(Math.round(calcDepressionScore(userSurvey, 'male', 9))).toBeCloseTo(118);
    });
  });

  describe('Calc depression score not elevated', () => {
    let userSurvey = { ...surveyTemplateNeitherElevated, 's1#q1': SimpleFreqScales[2] };

    test('calculate depression score for female', () => {
      expect(Math.round(calcDepressionScore(userSurvey, 'female', 5))).toBeCloseTo(45);
      expect(Math.round(calcDepressionScore(userSurvey, 'female', 7))).toBeCloseTo(45);
      expect(Math.round(calcDepressionScore(userSurvey, 'female', 9))).toBeCloseTo(44);
    });

    test('calculate calc depression score for others', () => {
      expect(Math.round(calcDepressionScore(userSurvey, 'male', 5))).toBeCloseTo(44);
      expect(Math.round(calcDepressionScore(userSurvey, 'male', 7))).toBeCloseTo(45);
      expect(Math.round(calcDepressionScore(userSurvey, 'male', 9))).toBeCloseTo(41);
    });
  });
});
