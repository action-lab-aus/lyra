import { UserSurvey } from 'app';
import { findByRanges, findByConditions, calcOptionsScores } from '../helpers';

describe('calcOptionsScores', () => {
  const calcScores = calcOptionsScores([
    ['q1', 'a'],
    ['q2', 'c'],
    ['q3', ['a', 'c']],
  ]);

  test('it returns all scored answers', () => {
    expect(
      calcScores({
        q1: 'a',
        q2: 'c',
        q3: 'a',
      }),
    ).toEqual([1, 1, 1]);
  });

  test('it returns correct scored answers', () => {
    expect(
      calcScores({
        q1: 'a',
        q2: 'b',
        q3: 'c',
      }),
    ).toEqual([1, 0, 1]);
  });

  test('it returns scored answers for missing input', () => {
    expect(
      calcScores({
        q3: 'c',
      }),
    ).toEqual([0, 0, 1]);
  });
});

describe('findByRanges', () => {
  let findByScore = findByRanges([
    [1, 'first'],
    [2, 'second'],
    [[5, 7], 'five to seven'],
  ]);

  test('it can select first exact match', () => {
    expect(findByScore(2)).toEqual('second');
  });

  test('it can select first match by range', () => {
    expect(findByScore(6)).toEqual('five to seven');
  });

  test('it will return undefined when out of range', () => {
    expect(findByScore(0)).toBeNull();
    expect(findByScore(4)).toBeNull();
    expect(findByScore(7)).toBeNull();
  });
});

describe('findByCondition', () => {
  let findByUserSurvey = findByConditions<Partial<UserSurvey>, string>([
    [(s: Partial<UserSurvey>) => s.a === true, 'question A true'],
    [(s: Partial<UserSurvey>) => s.a === false, 'question A false'],
  ]);

  test('it can find first matched', () => {
    expect(findByUserSurvey({ a: true })).toEqual('question A true');
    expect(findByUserSurvey({ a: false })).toEqual('question A false');
    expect(findByUserSurvey({ a: true, b: true })).toEqual('question A true');
  });

  test('it will return undefined when nothing is matched', () => {
    expect(findByUserSurvey({ b: false })).toBeNull();
  });
});
