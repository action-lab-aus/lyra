import { UserSurvey } from 'app';
import sum from 'lodash/sum';
import { calcScores as s21 } from './sections/Section1';
import { calcScores as s22 } from './sections/Section2';
import { calcScores as s23 } from './sections/Section3';
import { calcScores as s24 } from './sections/Section4';
import { calcScores as s25 } from './sections/Section5';
import { calcScores as s26 } from './sections/Section6';
import { calcScores as s27 } from './sections/Section7';
import { calcScores as s28 } from './sections/Section8';
import { calcScores as s29 } from './sections/Section9';

export { calcScore as calcS1Score } from './S1';

export const s2PradasScores = (userSurvey: UserSurvey) =>
  [s21, s22, s23, s24, s25, s26, s27, s28, s29].map((calcScores) => sum(calcScores(userSurvey)));

export * from './Feedback';
export * from './getSymptons';
export * from './FeedbackContent';
