import { styled } from '@material-ui/core';

export const Article = styled('article')(({ theme }) => {
  return {
    '& li': {
      ...theme.typography.body1,
      marginBottom: theme.spacing(1),
    },
  };
});
