import React from 'react';
import { Box, Container } from '@material-ui/core';
import { Story, Meta } from '@storybook/react';
import S4, { S4Props } from './S4';
import { UserProfile, UserSurvey } from 'app';
import * as mocks from 'components/helpers/mocks';

export default {
  title: 'components/feedback/S4',
  component: S4,
} as Meta;

const Template: Story<S4Props> = (args) => {
  return (
    <Box mt={2}>
      <Container>
        <S4 {...args} />
      </Container>
    </Box>
  );
};

const userSurvey = mocks.templates.userSurveys.s4Rcads();

const userProfile: UserProfile = mocks.templates.user({
  childName: 'Bob',
  childGender: 'male',
  childGrade: 11,
});

export const NeitherElevated = Template.bind({});
NeitherElevated.args = {
  userSurvey: userSurvey,
  user: userProfile,
};

export const DepressionOnly = Template.bind({});
DepressionOnly.args = {
  userSurvey: {
    ...userSurvey,
    's1#q1': 'Always',
    's1#q4': 'Always',
    's1#q8': 'Always',
    's1#q10': 'Always',
    's1#q13': 'Always',
    's1#q15': 'Always',
    's1#q16': 'Always',
    's1#q19': 'Always',
    's1#q21': 'Always',
    's1#q24': 'Always',
  },
  user: userProfile,
};

export const AnxietyOnly = Template.bind({});
AnxietyOnly.args = {
  userSurvey: {
    ...userSurvey,
    's1#q2': 'Always',
    's1#q3': 'Always',
    's1#q5': 'Always',
    's1#q6': 'Always',
    's1#q7': 'Always',
    's1#q9': 'Always',
    's1#q11': 'Always',
    's1#q12': 'Always',
    's1#q14': 'Always',
    's1#q17': 'Always',
    's1#q18': 'Always',
    's1#q20': 'Always',
    's1#q22': 'Always',
    's1#q23': 'Always',
    's1#q25': 'Always',
  },
  user: userProfile,
};

export const BothElevated = Template.bind({});
BothElevated.args = {
  userSurvey: {
    ...userSurvey,
    's1#q1': 'Always',
    's1#q4': 'Always',
    's1#q8': 'Always',
    's1#q10': 'Always',
    's1#q13': 'Always',
    's1#q15': 'Always',
    's1#q16': 'Always',
    's1#q19': 'Always',
    's1#q21': 'Always',
    's1#q24': 'Always',
    's1#q2': 'Always',
    's1#q3': 'Always',
    's1#q5': 'Always',
    's1#q6': 'Always',
    's1#q7': 'Always',
    's1#q9': 'Always',
    's1#q11': 'Always',
    's1#q12': 'Always',
    's1#q14': 'Always',
    's1#q17': 'Always',
    's1#q18': 'Always',
    's1#q20': 'Always',
    's1#q22': 'Always',
    's1#q23': 'Always',
    's1#q25': 'Always',
  },
  user: userProfile,
};
