import { UserProfile, UserSurvey } from 'app';
import { Typography } from '@material-ui/core';
import React from 'react';
import range from 'lodash/range';
import { userVocabulary } from 'components/helpers';
import { Article } from './Article';
import { calcScaleScore, findByConditions, findByRanges, ConfidentScales } from './helpers';

export type S1Props = {
  user: UserProfile;
  userSurvey: UserSurvey;
};

export const calcScore = calcScaleScore(
  range(1, 12).map((i) => `s1#q${i}`),
  {
    [ConfidentScales[0]]: 1,
    [ConfidentScales[1]]: 2,
    [ConfidentScales[2]]: 3,
    [ConfidentScales[3]]: 4,
  },
);

export default function S1(props: S1Props) {
  const { user, userSurvey } = props;
  const { childName, topicName } = React.useMemo(() => userVocabulary(user), [user]);
  const score = calcScore(userSurvey);

  return (
    <Article>
      <Typography variant="h5" color="primary" gutterBottom>
        Your confidence in parenting during the pandemic{' '}
      </Typography>
      <Typography paragraph>
        The COVID-19 pandemic has brought with it many challenges that parents and families have never faced before.
      </Typography>
      {findByRanges([
        [
          [0, 33],
          findByConditions<UserSurvey, React.ReactNode>([
            [
              (s) => s['s1#q5'] !== 'Very confident' && s['s1#q8'] !== 'Very confident',
              <Typography paragraph>
                When confronted with new challenges, it’s understandable to feel unsure about what to do and how to
                cope. There seem to be some areas of parenting during the pandemic that you are not very confident
                about. The{' '}<i>Partners in Parenting</i>{' '}program includes the modules <i>{topicName}</i>, <i>Connect</i> and{' '}
                <i>Calm versus conflict</i> which are designed to improve parents’ confidence generally and during the
                pandemic – we encourage you to check them out as part of your personalised parenting program. Improving
                your parenting confidence may help to improve your own and {childName}’s mental wellbeing during the
                pandemic.
              </Typography>,
            ],
            [
              (s) => s['s1#q5'] !== 'Very confident' && s['s1#q8'] === 'Very confident',
              <Typography paragraph>
                When confronted with new challenges, it’s understandable to feel unsure about what to do and how to
                cope. There seem to be some areas of parenting during the pandemic that you are not very confident
                about. The{' '}<i>Partners in Parenting</i>{' '}program includes the modules <i>{topicName}</i> and <i>Connect</i>{' '}
                which are designed to improve parents’ confidence generally and during the pandemic – we encourage you
                to check them out as part of your personalised parenting program. Improving your parenting confidence
                may help to improve your own and {childName}’s mental wellbeing during the pandemic.
              </Typography>,
            ],
            [
              (s) => s['s1#q5'] === 'Very confident' && s['s1#q8'] !== 'Very confident',
              <Typography paragraph>
                When confronted with new challenges, it’s understandable to feel unsure about what to do and how to
                cope. There seem to be some areas of parenting during the pandemic that you are not very confident
                about. The{' '}<i>Partners in Parenting</i>{' '}program includes the modules <i>{topicName}</i> and{' '}
                <i>Calm versus conflict</i> which are designed to improve parents’ confidence generally and during the
                pandemic – we encourage you to check them out as part of your personalised parenting program. Improving
                your parenting confidence may help to improve your own and {childName}
                ’s mental wellbeing during the pandemic.
              </Typography>,
            ],
            [
              (s) => s['s1#q5'] === 'Very confident' && s['s1#q8'] === 'Very confident',
              <Typography paragraph>
                When confronted with new challenges, it’s understandable to feel unsure about what to do and how to
                cope. There seem to be some areas of parenting during the pandemic that you are not very confident
                about. The{' '}<i>Partners in Parenting</i>{' '}program includes the module <i>{topicName}</i> which is designed to
                improve parents’ confidence during the pandemic – we encourage you to check it out as part of your
                personalised parenting program. Improving your parenting confidence may help to improve your own and{' '}
                {childName}’s mental wellbeing during the pandemic.
              </Typography>,
            ],
          ])(userSurvey),
        ],

        [
          [33, 42],
          findByConditions<UserSurvey, React.ReactNode>([
            [
              (userSurvey) => userSurvey['s1#q5'] !== 'Very confident' && userSurvey['s1#q8'] !== 'Very confident',
              <Typography paragraph>
                When confronted with new challenges, it’s understandable to feel unsure about what to do and how to
                cope. It’s great that you are feeling confident in some areas of parenting during the pandemic. We know
                that parents who are confident are more likely to parent in ways that support their teenager’s mental
                health. The{' '}<i>Partners in Parenting</i>{' '}program includes the modules <i>{topicName}</i>, <i>Connect</i> and{' '}
                <i>Calm versus conflict</i> which are designed to improve parents’ confidence generally and during the
                pandemic – we encourage you to check them out as part of your personalised parenting program. Improving
                your confidence in these areas may help to improve your own and {childName}’s wellbeing during the
                pandemic.
              </Typography>,
            ],
            [
              (userSurvey) => userSurvey['s1#q5'] !== 'Very confident' && userSurvey['s1#q8'] === 'Very confident',
              <Typography paragraph>
                When confronted with new challenges, it’s understandable to feel unsure about what to do and how to
                cope. It’s great that you are feeling confident in some areas of parenting during the pandemic. We know
                that parents who are confident are more likely to parent in ways that support their teenager’s mental
                health. The{' '}<i>Partners in Parenting</i>{' '}program includes the modules <i>{topicName}</i> and <i>Connect</i>{' '}
                which are designed to improve parents’ confidence generally and during the pandemic – we encourage you
                to check them out as part of your personalised parenting program. Improving your confidence in these
                areas may help to improve your own and {childName}’s wellbeing during the pandemic.
              </Typography>,
            ],
            [
              (userSurvey) => userSurvey['s1#q5'] === 'Very confident' && userSurvey['s1#q8'] !== 'Very confident',
              <Typography paragraph>
                When confronted with new challenges, it’s understandable to feel unsure about what to do and how to
                cope. It’s great that you are feeling confident in some areas of parenting during the pandemic. We know
                that parents who are confident are more likely to parent in ways that support their teenager’s mental
                health. The{' '}<i>Partners in Parenting</i>{' '}program includes the modules <i>{topicName}</i> and{' '}
                <i>Calm versus Conflict</i> which are designed to improve parents’ confidence generally and during the
                pandemic – we encourage you to check them out as part of your personalised parenting program. Improving
                your confidence in these areas may help to improve your own and {childName}’s wellbeing during the
                pandemic.
              </Typography>,
            ],
            [
              (userSurvey) => userSurvey['s1#q5'] === 'Very confident' && userSurvey['s1#q8'] === 'Very confident',
              <Typography paragraph>
                When confronted with new challenges, it’s understandable to feel unsure about what to do and how to
                cope. It’s great that you are feeling confident in some areas of parenting during the pandemic. We know
                that parents who are confident are more likely to parent in ways that support their teenager’s mental
                health. The{' '}<i>Partners in Parenting</i>{' '}program includes the module <i>{topicName}</i> which is designed to
                improve parents’ confidence during the pandemic – we encourage you to check it out as part of your
                personalised parenting program. Improving your confidence in these areas may help to improve your own
                and {childName}
                ’s wellbeing during the pandemic.
              </Typography>,
            ],
          ])(userSurvey),
        ],

        [
          [42, Number.MAX_VALUE],
          <Typography paragraph>
            It’s great that you are feeling confident about parenting {childName} during the pandemic. We know that
            parents who are confident are more likely to parent in ways that support their teenager’s mental health. If
            you’d like to learn more about parenting during the pandemic, including practical tips, check out
            the <i>{topicName}</i> module.
          </Typography>,
        ],
      ])(score)}
    </Article>
  );
}
