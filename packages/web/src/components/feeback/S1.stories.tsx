import React from 'react';
import { Box, Container } from '@material-ui/core';
import { Story, Meta } from '@storybook/react';
import S1, { S1Props } from './S1';
import { UserProfile, UserSurvey } from 'app';
import { ConfidentScales } from './helpers';
import * as mocks from 'components/helpers/mocks';

export default {
  title: 'components/feedback/S1',
  component: S1,
} as Meta;

const Template: Story<S1Props> = (args) => {
  return (
    <Box mt={2}>
      <Container maxWidth="md">
        <S1 {...args} />
      </Container>
    </Box>
  );
};

const user: UserProfile = mocks.templates.user({
  childName: 'Marry',
  childGender: 'female',
  childGrade: 12,
});

const userSurvey: UserSurvey = mocks.templates.userSurveys.s1Confidence();

export const LowConfidence = Template.bind({});
LowConfidence.args = {
  user,
  userSurvey,
};

export const ModerateConfidence = Template.bind({});
ModerateConfidence.args = {
  user,
  userSurvey: {
    ...userSurvey,
    's1#q6': ConfidentScales[3],
    's1#q7': ConfidentScales[3],
    's1#q8': ConfidentScales[3],
    's1#q9': ConfidentScales[3],
    's1#q10': ConfidentScales[3],
    's1#q11': ConfidentScales[3],
  },
};

export const HighConfidence = Template.bind({});
HighConfidence.args = {
  user,
  userSurvey: {
    ...(ModerateConfidence.args.userSurvey as UserSurvey),
    's1#q1': ConfidentScales[3],
    's1#q2': ConfidentScales[3],
    's1#q3': ConfidentScales[3],
  },
};
