import React from 'react';
import { Box, Container } from '@material-ui/core';
import { Story, Meta } from '@storybook/react';
import S2, { S2Props } from './S2';
import * as mocks from 'components/helpers/mocks';

export default {
  title: 'components/feedback/S2',
  component: S2,
} as Meta;

const Template: Story<S2Props> = (args) => {
  return (
    <Box mt={2}>
      <Container maxWidth="md">
        <S2 {...args} />
      </Container>
    </Box>
  );
};

export const Example = Template.bind({});
Example.args = {
  user: mocks.templates.user(),
  userSurvey: mocks.templates.userSurveys.s2Pradas(),
};

export const Lean = Template.bind({});
Lean.args = {
  ...Example.args,
  lean: true,
};
