import React from 'react';
import { Story, Meta } from '@storybook/react';
import { LandingPage } from './LandingPage';
import { AppProvider, AppProviderProps } from 'components/helpers';
import { rootState } from 'components/helpers/mocks';

export default {
  title: 'components/landing/LandingPage',
  component: LandingPage,
} as Meta;

const Template: Story<AppProviderProps> = (args) => (
  <AppProvider {...args}>
    <LandingPage />
  </AppProvider>
);

export const Initializeing = Template.bind({});
Initializeing.args = {
  state: rootState().state,
};

export const NotAuthenticated = Template.bind({});
NotAuthenticated.args = {
  state: rootState().authenticated({ authenticated: false }).state,
};

export const Authenticated = Template.bind({});
Authenticated.args = {
  state: rootState().authenticated().state,
};
