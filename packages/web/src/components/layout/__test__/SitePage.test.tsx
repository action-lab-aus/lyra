import React from 'react';
import { render } from '@testing-library/react';
import { AppProvider } from 'components/helpers';
import SitePage from '../SitePage';

describe(SitePage, () => {
  test.only('it should be defined', () => {
    expect(SitePage).toBeDefined();
    render(
      <AppProvider>
        <SitePage>test</SitePage>
      </AppProvider>,
    );
  });
});
