import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Signup } from './Signup';
import { RootState } from 'app';
import { AppProvider } from 'components/helpers';
import { rootState } from 'components/helpers/mocks';

export default {
  title: 'components/auth/Signup',
  component: Signup,
} as Meta;

const Template: Story<{ state: RootState }> = (args) => {
  return (
    <AppProvider {...args}>
      <Signup />
    </AppProvider>
  );
};

export const Example = Template.bind({});
Example.args = {
  state: rootState().authenticated({
    authenticated: false,
  }).state,
};
