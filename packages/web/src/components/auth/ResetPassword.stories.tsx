import React from 'react';
import { Meta, Story } from '@storybook/react';
import { ResetPassword } from './ResetPassword';
import { AppProvider } from 'components/helpers';
import { rootState } from 'components/helpers/mocks';
import { RootState } from 'app';

export default {
  title: 'components/auth/ResetPassword',
  component: ResetPassword,
} as Meta;

const Template: Story<{ state: RootState }> = (args) => {
  return (
    <AppProvider {...args}>
      <ResetPassword />
    </AppProvider>
  );
};

export const Example = Template.bind({});
Example.args = {
  state: rootState().authenticated({
    authenticated: false,
  }).state,
};
