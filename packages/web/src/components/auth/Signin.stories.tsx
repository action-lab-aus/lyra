import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Signin } from './Signin';
import { RootState } from 'app';
import { AppProvider } from 'components/helpers';
import { rootState } from 'components/helpers/mocks';

export default {
  title: 'components/auth/Sigin',
  component: Signin,
} as Meta;

const Template: Story<{ state: RootState }> = (args) => {
  return (
    <AppProvider {...args}>
      <Signin />
    </AppProvider>
  );
};

export const Example = Template.bind({});
Example.args = {
  state: rootState().authenticated({
    authenticated: false,
  }).state,
};

export const WaitForSignin = Template.bind({});
WaitForSignin.args = {
  state: rootState().authenticated({
    authenticated: false,
    status: {
      type: 'request',
    },
  }).state,
};
