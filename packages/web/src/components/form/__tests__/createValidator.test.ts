import { createValidator, notEmpty, mustBeEmail, passwordPolicy } from '../createValidator';

test('it can create validate function', () => {
  const validate = createValidator({});
  expect(validate).toBeDefined();
});

test('it can validate data', () => {
  const validate = createValidator<{ name: string | null }>({
    name: notEmpty('Name can not be empty'),
  });

  expect(validate({ name: 'Test' })).toBeFalsy();
  expect(validate({ name: null })).toEqual({
    name: 'Name can not be empty',
  });
});

test('it can chain multiple validate function', () => {
  const validate = createValidator<{ email: string | null }>({
    email: [notEmpty('Email can not be empty'), mustBeEmail('Invalid email address')],
  });

  expect(validate({ email: 'user@example.com' })).toBeFalsy();
  expect(validate({ email: null })).toEqual({
    email: 'Email can not be empty',
  });
  expect(validate({ email: 'invalid email' })).toEqual({
    email: 'Invalid email address',
  });
});
