import moment from 'moment';
import isEmpty from 'lodash/isEmpty';
import isEmail from 'validator/lib/isEmail';
import isMobilePhone, { MobilePhoneLocale } from 'validator/lib/isMobilePhone';

type Config<T> = {
  [key in keyof T]?: ValidateFn<T> | ValidateFn<T>[];
};

export type Validator<T> = (data: Partial<T>) => ValidatorErrors | null;

export type ValidatorErrors = Record<string, string>;

export type ValidateFn<T extends {}> = (value: any, context: Partial<T>) => string | null;

export function createValidator<T extends Record<string, any>>(config: Config<T>) {
  return (data: Partial<T>): ValidatorErrors | null => {
    const errors = Object.keys(config).reduce<ValidatorErrors>((errors, name) => {
      const firstError = (validates: ValidateFn<T>[]): string | null => {
        let error: string | null = null;
        for (let validate of validates) {
          error = validate(data[name], data);
          if (error !== null) break;
        }
        return error;
      };

      const validate = config[name]!;
      const error = Array.isArray(validate) ? firstError(validate) : validate(data[name], data);
      return error === null ? errors : { ...errors, [name]: error! };
    }, {});
    return !isEmpty(errors) ? errors : null;
  };
}

export function notEmpty<T>(message?: string): ValidateFn<T> {
  return (value: any) => {
    const valid = typeof value === 'number' ? true : !isEmpty(value);
    return !valid ? message || 'Can not be empty!' : null;
  };
}

export function mustBeEmail<T>(message?: string): ValidateFn<T> {
  return (value: string) => {
    return isEmpty(value) || !isEmail(value) ? message || `Not a valid email address` : null;
  };
}

export function mustBeAge<T>(message?: string): ValidateFn<T> {
  return (value: string) => {
    const isNumber = isNumeric(value);
    return !isNumber || (isNumber && parseInt(value) < 18) ? message || `Not a valid age` : null;
  };
}

export function mustBePostcode<T>(message?: string): ValidateFn<T> {
  return (value: string) => {
    const isNumber = isNumeric(value);
    return !isNumber || value.length !== 4 ? message || `Not a valid postcode` : null;
  };
}

export function mustBeDateOfBirth<T>(message?: string): ValidateFn<T> {
  return (value: string) => {
    const dateObj = moment(value, 'DD/MM/YYYY', true);
    return !dateObj.isValid() || !dateObj.isBefore() ? message || `Not a valid date of birth` : null;
  };
}

export function passwordPolicy<T>(
  policy: RegExp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/,
  message?: string,
): ValidateFn<T> {
  return (value: string) => {
    return !policy.test(value)
      ? message ||
          'Password must has at least 6 letters, and it contains at least one number, one lower case letter and one upper case letter.'
      : null;
  };
}

export function mustMatch<T>(field: string, message?: string): ValidateFn<T> {
  return (value: string, context: Partial<T>) => {
    return value && context[field] === value ? null : message || `Must match ${field} value`;
  };
}

export function mustBeMobilePhone<T>(locales: MobilePhoneLocale[], message?: string): ValidateFn<T> {
  return (value: string) => {
    return value && !isMobilePhone(value, locales)
      ? message || `${value} is not a valid Australian phone number`
      : null;
  };
}

export function mustBeAuNumber<T>(message?: string): ValidateFn<T> {
  const isAuPhone = (value: string) => {
    return (
      value.match(
        //       /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/,
        /(?:\+?61)?(?:\(0\)[23478]|\(?0?[23478]\)?)\d{8}/,
      ) !== null
    );
  };
  return (value: string) => {
    return value && !isAuPhone(value) ? message || `${value} is not a valid Australian phone number` : null;
  };
}

function isNumeric(value: string) {
  return /^-?\d+$/.test(value);
}
