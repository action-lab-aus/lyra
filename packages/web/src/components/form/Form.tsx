import React from 'react';
import { ValidatorErrors, Validator } from './createValidator';

type FormData = Record<string, any>;

export const FormContext = React.createContext<{
  data: FormData;
  errors: ValidatorErrors;
  update: (field: string, value: any) => void;
}>({
  data: {},
  errors: {},
  update: () => {},
});

export type FormProps<T> = Pick<React.HTMLProps<HTMLFormElement>, 'children' | 'id' | 'className' | 'style'> & {
  initial: Partial<T>;
  onSubmit: (data: T) => void;
  validate?: Validator<T>;
};

export function Form<T extends FormData>(props: FormProps<T>) {
  const { initial, onSubmit, validate, children, ...formProps } = props;
  const [data, setData] = React.useState<Partial<T>>(initial);
  const [errors, setErrors] = React.useState<ValidatorErrors>({});

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const errors = validate && validate(data);
    if (!errors) {
      onSubmit(data as T);
    }
    setErrors(errors || {});
  };

  const update = (field: keyof T, value: any) => {
    setData({ ...data, [field]: value });
  };

  return (
    <FormContext.Provider value={{ data, errors, update }}>
      <form {...formProps} onSubmit={handleSubmit}>
        {children}
      </form>
    </FormContext.Provider>
  );
}
