import React from 'react';
import { TextField as MuiTextField, TextFieldProps as MuiTextFieldProps } from '@material-ui/core';

import { FormContext } from './Form';

export type TextFieldProps = { name: string } & Omit<MuiTextFieldProps, 'value' | 'onChange' | 'error'>;

export function TextField(props: TextFieldProps) {
  const { name, helperText, ...textFieldProps } = props;
  const { data, update, errors } = React.useContext(FormContext);
  return (
    <MuiTextField
      {...textFieldProps}
      name={name}
      value={data[name] || ''}
      onChange={(e) => update(name, e.target.value)}
      error={Boolean(errors[name])}
      helperText={errors[name] || helperText}
    />
  );
}
