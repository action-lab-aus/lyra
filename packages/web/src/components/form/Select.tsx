import React from 'react';
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select as MuiSelect,
  SelectProps as MuiSelectProps,
  Typography,
} from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { FormContext } from './Form';

const useStyles = makeStyles((theme) =>
  createStyles<string, { error: boolean }>({
    helperText: ({ error }) => ({
      ...theme.typography.caption,
      color: error ? theme.palette.error.main : theme.palette.text.secondary,
      marginTop: 3,
      marginLeft: 14,
      marginRight: 14,
    }),
  }),
);

type SelectOption = { label: string; value: string };

export type SelectProps = {
  name: string;
  options: Array<string | SelectOption>;
  helperText?: string;
  variant?: 'outlined' | 'filled' | 'standard';
} & Omit<MuiSelectProps, 'value' | 'onChange' | 'error'>;

export function Select(props: SelectProps) {
  const { name, helperText, variant = 'standard', label, options: items, ...selectProps } = props;
  const { data, update, errors } = React.useContext(FormContext);
  const errorText = errors[name];
  const error = Boolean(errorText);
  const classes = useStyles({ error });

  const options = items.map<SelectOption>((item) => (typeof item === 'string' ? { value: item, label: item } : item));

  return (
    <FormControl variant={variant} error={error} fullWidth={selectProps.fullWidth}>
      {label && (
        <InputLabel id={name} required={selectProps.required}>
          {label}
        </InputLabel>
      )}
      <MuiSelect
        {...selectProps}
        name={name}
        value={data[name] || ''}
        onChange={(e) => update(name, e.target.value as string)}
        error={error}>
        {options.map((item) => (
          <MenuItem key={item.value} value={item.value}>
            {item.label}
          </MenuItem>
        ))}
      </MuiSelect>
      {helperText && <Typography className={classes.helperText}>{helperText}</Typography>}
    </FormControl>
  );
}
