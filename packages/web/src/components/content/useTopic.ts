import React from 'react';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';
import { useDispatch } from 'react-redux';
import { graphql, useStaticQuery, navigate } from 'gatsby';
import { Progress, useProfile, UserTopic } from 'app';
import { setLastVisited, setUserTopicEntryVisited, topicsCompleted, updateUserTopic } from 'app/profileSlice';
import { Topic, TopicEntry, TopicFulfillment } from './types';

type UseTopicResult = {
  topic: Topic;
  topicEntry: TopicEntry;
  userTopic: UserTopic;
  fulfillment: TopicFulfillment;
  topicProgress: Progress;
};
export function useTopic(pathname: string): UseTopicResult | null {
  const {
    allTopic: { nodes: topics },
  } = useStaticQuery<{
    allTopic: {
      nodes: Array<Topic>;
    };
  }>(graphql`
    query {
      allTopic {
        nodes {
          id
          title
          name
          path
          desc
          cover {
            childImageSharp {
              gatsbyImageData(width: 300, layout: CONSTRAINED)
            }
          }
          nav {
            sequence {
              title
              path
              key
              optional
              goals
              activityKeys
            }
          }
        }
      }
    }
  `);

  const topicName = pathname.split('/')[1];
  const dispatch = useDispatch();

  const topic = topics.find((topic) => topic.name === topicName);
  const topicEntry = topic?.nav.sequence.find((entry) => pathname.startsWith(entry.path));

  const profile = useProfile();
  const userTopic = topic && profile.data?.topics[topic.id];

  // calculate fulfillment
  const [fulfillment = null, topicProgress = null] = React.useMemo(() => {
    if (topic && userTopic) {
      const fulfillment = getFullfillment(topic, userTopic);
      const topicProgress = getFullfillmentProgress(fulfillment, {
        total: 0,
        completed: 0,
      });
      return [fulfillment, topicProgress];
    }
    return [];
  }, [topic, userTopic]);

  // redirect to dashboard if the topic is not unlocked
  React.useEffect(() => {
    if (profile.data && !userTopic) {
      navigate('/dashboard', { replace: true });
    }
  }, [profile.data, userTopic]);

  // topic or entry not found, set visited flag entry and update topic progress
  React.useEffect(() => {
    if (!topic || !topicEntry) {
      // topic or topicEntry didn't match path
      navigate('/404', { replace: true });
      return;
    }

    if (fulfillment && topicProgress) {
      if (fulfillment[topicEntry.key]?.completed === 0) {
        // set visited if the topicEntry has never been visited
        dispatch(setUserTopicEntryVisited(topic.id, topicEntry.key));
      }

      if (!isEqual(userTopic!.progress, topicProgress)) {
        // update progress if the topic progress is changed
        dispatch(updateUserTopic(topic.id, { progress: topicProgress }));
      }
    }
  }, [topic, topicEntry, fulfillment]);

  // set last visited effect
  React.useEffect(() => {
    if (topic && topicEntry) {
      dispatch(setLastVisited(topic.id, topicEntry.key));
    }
  }, [topic, topicEntry]);

  // set completed effect
  React.useEffect(() => {
    if (profile.data) {
      const {
        user: { topicCompletedAt },
        topics: userTopics,
      } = profile.data;

      if (!topicCompletedAt) {
        // calcuate total progress
        const progress = getTotalProgress(topics, userTopics);
        if (progress.total > 0 && progress.completed / progress.total === 1) {
          dispatch(topicsCompleted());
        }
      }
    }
  }, [topics, profile.data]);

  return topic && topicEntry && userTopic
    ? { topic, topicEntry, userTopic, fulfillment: fulfillment!, topicProgress: topicProgress! }
    : null;
}

function getFullfillment(topic: Topic, userTopic: UserTopic) {
  return topic.nav.sequence.reduce<TopicFulfillment>((fulfillment, topicEntry) => {
    const { key, activityKeys, optional, goals } = topicEntry;
    if (optional) {
      return fulfillment;
    }
    const entryData = userTopic.entries[key] || {};
    const keys = [...(activityKeys || []), '_visited'];
    let total = keys.length;
    let completed = keys.reduce((completed, key) => (entryData[key] === undefined ? completed : completed + 1), 0);
    if (goals) {
      total += 1;
      completed = completed + (isEmpty(userTopic.goals) ? 0 : 1);
    }

    return {
      ...fulfillment,
      [key]: {
        total,
        completed,
      },
    };
  }, {});
}

function getFullfillmentProgress(fulfillment: TopicFulfillment, topicProgress: Progress): Progress {
  return Object.entries(fulfillment).reduce<Progress>((progress, [entryKey, entryProgress]) => {
    return { total: progress.total + entryProgress.total, completed: progress.completed + entryProgress.completed };
  }, topicProgress);
}

function getTotalProgress(topics: Topic[], userTopics: Record<string, UserTopic>) {
  return topics.reduce<Progress>(
    (progress, topic) => {
      const userTopic = userTopics[topic.id];
      if (!userTopic?.mandatory) {
        return progress;
      }

      const topicTotal = topic.nav.sequence.reduce<number>((total, entry) => {
        if (entry.optional) {
          return total;
        } else {
          // visited + activities
          return total + (entry.activityKeys ? entry.activityKeys.length + 1 : 1);
        }
      }, 1);

      return {
        total: progress.total + topicTotal,
        completed: progress.completed + (userTopic?.progress?.completed || 0),
      };
    },
    {
      total: 0,
      completed: 0,
    },
  );
}
