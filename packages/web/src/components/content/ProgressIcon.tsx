import React from 'react';
import { SvgIcon } from '@material-ui/core';
import blueGrey from '@material-ui/core/colors/blueGrey';
import cyan from '@material-ui/core/colors/cyan';
import { RingShape } from 'components/RingShape';

export type ProgressIconProps = {
  percent: number;
};

export function ProgressIcon(props: ProgressIconProps) {
  const { percent } = props;

  return (
    <SvgIcon viewBox={'0 0 32 32'} style={{ width: 18, height: 18 }}>
      {percent === 1 ? (
        <circle cx="16" cy="16" r="15" stroke={cyan[400]} strokeWidth={2} fill="none" />
      ) : (
        <RingShape
          cx={16}
          cy={16}
          r={15}
          strokeWidth={2}
          percent={percent}
          color={blueGrey[300]}
          trackColor={blueGrey[100]}
        />
      )}
      <g transform="translate(4 4)" color={percent === 1 ? cyan[500] : blueGrey[100]}>
        <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z" />
      </g>
    </SvgIcon>
  );
}
