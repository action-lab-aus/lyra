import { Progress } from 'app';
import { FileNode } from 'gatsby-plugin-image/dist/src/components/hooks';

export type TopicEntry = {
  title: string;
  path: string;
  key: string;
  optional: boolean;
  goals: boolean;
  activityKeys: string[] | null;
};

export type Topic = {
  id: string;
  title: string;
  name: string;
  path: string;
  desc: string;
  cover: FileNode | null;
  nav: {
    sequence: TopicEntry[];
  };
};

export type TopicFulfillment = {
  [key: string]: Progress;
};

export type Goal = {
  id: string;
  title: string;
  content: React.ReactNode;
};
