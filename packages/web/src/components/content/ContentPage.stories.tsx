import React from 'react';
import { Typography } from '@material-ui/core';
import { Meta, Story } from '@storybook/react';
import * as mocks from 'components/helpers/mocks';
import { AppProvider, AppProviderProps, ThemeDecorator } from '../helpers';
import { ContentPage, ContentPageProps } from './ContentPage';

export default {
  title: 'components/content/ContentPage',
  comonent: ContentPage,
  decorators: [ThemeDecorator],
  argTypes: {
    path: {
      title: 'Path',
      type: 'string',
    },
  },
} as Meta;

const Template: Story<ContentPageProps & AppProviderProps> = (args) => {
  const { path, state, ...props } = args;
  return (
    <AppProvider path={path} state={state}>
      <ContentPage {...props} />
    </AppProvider>
  );
};

export const Default = Template.bind({});
Default.args = {
  path: '/m1-connect/00-home',
  state: mocks
    .rootState()
    .authenticated()
    .profile({
      fetched: true,
      data: {
        user: mocks.templates.user(),
        topics: mocks.templates.userTopics({
          m1: {
            entries: {
              '00-home': {
                _visited: { seconds: 0, nanoseconds: 0 },
              },
              '01-checkin': {
                _visited: { seconds: 0, nanoseconds: 0 },
              },
            },
          },
        }),
        surveys: {},
      },
    }).state,
  children: (
    <>
      <Typography variant="h3" gutterBottom>
        Heading
      </Typography>
      <Typography>Contents......</Typography>
    </>
  ),
};
