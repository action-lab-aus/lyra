import React from 'react';
import { navigate } from 'gatsby';
import { Box, ButtonBase, createStyles, makeStyles, Typography } from '@material-ui/core';
import { Topic } from './types';
import { formatTitle } from './components/utils';

const useStyles = makeStyles((theme) =>
  createStyles({
    button: {
      display: 'flex',
      flexDirection: 'column',
      padding: theme.spacing(1),
      borderRadius: theme.shape.borderRadius,
      color: theme.palette.primary.main,
    },
    disabled: {
      color: theme.palette.text.disabled,
    },
    caption: {
      ...theme.typography.caption,
      color: theme.palette.text.disabled,
    },
  }),
);
export type SequenceNavProps = {
  topic: Topic;

  pathname: string;
};

export function SequenceNav(props: SequenceNavProps) {
  const { topic, pathname } = props;
  const sequence = topic.nav.sequence!;
  const classes = useStyles();

  const current = sequence.findIndex((entry) => pathname.startsWith(entry.path));
  const handleNav = (at) => () => {
    navigate(sequence[at].path);
  };

  const prevDisabled = current < 1;
  const nextDisabled = current > sequence.length - 2;
  return (
    <Box display="flex" justifyContent="space-between" alignItems="flex-start">
      <ButtonBase
        classes={{ root: classes.button, disabled: classes.disabled }}
        onClick={handleNav(current - 1)}
        disabled={prevDisabled}
        style={{ alignItems: 'flex-start' }}>
        <Typography color="inherit" variant="button">
          Previous
        </Typography>
        {!prevDisabled && <span className={classes.caption}>{formatTitle(sequence[current - 1].title)}</span>}
      </ButtonBase>
      <ButtonBase
        classes={{ root: classes.button, disabled: classes.disabled }}
        onClick={handleNav(current + 1)}
        disabled={nextDisabled}
        style={{ alignItems: 'flex-end' }}>
        <Typography color="inherit" variant="button">
          Next
        </Typography>
        {!nextDisabled && <span className={classes.caption}>{formatTitle(sequence[current + 1].title)}</span>}
      </ButtonBase>
    </Box>
  );
}
