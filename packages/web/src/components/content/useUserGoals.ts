import React from 'react';
import { ContentContext } from './ContentContext';
import update from 'immutability-helper';

export function useUserGoals() {
  const { getGoals, setGoals } = React.useContext(ContentContext);
  const userGoals = getGoals() || {};
  return {
    userGoals,
    addUserGoal: (goalId: string) => {
      const next = { ...userGoals, [goalId]: false };
      setGoals(next);
    },
    removeUserGoal: (goalId: string) => {
      const next = update(userGoals, { $unset: [goalId] });
      setGoals(next);
    },
  };
}
