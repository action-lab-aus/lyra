import React from 'react';
import {
  Box,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Paper,
  Typography,
  Grid,
} from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import TickIcon from '@material-ui/icons/Check';
import { blue } from '@material-ui/core/colors';

import { Goal } from '../types';
import { useUserGoals } from '../useUserGoals';
import goalSetting from './goalSetting.png';
import goalArrow from './goalArrow.svg';

const useStyles = makeStyles((theme) =>
  createStyles({
    title: {
      display: 'flex',
      alignItems: 'center',
    },

    banner: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      '&>img': {
        maxWidth: 200,
      },
    },

    icon: {
      marginRight: theme.spacing(1),
      verticalAlign: 'middle',
    },

    largeIcon: {
      width: 96,
      height: 96,
    },

    textWithIcons: {
      '&>svg': {
        verticalAlign: 'middle',
      },
    },
  }),
);

export type GoalSettingProps = {
  goals: Goal[];
};

export function GoalSetting(props: GoalSettingProps) {
  const { goals } = props;
  const classes = useStyles();
  const { userGoals, addUserGoal, removeUserGoal } = useUserGoals();
  const [suggestedGoals, myGoals] = goals.reduce<[Goal[], Goal[]]>(
    (result, goal) => {
      const [suggestedGoals, myGoals] = result;
      return userGoals[goal.id] !== undefined
        ? [suggestedGoals, [...myGoals, goal]]
        : [[...suggestedGoals, goal], myGoals];
    },
    [[], []],
  );

  const hasUnfinishedGoal = Boolean(myGoals.find((goal) => userGoals[goal.id] === false));

  return (
    <Paper>
      <Box bgcolor={blue[50]} p={4} className={classes.banner}>
        <img src={goalSetting} />
      </Box>
      <Box p={2}>
        {myGoals.length > 0 ? (
          <>
            <Typography variant="h6">The goal you have selected for this week is...</Typography>
            <List>
              {myGoals.map((goal) => {
                return (
                  <ListItem key={goal.id}>
                    <ListItemText primary={goal.title} secondary={goal.content} />
                    <ListItemSecondaryAction>
                      {userGoals[goal.id] ? (
                        <TickIcon color="primary" />
                      ) : (
                        <IconButton
                          onClick={() => removeUserGoal(goal.id)}
                          edge="end"
                          aria-label="delete"
                          color="secondary">
                          <DeleteIcon />
                        </IconButton>
                      )}
                    </ListItemSecondaryAction>
                  </ListItem>
                );
              })}
            </List>
            <Grid container>
              <Grid item xs={12} md={6}>
                <Typography variant="h6" color="primary">
                  Think about:
                </Typography>

                <List>
                  <ListItem>
                    <ListItemText>When will this happen?</ListItemText>
                  </ListItem>
                  <ListItem>
                    <ListItemText>Who will be involved?</ListItemText>
                  </ListItem>
                  <ListItem>
                    <ListItemText>What might get in the way of this happening?</ListItemText>
                  </ListItem>
                </List>
              </Grid>
              <Grid item xs={12} md={6}>
                <Box p={4}>
                  <img src={goalArrow} style={{ width: 200 }} />
                </Box>
              </Grid>
            </Grid>
          </>
        ) : (
          <Box display="flex" flexDirection="column" alignItems="center" color="rgba(0,0,0,.2)">
            <Typography variant="h6" color="primary" align="center" gutterBottom>
              Select a goal to focus on this week
            </Typography>
            <Typography variant="body1" color="textSecondary" align="center" gutterBottom>
              Please select one goal from the list below that you think is achievable for you over the next week. Your
              goals will be available on your dashboard, where you can mark them as complete. After completing your
              goal, you can return to the module to select additional goals if you wish.
            </Typography>
            <Typography
              className={classes.textWithIcons}
              variant="body1"
              color="textSecondary"
              align="center"
              gutterBottom>
              Click the <AddIcon /> icon to select your goal. Remove a goal with the <DeleteIcon /> icon.
            </Typography>
          </Box>
        )}

        {suggestedGoals.length > 0 && (
          <Box mt={2}>
            <Divider style={{ marginBottom: 16 }} />
            <List>
              {suggestedGoals.map((goal) => {
                return (
                  <ListItem key={goal.id}>
                    <ListItemText primary={goal.title} secondary={goal.content} />
                    {!hasUnfinishedGoal && (
                      <ListItemSecondaryAction>
                        <IconButton onClick={() => addUserGoal(goal.id)} edge="end" aria-label="add" color="primary">
                          <AddIcon />
                        </IconButton>
                      </ListItemSecondaryAction>
                    )}
                  </ListItem>
                );
              })}
            </List>
          </Box>
        )}
      </Box>
    </Paper>
  );
}
