import React from 'react';
import { TextField as MuiTextField, TextFieldProps as MuiTextFieldProps } from '@material-ui/core';

export type TextFieldProps = { name: string } & Omit<MuiTextFieldProps, 'value' | 'onChange' | 'error'>;

export function TextField(props: TextFieldProps) {
  const { name, helperText, ...textFieldProps } = props;
  return <MuiTextField {...textFieldProps} name={name} />;
}
