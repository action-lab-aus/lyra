import React from 'react';
import { ExtLink, Link } from '../../Link';

export type GoToProps = {
  to: string;
  children: React.ReactNode;
  external?: boolean;
};

export function GoTo(props: GoToProps) {
  const { to, children, external } = props;
  return external ? <ExtLink href={to}>{children}</ExtLink> : <Link href={to}>{children}</Link>;
}
