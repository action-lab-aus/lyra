import React from 'react';
import clsx from 'clsx';
import { makeStyles, createStyles, Button, ButtonProps } from '@material-ui/core';
import { useContent } from '../useContent';

const useStyles = makeStyles(
  createStyles({
    helpful: {
      background: '#d2e7ce',
      '&:hover': {
        backgroundColor: '#d2e7ce',
      },
    },
    notHelpful: {
      background: '#f6c5c6',
      '&:hover': {
        backgroundColor: '#f6c5c6',
      },
    },
    label: {
      textTransform: 'none',
    },
  }),
);

export const SelectBox = (props: ButtonProps & { id: string; helpful: boolean; revealed: boolean }) => {
  const classes = useStyles();
  const { id: key, children, color, variant, helpful, revealed, ...other } = props;
  const [clicked, setClicked] = useContent(key);

  return (
    <Button
      {...other}
      className={clsx({
        [classes.helpful]: (revealed || clicked) && helpful,
        [classes.notHelpful]: (revealed || clicked) && !helpful,
        label: classes.label,
      })}
      variant="contained"
      onClick={() => setClicked(!clicked)}>
      {children}
    </Button>
  );
};
