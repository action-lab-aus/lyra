import React from 'react';
import PropTypes from 'prop-types';
import { Box, Typography, Link } from '@material-ui/core';

export type BasicAudioProps = {
  src: string;
  type: 'audio/mpeg' | 'audio/wav' | 'audio/ogg' | 'audio/aac' | 'audio/opus' | 'audio/vorbis';
  preload: 'none' | 'metadata' | 'auto';
};

export function BasicAudio(props: BasicAudioProps) {
  const { src, type, preload } = props;
  return (
    <Box clone display="block" my={2}>
      <audio controls preload={preload} controlsList="nodownload">
        <source src={src} type={type} />
        <Typography>
          Your browser doesn't support HTML5 audio. Here is a <Link href={src}>link to the audio</Link> instead.
        </Typography>
      </audio>
    </Box>
  );
}
