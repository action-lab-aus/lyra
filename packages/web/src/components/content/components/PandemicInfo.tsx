import React, { FunctionComponent } from 'react';
import { Grid } from '@material-ui/core';
import { Img } from './Img';

interface PandemicInformationProps {
  /** The url of the image you want to display */
  children?: React.ReactNode;
}

/**
 * React component for placing a single image in the document. Should scale the
 * image to fit the screen width if the screen is smaller than the image.
 */
export const PandemicInformation: FunctionComponent<PandemicInformationProps> = ({ children }) => {
  return (
    <Grid container justify="center" alignItems="center">
      <Grid xs={1} item>
        <Img style={{ width: '75%', height: '75%' }} src={'/images/m2/1-symbol.svg'} alt={'Pandemic Icon'} />
      </Grid>
      <Grid xs={11} item>
        {children}
      </Grid>
    </Grid>
  );
};
