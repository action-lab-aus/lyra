import React from 'react';
import { Card, CardContent, Fade, styled, Box } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import { TextCustom, TextCustomProps } from './TextCustom';
import { useContent } from '../useContent';
import { format } from 'date-fns';
import { ISOFormatString } from './utils/helpers';

const HoverCard = styled(Card)({
  '&:hover': {
    background: grey[100],
    cursor: 'pointer',
  },
});

export type TextRevealProps = TextCustomProps & {
  id: string;
  header: JSX.Element;
  mountOnEnter?: boolean;
};

/**
 * React component for placing a single image in the document. Should scale the
 * image to fit the screen width if the screen is smaller than the image.
 */
export function TextReveal(props: TextRevealProps) {
  const { id: key, header, mountOnEnter, children, ...textCustomProps } = props;
  const [clicked, setClicked] = useContent(key);

  function handleClick() {
    if (!clicked) {
      setClicked(format(new Date(), ISOFormatString));
    }
  }

  return (
    <HoverCard onClick={handleClick}>
      <CardContent>
        <Box mb={2}>
          <TextCustom variant="h5" {...textCustomProps}>
            {header}
          </TextCustom>
        </Box>
        <Fade in={clicked} timeout={1000} mountOnEnter={mountOnEnter}>
          <div>{children}</div>
        </Fade>
      </CardContent>
    </HoverCard>
  );
}
