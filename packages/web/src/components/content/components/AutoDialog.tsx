import React from 'react';
import { Button, Dialog, DialogActions, DialogContent, Typography } from '@material-ui/core';

export const AutoDialog = (props: { children: React.ReactNode }) => {
  const [open, setOpen] = React.useState(true);
  return (
    <Dialog open={open} onClose={() => setOpen(false)}>
      <DialogContent>
        <Typography>
          {props.children}
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => setOpen(false)}>OK</Button>
      </DialogActions>
    </Dialog>
  );
};
