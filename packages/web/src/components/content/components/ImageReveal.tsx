import React from 'react';
import { Card, CardContent, Fade } from '@material-ui/core';
import { useContent } from '../useContent';
import { Img } from '.';

type ImageRevealItem = {
  src: string;
  alt: string;
};

export type ImageRevealProps = {
  id: string;
  mainDescription: React.ReactNode;
  mainItem: ImageRevealItem;
  aboveReveal?: ImageRevealItem;
  belowReveal?: ImageRevealItem;
  /** How long the fade effect takes to transition from invisible to fully
   * visible. In milliseconds. */
  fadeSpeed: number;
};

/**
 * React component for rendering a grid list of images where each item will fade
 * in after the previous one.
 */
export function ImageReveal(props: ImageRevealProps) {
  const { id: key, fadeSpeed, mainItem, mainDescription, aboveReveal, belowReveal } = props;
  const [clicked, setClicked] = useContent(key);
  function click() {
    setClicked(true);
  }

  return (
    <Card onClick={click}>
      <CardContent>
        {aboveReveal ? (
          <Fade in={Boolean(clicked)} timeout={fadeSpeed}>
            <Img height={Boolean(clicked) ? undefined : 0} src={aboveReveal.src} alt={aboveReveal.alt} />
          </Fade>
        ) : null}
        {mainDescription}
        <Img src={mainItem.src} alt={mainItem.alt} />
        {belowReveal ? (
          <Fade in={Boolean(clicked)} timeout={fadeSpeed}>
            <Img src={belowReveal.src} alt={belowReveal.alt} />
          </Fade>
        ) : null}
      </CardContent>
    </Card>
  );
}
