import React, { FunctionComponent, useContext } from 'react';
import { Box, Grid } from '@material-ui/core';
import { Fragment } from 'react';
import { Img, TextCustom } from '.';
import { useContent } from '../useContent';

export type GoalDialogProps = {
  id: string;
  children?: React.ReactNode;
};

export function SelectedGoal(props: GoalDialogProps) {
  const { id, children } = props;
  const [keyData] = useContent('Goal');
  const thisGoalSelected = keyData === id;

  if (thisGoalSelected) {
    return (
      <Fragment>
        <TextCustom variant="h4" paragraph>
          The goal you have selected for this week is…
        </TextCustom>
        <TextCustom variant="h6" customColor="blue" paragraph>
          {children}
        </TextCustom>
        <Grid container>
          <Grid item xs={10} sm={5}>
            <h3>Think about:</h3>
            <Box fontSize="h6.fontSize">
              <ul>
                <li>When will this happen?</li>
                <li>Who will be involved?</li>
                <li>What might get in the way of this happening?</li>
              </ul>
            </Box>
          </Grid>
          <Grid item xs={8} sm={3}>
            <Img src="/images/shared/50_new_goal_sticky.svg" alt="set goal" />
          </Grid>
        </Grid>
      </Fragment>
    );
  }

  return null;
}
