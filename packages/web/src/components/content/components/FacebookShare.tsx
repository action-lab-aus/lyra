import React from 'react';
import { FacebookButton } from 'components/psg';
import { Grid } from '@material-ui/core';

export const FacebookShare = () => {
  return (
    <Grid container style={{ marginTop: '32px', marginBottom: '24px' }}>
      <Grid item xs={12} md={2} />
      <Grid item xs={12} md={8}>
        If you’re part of the online Peer Support Group, you can share your goal with the group by clicking here.
      </Grid>
      <Grid item xs={12} md={2}>
        <FacebookButton onClick={() => window.open('https://www.facebook.com/groups/3030932617228248')}>
          Share
        </FacebookButton>
      </Grid>
    </Grid>
  );
};
