import React from 'react';
import { Button, Grid, Box, Typography } from '@material-ui/core';
import { SelectBox } from './SelectBox';
import { useContent } from '../useContent';

type SelectBoxItems = {
  id: string;
  helpful: boolean;
  content: string;
};

type SelectBoxWrapper = {
  id: string;
  description: string;
  hiddenContent: string;
  items: SelectBoxItems[];
};

export const SelectBoxWrapper = (props: SelectBoxWrapper) => {
  const { id: key, description, hiddenContent, items } = props;
  const [revealed, setRevealed] = useContent(key);

  return (
    <div>
      <Grid container spacing={3}>
        {items &&
          items.map((item, idx) => {
            return (
              <Grid key={idx} item xs={12} sm={6} md={4}>
                <SelectBox id={item.id} fullWidth={true} helpful={item.helpful} revealed={revealed}>
                  <p>{item.content}</p>
                </SelectBox>
              </Grid>
            );
          })}
      </Grid>

      <Box my={4} display="flex" justifyContent="center">
        <Button
          id={key}
          onClick={() => {
            setRevealed(true);
          }}
          variant="contained"
          color="primary"
          size="large">
          {description}
        </Button>
      </Box>

      {revealed && <Typography>{hiddenContent}</Typography>}
    </div>
  );
};
