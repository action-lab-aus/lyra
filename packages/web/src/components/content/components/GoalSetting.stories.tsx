import React from 'react';
import { Story, Meta } from '@storybook/react';
import { GoalSetting } from './GoalSetting';
import { ContentProvider } from 'components/helpers';
import m1 from '../goals/m1';
import { UserGoals } from 'app';

export default {
  title: 'components/content/components/GoalSetting',
  component: GoalSetting,
} as Meta;

type Args = {
  userGoals: UserGoals;
};

const Template: Story<Args> = (args) => (
  <ContentProvider {...args}>
    <GoalSetting goals={m1} />
  </ContentProvider>
);

export const NoGoalsSelected = Template.bind({});
NoGoalsSelected.args = {
  userGoals: {},
};

export const OneUnfinishedGoal = Template.bind({});
OneUnfinishedGoal.args = {
  userGoals: {
    'm1-connect-goal1': false,
  },
};

export const OneCompletedGoal = Template.bind({});
OneCompletedGoal.args = {
  userGoals: {
    'm1-connect-goal1': true,
  },
};

export const MultipleGoalsSelected = Template.bind({});
MultipleGoalsSelected.args = {
  userGoals: {
    'm1-connect-goal1': true,
    'm1-connect-goal3': false,
  },
};
