import React from 'react';
import clsx from 'clsx';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Avatar, Box, Accordion, AccordionSummary, AccordionDetails, Typography } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { primaryDark } from '../../../theme';
import { useContent } from '../useContent';

export type ImageAccordionProps = {
  id: string;
  index: number;
  title: string;
  description: string;
  hasNumber: boolean;
};

const useStyles = makeStyles((theme) =>
  createStyles({
    avatar: {
      color: '#fff',
      backgroundColor: primaryDark,
    },
    box: {
      flex: 1,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      [theme.breakpoints.down('xs')]: {
        flexDirection: 'column',
        alignItems: 'flex-start',
      },
    },
    boxPadding: {
      paddingLeft: theme.spacing(4),
    },
  }),
);

/**
 * React component for rendering an accordion.
 */
export function ImageAccordion(props: ImageAccordionProps) {
  const classes = useStyles();
  const { id: key, index, title, description, hasNumber } = props;
  const [clicked, setClicked] = useContent(key);

  function click() {
    setClicked(true);
  }

  return (
    <Box mb={1}>
      <Accordion key={index} onChange={click}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          {hasNumber && <Avatar className={classes.avatar}>{index}</Avatar>}
          <Box className={clsx({ [classes.box]: true, [classes.boxPadding]: hasNumber })}>
            <Typography component="div">
              <b>{title}</b>
            </Typography>
          </Box>
        </AccordionSummary>
        <AccordionDetails>
          <Box display="flex" justifyContent="flex-start" alignItems="flex-start">
            <Typography variant="body1" style={{ flex: 1 }}>
              {description}
            </Typography>
          </Box>
        </AccordionDetails>
      </Accordion>
    </Box>
  );
}
