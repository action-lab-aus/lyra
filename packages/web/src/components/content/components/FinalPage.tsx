import React from 'react';
import { useSelector } from 'react-redux';
import { Typography, Grid, Box, Button, useMediaQuery, useTheme } from '@material-ui/core';
import FacebookIcon from '@material-ui/icons/Facebook';
import { FacebookButton, PSGDialog } from 'components/psg';
import { RootState } from 'app';
import { SingleImage } from './SingleImage';

export function FinalPage() {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('xs'));
  const [showPsg, setShowPsg] = React.useState(false);

  const { code, isParent } = useSelector<RootState, { code: string; isParent: boolean }>((state) => {
    const uid = state.auth.userInfo?.uid;
    const isParent = state.profile.data?.user.userCategory === 'parent';
    const code = uid ? uid.slice(-6) : '';
    return { code, isParent };
  });

  return (
    <>
      <h1 id="keep-in-mind">Keep in mind</h1>
      <Grid container>
        <Grid item xs={12} sm={8}>
          <Typography paragraph>
            Don't be afraid to try out new ways of parenting. Putting these ideas into practice with your teenager takes
            time. <strong>Be patient.</strong>
          </Typography>
          <Typography paragraph>
            If you feel you've made some parenting mistakes, don't be too hard on yourself. You can always learn from
            these mistakes. <strong>Be kind to yourself.</strong>
          </Typography>
          <Typography paragraph>
            If your teenager has already developed depression or clinical anxiety, don't feel that it is a failure on
            your part or blame yourself. Any teenager can develop these problems.{' '}
            <strong>
              Just by completing this module, you are already taking steps in the right direction towards helping your
              teenager.
            </strong>
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4}>
          <img src="/images/shared/52.svg" alt="take care" />
        </Grid>
      </Grid>

      <SingleImage gridSize={4} src="/images/shared/52.1.svg" alt="Keep in mind image" />

      {isParent && (
        <Box>
          <Grid container style={{ marginTop: '24px', marginBottom: '24px' }}>
            <Grid item xs={2}>
              <FacebookIcon style={{ fontSize: matches ? 48 : 72, color: '#3b5998' }} />
            </Grid>
            <Grid item xs={10}>
              <Typography color="primary">Did you know you don’t have to do this alone?</Typography>
              <Typography paragraph>
                You can connect with other parents completing the PiP+ program in our online peer support group on
                Facebook. Please click the COMMUNITY button to join the PiP+ community.
              </Typography>
            </Grid>
          </Grid>
          <Box display="flex" justifyContent="center">
            <FacebookButton onClick={() => setShowPsg(true)}>Go to Facebook community</FacebookButton>
          </Box>

          <PSGDialog open={showPsg} onClose={() => setShowPsg(false)} code={code} />
        </Box>
      )}
    </>
  );
}
