import { Button as MuiButton, styled } from '@material-ui/core';

export const Button = styled(MuiButton)(({ theme }) => ({
  margin: theme.spacing(2, 0),
  paddingX: theme.spacing(2),
}));
