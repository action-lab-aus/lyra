import React from 'react';
import { Box, Fade, Grid, Paper, PaperProps } from '@material-ui/core';
import { Colors, customColorOptions, getRandomInt, useInterval } from './utils';
import { TextCustom } from './TextCustom';
import { useContent } from '../useContent';
import { format } from 'date-fns';
import { ISOFormatString } from './utils/helpers';

type TextStepperItemProps = {
  long?: boolean;
  text: string | JSX.Element;
  color: Colors;
};

type RandomItem = {
  colors: number[];
};

export type TextStepperProps = PaperProps & {
  id: string;
  sequenceSpeed: number;
  fadeSpeed: number;
  textVariant?: any;
  list: TextStepperItemProps[];
};

/**
 * React component for rendering a grid list of text where each item will fade in after the previous one.
 */
export function TextStepper(props: TextStepperProps) {
  const { id: key, list, sequenceSpeed, fadeSpeed, textVariant, ...PaperProps } = props;
  const [keyData, setKeyData] = useContent(key);
  const [count, setCount] = React.useState(1);

  const randomItemLists = React.useMemo<RandomItem>(() => {
    const randomColorNumbers: number[] = [];
    list.forEach(() => {
      randomColorNumbers.push(getRandomInt(0, customColorOptions.length));
    });

    return { colors: randomColorNumbers };
  }, [list]);

  useInterval(() => {
    if (count <= list.length) {
      setCount(count + 1);
    }
    if (count === list.length && !keyData) {
      setKeyData(format(new Date(), ISOFormatString));
    }
  }, sequenceSpeed);

  return (
    <Box clone my={1} p={3}>
      <Paper {...PaperProps}>
        <Grid container spacing={3}>
          {list.map((item, index) => (
            <Grid key={index} item xs={12} sm={item.long ? undefined : 6} md={item.long ? undefined : 6}>
              <Fade in={index < count} timeout={fadeSpeed}>
                {typeof item.text === 'string' ? (
                  <TextCustom
                    variant={textVariant ?? 'h5'}
                    customColor={item.color ? item.color : customColorOptions[randomItemLists.colors[index]]}>
                    {item.text}
                  </TextCustom>
                ) : (
                  item.text
                )}
              </Fade>
            </Grid>
          ))}
        </Grid>
      </Paper>
    </Box>
  );
}
