import React, { ChangeEventHandler, FunctionComponent, useContext } from 'react';
import { Box, Card, CardContent, Typography } from '@material-ui/core';
import { useContent } from '../useContent';
import { Rating } from 'components/questions/Rating';

interface ConfidenceQuestionProps {
  id: string;
  begin: boolean;
}

export enum ConfidenceValues {
  notConfident = 'notConfident',
  littleConfident = 'littleConfident',
  somewhatConfident = 'somewhatConfident',
  veryConfident = 'veryConfident',
}

export const ConfidenceQuestion: FunctionComponent<ConfidenceQuestionProps> = (props) => {
  const { id: key } = props;
  const [keyData, setKeyData] = useContent(key, '');

  return (
    <Box mb={3}>
      <Card>
        <CardContent>
          <Typography paragraph variant="h6">
            Before we continue this module, we'd like to check in about how you feel you're coping at this time.
          </Typography>

          <Rating
            name={key}
            value={keyData}
            options={['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']}
            label="On a scale of 1 to 10, how well are you coping with the stress of parenting during the pandemic at the moment?"
            onChange={(value) => setKeyData(value)}
          />
        </CardContent>
      </Card>
    </Box>
  );
};
