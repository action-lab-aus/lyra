import React from 'react';
import { Box, Button, Typography } from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import ClipboardCheck from 'mdi-material-ui/ClipboardCheck';
import { Paper } from './Paper';
import { useContent } from '../useContent';

export type GoalDialogProps = {
  id: string;
  description: string;
  children?: React.ReactNode;
};

export function GoalDialog(props: GoalDialogProps) {
  // const context = useContext(ModuleControlContext);
  // const key = `Goal_${context.moduleID}`
  // const keyData = context.moduleData?.[key];
  // const thisGoalSelected = keyData === props.id;

  const { id, children } = props;
  const [keyData, setKeyData] = useContent('Goal');
  const thisGoalSelected = keyData === id;
  const moduleCompleted = false;

  const handleSelect = () => {
    // context.SetGoal(props.id, props.description);
    setKeyData(id);
  };

  return (
    <Paper>
      <Typography paragraph>{children}</Typography>
      <Box display="flex">
        <Button variant="contained" color="primary" onClick={handleSelect} disabled={moduleCompleted}>
          Select
        </Button>
        {thisGoalSelected ? (
          <Box display="flex">
            <ClipboardCheck htmlColor={green['A400']} fontSize="large" />
            <Typography>You've selected this goal.</Typography>
          </Box>
        ) : null}
      </Box>
    </Paper>
  );
}
