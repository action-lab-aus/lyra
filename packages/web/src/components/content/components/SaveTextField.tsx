import React from 'react';
import { useContent } from '../useContent';
import { TextField as MuiTextField, TextFieldProps as MuiTextFieldProps, Grid, Box, Button } from '@material-ui/core';
import update from 'immutability-helper';
import { WaitButton } from 'components';

export type TextFieldProps = { name: string } & Omit<MuiTextFieldProps, 'value' | 'onChange' | 'error'>;

export function SaveTextField(props: TextFieldProps) {
  const key = props.id!;
  const { name, helperText, multiline, fullWidth, ...textFieldProps } = props;
  const [keyData, setKeyData] = useContent(key, {});
  const [loading, setLoading] = React.useState(false);
  const [value, setValue] = React.useState(keyData['answer'] ? keyData['answer'] : '');

  function saveValue() {
    if (key) {
      setLoading(true);
      const newVal = update(keyData, { $set: { ['answer']: value } });
      setKeyData(newVal);
      setTimeout(() => {
        setLoading(false);
      }, 1000);
    }
  }

  function onChange(event) {
    setValue(event.target.value);
  }

  return (
    <Grid item xs={12} container spacing={1} justify="center" alignItems="center">
      <Grid item xs={12} md={fullWidth ? 12 : 10}>
        <MuiTextField
          {...textFieldProps}
          name={name}
          value={value}
          onChange={onChange}
          multiline
          fullWidth
          rows={multiline ? 5 : 1}
        />
      </Grid>

      <Grid item xs={12} md={fullWidth ? 12 : 2}>
        <Box display="flex" justifyContent="center">
          <WaitButton wait={loading} onClick={saveValue} variant="contained" color="primary" size="large">
            Save
          </WaitButton>
        </Box>
      </Grid>
    </Grid>
  );
}
