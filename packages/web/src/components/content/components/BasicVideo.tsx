import React from 'react';
import { Typography, Box } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import ReactPlayer from 'react-player';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    videoContainer: {
      position: 'relative',
      overflow: 'hidden',
      paddingTop: '56.25%',
    },
    videoInner: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
    },
    caption: {
      textAlign: 'center',
    },
  }),
);

type BasicVideoProps = {
  title?: string;
  url: string;
};

/**
 * Video Component wrap around react-youtube
 *
 * @param {object} props React props
 */
export function BasicVideo(props: BasicVideoProps) {
  const { title, url } = props;

  const classes = useStyles();
  return (
    <Box my={4} mx="auto">
      <div className={classes.videoContainer}>
        <ReactPlayer controls light url={url} className={classes.videoInner} width="100%" height="100%" />
      </div>

      {title && (
        <div className={classes.caption}>
          <Typography variant="caption" gutterBottom>
            {title}
          </Typography>
        </div>
      )}
    </Box>
  );
}
