import { Paper as MuiPaper, styled } from '@material-ui/core';

export const Paper = styled(MuiPaper)(({ theme }) => ({
  margin: theme.spacing(4, 0),
  padding: theme.spacing(2),
}));
