import React, { ChangeEvent, Fragment, ReactNode, useState } from 'react';
import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Fade,
  FormControlLabel,
  FormGroup,
  Grid,
  Typography,
  withStyles,
} from '@material-ui/core';
import { CheckboxProps } from '@material-ui/core/Checkbox';
import { format } from 'date-fns';
import { useContent } from '../useContent';
import { ISOFormatString } from './utils';
import update from 'immutability-helper';

const DefaultCheckbox = withStyles({
  checked: {},
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

type AnswerProps = AnswerData & {
  clicked: boolean;
  disabled: boolean;
  question: ReactNode;
  setClicked: (answerName: string) => void;
};

type AnswerData = {
  name: string;
  correct: boolean;
  response?: ReactNode;
};

export type QuizProps = {
  id: string;
  /**
   * If this is set to true then the user will only be able to attempt to answer
   * this question a single time. After their first attempt all answers will be
   * disabled. */
  once: boolean;
  /**
   * If singleAnswer is set then the question will only save the last answered question.
   */
  singleAnswer: boolean;
  /**
   * If true response will be displayed below the quiz instead of in a dialog.
   * */
  inline: boolean;
  question: ReactNode;
  fallbackResponse?: ReactNode;
  answers: AnswerData[];
};

export function Quiz(props: QuizProps) {
  const key = props.id;
  const [keyData, setKeyData] = useContent(key, {});
  const anyAnswerGiven = Object.keys(keyData).length > 0;

  function saveAnswer(answerName: string) {
    const value = update(keyData, { $set: { [answerName]: format(new Date(), ISOFormatString) } });
    setKeyData(value);
  }

  return (
    <Fragment>
      <Grid container alignItems="center" spacing={2}>
        <Grid xs item>
          <Typography>{props.question}</Typography>
        </Grid>
        <Grid item>
          <FormGroup row>
            {props.answers.map((answer, index) => {
              const answerChecked = keyData[answer.name] ? true : false;
              return (
                <Answer
                  key={index}
                  name={answer.name}
                  correct={answer.correct}
                  question={props.question}
                  response={props.inline ? undefined : answer.response ? answer.response : props.fallbackResponse}
                  clicked={answerChecked}
                  disabled={false}
                  setClicked={saveAnswer}
                />
              );
            })}
          </FormGroup>
        </Grid>
      </Grid>
      {props.inline ? (
        <Fade in={anyAnswerGiven} timeout={2000}>
          <Box mt={2}>{props.fallbackResponse}</Box>
        </Fade>
      ) : null}
    </Fragment>
  );
}

export const Answer = ({ correct, response, name, disabled, setClicked, clicked, question }: AnswerProps) => {
  const [open, setOpen] = useState(false);

  function handleClickOpen(event: ChangeEvent<HTMLInputElement>) {
    setClicked(name);
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <Fragment>
      <FormControlLabel
        control={<DefaultCheckbox checked={clicked} disabled={disabled && !clicked} onChange={handleClickOpen} />}
        label={name}
      />
      {response ? (
        <Dialog open={open} onClose={handleClose}>
          <DialogContent>
            <Box clone display="flex" flexDirection="row">
              <DialogTitle disableTypography>
                <Box>
                  <Typography>{question}</Typography>
                </Box>
              </DialogTitle>
            </Box>
            <Typography>{response}</Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Close</Button>
          </DialogActions>
        </Dialog>
      ) : null}
    </Fragment>
  );
};
