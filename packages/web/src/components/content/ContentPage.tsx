import React from 'react';
import get from 'lodash/get';
import { useDispatch } from 'react-redux';
import { useLocation } from '@reach/router';
import { MDXProvider } from '@mdx-js/react';
import { Grid, Box, CircularProgress, Container, Typography, Paper, Breadcrumbs } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { UserGoals } from 'app';
import { updateUserTopic, updateUserTopicEntry } from 'app/profileSlice';
import { Link } from 'components';
import { NavPage, AppBar } from 'components/layout';
import { Protected } from '../Protected';
import { Article } from './Article';
import { ContentContext } from './ContentContext';
import { SequenceNav } from './SequencyNav';
import { TopicNav } from './TopicNav';
import { useTopic } from './useTopic';
import * as contents from './components';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      overflow: 'hidden',
    },

    content: {
      position: 'relative',
      flexGrow: 1,
    },

    main: {
      width: '100%',
      height: '100%',
      overflow: 'auto',
    },

    footerBar: {
      ...theme.mixins.toolbar,
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: theme.palette.common.white,
      overflow: 'hidden',
      zIndex: theme.zIndex.appBar,
    },

    container: {
      paddingTop: theme.spacing(10),
      paddingBottom: 120,
    },

    lessonTitle: {
      marginTop: theme.spacing(2),
      color: theme.palette.grey[800],
      ...theme.typography.h4,
    },
  }),
);

const htmlElmMapping = {};

export type ContentPageProps = {
  children: React.ReactNode;
};

export function ContentPage(props: ContentPageProps) {
  const { children } = props;
  const mainElm = React.useRef<HTMLDivElement | null>(null);
  const classes = useStyles();
  const location = useLocation();
  const dispatch = useDispatch();
  const result = useTopic(location.pathname);
  const minimize = location.search === '?minimize';
  const hasResult = result !== null;

  React.useEffect(() => {
    if (hasResult) {
      window.parent.postMessage(location.pathname, '*');
    }
  }, [hasResult, location.pathname]);

  if (!hasResult) {
    return <div>Waiting...</div>;
  }

  const { topic, topicEntry, userTopic, fulfillment, topicProgress } = result;

  const getContent = (contentId: string, defaultValue?: any) => {
    return get(userTopic, ['entries', topicEntry.key, contentId]);
  };

  const setContent = (contentId: string, value: any) => {
    dispatch(updateUserTopicEntry(topic.id, topicEntry.key, contentId, value));
  };

  const getGoals = () => {
    return userTopic.goals || null;
  };

  const setGoals = (goals: UserGoals) => {
    dispatch(updateUserTopic(topic.id, { goals }));
  };

  return (
    <Protected>
      <div className={classes.root}>
        {!minimize ? (
          <NavPage
            drawerContent={
              <TopicNav
                topic={topic}
                topicProgress={topicProgress}
                fulfillment={fulfillment}
                pathname={location.pathname}
              />
            }
            responsive>
            <AppBar title={topic.title} responsive openMenu />

            <div className={classes.content}>
              <main className={classes.main} ref={mainElm}>
                <Container className={classes.container} maxWidth="md">
                  <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/">
                      Home
                    </Link>
                    <Link color="inherit" href="/dashboard">
                      Dashboard
                    </Link>
                    <Typography color="textPrimary">{topic.title}</Typography>
                    <Typography color="primary">{topicEntry.title}</Typography>
                  </Breadcrumbs>
                  <ContentContext.Provider value={{ getContent, setContent, getGoals, setGoals }}>
                    <Article>
                      <MDXProvider components={{ Grid, ...contents, ...htmlElmMapping }}>{children}</MDXProvider>
                    </Article>
                  </ContentContext.Provider>
                </Container>
              </main>

              <Paper className={classes.footerBar}>
                <Box px={2}>
                  <SequenceNav topic={topic} pathname={location.pathname} />
                </Box>
              </Paper>
            </div>
          </NavPage>
        ) : (
          <NavPage>
            <div className={classes.content}>
              <main className={classes.main} ref={mainElm}>
                <Container maxWidth="md">
                  <ContentContext.Provider value={{ getContent, setContent, getGoals, setGoals }}>
                    <Article>
                      <MDXProvider components={{ Grid, ...contents, ...htmlElmMapping }}>{children}</MDXProvider>
                    </Article>
                  </ContentContext.Provider>
                </Container>
              </main>
            </div>
          </NavPage>
        )}
      </div>
    </Protected>
  );
}

export default ContentPage;
