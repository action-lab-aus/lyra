import React from 'react';
import { ContentContext } from './ContentContext';

export function useContent<T extends any>(contentId: string, defaultValue?: T) {
  const { getContent, setContent } = React.useContext(ContentContext);
  const value = getContent(contentId) || defaultValue;
  return [
    value,
    (value: T) => {
      setContent(contentId, value);
    },
  ];
}
