import React from 'react';
import { navigate } from 'gatsby';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import {
  Box,
  Chip,
  IconButton,
  LinearProgress,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PrevIcon from '@material-ui/icons/ChevronLeft';
import NextIcon from '@material-ui/icons/ChevronRight';
import cyan from '@material-ui/core/colors/cyan';
import { Progress } from 'app';
import { Topic, TopicFulfillment } from './types';
import { ProgressIcon } from './ProgressIcon';
import { formatTitle } from './components/utils';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      backgroundColor: '#FFF',
    },
    toolbar: {
      padding: theme.spacing(2),
      display: 'flex',
      justifyContent: 'space-between',
    },
    progressBar: {
      height: 2,
      marginBottom: theme.spacing(1),
    },

    itemSelected: {
      color: theme.palette.primary.light,
    },

    itemTextPrimary: {
      color: 'inherit',
      marginLeft: 8,
      ...theme.typography.body2,
      fontFamily: 'Roboto Condensed',
      fontSize: '0.8rem',
      lineHeight: 1.2,
    },
  }),
);

export type TopicNavProps = {
  topic: Topic;
  fulfillment: TopicFulfillment;
  topicProgress: Progress;
  pathname: string;
};

export function TopicNav(props: TopicNavProps) {
  const { topic, pathname, fulfillment, topicProgress } = props;
  const classes = useStyles();
  const items = topic.nav.sequence;
  const selected = (path: string) => pathname.startsWith(path);
  const handleClick = (path: string) => (e: React.MouseEvent<HTMLElement>) => {
    navigate(path);
    e.preventDefault();
  };

  const percent = React.useMemo(() => {
    const { total, completed } = topicProgress;
    return total > 0 ? completed / total : 0;
  }, [topicProgress]);

  const sequence = topic.nav.sequence;
  const cover = topic.cover && getImage(topic.cover);
  const selectedIndex = sequence.findIndex((entry) => entry.path === pathname);

  return (
    <aside className={classes.root}>
      <Box p={2} bgcolor={cyan[50]}>
        {cover && <GatsbyImage image={cover} alt={topic.title} />}
        <Typography variant="subtitle1" color="primary" gutterBottom align="center">
          {topic.title}
        </Typography>
        <Box display="flex" justifyContent="space-between">
          <IconButton
            color="primary"
            onClick={() => navigate(sequence[selectedIndex - 1].path)}
            disabled={selectedIndex < 1}>
            <PrevIcon />
          </IconButton>
          <IconButton color="primary" onClick={() => navigate('/dashboard')}>
            <DashboardIcon />
          </IconButton>
          <IconButton
            color="primary"
            onClick={() => navigate(sequence[selectedIndex + 1].path)}
            disabled={selectedIndex > sequence.length - 2}>
            <NextIcon />
          </IconButton>
        </Box>
      </Box>
      <LinearProgress color="primary" variant="determinate" value={percent * 100} className={classes.progressBar} />
      <List>
        {items.map((item) => {
          const entryProgress = fulfillment[item.key];
          const percent = entryProgress && entryProgress.total > 0 ? entryProgress.completed / entryProgress.total : 0;

          return (
            <ListItem
              key={item.path}
              classes={{ selected: classes.itemSelected }}
              href={item.path}
              onClick={handleClick(item.path)}
              selected={selected(item.path)}
              button
              dense>
              <ListItemText classes={{ primary: classes.itemTextPrimary }} primary={formatTitle(item.title)} />
              {item.optional && (
                <ListItemSecondaryAction>
                  <Chip label="optional" size="small" variant="outlined" color="primary" />
                </ListItemSecondaryAction>
              )}
              {percent > 0 && (
                <ListItemSecondaryAction>
                  <ProgressIcon percent={percent} />
                </ListItemSecondaryAction>
              )}
            </ListItem>
          );
        })}
      </List>
    </aside>
  );
}
