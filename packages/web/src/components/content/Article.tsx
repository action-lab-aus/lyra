import { styled } from '@material-ui/core/styles';

export const Article = styled('article')(({ theme }) => ({
  ...theme.typography.body1,
  color: theme.palette.grey[800],

  '& h1, & h2': {
    ...theme.typography.h4,
    fontFamily: '"Roboto", sans-serif',
  },

  '& h3': {
    ...theme.typography.h5,
    fontFamily: '"Roboto", sans-serif',
  },

  '& h4': {
    ...theme.typography.h6,
    fontFamily: '"Roboto", sans-serif',
  },

  '& h5': {
    ...theme.typography.h6,
  },

  '& img': {
    display: 'block',
    width: '100%',
    margin: theme.spacing(2, 'auto'),
  },

  '& table': {
    width: '100%',
    display: 'table',
    borderSpacing: 0,
    borderCollapse: 'collapse',
  },

  '& thead': {
    backgroundColor: theme.palette.grey[700],
  },

  '& th': {
    color: theme.palette.grey[50],
  },

  '& th, & td': {
    display: 'table-cell',
    padding: 16,
    fontSize: '0.875rem',
    textAlign: 'left',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 400,
    lineHeight: 1.43,
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    letterSpacing: '0.01071em',
    verticalAlign: 'inherit',
  },
}));
