import React from 'react';
import { Typography, Breadcrumbs, Box } from '@material-ui/core';
import { Link } from 'components';
import { FeedbackContent } from 'components/feeback';
import { AppPage } from 'components/layout';

export default function FollowupFeedbackPage() {
  return (
    <AppPage title="All followup survey feedback">
      <Box mt={2}>
        <Breadcrumbs aria-label="breadcrumb">
          <Link color="inherit" href="/">
            Home
          </Link>
          <Typography color="textPrimary">All Follow-up Survey Feedback</Typography>
        </Breadcrumbs>
      </Box>
      <FeedbackContent surveyIds={['f1-evaluation', 'f2-pradas', 'f3-k6', 'f4-rcads']} />
    </AppPage>
  );
}
