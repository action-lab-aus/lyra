import firebase from 'firebase';
import { ActionStatus } from './helpers/actionStatus';
import { ThunkAction } from 'redux-thunk';
import { Action } from '@reduxjs/toolkit';

export type TopicPlanItem = {
  topicId: string;
  seq: number;
  tag?: string;
  mandatory: boolean;
};

/**
 * Auth state
 */
export type Credential = {
  email: string;
  password: string;
  rememberMe?: boolean;
};

export type UserInfo = firebase.UserInfo;

type AdditionalUserInfo = {
  emailVerified?: boolean;
  metadata?: firebase.auth.UserMetadata;
  providerData?: UserInfo[];
};

export type AuthState = {
  authenticated: boolean | null;
  userInfo: (Partial<UserInfo> & AdditionalUserInfo) | null;
  status: ActionStatus | null;
};

/**
 * Profile state
 */

export type UserProfileStage = 'survey' | 'topic' | 'followup';

export type Gender = 'male' | 'female' | 'non-binary';

export type UserProfile = {
  userCategory: 'parent' | 'professional' | 'ineligible-under-12' | 'ineligible-over-17';
  userFirstname: string;
  userSurname: string;
  userAge: string;
  userGender: Gender;
  userPostcode: string;
  userPhone: string;
  userPhoneAlt?: string;
  userEducation?: string;
  userContactEmail?: string;
  familyEthnicity?: string;
  familyRelationship?: string;
  familyIncome?: string;
  familyCovidImpact?: string[];
  smsNotification: boolean;
  childName: string;
  childDob: string;
  childGrade: number;
  childGender: Gender;
  childRelationship: string;
  childOtherParentsJoined: string;
  discoverChannels?: string;
  discoverReason?: string;
  discoverFutureResearch?: string;
  currentStage?: UserProfileStage;
  topicSuggestion?: string[];
  optOutAt?: firebase.firestore.Timestamp;
  surveyCompletedAt?: firebase.firestore.Timestamp;
  followupCompletedAt?: firebase.firestore.Timestamp;
  topicCompletedAt?: firebase.firestore.Timestamp;
  slug?: string;
};

export type UserSurvey = {
  _total: number;
  _completed: number;
  _step: number;
} & Record<string, any>;

export type UserGoals = Record<string, boolean>;

export type UserTopicEntry = {
  _visited?: { seconds: number; nanoseconds: number };
} & { [contentKey: string]: any };

export type UserTopic = {
  mandatory: boolean;
  locked: boolean;
  seq: number;
  tag?: string;
  entries: Record<string, UserTopicEntry>;
  progress?: Progress;
  goals?: UserGoals;
  lastVisited?: string;
};

export type Progress = {
  total: number;
  completed: number;
};

export type ProfileData = {
  user: UserProfile;
  topics: Record<string, UserTopic>;
  surveys: Record<string, UserSurvey>;
};

export type ProfileState = {
  fetched: boolean;
  status: ActionStatus | null;
  data: ProfileData | null;
};

/**
 * Root state
 */
export type RootState = {
  auth: AuthState;
  profile: ProfileState;
};

/**
 * App thunk
 */
export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;
