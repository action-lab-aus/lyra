import { UserSurvey } from '../types';

export function surveysCompleted(surveyIds: string[], userSurveys: Record<string, UserSurvey>) {
  const score = surveyIds.reduce((score, surveyId) => {
    const { _completed = 0, _total = 0 } = userSurveys[surveyId] || {};
    return _total > 0 ? score + _completed / _total : score;
  }, 0);
  return score === surveyIds.length;
}
