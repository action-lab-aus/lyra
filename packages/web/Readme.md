## content structure

Courses are stored directly under contents directory. The metata data should be provided as `index.json` at the root of
each directory.

Path structure under each course can be nested. By default the path name will be the name of the folder, however user
can place an `meta.json` at each directory to provide meta data for that path.

Each document should provide its metadata via the `frontmatter` structure.
