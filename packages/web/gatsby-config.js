const path = require('path');
const { generateConfig } = require('gatsby-plugin-ts-config');

const activeEnv = process.env.GATSBY_ACTIVE_ENV || process.env.NODE_ENV || 'development';
console.log(`Using environment config: '${activeEnv}'`);
require('dotenv').config({
  path: `.env.${activeEnv}`,
});

module.exports = generateConfig({
  projectRoot: __dirname,
  configDir: '.gatsby',
});
