## Welcome and congratulations on taking this step to protect your teenager’s mental wellbeing.

Partners in Parenting is a free online program that aims to prevent depression and anxiety among teenagers.

Parents have an important positive influence on their teenager’s mental wellbeing. There’s now good evidence to show that by supporting parents in their parenting we can help teenagers to stay well.
By participating in this program you’ll learn some new parenting strategies, and by completing some optional evaluation surveys, you’ll be helping us to improve the impact of the program.

### Program features:

- Take our parenting survey to receive personalised feedback about your parenting
- Receive feedback about your own and your teen’s mental wellbeing
- Access up to 10 interactive online modules (15-25 mins each) tailored for you
- New, expert-endorsed information about parenting through the COVID-19 pandemic
- Access to an online peer support community, to connect with other PiP parents
- Be part of a world leading research project.

### Who can sign up?

Parents or guardians of a teenager aged 12 to 17 who:

- Live in Australia
- Are fluent in English
- Have Internet access

**To find out more about what participating in this program involves, please read the Explanatory Statement [here](link to pdf ES)**

Please note, our program is not designed to treat depression and anxiety conditions that have already developed. If your teenager is currently experiencing such difficulties, we recommend that you consult your GP or a mental health professional for specific assistance. However, you are still welcome to participate in this program as an additional source of support if you wish (see our [Useful Resources page](link to the resources page) for more information on seeking professional support).

#### Eligibility check:

Are you a parent, guardian, or carer of at least one child aged 12 to 17? If you are both a parent and professional, please register as a parent if you intend to complete the program as a parent. If you are only signing up to check out the program for professional reasons, please select option 'No, I am a professional checking out the program'.
a) Yes
b) No, I am a parent of a child under 12
c) No, I am a parent of a child aged over 17
d) No, I am a professional checking out the program

> Notes for dev:
> For those who select A 'Yes', set userType to 'parent'
> For those who select D, set userType to 'researcher' - They will have the same UX as 'parent' users, but their data will be excluded from data analytics and research protocols.
> For those who select B or C - they should not proceed further, the following 22 questions will be hidden for them with a message 'Sorry, our program is designed only for Parents or guardians of a teenager aged 12 to 17. Thanks for your interest.'

### About you `* question must be completed`

1. First name \*
2. Surname \*
3. Age \*
4. Gender ['Male', 'Female', 'Non-binary/gender diverse'] \*
5. Postcode \*
6. Preferred phone number \* (If landline, please include area code, e.g. (03).) `use [Twilio E.164](https://www.twilio.com/docs/glossary/what-e164) standard for phone number validation`
7. Alternative phone number (If landline, please include area code, e.g. (03).)

**The following few questions ask some personal information about you and your family. Answering these questions will help us to learn more about the parents who use the PiP program, and who benefits most. While we’d really appreciate you answering, if you prefer not to, you can skip these questions.**

8. Which ethnicity do you most identify with? The following includes some of the most common in Australia, if yours is not captured, please use 'Other' to specify. You can select as many as are relevant.

[
'North African',
'Sub-Saharan African',
'North American',
'Central American',
'South American',
'East Asian e.g. Chinese, Japanese, Korean',
'South Asian e.g. Indian, Sri Lankan',
'Southeast Asian e.g. Vietnamese, Malaysian',
'Australian Aboriginal or Torres Strait Islander',
'Australian/ New Zealand',
'European - Eastern',
'European - Western',
'Maori',
'Middle Eastern e.g. Turkish, Syrian',
'Pacific Islander',
'Other (describe): ______',
'Not sure/ I don't know',
'Prefer not to say'
]

9. What is your relationship status?
   [
   'Married or defacto',
   'Single / no current partner',
   'Separated or divorced',
   'Widowed',
   'Prefer not to say'
   ]

10. What is your highest level of education?
    [
    'Year 7 to 11',
    'Year 12',
    'Trade/apprenticeship',
    'Other TAFE or technical course (inc. Cert I to IV)',
    'Diploma, advanced Diploma or associate degree',
    'Bachelor degree',
    'Bachelor Honours degree, Graduate Certificate or Graduate Diploma ',
    'Postgraduate degree (masters or doctorate/PhD)',
    'Other (specify)',
    'Prefer not to say',
    ]

11. What is the current total income of your household, before tax?
    [
    'Nil income',
    '$1 - $149 per week ($1 - $7,799 per year)',
    '$150 - $299 per week ($7,800 - $15,599 per year)',
    '$300 - $399 per week ($15,600 - $20,799 per year)',
    '$400 - $499 per week ($20,800 - $25,999 per year)',
    '$500 - $649 per week ($26,000 - $33,799 per year)',
    '$650 - $799 per week ($33,800 - $41,599 per year)',
    '$800 - $999 per week ($41,600 - $51,999 per year)',
    '$1000 - $1,249 per week ($52,000 - $64,999 per year)',
    '$1,250 - $1,499 per week ($65,000 - $77,999 per year)',
    '$1,500 - $1,749 per week ($78,000 - $90,999 per year)',
    '$1,750 - $1,999 per week ($91,000 - $103,999 per year)',
    '$2,000 - $2,999 per week ($104,000 - $155,999 per year)',
    '$3,000 - $3,499 per week ($156,000 - $181,999 per year)',
    '$3,500 - $3,999 per week ($182,000 - $207,999 per year)',
    '$4,000-$4,499 per week ($208,000-$233,999 per year)',
    '$4,500-$4,999 per week ($234,000-$259,999 per year)',
    '$5,000-$5,999 per week ($260,000-$311,999 per year)',
    '$6,000-$7,999 per week ($312,000-$415,999 per year)',
    '$8,000 or more per week ($416,000 or more per year)',
    'Prefer not to say',
    ]

12. Due to COVID-19, have any of these things happened in your household? Please check all that apply.
    [
    'Job loss by one parent/guardian',
    'Job loss by two parents/guardians',
    'Reduced job hours for one parent/guardian',
    'Reduced job hours for two parents/guardians',
    'Difficulty paying bills or buying necessities (e.g., food)',
    'Parent/guardian having to work longer hours',
    'Applied for government (financial) assistance',
    'Received government (financial) assistance',
    ]

### About your teenager

This program is designed for parents of teenagers aged between 12 and 17.

If you have multiple teenagers, please complete the information below in reference to the teenager you are most likely to apply the program strategies with.

13. Your teen's first name \*
    Note: this is required to personalise the program to you and your teen.

14. Date of birth \*

15. School grade/year level \*
    If your teenager is not attending school, please select the approximate grade they would be in.
    [
    'Year 5',
    'Year 6',
    'Year 7',
    'Year 8',
    'Year 9',
    'Year 10',
    'Year 11',
    'Year 12',
    ]

16. Gender \*
    Note: this is required to personalise the program to you and your teen.
    [
    'Male',
    'Female',
    'Non-binary/gender diverse'
    ]

17. Your relationship to them \*
    [
    'Mother',
    'Father',
    'Step-mother',
    'Step-father',
    'Grandmother',
    'Grandfather',
    'Guardian',
    'Other (please specify)'
    ]

18. Is your child’s other parent (or co-parent) also taking part in this program? For example, biological parents, step-parents or other guardians. \*
    ['Yes', 'No', 'Unsure']

### How you found us

19. How did you find out about our program? (select all that apply)
    [
    'Friends, family, or another parent',
    'Online (e.g. social media, website)',
    'In the media (e.g. radio, TV)',
    'My teenager’s school',
    'Parents Victoria or Victorian Parents Council',
    'Raising Children’s Network',
    'A parenting or family support agency',
    'My local council',
    'My GP or another health or mental health professional',
    'Prevention United',
    'I have participated in a related Monash University research study',
    'Other (please specify)'
    ]

20. Can you briefly tell us your reasons for wanting to take part in the program and what you’re hoping to get from it?

21. Would you be interested in hearing about future research or parenting programs that you may be eligible for? If yes, we may get in touch via email when we are seeking parents.
    ['Yes', 'No']

### What you're signing up for

- As a parent, I understand that my participation will involve receiving the Partners in Parenting program, consisting of:
  1. Completing a confidential online survey to receive tailored feedback about my parenting. I will also receive feedback about my own and my teenager’s mental wellbeing.
  2. Receiving a copy of the Reducing teenagers’ risk of depression and anxiety disorders: Strategies for parents during the COVID-19 pandemic booklet immediately after completing the survey.
  3. Access to up to 10 interactive online modules, which provide more detailed parenting tips, and each take around 15-25 minutes to complete.
  4. Access to an optional online peer support community with other PiP parents, via Facebook
- I understand that I will be invited to complete some optional evaluation surveys around 3-months later, to look at how my parenting approach, my teenager’s mental wellbeing, and my own mental wellbeing have changed over time.
- If I choose to join the online peer support community, I will be provided with more information about my privacy and data security before I join.
- I may also be invited to participate in an optional interview (via phone or Zoom video conferencing) with the research team to give feedback on my experience of using the program.
- I understand that I am free to use the Partners in Parenting program without completing the optional evaluation surveys, and if I do complete them I can withdraw consent and discontinue participation in the evaluation at any time without explanation. I am also able to withdraw any unprocessed identifiable data previously supplied.
- My decision whether or not to use the program or participate in the evaluation will not prejudice any future relationship I or my teenager may have with Monash University or Prevention United.
- I understand that all information provided by me will be kept strictly confidential within the limits of the law, unless the Partners in Parenting team deems that I, my child, or someone else is at serious risk of harm (e.g. child abuse, self-harm) that they are bound by professional codes of ethics to take reasonable action to prevent, even if this means breaching confidentiality.
- I agree that data gathered from the surveys I complete may be published in a scientific journal or conference presentation, provided that I cannot be identified.
- I understand that non-identifiable data (i.e. no one will ever be able to link my name to my responses) will be made available for future research via a secure online repository, e.g. Bridges.

Any concerns about the scientific aspects of the program and its evaluation can be directed to Associate Professor Marie Yap via email, marie.yap@monash.edu
Any complaints about the ethical aspects of the research may be directed to the Executive Officer, Monash University Human Research Ethics Committee (MUHREC), Monash University, Clayton, Vic 3800, email: muhrec@monash.edu, phone 03 9905 2058, fax 03 9347 6739.

You can view the Explanatory Statement [here](link to the ES pdf).

22. Please let us know that you agree to the above \*
    I have read and understood the information in the Explanatory Statement, and agree to participate in this program.
