## Symptom elevation EMAIL text

> Note for development:
> 
> This document outlines the email text to be sent to parents if they or their child report elevated symptoms of anxiety or depression or psychological distress. There are three versions of email text based on which measures are elevated:
> > Email 1: to be sent when ONLY K6 elevated
> >
> > Email 2: to be sent when ONLY RCADS (either or both subscales) is elevated
> >
> > Email 3: to be sent when BOTH K6 and RCADS (either or both subscales) are elevated
> 
> Emails 2 and 3 need to be tailored based on whether the depression, anxiety, or both subscales are about threshold. Tailoring is indicated in square brackets throughout -  [depression] OR [anxiety] OR [depression and anxiety].
> 
> All emails to be send to parents’ registered PiP email.
>
> ALL three emails, include attachment: Useful_Resources.pdf
> 
> ALL three emails, subject line: Checking in about your survey responses

### Email 1 – only K6 is elevated

```
Dear [Parent],

Thank you for completing your first online survey as part of the Partners in Parenting (PiP) program. We are getting in touch to provide feedback about your wellbeing.

Your mental wellbeing [K6 moderate distress, insert:]

Your survey responses suggest that you may be experiencing some distress at the moment. We encourage you to seek support from a trusted friend or family member, and to consider seeking professional support. Below we’ve included some suggestions of where you can access professional mental health support. You can also check out the Useful Resources page on the PiP website.

OR [K6 high distress, insert:]

Your survey responses suggest that you may be experiencing high levels of distress at the moment. If you haven’t already, we encourage you to seek support from a mental health professional. Below we’ve included some suggestions of where you can access professional mental health support. You can also check out the Useful Resources page on the PiP website.

Where to seek professional support for yourself
We’ve attached another copy of the Useful Resources which we sent to you when you first registered for PiP. In this document, you’ll find some links to useful resources about depression and anxiety, including information about where you can seek help.

Your local GP is usually a good first point of contact, and they are likely to be aware of local mental health services in your area. We encourage you to chat to your GP about the best options for you and your family.

If you would like to find a registered psychologist in your area, the Australian Psychological Society’s Find a Psychologist service can assist with this (http://www.psychology.org.au/FaP/). You may also wish to speak to your GP about getting a referral to a psychologist.

For urgent support

For immediate support at any stage, or to speak to a counsellor over the phone, you can call Lifeline on 13 11 14 (https://www.lifeline.org.au/), or {childName} can call Kids Help Line on 1800 551800 (http://www.kidshelp.com.au/).

Thank you again for your participation in the Partners in Parenting program. Please don’t hesitate to contact us (via email med-pip@monash.edu or on PHONE) if you have any further queries.

Yours truly,
The Partners in Parenting team

```

### Email 2 – only RCADS is elevated

```
Dear [Parent],

Thank you for completing your first online survey as part of the Partners in Parenting (PiP) program. We are getting in touch to provide feedback about {childName}’s wellbeing.


{childName}’s mental wellbeing

Based on what you told us about {childName}’s recent thoughts and feelings, it seems that {they} could be experiencing some symptoms of [depression] OR [anxiety] OR [depression and anxiety]. While the surveys only give a brief snapshot of {childName}’s mental health, some of your responses suggest that {they} may need some extra support. We believe that it might be helpful for {childName} to see a health professional (e.g. a GP, school counsellor, or psychologist) to discuss how {they} [is/are] feeling. You might already be checking in with {childName} about how {they} [is/are] feeling at the moment; we certainly encourage you to have a chat with {them}, and discuss the options for seeking professional support.

Where to seek professional support for {childName}

We’ve attached another copy of the Useful Resources which we sent to you when you first registered for PiP. In this document, you’ll find some links to useful resources about depression and anxiety, including information about where you can seek help.

Your local GP is usually a good first point of contact, and they are likely to be aware of local mental health services in your area. We encourage you to chat to your GP about the best options for you and your family.

If you would like to find a registered psychologist in your area, the Australian Psychological Society’s Find a Psychologist service can assist with this (http://www.psychology.org.au/FaP/). You may also wish to speak to your GP about getting a referral to a psychologist.

There may also be a school counsellor or welfare coordinator at {childName}’s school whom they can talk to. This person may be able to suggest other professionals in your area.

For urgent support

For immediate support at any stage, or to speak to a counsellor over the phone, you can call Lifeline on 13 11 14 (https://www.lifeline.org.au/), or {childName} can call Kids Help Line on 1800 551800 (http://www.kidshelp.com.au/).

Thank you again for your participation in the Partners in Parenting program. Please don’t hesitate to contact us (via email med-pip@monash.edu or on PHONE) if you have any further queries.

Yours truly,
The Partners in Parenting team
```

### Email 3 – BOTH K6 and RCADS are elevated

```
Dear [Parent],

Thank you for completing your first online survey as part of the Partners in Parenting (PiP) program. We are getting in touch to provide feedback about your own and {childName}’s wellbeing.

Your mental wellbeing [K6 moderate distress, insert:]

Your survey responses suggest that you may be experiencing some distress at the moment. We encourage you to seek support from a trusted friend or family member, and to consider seeking professional support. Below we’ve included some suggestions of where you can access professional mental health support. You can also check out the Useful Resources page on the PiP website.

OR [K6 high distress, insert:]

Your survey responses suggest that you may be experiencing high levels of distress at the moment. If you haven’t already, we encourage you to seek support from a mental health professional. Below we’ve included some suggestions of where you can access professional mental health support. You can also check out the Useful Resources page on the PiP website.

{childName}’s mental wellbeing

Based on what you told us about {childName}’s recent thoughts and feelings, it seems that {they} could be experiencing some symptoms of [depression] OR [anxiety] OR [depression and anxiety]. While the surveys only give a brief snapshot of {childName}’s mental health, some of your responses suggest that {they} may need some extra support. We believe that it might be helpful for {childName} to see a health professional (e.g. a GP, school counsellor, or psychologist) to discuss how {they} [is/are] feeling. You might already be checking in with {childName} about how {they} [is/are] feeling at the moment; we certainly encourage you to have a chat with {them}, and discuss the options for seeking professional support.

Where to seek professional support for yourself or {childName}

We’ve attached another copy of the Useful Resources which we sent to you when you first registered for PiP. In this document, you’ll find some links to useful resources about depression and anxiety, including information about where you can seek help.

Your local GP is usually a good first point of contact, and they are likely to be aware of local mental health services in your area. We encourage you to chat to your GP about the best options for you and your family.

If you would like to find a registered psychologist in your area, the Australian Psychological Society’s Find a Psychologist service can assist with this (http://www.psychology.org.au/FaP/). You may also wish to speak to your GP about getting a referral to a psychologist.

There may also be a school counsellor or welfare coordinator at {childName}’s school whom they can talk to. This person may be able to suggest other professionals in your area.

For urgent support

For immediate support at any stage, or to speak to a counsellor over the phone, you can call Lifeline on 13 11 14 (https://www.lifeline.org.au/), or {childName} can call Kids Help Line on 1800 551800 (http://www.kidshelp.com.au/).

Thank you again for your participation in the Partners in Parenting program. Please don’t hesitate to contact us (via email med-pip@monash.edu or on PHONE) if you have any further queries.

Yours truly,
The Partners in Parenting team
```