## Confidence feedback messages – COVID-19-PSES

> Note for development:
> - To generate this feedback, the 11 confidence questions need to be scored, and a total score calculated.
> - Items are scored as follows:
>   - Not at all confident = 1
>   - A little confident = 2
>   - Somewhat confident = 3
>   - Very confident = 4
>
> - The total score is the sum of item scores. The total score can therefore range from 11 to 44.
> - The feedback provided to parents will depend on both the total score AND their individual item responses to items 5 and 8.
> - Item 5 corresponds to CONNECT Item 8 to CALM VS CONFLICT
> - Name of COVID module is yet to be decided.
> 
> - Confidence groups:
>   - low: <= 32
>   - moderate: > 32 && < 42
>   - high: >= 42

All feedback begin with the following messge:

```
<h2> Your confidence in parenting during the pandemic </h2>

<p>The COVID-19 pandemic has brought with it many challenges that parents and families have never faced before.</p>
```

### Low confidence group

if both Q5 != 4 && Q8 != 4, recommend `Connect` and `Calm versus conflict`

Append feedback message as follows:
```
When confronted with new challenges, it’s understandable to feel unsure about what to do and how to cope. There seem to be some areas of parenting during the pandemic that you are not very confident about. The Partners in Parenting program includes the modules {topicName}, Connect and Calm versus conflict which are designed to improve parents’ confidence generally and during the pandemic – we encourage you to check them out as part of your personalised parenting program. Improving your parenting confidence may help to improve your own and {childName}’s mental wellbeing during the pandemic.
```

if Q5 != 4 && Q8 == 4, recommend `Connect`
Append feedback message as follows:
```
When confronted with new challenges, it’s understandable to feel unsure about what to do and how to cope. There seem to be some areas of parenting during the pandemic that you are not very confident about. The Partners in Parenting program includes the modules {topicName} and Connect which are designed to improve parents’ confidence generally and during the pandemic – we encourage you to check them out as part of your personalised parenting program. Improving your parenting confidence may help to improve your own and {childName}’s mental wellbeing during the pandemic.
```
if Q5 == 4 && Q8 != 4, recommend `Calm versus conflict`
Append feedback message as follows:
```
When confronted with new challenges, it’s understandable to feel unsure about what to do and how to cope. There seem to be some areas of parenting during the pandemic that you are not very confident about. The Partners in Parenting program includes the modules {topicName} and Calm versus conflict which are designed to improve parents’ confidence generally and during the pandemic – we encourage you to check them out as part of your personalised parenting program. Improving your parenting confidence may help to improve your own and {childName}’s mental wellbeing during the pandemic.
```
if Q5 == 4 && Q8 == 4, not recommend any modules
Append feedback message as follows:
```
When confronted with new challenges, it’s understandable to feel unsure about what to do and how to cope. There seem to be some areas of parenting during the pandemic that you are not very confident about. The Partners in Parenting program includes the module {topicName} which is designed to improve parents’ confidence during the pandemic – we encourage you to check it out as part of your personalised parenting program. Improving your parenting confidence may help to improve your own and {childName}’s mental wellbeing during the pandemic.
```

### Moderate confidence group
if both Q5 != 4 && Q8 != 4, recommend `Connect` and `Calm versus conflict`

Append feedback message as follows:
```
When confronted with new challenges, it’s understandable to feel unsure about what to do and how to cope. It’s great that you are feeling confident in some areas of parenting during the pandemic. We know that parents who are confident are more likely to parent in ways that support their teenager’s mental health. The Partners in Parenting program includes the modules {topicName}, Connect and Calm versus conflict which are designed to improve parents’ confidence generally and during the pandemic – we encourage you to check them out as part of your personalised parenting program. Improving your confidence in these areas may help to improve your own and {childName}’s wellbeing during the pandemic.
```

if Q5 != 4 && Q8 == 4, recommend `Connect`
Append feedback message as follows:
```
When confronted with new challenges, it’s understandable to feel unsure about what to do and how to cope. It’s great that you are feeling confident in some areas of parenting during the pandemic. We know that parents who are confident are more likely to parent in ways that support their teenager’s mental health. The Partners in Parenting program includes the modules {topicName} and Connect which are designed to improve parents’ confidence generally and during the pandemic – we encourage you to check them out as part of your personalised parenting program. Improving your confidence in these areas may help to improve your own and {childName}’s wellbeing during the pandemic.
```

if Q5 == 4 && Q8 != 4, recommend `Calm versus conflict`
Append feedback message as follows:
```
When confronted with new challenges, it’s understandable to feel unsure about what to do and how to cope. It’s great that you are feeling confident in some areas of parenting during the pandemic. We know that parents who are confident are more likely to parent in ways that support their teenager’s mental health. The Partners in Parenting program includes the modules {topicName} and Calm versus Conflict which are designed to improve parents’ confidence generally and during the pandemic – we encourage you to check them out as part of your personalised parenting program. Improving your confidence in these areas may help to improve your own and {childName}’s wellbeing during the pandemic.
```

if Q5 == 4 && Q8 == 4, not recommend any modules
Append feedback message as follows:
```
When confronted with new challenges, it’s understandable to feel unsure about what to do and how to cope. It’s great that you are feeling confident in some areas of parenting during the pandemic. We know that parents who are confident are more likely to parent in ways that support their teenager’s mental health. The Partners in Parenting program includes the module {topicName} which is designed to improve parents’ confidence during the pandemic – we encourage you to check it out as part of your personalised parenting program. Improving your confidence in these areas may help to improve your own and {childName}’s wellbeing during the pandemic.
```

### High confidence group
```
It’s great that you are feeling confident about parenting {childName} during the pandemic. We know that parents who are confident are more likely to parent in ways that support their teenager’s mental health. If you’d like to learn more about parenting during the pandemic, including practical tips, check out the [insert covid module name] module.
```