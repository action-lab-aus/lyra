# PRADAS FEEDBACK

> Note for development:
>
> All instances of {childName}, {they}(he/she/they), {their}(his/her/their), {them}(him/her/them), {themselves}(himself/herself/themselves), etc. are to be automatically tailored based on the adolescent’s name and gender (from the registration form).
>
> Verbs in present tense:

```
{have}(has/have)
{are}(is/are)
{dont}(doesn't/don't)
{appear}(appear/appears)
{seem}(seems/seem)
{need}(needs/need)
{shut}(shuts/shut)
{trust}(trusts/trust)
{take}(takes/take)
{ask}(asks/ask)
{find}(finds/find)
{think}(thinks/think)
```

## 0. Feedback report begins with the below text

```
<h2>Your Personalised Parenting Tips</h2>

We can now provide you with feedback about your role in reducing {childName}’s risk of developing depression and clinical anxiety. We also provide you with some practical strategies that you could use to further support {childName}.

<h3>What next?</h3>

This feedback message contains a number of strategies that may be useful for you and {childName}. This may seem like a lot of information to take in at once. If you’re not sure where to start, you may like to have a quick read over it and pick one or two sections to focus on first. You may prefer to start with the changes that you feel would make the most difference to you and {childName}, or those that would be easiest to implement. You don’t need to try all of these strategies at once. Remember, change can take time and patience. <strong>You might already feel that you do some of the things recommended to you. If this is the case, you could consider doing them more often.</strong> If you feel that there are things you could have done differently as a parent, try not to be too hard on yourself. You’ve already taken a positive step by completing the survey, well done!
```

## 1. Your Relationship with {childName}

0. Feedback for this section begins with the following:

```
<h3>Your Relationship with {childName}</h3>

Your care and support for {childName} helps to reduce {their} risk of depression and clinical anxiety.
```

1. Fisrt, calculate the total scoure for 8 questions (0-8)

> Total score = 8, feedback begin with the following:

```
You are doing a great job of building a good relationship with {childName} by being involved in {their} life, encouraging communication between the two of you, and supporting {them}. Keep up the good work!
```

> Total score = 7, feedback begin with the following:

```
You are doing a great job of building a good relationship with {childName} by being involved in {their} life, encouraging communication between the two of you, and supporting {them}. Keep up the good work!

You may also like to consider:
```

> Total score > 3 && < 7, recommend intensive module `Connect`, feedback begin with the following:

```
You are doing lots of things to build a good relationship with {childName}. There are some other things you may also like to consider doing, such as:
```

> Total score <= 3, recommend intensive module `Connect`, feedback begin with the following:

```
{childName} would benefit from having a closer relationship with you. There are some things you could do to improve your relationship, such as:
```

2. Second, append the following dot points, for each `0` answer to Q1-Q8

> Q1 = 0, add the following:

```
- Verbally expressing your affection for {childName} more often, such as telling {them} that you love {them}. Adapt the way you show affection according to {childName}’s age and maturity.
```

> Q2 = 0, add the following:

```
- Making time each day to ask {childName} about {their} day and what {they} {have} been doing, regardless of {their} response.
```

> Q3 = 0, add the following:

```
- Trying to start conversations with {childName} when {they} {appear} most open to conversation. While it may be hard to tell when {they} {are} open to talking, try to chat when {they} {seem} most responsive to you. For example, this could be in the car, at the shops, or before bed.
```

> Q4 = 0, add the following:

```
- Showing respect for {childName}’s opinions, even if you don’t agree with {them}.
```

> Q5 = 0, add the following:

```
- Letting {childName} know that you are there for {them} whenever {they} {need} it and that {they} can talk to you about anything, even difficult issues.
```

> Q6 = 0, add the following:

```
- Choosing somewhere private to talk with {childName} about sensitive issues. Having other people around may make it difficult for {them} to be honest with you.
```

> Q7 = 0, add the following:

```
- Showing your concern when {childName} is feeling a strong emotion by asking about {their} feelings (e.g. “You look worried, is there something on your mind?”). Encourage {them} to talk with you about {their} emotions, and take the time to listen.
```

> Q8 = 0, add the following:

```
- Not telling {childName} to toughen up when {they} {are} upset. Responding in this way can lead {childName} to believe that {their} emotions are wrong and {they} {are} bad for having them. Instead, let {them} know that you are concerned and offer to chat with {them} about what’s going on.
```

3. Finally, append below sentence to all scenarios:

```
For more strategies to help you build a strong relationship with {childName}, check out the “Connect” module.
```

## 2. Your involvement in {childName}’s life

0. Feedback for this section begins with the following:

```
<h3>Your Involvement in {childName}’s Life</h3>

It is important to find a balance between being involved in {childName}’s life and giving {them} age‐appropriate independence.
```

1. Fisrt, calculate the total scoure for 8 questions (0-8)
   > Total score >= 7, feedback begin with the following:

```
You seem to have found a good balance, well done!
```

> Total score > 3 && < 7, recommend intensive module `Nurture roots and inspire wings`

```
Teenagers benefit most when their parents continue to show interest in their lives without being intrusive, and respect their need for growing independence.
```

> Total score <= 3, recommend intensive module `Nurture roots and inspire wings`

```
Teenagers benefit most when their parents continue to show interest in their lives without being intrusive, and respect their need for growing independence.
```

2. Second, calculate the total score for Q1, Q2, Q4, Q5
   > If <4, add below text first:

```
You can be more involved in {childName}’s life by:
```

> Then for each `0` answer to Q1, Q2, Q4, Q5, add corresponding dot points:

> Q1 = 0, append the following:

```
- Eating dinner together as a family more often.
```

> Q2 = 0, append the following:

```
- Doing enjoyable one‐ on‐one activities with {childName} regularly. This could be anything that you both enjoy ‐ be creative!
```

> Q4 = 0, append the following:

```
- Showing interest in what {childName} is doing at school. For example, you could ask who {they} spent time with at lunch time, or what was the easiest/ hardest thing {they} had to do at school that day.
```

> Q5 = 0, append the following:

```
- If {childName} is going out without you, have a casual chat with {them} about what {they} will be doing, where {they} will be, and who {they} will be with.
```

3. Third, calculate the total score for Q3, Q6, Q7, Q8
   > If <4, add below text first:

```
You can further support {childName}’s growing independence by:
```

> Then for each `0` answer to Q3, Q6, Q7, Q8, add corresponding dot points:
> Q3 = 0, append the following:

```
- Encouraging {childName} to try out extra‐curricular activities (e.g. sports, music, or anything else {they} {are} interested in).
```

> Q6 = 0, append the following:

```
- Allowing {childName} to become more independent of you over time. Evaluate whether you are taking over things too much. For example, you can ask yourself, “Did I really need to step in?” and “What would have been the worst thing to happen if I didn’t step in?”
```

> Q7 = 0, append the following:

```
- Encouraging {childName} to try out a variety of activities, to find out what {they} {are} interested in and what {they} {are} good at. This helps to build {their} self‐ confidence.
```

> Q8 = 0, append the following:

```
- Gradually increasing {childName}’s responsibilities and independence over time to allow {them} to mature.
```

4. Finally, append below sentence to all scenarios:

```
You can learn more about the balance between being involved in {childName}’s life and supporting {their} developing autonomy in the module “Nurture roots and inspire wings”.
```

## 3. {childName}’s relationships with others

0. Feedback for this section begins with the following:

```
<h3>{childName}’s Relationships with Others</h3>

Having good social skills and supportive relationships with a range of people of different ages helps to reduce {childName}’s risk of depression and clinical anxiety.
```

1. Fisrt, calculate the total scoure for 6 questions (0-6)
   > Total score = 6, feedback begin with the following:

```
It’s great that you help {childName} to develop {their} social skills and have positive relationships with others in lots of ways.
```

> Total score = 5, feedback begin with the following:

```
It’s great that you help {childName} to develop {their} social skills and have positive relationships with others in lots of ways.

To further improve {their} social skills, you could also:
```

> Total score > 2 && < 5, recommend intensive module `Good friends = supportive relationships`

```
You already help {childName} in this area in several ways. To help build {their} social skills further, you could:
```

> Total score <= 2, recommend intensive module `Good friends = supportive relationships`

```
To help {childName} in this area, you could try the following strategies:
```

2. Second, append the following dot points, for each `0` answer to Q1-Q6

> Q1 = 0, add the following:

```
- Encourage {childName} to spend time with {their} friends more often.
```

> Q2 = 0, add the following:

```
- Encourage {childName} to do kind things for others.
```

> Q3 = 0, add the following:

```
- Encourage {childName} to participate in a range of social situations.
```

> Q4 = 0, add the following:

```
- Encourage {childName} to spend time with people of various ages (both young and old), to help {them} develop a range of social skills and supportive relationships. These could include friends, relatives (e.g. younger and older cousins), neighbours, or other important people in {their} life.
```

> Q5 = 0, add the following:

```
- Take some time to talk through any social problems {childName} may have.
```

> Q6 = 0, add the following:

```
- Encourage {childName} to spend time with both friends and family on weekends.
```

3. Finally, append below sentence to all scenarios:

```
If you’d like to learn more about helping {childName} build supportive relationships, check out the module “Good friends = supportive relationships”.
```

## 4. Your Family Rules

0. Feedback for this section begins with the following:

```
<h3>Your Family Rules</h3>
```

1. First, check value for Q1 && Q2, feedback start from 1 of the below 4 variations:
   > Q1 && Q2
   >
   > Any combination other than [1,1]: Recommend intensive module `Raising good kids into great adults`

[1,1]

```
It is great that you have set specific, defined rules and consequences for {childName}’s behaviour, as this helps to reduce {their} risk of depression and clinical anxiety.

It is also great that:
```

[1,0]

```
It is great that you have set specific, defined rules for {childName}’s behaviour. It is also important to set clear consequences for when {they} {dont} follow these rules. Having clear rules and consequences helps to reduce {their} risk of depression and clinical anxiety.

It is also great that:
```

[0,1]

```
It is great that you have set specific, defined consequences for {childName}’s behaviour. It is also important to establish clear rules that are linked to these consequences. Having clear rules and consequences helps to reduce {their} risk of depression and clinical anxiety.

It is also great that:
```

[0,0]

```
Establishing clear rules and consequences for {childName}’s behaviour is important in reducing {their} risk of depression and clinical anxiety. If clear rules are established from an early age, {childName} is more likely to accept the rules than if they are established for the first time when {they} {are} older.

You could help {childName} by establishing specific, defined rules for {their} behaviour, and consequences for when {they} {don't} follow the rules.

It is great that:
```

2. Check value of Q3: 1/0 , append feedback text accordingly:
   > Q3

[x,x,1]

```
 - {childName} is involved in developing the rules for their behaviour.
```

[x,x,0]

```
- Involving {childName} in the development of rules for their behaviour.
```

3. Check value of Q4: 1/0 , append feedback text accordingly:
   > Q4

[x,x,x,1]

```
 - You have talked with {childName}to help {them} understands the reasons behind the family rules (e.g. to ensure {their} safety).
```

[x,x,x,0]

```
- Talking with {childName} to help them understand the reasons behind the family rules (e.g. to ensure {their} safety).
```

4. Check value of Q5: 1/0 (option 1 || option 3) , append feedback text accordingly:
   > Q5

[x,x,x,x,1]

```
 - You adapt the family rules to reflect {childName}’s maturity and responsibility.
```

[x,x,x,x,0]

If answer is Option 1: “Yes, all the time”

```
 - Keeping family rules stable and consistent, and only changing them to reflect {childName}’s maturity and responsibility.
```

If answer is Option 3: "No, family rules apply for as long as my teenager is under my care."

```
- Reviewing your family rules at regular intervals (e.g. once a year) and adapting them to reflect {childName}’s maturity and responsibility.
```

5. Check value of Q6: 1/0 , append feedback text accordingly:
   > Q6

[x,x,x,x,x,1]

```
 - You uphold the family rules and consequences, even if {childName} isn’t happy about it.
```

[x,x,x,x,x,0]

```
 - Upholding the family rules and consequences, even if {childName} isn’t happy about it.
```

6. Check value of Q7: 1/0 , append feedback text accordingly:
   > Q7

[x,x,x,x,x,x,1]

```
 - When you enforce consequences, you talk with {childName} about why {their} behaviour was not acceptable.
```

[x,x,x,x,x,x,0]

```
 - When you enforce consequences, explain to {childName} why {their} behaviour wasn’t acceptable.
```

7. Check value of Q8: 1/0 , append feedback text accordingly:
   > Q8

[x,x,x,x,x,x,x,1]

```
 - You don’t use consequences that make {childName} feel embarrassed.
```

[x,x,x,x,x,x,x,0]

```
 - Not using consequences that make {childName} feel embarrassed.
```

8. Check value of Q9: 1/0 , append feedback text accordingly:
   > Q9

[x,x,x,x,x,x,x,x,1]

```
 - You notice when {childName} behaves well and reward {them} with positive consequences (e.g. praise or privileges).
```

[x,x,x,x,x,x,x,x,0]

```
 - Noticing when {childName} behaves well, and rewarding {them} with positive consequences (e.g. praise or privileges).
```

9. Check total score of Q3-Q9
   > if <= 5, append below feedback text:

```
{childName} is more likely to follow the family rules if you do these things.
```

10. Finally, append below sentence to all scenarios:

```
For more information about setting family rules, see the module “Raising good kids into great adults”.
```

## 5. Your home environment

0. Feedback for this section begins with the following:

```
<h3>Your Home Environment</h3>

Having a supportive and safe home environment helps to reduce {childName}’s risk of depression and clinical anxiety. This includes minimising conflict at home, and setting an example for {childName} by using positive approaches to handling conflict.
```

1. Fisrt, calculate the total scoure for 8 questions (0-8)
   > Total score = 8, feedback begin with the following:

```
You are doing a great job of managing conflict at home. Well done!
```

> Total score = 7, feedback begin with the following:

```
You are doing a great job of managing conflict at home. Well done!

To further improve in this area, remember this:
```

> Total score > 3 && < 7, recommend intensive module `Calm versus Conflict`

```
Being a part of a family where there is frequent or unresolved conflict increases {childName}’s risk of depression and clinical anxiety. You already manage conflict at home in a number of ways. There are some other things that you could do, including:
```

> Total score <= 3, recommend intensive module `Calm versus Conflict`

```
Being a part of a family where there is frequent or unresolved conflict increases {childName}’s risk of depression and clinical anxiety. While you may not be able to avoid conflict altogether, there are a number of things that you can do to make the home environment supportive and safe for {childName}, including:
```

2. Second, append the following dot points, for each `0` answer to Q1-Q8

> Q1 = 0, add the following:

```
- If you have an argument or conflict with {childName}, try to problem‐solve the issue together.
```

> Q2 = 0, add the following:

```
- If you are experiencing conflict with {childName}, continue to show {them} affection and keep having normal everyday conversations with {them}. Tell {them} that you are still there for {them}, even if {they} {shut} you out.
```

> Q3 = 0, add the following:

```
- Try to minimise conflict with {childName} where possible. You can do this by considering which issues are minor and can be ignored, and which are important for {childName}’s safety and wellbeing and therefore should be addressed.
```

> Q4 = 0, add the following:

```
 - If you are not happy with {childName}’s behaviour, don’t criticise {them} in a personal way (e.g. “You are so lazy and spoilt”). Instead, talk to {them} specifically about {their} actions (e.g. “You put in the effort for sport, but what about your studies? Can we find a way to balance your time better?”). Encourage {them} to think of {their} specific actions as good or bad, rather than seeing {themselves} as a good or bad person.
```

> Q5 = 0, add the following:

```
- If you lose your temper with {childName}, acknowledge it and apologise to {them} rather than brushing it off and forgetting about it.
```

> Q6 = 0, add the following:

```
- Try not to argue with your partner if {childName} can hear. Frequent and intense conflict between parents increases a teenager’s risk of depression and clinical anxiety.
```

> Q7 = 0, add the following:

```
- If you argue with your partner, do not ask {childName} to choose sides.
```

> Q8 = 0, add the following:

```
- If you feel angry with others at home, try to take some time to calm down before trying to resolve the issue.
```

3. Finally, append below sentence to all scenarios:

```
Check out the module “Calm versus conflict” for more strategies to help keep your home environment supportive for {childName}.
```

## 6. Health Habits

0. Feedback for this section begins with the following:

```
<h3>Health Habits</h3>

Having a healthy diet and lifestyle is important for {childName}’s mental health and wellbeing. This includes eating a balanced diet, exercising regularly, getting enough sleep, and not using alcohol or other drugs.
```

1. Fisrt, calculate the total scoure for the first 8 questions (0-8)
   > Total score = 8, feedback begin with the following:

```
You are doing a great job of encouraging {childName} to develop good health habits – keep up the great work!
```

> Total score = 7 && ( Q1=0 || Q2=0 || Q3=0 || Q7=0 || Q8=0), feedback begin with the following:

```
You are doing a great job of encouraging {childName} to develop good health habits – keep up the great work!

It is also important to remember this:
```

> Total score > 3 && < 7, recommend intensive module `Good Health Habits for Good Mental Health`

```
You are already helping {childName} to develop some good health habits, well done! To improve {their} health habits, here are some strategies you could use:
```

> Total score <= 3, recommend intensive module `Good Health Habits for Good Mental Health`

```
Here are some strategies you could use to help {childName} develop good health habits:
```

2. Second, append the following dot points, for each `0` answer to Q1, Q2, Q3, Q7, Q8
   > Q1 = 0, add the following:

```
- Make sure that {childName} eats a healthy, balanced diet, including a wide variety of nutritious foods. For information on the Australian dietary guidelines, click here [link to: pdf, from: : https:// www.eatforhealth.gov.au/ sites/default/files/content/ The%20Guidelines/ n55f_children_brochure.pdf]
```

> Q2 = 0, add the following:

```
- Limit the amount of unhealthy foods and drinks (e.g. chips, biscuits, chocolates, soft drinks) in the house.
```

> Q3 = 0, add the following:

```
- Set an example for {childName} by having good health habits (i.e. healthy diet, regular exercise and responsible use of alcohol) yourself.
```

> Q7 = 0, add the following:

```
- Encourage {childName} to get regular physical exercise, at least most days. If {they} {are} not interested in sports, encourage {them} to find other activities, such as going for a walk, riding a bike, or walking to school. For more information on the Australian physical activity guidelines for young people, click here [link to pdf, from http://www.health.gov.au/internet/ main/publishing.nsf/Content/ F01F92328EDADA5BCA257BF0001E7 20D/$File/ brochure%20PA%20Guidelines_A5_ 13‐17yrs.pdf
```

> Q8 = 0, add the following:

```
- Do not allow {childName} to have any alcohol. Research shows that drug and alcohol use is harmful for teenagers’ physical health and brain development. The National Health and Medical Research Council (NHMRC) recommends that teenagers under 15 years do not consume any alcohol, and that alcohol consumption is delayed for as long as possible in older teenagers. To find out more about young people and alcohol use, click here [link to http:// www.parentingstrategies.net/ alcohol/ guidelines_introduction/
```

3. Third, check total score of Q4-Q6
   > If < 3 (one or more = 0), add below subheading first:

```
<h4>{childName}’s sleep</h4>

Getting enough sleep each night can reduce {childName}’s risk of developing depression and clinical anxiety. You can improve your [teenager]’s sleep by encouraging {them} to have healthy sleep habits, including:
```

> Then for each `0` answer to Q4-Q6, add corresponding dot points:
> append the following dot points, for each `0` answer to Q4-Q6

> Q4 = 0, add the following:

```
- Going to bed and getting up at roughly the same time each day (no more than a two‐hour difference between weekdays and weekends). This helps to regulate {childName}’s internal ‘body clock,’ which will help {them} get a better night’s sleep.
```

> Q5 = 0, add the following:

```
- Not watching TV or using electronic devices in bed before going to sleep. The bright light from electronic devices can make it harder for {childName} to get to sleep. Instead, encourage {them} to wind down with relaxing activities (e.g. reading, having a shower or bath, or listening to calming music).
```

> Q6 = 0 && Q4 = 1, add the following:

```
 - Not sleeping‐in on weekends. Instead, encourage {childName} to get up at roughly the same time each day (no more than two hours difference between weekdays and weekends). If you think {childName} needs more sleep, encourage {them} to go to bed earlier each night. Sleeping in too late on weekends can make it harder for {childName} to wake up on weekday mornings.
```

> Q6 = 0 && Q4 = 0, add the following:

```
- Not sleeping‐in on weekends. If you think {childName} needs more sleep, encourage {them} to go to bed earlier each night. Sleeping in too late on weekends can make it harder for {childName} to wake up on weekday mornings.
```

> Finally for this section, append the following

```
To read more about teenage sleep, click here [link to
http://raisingchildren.net.au/articles/sleep_early_teens.html/context/1069]
```

4. Check total value for Q9-Q12
   > For all scenarios, add below subheading first:

```
<h4>Alcohol or drug use</h4>

It is not uncommon for teenagers to experiment with alcohol or other drugs.
```

> If total value for Q9-Q12 = 4, append the following:

```
You already seem to have a good idea of what to do if you find out that {childName} is using alcohol or other drugs.

For guidelines on parenting strategies to prevent alcohol misuse in teenagers, click here [link to http:// www.parentingstrategies.net/alcohol/ guidelines_introduction/].
```

> If total value for Q9-Q12 < 4, append the following, recommend intensive module `Good Health Habits for Good Mental Health`

```
It is important that you know how to respond to this situation if it does occur. We suggest you handle it by:
- Calmly talking with {childName} about why
{they} {are} using the alcohol or drugs.
- Being careful to express your disappointment with {their} behaviour without being overly
critical of {them} as a person.
- Enforcing a reasonable consequence, rather
than punishing {them} in anger.
- Seeking professional help if you think it is
needed.
For guidelines on parenting strategies to prevent alcohol misuse in teenagers that are supported by research evidence and endorsed by national experts, click here [link to http:// www.parentingstrategies.net/alcohol/ guidelines_introduction/
```

5. Finally, append below sentence to all scenarios:

```
To find out more about supporting {childName}’s health habits, see the module “Good health habits for good mental health”.
```

## 7. Dealing with Problems in {childName}’s life

0. Feedback for this section begins with the following:

```
<h3>Dealing with Problems in {childName}’s Life</h3>

Learning to deal well with problems can help protect {childName} from developing depression and clinical anxiety.
```

1. Fisrt, calculate the total scoure for 10 questions (0-10)
   > Total score = 10, feedback begins with the following:

```
You are doing a great job of helping {childName} to deal with problems in {their} life, keep it up!
```

> Total score = 9 && (Q1 =0 || Q2=0 || Q3=0 || Q4=0), feedback begins with the following:

```
You are doing a great job of helping {childName} to deal with problems in {their} life, keep it up!

To help {childName} further develop {their} problem‐solving skills, keep this in mind:
```

> Total score > 4 && < 9, recommend intensive module `Partners in Problem Solving`

```
You can help {childName} to do this by demonstrating effective problem‐solving skills yourself, and teaching {them} how to manage problems and stress in {their} own life.

You are doing some of this already. Here are some other things you could do:
```

> Total score <= 4, recommend intensive module `Partners in Problem Solving`

```
You can help {childName} to do this by demonstrating effective problem‐solving skills yourself, and teaching {them} how to manage problems and stress in {their} own life. Here are some useful strategies:
```

2. Second, append the following dot points, for each `0` answer to Q1-Q4:
   > Q1 = 0, add the following:

```
- Encourage {childName} to set realistic goals. You can help {them} to think of different ways {they} can achieve {their} goals, and select the best one.
```

> Q2 = 0, add the following:

```
- Pay attention to {childName}’s behaviour and look for signs of stress. If you notice that {they} {are} stressed or upset, help {them} to learn ways to cope with these emotions (e.g. by talking to someone {they} {trust}).
```

> Q3 = 0, add the following:

```
- When talking with {childName} about problems that {they} {have} dealt with, recognise and praise {their} problem‐ solving efforts (i.e. what {they} did well when trying to solve the problem) rather than focussing on the outcome {they} achieved.
```

> Q4 = 0, add the following:

```
- Set an example for {childName} by persisting at tasks, even when they are difficult.
```

3. Third, calculate the total value for Q6-Q10

> If < 5, add below subheading first:

```
<h4>When {childName} faces problems in {their} life</h4>
```

> Then for each `0` answer to Q6-Q10, add corresponding dot points:

> Q6 = 0, add the following:

```
- Give {them} time to talk through the problem before offering to discuss solutions.
```

> Q7 = 0, add the following:

```
- Support {childName} to develop the skills to solve the problem {themselves}, rather than trying to solve the problem for {them}.
```

> Q8 = 0, add the following:

```
- Help {them} to break the problem down into smaller, more manageable steps.
```

> Q9 = 0, add the following:

```
- Don’t brush off or dismiss {childName}’s problems or concerns. It is important for you to help {childName} learn helpful ways to deal with problems in {their} life. Offer to talk the problem through with {them}, and help {them} to problem‐solve the issue.
```

> Q10 = 0, add the following:

```
- Encourage {them} to consider the effects of {their} actions on other people.
```

4. Fourth, check Q5
   > If Q5 = 0, append the following:

```
Teenagers often feel pressure to live up to expectations from a range of sources (e.g. themselves, their friends, school, the media, or family members). It is important for you to be aware of these pressures so that you can help {childName} deal with them. Have a conversation with {childName} about the pressures on {them} (e.g. pressure to do well at school, or pressure from the media to look a certain way) and how these may be affecting {them}. Try not to put too much pressure on {childName} to achieve or perform. Excessive parental pressure can increase {their} risk of depression and clinical anxiety.
```

5. Finally, append below sentence to all scenarios:

```
For more strategies to help support {childName}’s problem‐solving skills, check out the “Partners in problem solving” module.
```

## 8. Coping with Anxiety

0. Feedback for this section begins with the following:

```
<h3>Coping with Anxiety</h3>

It is important for {childName} to learn to manage everyday anxiety so that it doesn’t develop into clinical anxiety. All young people experience some anxiety, however it can become a problem if it is severe, long‐lasting, or interferes with {childName}’s school or other activities.
```

1. Fisrt, calculate the total score for 9 questions (0-9)
   > Total score = 9, feedback begin with the following:

```
You are doing a great job of helping {childName} learn to deal with anxiety.
```

> Total score = 8 && (Q1=0 || Q2=0 || Q4=0 || Q5=0), feedback begin with the following:

```
You are doing a great job of helping {childName} learn to deal with anxiety.

To further help {childName} in this area, keep this in mind:
```

> Total score > 3 && < 8, recommend intensive module `From surviving to thriving: Helping your teenager deal with anxiety`

```
You are already doing some good things to help {childName} learn how to deal with everyday anxiety.
```

> Total score > 3 && < 8 && (Q1=0 || Q2=0 || Q4=0 || Q5=0), recommend intensive module `From surviving to thriving: Helping your teenager deal with anxiety`

```
You are already doing some good things to help {childName} learn how to deal with everyday anxiety.

You could also try the following:
```

> Total score <= 3, recommend intensive module `From surviving to thriving: Helping your teenager deal with anxiety`

```
You can help {childName} manage their everyday anxiety by trying the following:
```

2. Second, append the following dot points, for each `0` answer to Q1, Q2, Q4, Q5:
   > Q1 = 0 || Q5 = 0, add the following:

```
- Encourage {childName} to face situations that make {them} anxious (unless {their} safety or well‐being is at risk). Facing anxiety‐ provoking situations is one of the best ways to reduce anxiety, because you find out that it wasn’t as bad as you had feared or that you coped with it. If {childName} avoids anxiety‐provoking situations, {they} {are} likely to become more anxious, not less. However, make sure that {childName} is capable of handling the situation before you encourage {them} to tackle it.
```

> Q2 = 0, add the following:

```
- Reward or praise {childName} if {they} {take} steps to manage {their} anxiety. If you find yourself becoming impatient with {childName}’s anxiety, remind yourself of how daunting it can be to face one’s fears.
```

> Q4 = 0, add the following:

```
- Try not to step in to help {childName} at the very first sign of any stress or anxiety, as the way you respond to {childName}’s anxiety may unintentionally increase {their} anxiety. Instead, let {them} try to manage the situation {themselves}, and provide help if {they} {ask} you to or if the anxiety persists.
```

3. Third, calculate the total score for Q7, Q8, Q9
   > If <3, add below text first:

```
It would also be good for you to help {childName} understand that:
```

> Then for each `0` answer to Q7, Q8, Q9, add corresponding dot points:
>
> Q7 = 0, add the following:

```
- All teenagers experience some anxiety.
```

> Q8 = 0, add the following:

```
- Anxiety is normal and helps us to prepare for situations or perform at our best.
```

> Q9 = 0, add the following:

```
- Anxiety can become a problem if it is severe, long‐ lasting, or interferes with school or other activities.
```

4. Fourth, check total value of Q1-Q9, and total value to Q3 && Q6
   > If total value to Q3 && Q6 = 2, and total value of Q1-Q9 > 3, add the following:

```
You are also doing an excellent job of setting an example for {childName} by showing {them} how you manage your own anxiety. Keep up the good work!
```

> If total value to Q3 && Q6 = 2, and total value of Q1-Q9 <= 3, add the following:

```
It is great that you already set an example for {childName} by showing {them} how you manage your own anxiety. Keep it up!
```

> If total value to Q3 && Q6 < 2, add below text first:

```
It is also important that you set an example for {childName} by showing {them} how you can manage your own anxiety. To do this, you could try the following:
```

> Then for each `0` answer to Q3, Q6, add corresponding dot points:
>
> Q3 = 0, add the following:

```
- When you talk to {childName} about things {they} {find} anxiety‐ provoking, try to remain calm and relaxed.
```

> Q6 = 0, add the following:

```
- If you feel anxious yourself, set an example for {childName} by showing {them} how you use strategies to manage your anxiety. Seek professional help if you feel you need assistance to manage your own anxiety.
```

5. Finally, append below sentence to all scenarios:

```
You can learn more about supporting {childName} to manage anxiety in the module “From thriving to surviving: Helping your teenager deal with anxiety”.
```

## 9. Getting help when needed

0. Feedback for this section begins with the following:

```
<h3>Getting Help When Needed</h3>

During adolescence, a lot of changes occur in a person’s emotions, thinking, and behaviour. This can make it hard to tell whether a change is due to depression or clinical anxiety, or is part of normal adolescent development.
```

1. Fisrt, calculate the total scoure for 9 questions (0-9)
   > Total score = 9, feedback begin with the following:

```
You have a good idea of what to do if you think {childName} needs professional help for depression or anxiety, which is important in protecting {their} health and wellbeing.
```

> Total score = 8 && (Q1=0 || Q2=0 || Q3=0 || Q4=0), feedback begin with the following:

```
You have a good idea of what to do if you think {childName} needs professional help for depression or anxiety, which is important in protecting {their} health and wellbeing.

If you do notice a persistent change in {childName}’s mood or behaviour:
```

> Total score > 3 && < 8, recommend intensive module `When things aren’t okay? Seeking professional help`

```
If you do notice a persistent change in {childName}’s mood or behaviour:
```

> Total score <= 3, recommend intensive module `When things aren’t okay? Seeking professional help`

```
If you do notice a persistent change in {childName}’s mood or behaviour:
```

2. Second, append the following dot points, for each `0` answer to Q1-Q4:
   > Q1 = 0, add the following:

```
- Encourage {them} to talk to you about what’s on {their} mind, and really listen to what {they} {are} saying.
```

> Q2 = 0, add the following:

```
- Don’t dismiss {their} feelings, or tell {them} to ‘snap out of it.’ Instead, show concern and encourage {them} to talk to you about what’s going on for {them}.
```

> Q3 = 0, add the following:

```
- Try to determine whether the change in mood or behaviour is caused by a temporary situation or a more ongoing problem.
```

> Q4 = 0, add the following:

```
- Take {them} to a trained mental health professional.
```

3. Third, check Q5-Q9
   > If Q5 = 0, add the following:

```
While it is important to seek help if you think that {childName} may be experiencing clinical anxiety, remember that everyone experiences anxiety sometimes, and that normal anxiety is not necessarily a problem. Seeking help too soon may actually increase {childName}’s anxiety. Rather than seeking help at the very first sign of anxiety, try to determine whether the problem is serious and persistent, and needs professional help. You should seek professional help if:

- Your attempts to help reduce {childName}’s anxiety don’t work. Or,
- {childName} experiences symptoms of anxiety for more than 6 months. Or,
- Anxiety symptoms begin to take over {childName}’s life or limit {their} activities.
```

> If Q6 = 0, add the following:

```
 If you think that you may be experiencing problems with depression or clinical anxiety, it is important that you look after yourself. You can set an example for {childName} by seeking professional help. Getting treatment may not only help you, but also {childName}. By getting treatment yourself, you will be better able to support {childName}.
```

> If Q7 = 0, add the following:

```
To find out more about where to get help for depression or clinical anxiety, check out: http://www.parentingstrategies.net/depression/links.php

You can also talk to your family doctor or local health service.
```

> If Q8 = 0, add the following:

```
Provide {childName} with information about where {they} can seek help for depression and clinical anxiety if {they} {need} to. For example, you could talk with {childName} about where {they} could seek help (e.g. the school counselor or family doctor), put some information or resources up on the fridge, or give {them} a list of local services available.

You can find some more resources for {childName} here: http://www.parentingstrategies.net/depression/links.php
```

> If Q9 = 0, add the following:

```
Sometimes, it can be difficult for both parents and teenagers to accept that professional help is needed. Teenagers may be reluctant to seek help due to concerns about privacy or embarrassment. Encourage {childName} to seek help {themselves} if {they} {think} {they} {need} it. However, if you believe that {childName} needs professional help, you should seek help on {their} behalf even if {they} {are} reluctant.
```

4. Finally, append below sentence to all scenarios:

```
If you’d like to learn more about when and where to seek professional help, please see the module “When things aren’t OK? Seeking professional help”.
```

## 10. At end of feedback report, append to all scenarios

```
<h3>Don’t Blame Yourself</h3>

If you feel that there are things you could have done differently as a parent, it’s important not to be too hard on yourself. If, despite your best efforts, {childName} does develop depression or anxiety, you should not view it as a failure on your part. Any teenager can develop these problems, even in happy, well adjusted families. Remember that it is important to take care of yourself, and seek professional help if you think you need it.

You can find more information at: http://www.parentingstrategies.net/depression/links.php
```
