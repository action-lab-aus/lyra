## RCADS-Parent-25

> Note for development:
>
> The RCADS has two subscales – Depression (10 items) and Anxiety (15 items). We will treat these like two separate measures.
>
> - The 10 Depression subscale comprises of items [1, 4, 8, 10, 13, 15, 16, 19, 21, 24]
> - The 15 Anxiety subscale comprises of items [2, 3, 5, 6, 7, 9, 11, 12, 14, 17, 18, 20, 22, 23, 25]
>
> After calculating the subscale scores, these scores need to be converted to t-scores to determine symptom classifications.
>
> Symptom classifications:
>
> 1. Not elevated (on either subscale)
> 2. Elevated Depression
> 3. Elevated Anxiety
> 4. Elevated BOTH Depression and Anxiety
>
> Based on these symptom classifications, the correct feedback message and email is decided.
>
> To calculate t-scores, two other variables are needed (these will be captured in the registration form):
>
> > - Child gender ${childGender}
> >   - Male = 1
> >   - Female = 2
> >   - Non-binary = 1
> >   - Child school grade/year level ${childGrade}
> >   - Numeric: from 5 to 12
>
> Calculation steps:
>
> 1. Sum item scores to form the two subscale scores. All items are scored:
>
> > - Never = 0
> > - Sometimes = 1
> > - Often = 2
> > - Always = 3
>
> - Sum Depression subscale (sum of item scores for Depression items, as per above)
> - Sum Anxiety subscale (sum of item scores for Anxiety items, as per above)
> - Ignore missing items – just calculate sum of completed items.
>
> 2. Calculate T scores, based on below formulas, which will calculate two t-score variables:
>
> - Anxiety subscale score: anxiety_total_raw
> - Depression subscale score: depression_total_raw
> - Depression T score: depression_t_score
> - Anxiety T score: anxiety_t_score
>
> 3. Determine whether T score is above threshold for elevation (≥ 70)
>    > 1. Not elevated for either: anxiety_t_score < 70 && depression_t_score < 70
>    > 2. Elevated Anxiety: anxiety_t_score ≥ 70 && depression_t_score < 70
>    > 3. Elevated Depression: depression_t_score ≥ 70 && anxiety_t_score < 70
>    > 4. Elevated Depression && Anxiety: anxiety_t_score ≥ 70 && depression_t_score ≥ 70
>
> These elevation categories inform the feedback messages and email text.

### T score calculation formulas

> Anxiety - Female

```
IF ((childGrade = 5) || (childGrade = 6)) && (childGender = 2)
anxiety_t_score = ((anxiety_total_raw  -  7.29)*10)/ 5.25 + 50

IF ((childGrade = 7) || (childGrade = 8)) && (childGender = 2)
anxiety_t_score = ((anxiety_total_raw  -  5.80)*10)/ 3.91 + 50

IF ((childGrade = 9) || (childGrade = 10)) && (childGender = 2)
anxiety_t_score = ((anxiety_total_raw  -  5.94)*10)/ 5.27 + 50

IF (childGrade >= 11) && (childGender = 2)
anxiety_t_score = ((anxiety_total_raw  -  5.76)*10)/ 3.97 + 50
```

> Anxiety - Other

```
IF ((childGrade = 5) || (childGrade = 6)) && (childGender = 1)
anxiety_t_score = ((anxiety_total_raw  -  6.10)*10)/ 4.15 + 50

IF ((childGrade = 7) || (childGrade = 8)) && (childGender = 1)
anxiety_t_score = ((anxiety_total_raw  -  5.27)*10)/ 3.95+ 50

IF ((childGrade = 9) || (childGrade = 10)) && (childGender = 1)
anxiety_t_score = ((anxiety_total_raw  -  6.23)*10)/ 4.56 + 50

IF (childGrade >= 11) && (childGender = 1)
anxiety_t_score = ((anxiety_total_raw  -  4.66)*10)/ 3.58 + 50
```

> Depression - Female

```
IF ((childGrade = 5) || (childGrade = 6)) && (childGender = 2)
depression_t_score = ((depression_total_raw -  3.75 )*10)/ 3.63 + 50

IF ((childGrade = 7) || (childGrade = 8)) && (childGender = 2)
depression_t_score = ((depression_total_raw - 3.60 )*10)/ 3.37 + 50

IF ((childGrade = 9) || (childGrade = 10)) && (childGender = 2)
depression_t_score = ((depression_total_raw -  3.97)*10)/ 3.25 + 50

IF (childGrade >= 11) && (childGender = 2)
depression_t_score = ((depression_total_raw -  4.91)*10)/3.17 + 50

```

> Depression - Other

```
IF ((childGrade = 5) || (childGrade = 6)) && (childGender = 1)
depression_t_score = ((depression_total_raw -  3.62) *10)/ 2.87 + 50

IF ((childGrade = 7) || (childGrade = 8)) && (childGender = 1)
depression_t_score = ((depression_total_raw -  3.54) *10)/ 3.18 + 50

IF ((childGrade = 9) || (childGrade = 10)) && (childGender = 1)
depression_t_score = ((depression_total_raw -  5.21) *10)/ 3.51 + 50

IF (childGrade >= 11) && (childGender = 1)
depression_t_score = ((depression_total_raw -  3.94) *10)/ 3.88 + 50
```

> Survey questions

```
Never
Sometimes
Often
Always
```

Please select the word that shows how often each of these things happens for your child.

1. My child feels sad or empty
2. My child worries when he/she thinks he/she has done poorly at something
3. My child feels afraid of being alone at home
4. Nothing is much fun for my child anymore
5. My child worries that something awful will happen to someone in the family
6. My child is afraid of being in crowded places (like shopping centers, the movies, buses, busy playgrounds)
7. My child worries what other people think of him/her
8. My child has trouble sleeping
9. My child feels scared to sleep on his/her own
10. My child has problems with his/her appetite
11. My child suddenly becomes dizzy or faint when there is no reason for this
12. My child has to do some things over and over again (like washing hands, cleaning, or putting things in a certain order)
13. My child has no energy for things
14. My child suddenly starts to tremble or shake when there is no reason for this
15. My child cannot think clearly
16. My child feels worthless
17. My child has to think of special thoughts (like numbers or words) to stop bad things from happening
18. My child thinks about death
19. My child feels like he/she doesn’t want to move
20. My child worries that he/she will suddenly get a scared feeling when there is nothing to be afraid of
21. My child is tired a lot
22. My child feels afraid that he/she will make a fool of him/herself in front of people
23. My child has to do some things in just the right way to stop bad things from happening
24. My child feels restless
25. My child worries that something bad will happen to him/her
