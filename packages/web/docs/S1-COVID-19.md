## COVID-19 Parental Self-Efficacy Scale (COVID-19-PSES) – Primary outcome measure
> The scale includes 11 single selection questions with below `confidence scale` apply all:
```
Not at all confident
A little confident
Somewhat confident
Very confident
```

<p>The next questions ask about your confidence in parenting {childName} during the COVID-19 pandemic.</p>

<p><strong>How confident do you feel about...</strong></p>

1. Your overall approach to parenting {childName} during the pandemic?
2. How confident do you feel about your ability to manage conversations with {childName} about the pandemic and associated regulations and restrictions? 
3. How confident do you feel about your ability to communicate to {childName} accurate information about the pandemic and associated regulations and restrictions?  
4. How confident do you feel about your ability to support {childName} to adapt to the pandemic and changing regulations and restrictions?
5.  How confident do you feel about your ability to help {childName} to manage {their} emotions during the pandemic?
6.  How confident do you feel about your ability to adjust your expectations of {childName} given the circumstances of the pandemic?
7. How confident do you feel about your ability to support {childName} if a loved one becomes infected with COVID-19?
8.  How confident do you feel about your ability to manage any increased conflict within the family during the pandemic?
9. How confident do you feel about your ability to model helpful coping strategies to {childName} during the pandemic? 
10. How confident do you feel about your ability to support {childName}’s online learning during school closures, if needed?
11. How confident do you feel about your ability to support {childName}’s return to face-to-face schooling after a period of online learning, if needed?
