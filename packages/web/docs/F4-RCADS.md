## ADOLESCENT SYMPTOM FEEDBACK

> Note for development:
>
> The only difference between the last three feedback messages are the words “anxiety”, “depression”, or “anxiety and depression” at the end of the second sentence.
>
> 1. Not Elevated one either Depression or Anxiety: both T scores < 70
> 2. Elevated Anxiety: T score ≥ 70, Depression T score < 70
> 3. Elevated Depression: T score ≥ 70, Anxiety T score < 70
> 4. Elevated BOTH Depression and Anxiety: Both T scores ≥ 70
>
> Trigger symptom elevation email for the last 3 elevated groups

```
<h2>{childName}’s mental wellbeing</h2>
```

### 1. Feedback text for Not Elevated neither Depression or Anxiety

```
Thanks for answering the questions about {childName}’s thoughts and feelings. Based on your responses, {childName} does not seem to be experiencing significant symptoms of anxiety or depression. Keep in mind that these questions only give us a snapshot of {childName}’s mental health, and are not a replacement for professional advice. You know your child best – if you are ever worried about {childName}’s mental health, we suggest you check in with {them} about how {they} {are} feeling. You can also speak with your GP or health professional.
```

### 2. Feedback text for Elevated Anxiety only

```
Thanks for answering the questions about {childName}’s thoughts and feelings. Based on your responses, {childName} could be experiencing symptoms of anxiety. Keep in mind that these questions only give us a snapshot of {childName}’s mental health, and are not a replacement for professional advice. We suggest you check in with {childName} about how {they} {are} feeling and also consider seeking professional support. We’ve sent you an email with more information – keep an eye out for this in your inbox.
```

### 3. Feedback text for Elevated Depression only

```
Thanks for answering the questions about {childName}’s thoughts and feelings. Based on your responses, {childName} could be experiencing symptoms of depression. Keep in mind that these questions only give us a snapshot of {childName}’s mental health, and are not a replacement for professional advice. We suggest you check in with {childName} about how {they} {are} feeling and also consider seeking professional support. We’ve sent you an email with more information – keep an eye out for this in your inbox.
```

### 4. Feedback text for Elevated BOTH Depression and Anxiety

```
Thanks for answering the questions about {childName}’s thoughts and feelings. Based on your responses, {childName} could be experiencing symptoms of anxiety and depression. Keep in mind that these questions only give us a snapshot of {childName}’s mental health, and are not a replacement for professional advice. We suggest you check in with {childName} about how {they} {are} feeling and also consider seeking professional support. We’ve sent you an email with more information – keep an eye out for this in your inbox.
```
