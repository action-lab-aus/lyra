```
Note for development:

- Demographic questions will be administered at registration, so are not included here.

- After each of these sections, the parent will have immediate access to their feedback for the corresponding questions. They can choose to view the feedback immediately, or skip this and read the full feedback report at the end of the survey package.

- Throughout this document, {childName} indicate where the teen’s first name should be automatically inserted.

- Question on teen's gender includes below options:
Male
Female
Non-binary/gender diverse

- Gender personalisation is also indicated in square brackets [him/her/them], [his/her/their] etc. In some sentences, another word also needs to be replaced for the non-binary gender, as indicated in square brackets, e.g. [has/have]. In this case the second option is always for the non-binary gender. The first option is always for both male and female. i.e. “he has”, “she has”, “they have”.

```

## Content of baseline survey package

The baseline survey package will contain the following 4 sections:

1. COVID-19-PSES (confidence questions, 11 items) [Parental Self Efficacy Scale ]
2. PRADAS + PSES (79 PRADAS items used to generate feedback, + PSES confidence item at end of each section) [Parenting to Reduce Depression and Anxiety Scale]
3. K6 parent psychological distress, 6 items
4. RCADS teen anxiety & depression, 25 items [Revised Children’s Anxiety and Depression Scale]

### Display feedback

After each of these sections, the parent will have immediate access to their feedback for the corresponding questions. They can choose to view the feedback immediately, or skip this and read the full feedback report at the end of the survey package.

### Display Module

#### PRADAS scoring overview

- Items are scored as 1 (green shading) or 0 (white).
- For each subscale (survey section), the item scores are summed to score a subscale score.
- Each subscale has a `cutoff score` to be considered `concordant` with that section. If concordant, the corresponding module is `not recommended`. If not concordant (< `cutoff score`), the module is recommended.

#### PRADAS Subscale / Module relationship

| Q#  | Cut-off (less than) | PRADAS subscale                                 | Module                                             | Module # |
| :-- | :------------------ | :---------------------------------------------- | :------------------------------------------------- | -------- |
| 8   | 7                   | Your relationship with `your teenager`          | Connect                                            | m1       |
| 8   | 7                   | Your involvement in `your teenager`’s life      | Nurture roots and inspire wings                    | m4       |
| 6   | 5                   | `your teenager`’s relationships with others     | Raising good kids into great adults                | m3       |
| 9   | 8                   | Your family rules                               | Calm versus conflict                               | m5       |
| 8   | 7                   | Your home environment                           | Good friends = supportive relationships            | m6       |
| 10  | 9                   | Health Habits                                   | Good health habits                                 | m7       |
| 10  | 9                   | Dealing with problems in `your teenager`’s life | Partners in problem solving                        | m8       |
| 9   | 8                   | Coping with anxiety                             | From surviving to thriving                         | m9       |
| 9   | 8                   | Getting help when needed                        | When things aren’t okay: Seeking professional help | m10      |

#### Select and Unlock Module

- All 10 modules are displayed in two groups: `recommended` modules in a `calculated` display order, or `optional` modules in a `default` display order.
- User need to have at least 1 module in the `recommended` module group. If all PRADAS subscale scores are >= cutoff score, ask user to selecte at least one module and add to the recommended group.
- User can select (the module then move to `recommended` module group) or de-select (the module then move to `optional` module group) only at the module selection step.
- The 1st module in `recommended` module group is auto-unlocked, all other modules in `recomended` group by default are in `locked` state.
- All modules in the `optional` module group are by default `locked`.
- User can unlock any module anytime.
- Once a module is `unlocked`, cannot revoke back to `locked` state.
- If having `recommended` but `locked` modules, according to the display order, one module is scheduled to be `unlocked` weekly. User receives notification message once a new module is schedully `unlocked`.
- If having `optional` but `locked` modules, according to the display order, one module is scheduled to be `unlocked` weekly after all recommended modules are `unlocked`. **Do we also send notifications?**

#### Default display order of modules (assuming all subscale scores are equal):

1. Connect
2. NEW COVID module (name TBC)
3. Raising good kids into great adults: establishing family rules
4. Nurture roots & inspire wings
5. Calm versus conflict
6. Good friends = supportive relationships
7. Good health habits for good mental health
8. Partners in problem solving
9. From surviving to thriving: Helping your teenager deal with anxiety
10. When things aren’t okay: Getting professional help

#### Calculate display order of modules in the recommended module group

1. For all parents, regardless of scoring, these three modules should be displayed first IF the parent locks in the modules. In this order:

   1. Connect
   2. NEW COVID module
   3. Raising good kids into great adults

2. The remaining recommended modules should be presented according to PRADAS subscale score, from lowest to highest percentage score. (This means that parents get the modules with the greatest room for improvement first, followed by modules with higher percentage scores.)
   1. PRADAS subscale scores first need to be converted to a percentage of the total possible subscale score, to determine the parenting domains with greatest room for improvement.
   2. Percentage calculation is based on the core PRADAS items, not the confidence items at the end of each section. For example, section 1 (parent child relationship) has 8 items. So a score of 7 would be: (7/8)\*100 = 87.5%.
   3. If subscale scores receive the same percentage (e.g. two subscales have 87.5%), these modules are to be presented according to the default order above.

#### Display pop-up message if user try to de-select `Connect` module in recommended group

We want to encourage all parents to complete Connect (if recommended by program), as it is the foundational module. If a parent is recommended Connect and they de-select it (i.e. go against the program’s recommendation and do not ‘lock in’ the module), parents will be displayed a pop-up message:

“You have chosen not to select the Connect module. This module is a foundation of the program, as it can help with applying some of the strategies in later modules. We encourage you to select the Connect module. Please click 'Confirm' to continue with your current selection or 'Cancel' to go back and change your selection.”
