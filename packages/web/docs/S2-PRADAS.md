> Note for development:

> All instances of {childName}, {they}(he/she/they), {their}(his/her/their), {them}(him/her/them), {themselves}(himself/herself/themselves), etc. are to be automatically tailored based on the adolescent’s name and gender (from the registration form).

# Your Personalised Parenting Tips

We can now provide you with feedback about your role in reducing {childName}’s risk of developing depression and clinical anxiety. We also provide you with some practical strategies that you could use to further support {childName}.

## What next?

This feedback message contains a number of strategies that may be useful for you and {childName}. This may seem like a lot of information to take in at once. If you’re not sure where to start, you may like to have a quick read over it and pick one or two sections to focus on first. You may prefer to start with the changes that you feel would make the most difference to you and {childName}, or those that would be easiest to implement. You don’t need to try all of these strategies at once. Remember, change can take time and patience. <strong>You might already feel that you do some of the things recommended to you. If this is the case, you could consider doing them more often.</strong> If you feel that there are things you could have done differently as a parent, try not to be too hard on yourself. You’ve already taken a positive step by completing the survey, well done!

## Section 1 - Your Relationship with {childName}

Your care and support for {childName} helps to reduce {their} risk of depression and clinical anxiety.

Please indicate how often you do the following.

1. I let {childName} know that I love {them}.
2. I make time to ask {childName} about {their} day and what [he/she/they] [has/have] been doing.
3. I can tell when {childName} is open to talking with me.
4. If I don’t agree with {childName}’s opinion, I just tell {them} that [he/she/they] [is/are] wrong.
5. I make myself available for {childName} whenever [he/she/they] want to talk about {their} concerns.
6. If {childName} wants to discuss a sensitive topic with me, I do it right away regardless of whether other people can hear.
7. When {childName} looks worried, upset, or angry, I show my concern by asking about {their} feelings.
8. When {childName} is upset, I encourage {them} to toughen up.

```
Never
Rarely
Sometimes
Often

1: [0,0,0,1]
2: [0,0,1,1]
3: [0,0,1,1]
4: [1,0,0,0]
5: [0,0,0,1]
6: [1,0,0,0]
7: [0,0,0,1]
8: [1,0,0,0]
```

Example:
| Q# | Never | Rarely | Sometimes | Often |
| :- | :---- | :----- | :-------- | :---- |
| 1. | 0 | 0 | 0 | 1 |
| 2. | 0 | 0 | 1 | 1 |
| 3. | 0 | 0 | 1 | 1 |
| 4. | 1 | 0 | 0 | 0 |
| 5. | 0 | 0 | 0 | 1 |
| 6. | 1 | 0 | 0 | 0 |
| 7. | 0 | 0 | 0 | 1 |
| 8. | 1 | 0 | 0 | 0 |

## Section 2 - Your involvement in {childName}’s life

Please indicate how often you do the following.

1. We eat dinner together as a family.
2. {childName} and I do one-on-one activities together that we both enjoy.
3. I discourage {childName} from participating in any extra-curricular activities (e.g. sports, music), so that [he/she/they] can focus on {their} studies.
4. I let {childName} decide what [he/she/they] [wants/want] to tell me about school, so I don’t put too much pressure on {them}.
5. When {childName} is going out without me, I know where [he/she/they] [is/are] going, who [he/she/they] will be with, and what [he/she/they] will be doing.
6. I make it clear to {childName} that [he/she/they] [is/are] still dependent on me, as it is important that [he/she/they] [realises/realise] this.
7. I encourage {childName} to try out a variety of activities, to find out what [he/she/they] [is/are] interested in and what [he/she/they] [is/are] good at.
8. I increase {childName}’s responsibilities and independence over time (e.g. let {them} make more decisions about {their} life).

```
Never
Rarely
Sometimes
Often

1: [0,0,0,1]
2: [0,0,1,1]
3: [1,0,0,0]
4: [1,1,0,0]
5: [0,0,0,1]
6: [1,1,0,0]
7: [0,0,1,1]
8: [0,0,0,1]
```

## Section 3 - {childName}’s relationships with others

Please indicate how often you do the following.

1. I encourage {childName} to spend time with {their} friends.
2. I encourage {childName} to be kind to others.
3. I encourage {childName} to participate in a range of social situations to help build {their} social skills.
4. I encourage {childName} to only socialise with people {their} own age.
5. When {childName} has social problems, I talk to {them} about the problem and how [he/she/they] may manage it.
6. I encourage {childName} to spend time at home with the family on weekends, because [he/she/they] [sees/see] plenty of {their} friends during the week.

```
Never
Rarely
Sometimes
Often

1: [0,0,1,1]
2: [0,0,1,1]
3: [0,0,1,1]
4: [1,1,0,0]
5: [0,0,1,1]
6: [1,1,0,0]
```

## Section 4 - Your family rules

Now we are going to ask you some questions about family rules.
Note: By family rules we mean any type of established expectations, limits or guidelines for {childName}’s behaviour that {childName} is aware of.

1. Have you set specific, defined rules for {childName}’s behaviour?
2. Have you set specific, defined consequences for when {childName} doesn’t follow the family rules?
3. Do you involve {childName} in developing rules for {their} behaviour?
4. Do you talk with {childName} about the reasons behind rules for {their} behaviour?

```
Yes
No

1: [1,0]
2: [1,0]
3: [1,0]
4: [1,0]
```

5. Do you ever review or adjust the family rules?

```
Yes, all the time.
Yes, to adapt to {childName}’s maturity and responsibility.
No, family rules apply for as long as {childName} is under my care.

5: [0,1,0]
```

Please indicate how often you do the following.

6. If {childName} gets upset with me for enforcing consequences, I let {them} get away with breaking the rule to keep peace.
7. When I give {childName} consequences as a result of breaking rules, I talk to {them} about why {their} behaviour was not acceptable.
8. When {childName} breaks the rules, I use consequences to teach {them} a lesson, even if it embarrasses {them}.
9. When {childName} behaves well, I reward {them} with positive consequences (e.g. praise, attention, or privileges).

```
Never
Rarely
Sometimes
Often

6: [1,0,0,0]
7: [0,0,0,1]
8: [1,0,0,0]
9: [0,0,1,1]
```

## Section 5 - Your home environment

Please indicate how often you do the following.

1. When I have an argument or conflict with {childName}, I problem solve the issue with {them}.
2. When I am angry with {childName}, I give {them} the cold shoulder so that [he/she/they] [learns/learn] a lesson.
3. I try to minimise conflict with {childName} by ignoring minor issues that irritate me.
4. I tell {childName} if I think [he/she/they] [is/are] a lazy, spoilt or selfish person.
5. After I lose my temper with {childName}, I try to brush it off and forget about it.

```
Never
Rarely
Sometimes
Often

1: [0,0,1,1]
2: [1,0,0,0]
3: [0,0,1,1]
4: [1,0,0,0]
5: [1,0,0,0]
```

6. If I argue with my partner, I make sure that {childName} can’t hear.
7. If I argue with my partner, I try to get {childName} to be on my side.

```
Never
Rarely
Sometimes
Often
Not applicable, I don’t have a partner.

6: [0,0,0,1,1]
7: [1,0,0,0,1]
```

8. If I feel angry with others at home, I try to resolve the issue when I’m calm.

```
Never
Rarely
Sometimes
Often

8: [0,0,0,1]
```

## Section 6 - Health Habits

Please indicate how often you do the following.

1. I encourage {childName} to eat a healthy, balanced diet, including plenty of fresh vegetables and water.
2. Treats (e.g. chips, biscuits, chocolates, or soft drinks) are readily available in our house.
3. I have good health habits (i.e. healthy diet, regular exercise, responsible use of alcohol) myself.
4. I encourage {childName} to go to bed and get up at roughly the same time each day (even on weekends).
5. I encourage {childName} not to watch TV or use an electronic device in bed before going to sleep.
6. I encourage {childName} to sleep-in on the weekends if [he/she/they] [hasn’t/haven’t] slept enough during the week.
7. I help {childName} to engage in physical activity (e.g. transporting {them} to sports or dance classes, riding a bike or walking with {them}, encouraging {them} to participate in sports at school).
8. I allow {childName} to have an alcoholic drink at home to help {them} learn to drink responsibly.

```
Never
Rarely
Sometimes
Often

1: [0,0,0,1]
2: [1,1,0,0]
3: [0,0,0,1]
4: [0,0,0,1]
5: [0,0,0,1]
6: [1,0,0,0]
7: [0,0,1,1]
8: [1,0,0,0]
```

If you found out that {childName} had been misusing alcohol or other drugs, how likely would you be to:

9. Calmly talk to {them} about why [he/she/they] [is/are] using the alcohol or drugs.
10. Tell {them} how disappointed you are in {them}.
11. Enforce a punishment [he/she/they] will not easily forget, so that [he/she/they] [learns/learn] not to do this again.
12. Seek professional help if you thought it was needed.

```
Very Unlikely
Unlikely
Likely
Very Likely

9: [0,0,1,1]
10: [1,1,0,0]
11: [1,1,0,0]
12: [0,0,1,1]
```

## Section 7 - Dealing with Problems in {childName}’s life

Please indicate how often you do the following.

1. I encourage {childName} to work towards realistic goals.
2. When {childName} is stressed or upset, I help {them} to learn how to cope with these emotions (e.g. by talking about it with someone [he/she/they] [trusts/trust]).
3. When talking with {childName} about a problem [he/she/they] [has/have] managed, I praise {their} problem-solving efforts, rather than focusing on the outcome.
4. I give up on tasks that prove to be too difficult.
5. I think about the pressures {childName} receives from different sources (e.g. school, friends, the media or family members).

```
Never
Rarely
Sometimes
Often

1: [0,0,0,1]
2: [0,0,1,1]
3: [0,0,1,1]
4: [1,1,0,0]
5: [0,0,1,1]
```

When {childName} faces problems in {their} life:

6. I give {them} time to talk through the problem.
7. I attempt to solve the problem for {them}.
8. I help {them} to break the problem down into smaller, more manageable steps.
9. I tell {them} it’s not such a big deal, so that [he/she/they] [doesn’t/don’t] get too upset.
10. I encourage {them} to think about how {their} actions may affect other people.

```
Never
Rarely
Sometimes
Often

6: [0,0,0,1]
7: [1,1,0,0]
8: [0,0,1,1]
9: [1,0,0,0]
10: [0,0,0,1]
```

## Section 8 - Coping with Anxiety

The following section includes questions about anxiety. People may describe anxiety as feeling stressed, nervous, on edge, worried, or scared. Although anxiety may be unpleasant, it can be quite useful in helping us to avoid dangerous situations or solve everyday problems.

Please indicate how often you do the following.

1. I let {childName} avoid situations that make {them} anxious, so that [he/she/they] [doesn’t/don’t] become overwhelmed by anxiety.
2. If {childName} takes steps to manage {their} anxiety, I praise {them} for doing it.
3. When talking to {childName} about things that [he/she/they] [finds/find] anxiety-provoking, I try to remain calm and relaxed.
4. I step in to help {childName} at the very first sign of stress or anxiety.
5. I encourage {childName} to face situations that [he/she/they] [finds/find] anxiety-provoking, to help {them} learn to manage anxiety.
6. I let {childName} know the strategies I use when I’m feeling anxious.

```
Never
Rarely
Sometimes
Often

1: [1,1,0,0]
2: [0,0,1,1]
3: [0,0,0,1]
4: [1,0,0,0]
5: [0,0,1,1]
6: [0,0,1,1]
```

Have you helped {childName} understand that:

7. All teenagers experience some level of anxiety.
8. Normal anxiety is useful, as it helps us prepare for situations or perform our best.
9. Anxiety can become a problem if it is severe, long-lasting, or if it interferes with school or other activities.

```
Yes, definitely
Yes, partly
No

7: [1,1,0]
8: [1,1,0]
9: [1,1,0]
```

## Section 9 - Getting help when needed

If you noticed a persistent change in {childName}’s mood or behaviour, how likely would you be to:

1. Encourage {them} to talk to you about what’s going on for {them}.
2. Encourage {them} to ‘snap out of it’ so that it doesn’t become a more serious problem.
3. Try to determine whether the change in mood or behaviour is caused by a temporary situation or a more ongoing problem.
4. Take {them} to a trained mental health professional.

```
Very unlikely
Unlikely
Likely
Very likely

1: [0,0,1,1]
2: [1,1,0,0]
3: [0,0,1,1]
4: [0,0,1,1]
```

Please indicate how likely you would be to do the following.

5. I would seek help at the very first sign of any anxiety in {childName}.
6. I would seek professional help if I was experiencing problems with depression or anxiety myself.

```
Very unlikely
Unlikely
Likely
Very likely

5: [1,1,0,0]
6: [0,0,1,1]
```

If {childName} had mental health problems that were interfering with {their} life:

7. I would know where to seek appropriate professional help for {childName}.
8. {childName} would know where and how to seek appropriate professional help for [himself/herself/themself].
9. {childName} would be willing to accept professional help.

```
Very unlikely
Unlikely
Likely
Very likely

7: [0,0,1,1]
8: [0,0,1,1]
9: [0,0,1,1]
```
