## COVID-19-PSES survey questions

> Note for development: The last item in each section of the PRADAS, labelled “PSES item” is NOT INCLUDED in the scoring & feedback algorithm. It applies the below `confidence scale`:

```
Not at all confident
A little confident
Somewhat confident
Very confident
```

### PRADAS.S1

```
How confident do you feel about your ability to build and maintain a close relationship with {childName}?
```

### PRADAS.S2

```
How confident do you feel about your ability to find a balance between being involved in {childName}’s life and encouraging age-appropriate independence?
```

### PRADAS.S3

```
How confident do you feel about your ability to help {childName} to build [his/her/their] social skills?
```

### PRADAS.S4

```
How confident do you feel about your ability to establish family rules and consequences?
```

### PRADAS.S5

```
How confident do you feel about your ability to solve conflicts with {childName} in a constructive manner?
```

### PRADAS.S6

```
How confident do you feel about your ability to influence {childName} to make healthy lifestyle choices?
```

### PRADAS.S7

```
How confident do you feel about your ability to support {childName} when [he/she/they] [faces/face] problems in their life?
```

### PRADAS.S8

```
How confident do you feel about your ability to help {childName} cope with anxiety?
```

### PRADAS.S9

```
If you noticed a persistent change in {childName}’s mood or behaviour, how confident do you feel about your ability to help {childName} seek appropriate professional help?
```
