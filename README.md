## Structure of Monorepo
This git repository contains multiple logically associated projects in a package structure as follows:
- core - sharable libraries across projects
- web - the PiP+ website for parenting intervention
- functions - cloud functions
- admin - the PiP+ admin dashboard UI

## Use of yarn workspce and lerna to manage package version

Install lerna globally:

```
$ yarn global add lerna
```

In your local project root directory, please run below to bypass cloud function node version constraint:

```
$ yarn install --ignore-engines
```

## Run the web project
To run the web project, in your local project root directory, please run below to start the website using @lyra/web as the namespace:

```
$ yarn workspace @lyra/web start
```

Similarly, you can run below commands to run storybook which is the UI test environment, and run jest test cases:

```
$ yarn workspace @lyra/web storybook
$ yarn workspace @lyra/web test
```